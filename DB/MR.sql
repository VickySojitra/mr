USE [MR-App]
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkWithMaster_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkWithMaster_Delete]
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkWithMaster_ById]
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkWithMaster_All]
GO
/****** Object:  StoredProcedure [dbo].[WorkType_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkType_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[WorkType_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkType_Delete]
GO
/****** Object:  StoredProcedure [dbo].[WorkType_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkType_ById]
GO
/****** Object:  StoredProcedure [dbo].[WorkType_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkType_All]
GO
/****** Object:  StoredProcedure [dbo].[WorkExperienceUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkExperienceUpsert]
GO
/****** Object:  StoredProcedure [dbo].[WorkExperienceDeleteByEmpID]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkExperienceDeleteByEmpID]
GO
/****** Object:  StoredProcedure [dbo].[WorkExperienceByEmployeeId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[WorkExperienceByEmployeeId]
GO
/****** Object:  StoredProcedure [dbo].[VisitType_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[VisitType_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[VisitType_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[VisitType_Delete]
GO
/****** Object:  StoredProcedure [dbo].[VisitType_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[VisitType_ById]
GO
/****** Object:  StoredProcedure [dbo].[VisitType_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[VisitType_All]
GO
/****** Object:  StoredProcedure [dbo].[UserPlans_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserPlans_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[UserPlans_ByUserId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserPlans_ByUserId]
GO
/****** Object:  StoredProcedure [dbo].[UserPlans_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserPlans_All]
GO
/****** Object:  StoredProcedure [dbo].[TourPlanMonth_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlanMonth_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[TourPlanMonth_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlanMonth_ById]
GO
/****** Object:  StoredProcedure [dbo].[TourPlanMonth_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlanMonth_All]
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlan_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_IsSubmitted]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlan_IsSubmitted]
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlan_ById]
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[TourPlan_All]
GO
/****** Object:  StoredProcedure [dbo].[StateUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateUpsert]
GO
/****** Object:  StoredProcedure [dbo].[StateSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateRegionHq_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateRegionHq_Delete]
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateRegionHq_ById]
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_AllRegion]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateRegionHq_AllRegion]
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[StateRegionHq_All]
GO
/****** Object:  StoredProcedure [dbo].[State_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[State_ById]
GO
/****** Object:  StoredProcedure [dbo].[SpecializationUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[SpecializationUpsert]
GO
/****** Object:  StoredProcedure [dbo].[SpecializationSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[SpecializationSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[Specialization_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Specialization_ById]
GO
/****** Object:  StoredProcedure [dbo].[RouteUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RouteUpsert]
GO
/****** Object:  StoredProcedure [dbo].[RouteTypeUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RouteTypeUpsert]
GO
/****** Object:  StoredProcedure [dbo].[RouteTypeSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RouteTypeSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[RouteSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RouteSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[RouteSelect]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RouteSelect]
GO
/****** Object:  StoredProcedure [dbo].[RoleUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RoleUpsert]
GO
/****** Object:  StoredProcedure [dbo].[RoleSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RoleSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[RegionUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RegionUpsert]
GO
/****** Object:  StoredProcedure [dbo].[RegionSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[RegionSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[Region_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Region_ById]
GO
/****** Object:  StoredProcedure [dbo].[QuantitySIUnites_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[QuantitySIUnites_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[QuantitySIUnites_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[QuantitySIUnites_ById]
GO
/****** Object:  StoredProcedure [dbo].[QuantitySIUnites_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[QuantitySIUnites_All]
GO
/****** Object:  StoredProcedure [dbo].[QualificationUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[QualificationUpsert]
GO
/****** Object:  StoredProcedure [dbo].[QualificationSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[QualificationSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[Qualification_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Qualification_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsDoctor_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsDoctor_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_ByProductId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsDoctor_ByProductId]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_ByDoctorId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsDoctor_ByDoctorId]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsDoctor_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsChemist_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsChemist_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_ByProductId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsChemist_ByProductId]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_ByChemistId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsChemist_ByChemistId]
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductVsChemist_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductType_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductType_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductType_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductType_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductType_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductType_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductType_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductType_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductPrescription_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductPrescription_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductPrescription_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductPrescription_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductManufacture_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductManufacture_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductManufacture_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductManufacture_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductImages_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductImages_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductImages_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductImages_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductHightlights_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHightlights_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductHightlights_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHightlights_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductHightLights_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHightLights_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductHightlights_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHightlights_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHighlight_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_DeleteByProductId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHighlight_DeleteByProductId]
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHighlight_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHighlight_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductHighlight_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategory_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategory_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategory_ById]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategory_All]
GO
/****** Object:  StoredProcedure [dbo].[Product_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Product_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Product_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Product_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Product_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Product_ById]
GO
/****** Object:  StoredProcedure [dbo].[Product_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Product_All]
GO
/****** Object:  StoredProcedure [dbo].[Product_ActInact]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Product_ActInact]
GO
/****** Object:  StoredProcedure [dbo].[Presentation_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Presentation_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Presentation_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Presentation_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Presentation_ByProductId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Presentation_ByProductId]
GO
/****** Object:  StoredProcedure [dbo].[Presentation_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Presentation_ById]
GO
/****** Object:  StoredProcedure [dbo].[Presentation_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Presentation_All]
GO
/****** Object:  StoredProcedure [dbo].[PrescriberProducts_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[PrescriberProducts_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[PrescriberProducts_ByPrescriberId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[PrescriberProducts_ByPrescriberId]
GO
/****** Object:  StoredProcedure [dbo].[PrescriberProducts_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[PrescriberProducts_ById]
GO
/****** Object:  StoredProcedure [dbo].[Prescriber_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Prescriber_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Prescriber_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Prescriber_ById]
GO
/****** Object:  StoredProcedure [dbo].[Prescriber_ByDoctorId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Prescriber_ByDoctorId]
GO
/****** Object:  StoredProcedure [dbo].[Outcome_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Outcome_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Outcome_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Outcome_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Outcome_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Outcome_ById]
GO
/****** Object:  StoredProcedure [dbo].[Outcome_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Outcome_All]
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_PresentationResources_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_PresentationResources_Delete]
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_ByMR_PresentationId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_PresentationResources_ByMR_PresentationId]
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_ByMR_Presentation_ProductsId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_PresentationResources_ByMR_Presentation_ProductsId]
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_Presentation_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Products_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_Presentation_Products_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Products_ByMR_Presentation_Id]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_Presentation_Products_ByMR_Presentation_Id]
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_Presentation_Delete]
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_Presentation_ById]
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MR_Presentation_All]
GO
/****** Object:  StoredProcedure [dbo].[Mr_Doctor_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Mr_Doctor_All]
GO
/****** Object:  StoredProcedure [dbo].[MasterWorkAt_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[MasterWorkAt_All]
GO
/****** Object:  StoredProcedure [dbo].[Masters_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Masters_All]
GO
/****** Object:  StoredProcedure [dbo].[LeaveTypeMaster_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[LeaveTypeMaster_All]
GO
/****** Object:  StoredProcedure [dbo].[Leave_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Leave_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Leave_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Leave_ById]
GO
/****** Object:  StoredProcedure [dbo].[Leave_ByEmployeeId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Leave_ByEmployeeId]
GO
/****** Object:  StoredProcedure [dbo].[JointWorking_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[JointWorking_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[JointWorking_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[JointWorking_All]
GO
/****** Object:  StoredProcedure [dbo].[HeadquarterUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[HeadquarterUpsert]
GO
/****** Object:  StoredProcedure [dbo].[HeadquarterSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[HeadquarterSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[Headquarter_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Headquarter_ById]
GO
/****** Object:  StoredProcedure [dbo].[Gift_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Gift_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Gift_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Gift_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Gift_ChampaignMasterId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Gift_ChampaignMasterId]
GO
/****** Object:  StoredProcedure [dbo].[Gift_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Gift_ById]
GO
/****** Object:  StoredProcedure [dbo].[FieldName_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[FieldName_All]
GO
/****** Object:  StoredProcedure [dbo].[EmployeeUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[EmployeeUpsert]
GO
/****** Object:  StoredProcedure [dbo].[EmployeeSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[EmployeeSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[EmployeeSelect]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[EmployeeSelect]
GO
/****** Object:  StoredProcedure [dbo].[EmployeeRegion_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[EmployeeRegion_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[EmployeeRegion_ByUserId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[EmployeeRegion_ByUserId]
GO
/****** Object:  StoredProcedure [dbo].[EmployeeDelete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[EmployeeDelete]
GO
/****** Object:  StoredProcedure [dbo].[Employee_Login]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Employee_Login]
GO
/****** Object:  StoredProcedure [dbo].[Employee_ChangePassword]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Employee_ChangePassword]
GO
/****** Object:  StoredProcedure [dbo].[Employee_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[Employee_ById]
GO
/****** Object:  StoredProcedure [dbo].[DSRStatus_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSRStatus_All]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Products_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Products_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Products_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Products_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ById]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedCampaignId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedCampaignId]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_ById]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_ById]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_ByDSRId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedDoctor_ByDSRId]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Products_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_Products_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Products_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_Products_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Products_ByDSR_VisitedChemistId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_Products_ByDSR_VisitedChemistId]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_ById]
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_ByDSRId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_VisitedChemist_ByDSRId]
GO
/****** Object:  StoredProcedure [dbo].[DSR_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DSR_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DSR_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_ById]
GO
/****** Object:  StoredProcedure [dbo].[DSR_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DSR_All]
GO
/****** Object:  StoredProcedure [dbo].[DoctorUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorUpsert]
GO
/****** Object:  StoredProcedure [dbo].[DoctorSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[DoctorSelect]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorSelect]
GO
/****** Object:  StoredProcedure [dbo].[DoctorProducts_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorProducts_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DoctorProducts_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorProducts_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DoctorProducts_ByDoctorId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorProducts_ByDoctorId]
GO
/****** Object:  StoredProcedure [dbo].[DoctorCategoryUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorCategoryUpsert]
GO
/****** Object:  StoredProcedure [dbo].[DoctorCategorySelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorCategorySelectAll]
GO
/****** Object:  StoredProcedure [dbo].[DoctorCategory_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorCategory_ById]
GO
/****** Object:  StoredProcedure [dbo].[DoctorAndChemist_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorAndChemist_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DoctorAndChemist_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorAndChemist_Delete]
GO
/****** Object:  StoredProcedure [dbo].[DoctorAndChemist_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DoctorAndChemist_All]
GO
/****** Object:  StoredProcedure [dbo].[DayTypeMaster_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[DayTypeMaster_All]
GO
/****** Object:  StoredProcedure [dbo].[CityUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CityUpsert]
GO
/****** Object:  StoredProcedure [dbo].[CitySelectByStateId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CitySelectByStateId]
GO
/****** Object:  StoredProcedure [dbo].[CitySelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CitySelectAll]
GO
/****** Object:  StoredProcedure [dbo].[City_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[City_ById]
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistVsDoctor_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistVsDoctor_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_ByDoctorId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistVsDoctor_ByDoctorId]
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_ByChemistId]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistVsDoctor_ByChemistId]
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistVsDoctor_All]
GO
/****** Object:  StoredProcedure [dbo].[ChemistUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistUpsert]
GO
/****** Object:  StoredProcedure [dbo].[ChemistSelectAll]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[ChemistSelect]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChemistSelect]
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChampaignMaster_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChampaignMaster_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChampaignMaster_ById]
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ChampaignMaster_All]
GO
/****** Object:  StoredProcedure [dbo].[CategoryMaster_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CategoryMaster_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[CategoryMaster_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CategoryMaster_ById]
GO
/****** Object:  StoredProcedure [dbo].[CategoryMaster_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CategoryMaster_All]
GO
/****** Object:  StoredProcedure [dbo].[CallIn_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CallIn_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[CallIn_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CallIn_Delete]
GO
/****** Object:  StoredProcedure [dbo].[CallIn_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CallIn_ById]
GO
/****** Object:  StoredProcedure [dbo].[CallIn_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[CallIn_All]
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_Upsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ApplyMaster_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_Delete]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ApplyMaster_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_ById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ApplyMaster_ById]
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_All]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[ApplyMaster_All]
GO
/****** Object:  StoredProcedure [dbo].[AddressUpsert]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[AddressUpsert]
GO
/****** Object:  StoredProcedure [dbo].[AddressSelectById]    Script Date: 01-08-2021 12:02:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[AddressSelectById]
GO
/****** Object:  Index [IX_ProductCategory]    Script Date: 01-08-2021 12:02:02 ******/
DROP INDEX IF EXISTS [IX_ProductCategory] ON [dbo].[ProductCategory]
GO
/****** Object:  Table [dbo].[WorkWithMaster]    Script Date: 01-08-2021 12:02:02 ******/
DROP TABLE IF EXISTS [dbo].[WorkWithMaster]
GO
/****** Object:  Table [dbo].[WorkType]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[WorkType]
GO
/****** Object:  Table [dbo].[WorkExperience]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[WorkExperience]
GO
/****** Object:  Table [dbo].[VisitType]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[VisitType]
GO
/****** Object:  Table [dbo].[TourPlanMonth]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[TourPlanMonth]
GO
/****** Object:  Table [dbo].[TourPlan]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[TourPlan]
GO
/****** Object:  Table [dbo].[StateRegionHq]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[StateRegionHq]
GO
/****** Object:  Table [dbo].[State]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[State]
GO
/****** Object:  Table [dbo].[Specialization]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Specialization]
GO
/****** Object:  Table [dbo].[RouteType]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[RouteType]
GO
/****** Object:  Table [dbo].[Route]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Route]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Role]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Region]
GO
/****** Object:  Table [dbo].[QuantitySIUnites]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[QuantitySIUnites]
GO
/****** Object:  Table [dbo].[Qualification]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Qualification]
GO
/****** Object:  Table [dbo].[ProductVsDoctor]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductVsDoctor]
GO
/****** Object:  Table [dbo].[ProductVsChemist]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductVsChemist]
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductType]
GO
/****** Object:  Table [dbo].[ProductPrescription]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductPrescription]
GO
/****** Object:  Table [dbo].[ProductManufacture]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductManufacture]
GO
/****** Object:  Table [dbo].[ProductImages]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductImages]
GO
/****** Object:  Table [dbo].[ProductHightlights]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductHightlights]
GO
/****** Object:  Table [dbo].[ProductHighlight]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductHighlight]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ProductCategory]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Product]
GO
/****** Object:  Table [dbo].[Presentation]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Presentation]
GO
/****** Object:  Table [dbo].[PrescriberProducts]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[PrescriberProducts]
GO
/****** Object:  Table [dbo].[Prescriber]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Prescriber]
GO
/****** Object:  Table [dbo].[Outcome]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Outcome]
GO
/****** Object:  Table [dbo].[MR_PresentationResources]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[MR_PresentationResources]
GO
/****** Object:  Table [dbo].[MR_Presentation_Products]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[MR_Presentation_Products]
GO
/****** Object:  Table [dbo].[MR_Presentation]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[MR_Presentation]
GO
/****** Object:  Table [dbo].[MasterWorkAt]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[MasterWorkAt]
GO
/****** Object:  Table [dbo].[Masters]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Masters]
GO
/****** Object:  Table [dbo].[MasterPlans]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[MasterPlans]
GO
/****** Object:  Table [dbo].[LeaveTypeMaster]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[LeaveTypeMaster]
GO
/****** Object:  Table [dbo].[Leave]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Leave]
GO
/****** Object:  Table [dbo].[JointWorking]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[JointWorking]
GO
/****** Object:  Table [dbo].[Headquarter]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Headquarter]
GO
/****** Object:  Table [dbo].[Gift]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Gift]
GO
/****** Object:  Table [dbo].[FieldName]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[FieldName]
GO
/****** Object:  Table [dbo].[EmployeeRegion]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[EmployeeRegion]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Employee]
GO
/****** Object:  Table [dbo].[DSRStatus]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSRStatus]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor_Products]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR_VisitedDoctor_Products]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor_Campaign_Gifts]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign_Gifts]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor_Campaign]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR_VisitedDoctor_Campaign]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR_VisitedDoctor]
GO
/****** Object:  Table [dbo].[DSR_VisitedChemist_Products]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR_VisitedChemist_Products]
GO
/****** Object:  Table [dbo].[DSR_VisitedChemist]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR_VisitedChemist]
GO
/****** Object:  Table [dbo].[DSR]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DSR]
GO
/****** Object:  Table [dbo].[DoctorProducts]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DoctorProducts]
GO
/****** Object:  Table [dbo].[DoctorCategory]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DoctorCategory]
GO
/****** Object:  Table [dbo].[DoctorAndChemist]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DoctorAndChemist]
GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Doctor]
GO
/****** Object:  Table [dbo].[DayTypeMaster]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[DayTypeMaster]
GO
/****** Object:  Table [dbo].[City]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[City]
GO
/****** Object:  Table [dbo].[ChemistVsDoctor]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ChemistVsDoctor]
GO
/****** Object:  Table [dbo].[Chemist]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Chemist]
GO
/****** Object:  Table [dbo].[ChampaignMaster]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ChampaignMaster]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[CategoryMaster]
GO
/****** Object:  Table [dbo].[CallIn]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[CallIn]
GO
/****** Object:  Table [dbo].[ApplyMaster]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[ApplyMaster]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 01-08-2021 12:02:03 ******/
DROP TABLE IF EXISTS [dbo].[Address]
GO
/****** Object:  User [sa]    Script Date: 01-08-2021 12:02:03 ******/
DROP USER IF EXISTS [sa]
GO
USE [master]
GO
/****** Object:  Database [MR-App]    Script Date: 01-08-2021 12:02:03 ******/
DROP DATABASE IF EXISTS [MR-App]
GO
/****** Object:  Database [MR-App]    Script Date: 01-08-2021 12:02:03 ******/
CREATE DATABASE [MR-App]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MR-App', FILENAME = N'D:\rdsdbdata\DATA\MR-App.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MR-App_log', FILENAME = N'D:\rdsdbdata\DATA\MR-App_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [MR-App] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MR-App].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MR-App] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MR-App] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MR-App] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MR-App] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MR-App] SET ARITHABORT OFF 
GO
ALTER DATABASE [MR-App] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MR-App] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MR-App] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MR-App] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MR-App] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MR-App] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MR-App] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MR-App] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MR-App] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MR-App] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MR-App] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MR-App] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MR-App] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MR-App] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MR-App] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MR-App] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MR-App] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MR-App] SET RECOVERY FULL 
GO
ALTER DATABASE [MR-App] SET  MULTI_USER 
GO
ALTER DATABASE [MR-App] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MR-App] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MR-App] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MR-App] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MR-App] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MR-App] SET QUERY_STORE = OFF
GO
USE [MR-App]
GO
/****** Object:  User [sa]    Script Date: 01-08-2021 12:02:07 ******/
CREATE USER [sa] FOR LOGIN [sa] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [sa]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 01-08-2021 12:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Pincode] [varchar](10) NULL,
	[Headquarter] [int] NULL,
	[Region] [int] NULL,
	[State] [int] NULL,
	[City] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplyMaster]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplyMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_ApplyMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CallIn]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CallIn](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_CallIn] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FieldNameId] [bigint] NULL,
	[MastersId] [bigint] NULL,
	[Name] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_CategoryMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChampaignMaster]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChampaignMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CampaignName] [varchar](500) NULL,
	[StartDate] [varchar](500) NULL,
	[EndDate] [varchar](500) NULL,
	[ApplyMasterId] [bigint] NULL,
	[RegionId] [bigint] NULL,
	[HeadquarterId] [bigint] NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_ChampaignMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chemist]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chemist](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[Mobile] [varchar](15) NULL,
	[Phone] [varchar](15) NULL,
	[Headquarter] [int] NULL,
	[Region] [int] NULL,
	[ContactPerson] [nvarchar](max) NULL,
	[WebSite] [nvarchar](max) NULL,
	[Address] [int] NULL,
	[BuisnessPotential] [nvarchar](100) NULL,
	[Remarks] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Chemist] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChemistVsDoctor]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChemistVsDoctor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DoctorId] [bigint] NULL,
	[ChemistId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_ChemistVsDoctor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[StateId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DayTypeMaster]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DayTypeMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_DayTypeMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doctor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[Gender] [bit] NULL,
	[Mobile] [varchar](15) NULL,
	[Phone] [varchar](15) NULL,
	[Birthdate] [datetime] NULL,
	[Anniversary] [datetime] NULL,
	[Photo] [nvarchar](max) NULL,
	[Specialization] [int] NULL,
	[Qualification] [int] NULL,
	[Category] [int] NULL,
	[WebSite] [nvarchar](max) NULL,
	[Address] [int] NULL,
	[BuisnessPotential] [nvarchar](100) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Doctors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoctorAndChemist]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoctorAndChemist](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TourPlanMonthId] [int] NULL,
	[DoctorId] [varchar](max) NULL,
	[ChemistId] [varchar](max) NULL,
	[IsDelete] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
 CONSTRAINT [PK_DoctorAndChemist] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoctorCategory]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoctorCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_DoctorCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoctorProducts]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoctorProducts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DoctorId] [bigint] NULL,
	[PRoductId] [bigint] NULL,
	[ExpectedSaleQty] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_DoctorProducts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [varchar](500) NULL,
	[RouteTypeId] [bigint] NULL,
	[WorkTypeId] [bigint] NULL,
	[RouteId] [bigint] NULL,
	[VisitTypeId] [bigint] NULL,
	[DoctorId] [bigint] NULL,
	[ChemistId] [bigint] NULL,
	[EmployeeId] [int] NULL,
	[StatusId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR_VisitedChemist]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR_VisitedChemist](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DSRId] [bigint] NULL,
	[ChemistId] [bigint] NULL,
	[CallInId] [bigint] NULL,
	[OutcomeId] [bigint] NULL,
	[WorkWithId] [bigint] NULL,
	[Remarks] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR_VisitedChemist] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR_VisitedChemist_Products]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR_VisitedChemist_Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DSR_VisitedChemistId] [bigint] NULL,
	[ProductId] [bigint] NULL,
	[OrderBookedQty] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR_VisitedChemist_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR_VisitedDoctor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DSRId] [bigint] NULL,
	[DoctorId] [bigint] NULL,
	[CallInId] [bigint] NULL,
	[OutcomeId] [bigint] NULL,
	[WorkWithId] [bigint] NULL,
	[Remarks] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR_VisitedDoctor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor_Campaign]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR_VisitedDoctor_Campaign](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DSR_VisitedDoctorId] [bigint] NULL,
	[ChampaignMasterId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR_VisitedDoctor_Campaign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor_Campaign_Gifts]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR_VisitedDoctor_Campaign_Gifts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DSR_VisitedDoctor_CampaignId] [bigint] NULL,
	[GiftId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR_VisitedDoctor_Campaign_Gifts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSR_VisitedDoctor_Products]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSR_VisitedDoctor_Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DSR_VisitedDoctorId] [bigint] NULL,
	[ProductId] [bigint] NULL,
	[OrderBookedQty] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_DSR_VisitedDoctor_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DSRStatus]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DSRStatus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_DSRStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[Gender] [int] NULL,
	[Mobile] [varchar](15) NULL,
	[Phone] [varchar](15) NULL,
	[EmergencyNumber] [varchar](15) NULL,
	[Birthdate] [datetime] NULL,
	[Anniversary] [datetime] NULL,
	[Email] [nvarchar](256) NULL,
	[Photo] [nvarchar](max) NULL,
	[PermanentAddress] [int] NULL,
	[CurrentAddress] [int] NULL,
	[AddressProof] [nvarchar](max) NULL,
	[DrivingLicense] [nvarchar](max) NULL,
	[Remarks] [nvarchar](max) NULL,
	[Qualification] [nvarchar](max) NULL,
	[JoiningDate] [datetime] NULL,
	[ConfirmationDate] [datetime] NULL,
	[Role] [int] NULL,
	[Headquarter] [int] NULL,
	[Region] [int] NULL,
	[ReportingPerson] [int] NULL,
	[SalaryAmount] [decimal](18, 2) NULL,
	[PaymentMode] [nvarchar](max) NULL,
	[BankName] [nvarchar](100) NULL,
	[BankBranch] [nvarchar](100) NULL,
	[BankAccountNo] [nvarchar](20) NULL,
	[BankIFSCCode] [nvarchar](20) NULL,
	[WebPassword] [nvarchar](max) NULL,
	[MobilePassword] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeRegion]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeRegion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[RegionId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_EmployeeRegion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FieldName]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldName](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[MasterId] [int] NULL,
 CONSTRAINT [PK_FieldName] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gift]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gift](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ChampaignMasterId] [bigint] NULL,
	[GiftName] [varchar](500) NULL,
	[GiftQty] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_Gift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Headquarter]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Headquarter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[RouteType] [int] NULL,
	[RegionId] [int] NULL,
	[IsActive] [bit] NULL,
	[ManagerId] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Headquarter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JointWorking]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JointWorking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TourPlanMonthId] [int] NULL,
	[DoctorAndChemistId] [int] NULL,
	[EmployeeIds] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_JointWorking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Leave]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Leave](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [bigint] NULL,
	[LeaveTypeId] [bigint] NULL,
	[DayTypeId] [bigint] NULL,
	[FromDate] [varchar](250) NULL,
	[ToDate] [varchar](250) NULL,
	[LeaveReason] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Leave] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LeaveTypeMaster]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LeaveTypeMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_LeaveTypeMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterPlans]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterPlans](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Price] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_MasterPlans] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Masters]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Masters](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Masters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterWorkAt]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterWorkAt](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_MasterWorkAt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MR_Presentation]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MR_Presentation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
	[DoctorId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MR_Presentation_Products]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MR_Presentation_Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MR_Presentation_Id] [bigint] NULL,
	[ProductId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_MR_Presentation_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MR_PresentationResources]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MR_PresentationResources](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MR_PresentationId] [bigint] NULL,
	[PresentationId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
	[MR_Presentation_ProductsId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Outcome]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Outcome](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_Outcome] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prescriber]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prescriber](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DoctorId] [bigint] NULL,
	[EmployeeId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Prescriber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PrescriberProducts]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrescriberProducts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PrescriberId] [bigint] NULL,
	[ProductId] [bigint] NULL,
	[Price] [varchar](500) NULL,
	[Qty] [varchar](500) NULL,
	[TotalBusiness] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_PrescriberProducts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Presentation]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Presentation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileURL] [varchar](max) NULL,
	[FileName] [varchar](500) NULL,
	[Size] [varchar](500) NULL,
	[FileType] [varchar](500) NULL,
	[ProductId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Presentation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CategoryId] [bigint] NULL,
	[ProductTypeId] [bigint] NULL,
	[Quantity] [bigint] NULL,
	[SIUnitesId] [bigint] NULL,
	[ManufactureId] [bigint] NULL,
	[StoreBelowTemprature] [varchar](500) NULL,
	[PrescriptionId] [bigint] NULL,
	[NRV] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[Price] [decimal](18, 2) NULL,
	[SellingPrice] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK__Product__3214EC07B914F539] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductHighlight]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductHighlight](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[ProductId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductHighlight] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductHightlights]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductHightlights](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductImages]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[URL] [varchar](max) NULL,
	[ProductId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductManufacture]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductManufacture](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[Website] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductManufacture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductPrescription]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPrescription](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductPrescription] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductVsChemist]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductVsChemist](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ChemistId] [bigint] NULL,
	[ProductId] [bigint] NULL,
	[Quantity] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductVsChemist] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductVsDoctor]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductVsDoctor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DoctorId] [bigint] NULL,
	[ProductId] [bigint] NULL,
	[Quantity] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_ProductVsDoctor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Qualification]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Qualification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Qualification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuantitySIUnites]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuantitySIUnites](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_QuantitySIUnites] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[IsActive] [bit] NULL,
	[ManagerId] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Details] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Route]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Route](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[IsActive] [bit] NULL,
	[RouteStart] [nvarchar](max) NULL,
	[RouteEnd] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Distance] [nvarchar](50) NULL,
	[TotalFair] [decimal](18, 2) NULL,
	[RegionId] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Route] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RouteType]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RouteType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_RouteType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Specialization]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Specialization](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Specilization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StateRegionHq]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StateRegionHq](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[ParentId] [int] NULL,
	[GrandParentId] [int] NULL,
	[IsDeleted] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
 CONSTRAINT [PK_StateRagionHq] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TourPlan]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TourPlan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Month] [varchar](100) NULL,
	[SubmittedDate] [varchar](100) NULL,
	[LastDate] [varchar](100) NULL,
	[IsSubmitted] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_TourPlan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TourPlanMonth]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TourPlanMonth](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TourPlanId] [int] NULL,
	[Date] [varchar](100) NULL,
	[Day] [varchar](100) NULL,
	[WorkAtId] [int] NULL,
	[WorkTypeId] [int] NULL,
	[WorkRouteIds] [varchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_TourPlanMonth] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitType]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_VisitType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkExperience]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkExperience](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[CompanyName] [nvarchar](max) NULL,
	[Position] [nvarchar](100) NULL,
	[JoiningDate] [datetime] NULL,
	[LeavingDate] [datetime] NULL,
	[Salary] [decimal](18, 2) NULL,
	[LeavingReason] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_WorkExperience] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkType]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_WorkType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkWithMaster]    Script Date: 01-08-2021 12:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkWithMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'demo', N'dmeo', N'dmeo', 0, 0, 1, 1, CAST(N'2021-07-16T18:31:09.950' AS DateTime), 3, CAST(N'2021-07-19T15:34:22.630' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'demo', N'demo', N'dmeo', 0, 0, 1, 1, CAST(N'2021-07-16T18:31:10.040' AS DateTime), 3, CAST(N'2021-07-19T15:34:22.870' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'demo', N'demo', N'76575', 2, 1, 0, 0, CAST(N'2021-07-16T18:38:13.040' AS DateTime), 0, CAST(N'2021-07-17T05:49:35.393' AS DateTime), 0)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'test', N'test', N'test', 0, 0, 0, 0, CAST(N'2021-07-17T05:18:32.610' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'test', N'test', N'test', 0, 0, 0, 0, CAST(N'2021-07-17T05:18:32.700' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'899, sv sqare', NULL, N'356988', 2, 1, 0, 0, CAST(N'2021-07-17T10:43:02.513' AS DateTime), 0, CAST(N'2021-07-17T11:00:59.110' AS DateTime), 0)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, NULL, NULL, NULL, 0, 0, 0, 0, CAST(N'2021-07-19T08:02:20.113' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, NULL, NULL, NULL, 0, 0, 0, 0, CAST(N'2021-07-19T08:02:20.203' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, N'demo', N'demo', N'demo', 0, 0, 1, 1, CAST(N'2021-07-19T08:02:22.720' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (10, N'demo', N'demo', N'demo', 0, 0, 1, 1, CAST(N'2021-07-19T08:02:22.810' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (11, N'demo', N'demo', N'demo', 0, 0, 1, 1, CAST(N'2021-07-19T08:02:54.580' AS DateTime), 3, CAST(N'2021-07-19T08:03:21.797' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (12, N'demo', N'demo', N'demo', 0, 0, 1, 1, CAST(N'2021-07-19T08:02:54.657' AS DateTime), 3, CAST(N'2021-07-19T08:03:21.873' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (13, N'demo', N'demo', N'demo', 3, 1, 0, 0, CAST(N'2021-07-19T08:52:28.107' AS DateTime), 0, CAST(N'2021-07-20T06:05:10.233' AS DateTime), 0)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (14, N'demo', N'demo', N'demo', 10, 1, 0, 0, CAST(N'2021-07-19T08:55:53.413' AS DateTime), 0, CAST(N'2021-07-19T09:07:28.780' AS DateTime), 0)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (15, N'Ahmedabad', N'Ahmedabad', N'380021', 1, 1, 0, 0, CAST(N'2021-07-19T10:42:04.590' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (16, N'Bopal', N'Bopal', N'380021', 0, 0, 1, 1, CAST(N'2021-07-19T10:46:33.307' AS DateTime), 3, CAST(N'2021-07-19T15:45:00.390' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (17, N'Bopal', N'Bopal', N'380021', 0, 0, 1, 1, CAST(N'2021-07-19T10:46:33.380' AS DateTime), 3, CAST(N'2021-07-19T15:45:00.480' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (18, NULL, NULL, NULL, 0, 0, 3, 6, CAST(N'2021-07-20T12:11:14.067' AS DateTime), 5, CAST(N'2021-07-31T05:08:02.700' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (19, NULL, NULL, NULL, 0, 0, 3, 6, CAST(N'2021-07-20T12:11:14.150' AS DateTime), 5, CAST(N'2021-07-31T05:08:02.710' AS DateTime), 3)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (20, N'A 504 Sector5  Sun City', N'South Bopal', N'380015', 0, 0, 1, 1, CAST(N'2021-07-20T12:33:49.083' AS DateTime), 5, NULL, NULL)
INSERT [dbo].[Address] ([Id], [Address1], [Address2], [Pincode], [Headquarter], [Region], [State], [City], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (21, N'A 504, Sector5, Sun City', N'South Bopal', N'380058', 0, 0, 3, 6, CAST(N'2021-07-20T12:33:49.173' AS DateTime), 5, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Address] OFF
GO
SET IDENTITY_INSERT [dbo].[ApplyMaster] ON 

INSERT [dbo].[ApplyMaster] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'string', CAST(N'2021-07-23T08:20:46.307' AS DateTime), 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[ApplyMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[CallIn] ON 

INSERT [dbo].[CallIn] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'Morning', CAST(N'2021-07-19T09:48:41.017' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[CallIn] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'Evening', CAST(N'2021-07-19T09:48:57.437' AS DateTime), 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[CallIn] OFF
GO
SET IDENTITY_INSERT [dbo].[Chemist] ON 

INSERT [dbo].[Chemist] ([Id], [Name], [IsActive], [Mobile], [Phone], [Headquarter], [Region], [ContactPerson], [WebSite], [Address], [BuisnessPotential], [Remarks], [Type], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Chemist', 1, N'1232656585', NULL, 2, 1, N'jay', N'www.Nobel.com', 6, N'$1250', N'sdfds', 2, CAST(N'2021-07-17T10:43:02.727' AS DateTime), 3, CAST(N'2021-07-17T11:00:59.333' AS DateTime), 3)
INSERT [dbo].[Chemist] ([Id], [Name], [IsActive], [Mobile], [Phone], [Headquarter], [Region], [ContactPerson], [WebSite], [Address], [BuisnessPotential], [Remarks], [Type], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Test Chemist', 1, N'7657657657', NULL, 4, 1, N'2346573245', N'demo.gmail.com', 14, N'8362876', N'demo
test
demo', 2, CAST(N'2021-07-19T08:55:53.503' AS DateTime), 3, CAST(N'2021-07-19T09:07:28.923' AS DateTime), 3)
INSERT [dbo].[Chemist] ([Id], [Name], [IsActive], [Mobile], [Phone], [Headquarter], [Region], [ContactPerson], [WebSite], [Address], [BuisnessPotential], [Remarks], [Type], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Nirdosh Vachhani', 0, N'9904187041', NULL, 1, 1, N'9427922314', N'rushkar.com', 15, NULL, N'Project Manager', 5, CAST(N'2021-07-19T10:42:04.680' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Chemist] OFF
GO
SET IDENTITY_INSERT [dbo].[ChemistVsDoctor] ON 

INSERT [dbo].[ChemistVsDoctor] ([Id], [DoctorId], [ChemistId], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, 1, 1, CAST(N'2021-07-17T10:55:37.953' AS DateTime), NULL, 3, NULL, NULL, 0)
INSERT [dbo].[ChemistVsDoctor] ([Id], [DoctorId], [ChemistId], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, 1, 2, CAST(N'2021-07-19T08:56:48.450' AS DateTime), NULL, 3, NULL, NULL, 0)
INSERT [dbo].[ChemistVsDoctor] ([Id], [DoctorId], [ChemistId], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, 2, 2, CAST(N'2021-07-19T08:56:51.407' AS DateTime), NULL, 3, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[ChemistVsDoctor] OFF
GO
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([Id], [Name], [StateId], [CreatedBy], [CreatedAt], [UpdatedBy], [UpdatedAt]) VALUES (1, N'Ahmedabad', 1, 0, CAST(N'2021-07-19T07:56:00.210' AS DateTime), NULL, NULL)
INSERT [dbo].[City] ([Id], [Name], [StateId], [CreatedBy], [CreatedAt], [UpdatedBy], [UpdatedAt]) VALUES (2, N'Surat', 1, 3, CAST(N'2021-07-19T13:03:58.790' AS DateTime), NULL, NULL)
INSERT [dbo].[City] ([Id], [Name], [StateId], [CreatedBy], [CreatedAt], [UpdatedBy], [UpdatedAt]) VALUES (3, N'Bhavnagar', 1, 3, CAST(N'2021-07-19T13:04:55.573' AS DateTime), NULL, NULL)
INSERT [dbo].[City] ([Id], [Name], [StateId], [CreatedBy], [CreatedAt], [UpdatedBy], [UpdatedAt]) VALUES (4, N'Vadodara', 1, 3, CAST(N'2021-07-19T14:55:55.937' AS DateTime), NULL, NULL)
INSERT [dbo].[City] ([Id], [Name], [StateId], [CreatedBy], [CreatedAt], [UpdatedBy], [UpdatedAt]) VALUES (5, N'PUNE', 2, 3, CAST(N'2021-07-19T14:56:34.890' AS DateTime), 3, CAST(N'2021-07-19T14:57:08.133' AS DateTime))
INSERT [dbo].[City] ([Id], [Name], [StateId], [CreatedBy], [CreatedAt], [UpdatedBy], [UpdatedAt]) VALUES (6, N'Mumbai', 2, 3, CAST(N'2021-07-19T15:46:30.803' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[City] OFF
GO
SET IDENTITY_INSERT [dbo].[Doctor] ON 

INSERT [dbo].[Doctor] ([Id], [FirstName], [LastName], [IsActive], [Gender], [Mobile], [Phone], [Birthdate], [Anniversary], [Photo], [Specialization], [Qualification], [Category], [WebSite], [Address], [BuisnessPotential], [Remarks], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Nehal', N'Panchal', 1, 0, N'8768686866', N'87687686876', CAST(N'2021-07-16T00:00:00.000' AS DateTime), CAST(N'2021-07-16T00:00:00.000' AS DateTime), N'', 29, 51, 2, N'demo@gmail.com', 3, NULL, N'test
demo', CAST(N'2021-07-16T18:38:13.127' AS DateTime), 3, CAST(N'2021-07-17T05:49:35.470' AS DateTime), 3)
INSERT [dbo].[Doctor] ([Id], [FirstName], [LastName], [IsActive], [Gender], [Mobile], [Phone], [Birthdate], [Anniversary], [Photo], [Specialization], [Qualification], [Category], [WebSite], [Address], [BuisnessPotential], [Remarks], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Dr.Rana', N'rana', 1, 0, N'8746583648', NULL, CAST(N'2021-07-19T00:00:00.000' AS DateTime), CAST(N'2021-07-19T00:00:00.000' AS DateTime), NULL, 27, 46, 1, N'demo.gmail.com', 13, NULL, N'test
demo', CAST(N'2021-07-19T08:52:28.363' AS DateTime), 3, CAST(N'2021-07-20T06:05:10.310' AS DateTime), 5)
SET IDENTITY_INSERT [dbo].[Doctor] OFF
GO
SET IDENTITY_INSERT [dbo].[DoctorAndChemist] ON 

INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, 1, N'2', N'0', 0, CAST(N'2021-07-30T12:40:23.323' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, 1, N'1', N'0', 0, CAST(N'2021-07-30T12:40:23.327' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, 1, N'0', N'2', 0, CAST(N'2021-07-30T12:40:27.993' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, 1, N'0', N'1', 0, CAST(N'2021-07-30T12:40:27.993' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, 2, N'0', N'3', 0, CAST(N'2021-07-30T12:44:29.887' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, 2, N'0', N'2', 0, CAST(N'2021-07-30T12:44:29.887' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (7, 2, N'0', N'1', 0, CAST(N'2021-07-30T12:44:29.890' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (8, 5, N'1', N'0', 0, CAST(N'2021-07-30T13:35:48.867' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (9, 3, N'2', N'0', 0, CAST(N'2021-07-31T08:43:10.223' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (10, 3, N'1', N'0', 0, CAST(N'2021-07-31T08:43:10.223' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (11, 4, N'2', N'0', 0, CAST(N'2021-07-31T09:46:25.553' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (12, 4, N'1', N'0', 0, CAST(N'2021-07-31T09:46:25.557' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (13, 4, N'0', N'3', 0, CAST(N'2021-07-31T09:46:32.740' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DoctorAndChemist] ([Id], [TourPlanMonthId], [DoctorId], [ChemistId], [IsDelete], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (14, 4, N'0', N'2', 0, CAST(N'2021-07-31T09:46:32.740' AS DateTime), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DoctorAndChemist] OFF
GO
SET IDENTITY_INSERT [dbo].[DoctorCategory] ON 

INSERT [dbo].[DoctorCategory] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'KOL', CAST(N'2021-07-16T13:36:17.917' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DoctorCategory] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'KBL', CAST(N'2021-07-16T13:36:27.713' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DoctorCategory] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Core', CAST(N'2021-07-19T09:03:32.920' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DoctorCategory] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'None Core', CAST(N'2021-07-19T09:03:42.543' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DoctorCategory] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'CDL', CAST(N'2021-07-19T09:04:05.803' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DoctorCategory] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'Test category', CAST(N'2021-07-19T12:57:12.750' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DoctorCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[DoctorProducts] ON 

INSERT [dbo].[DoctorProducts] ([Id], [DoctorId], [PRoductId], [ExpectedSaleQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, 2, 3, CAST(0.00 AS Decimal(18, 2)), CAST(N'2021-07-30T05:33:38.333' AS DateTime), 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[DoctorProducts] OFF
GO
SET IDENTITY_INSERT [dbo].[DSR] ON 

INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'17/07/2021', 0, 0, 2, 0, 0, 0, 3, 0, CAST(N'2021-07-17T06:04:37.243' AS DateTime), 3, CAST(N'2021-07-17T06:05:25.730' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'18/07/2021', 0, 0, 1, 0, 0, 0, 3, 0, CAST(N'2021-07-17T06:07:55.923' AS DateTime), 3, NULL, NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'17/07/2021', 0, 0, 0, 0, 0, 0, 3, 0, CAST(N'2021-07-17T06:32:08.577' AS DateTime), 3, CAST(N'2021-07-17T06:33:46.797' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'17/07/2021', 1, 1, 2, 1, 0, 0, 3, 1, CAST(N'2021-07-17T06:34:18.270' AS DateTime), 3, CAST(N'2021-07-21T12:45:58.127' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'17/7/2021', 1, 1, 2, 3, 0, 0, 3, 2, CAST(N'2021-07-17T09:28:10.477' AS DateTime), 0, CAST(N'2021-07-22T04:37:03.207' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, N'20/02/2019', 3, 2, 5, 2, 0, 0, 3, 0, CAST(N'2021-07-17T09:39:05.957' AS DateTime), 0, CAST(N'2021-07-19T09:55:24.237' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (7, N'17/7/2021', 3, 1, 7, 1, 0, 0, 3, 1, CAST(N'2021-07-17T09:55:23.603' AS DateTime), 0, CAST(N'2021-07-19T09:53:19.807' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (8, N'19/07/2021', 2, 1, 9, 3, 0, 0, 3, 1, CAST(N'2021-07-19T09:40:27.140' AS DateTime), 3, CAST(N'2021-07-19T09:48:24.743' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (9, N'19/07/2021', 3, 1, 9, 3, 0, 0, 3, 1, CAST(N'2021-07-19T09:53:21.197' AS DateTime), 3, CAST(N'2021-07-19T09:55:35.010' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (10, N'21/07/2021', 3, 2, 8, 3, 0, 0, 3, 0, CAST(N'2021-07-19T09:58:12.933' AS DateTime), 3, CAST(N'2021-07-22T04:52:52.800' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (11, N'24/07/2021', 2, 3, 5, 3, 0, 0, 3, 1, CAST(N'2021-07-19T10:27:52.670' AS DateTime), 3, CAST(N'2021-07-19T10:31:25.990' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (12, N'09/07/2021', 3, 1, 9, 3, 0, 0, 0, 0, CAST(N'2021-07-20T08:06:20.520' AS DateTime), 5, CAST(N'2021-07-29T07:38:26.493' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (13, N'22/07/2021', 3, 1, 5, 3, 0, 0, 3, 0, CAST(N'2021-07-22T04:13:00.883' AS DateTime), 3, CAST(N'2021-07-22T05:32:20.927' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (14, N'31/7/2021', 2, 1, 2, 2, 0, 0, 3, 0, CAST(N'2021-07-22T05:38:54.147' AS DateTime), 0, CAST(N'2021-07-22T05:58:16.230' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (15, N'4/7/2021', 3, 1, 9, 1, 0, 0, 3, 0, CAST(N'2021-07-22T10:25:23.827' AS DateTime), 0, CAST(N'2021-07-22T10:35:34.503' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (16, N'28/07/2021', 1, 1, 9, 1, 0, 0, 5, 0, CAST(N'2021-07-28T10:42:37.563' AS DateTime), 5, NULL, NULL, NULL, 0)
INSERT [dbo].[DSR] ([Id], [Date], [RouteTypeId], [WorkTypeId], [RouteId], [VisitTypeId], [DoctorId], [ChemistId], [EmployeeId], [StatusId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (17, N'16/07/2021', 2, 1, 6, 3, 0, 0, 0, 2, CAST(N'2021-07-31T10:02:55.443' AS DateTime), 3, CAST(N'2021-07-31T10:03:41.270' AS DateTime), NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[DSR] OFF
GO
SET IDENTITY_INSERT [dbo].[DSR_VisitedChemist] ON 

INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 6, 1, 2, 3, 4, NULL, CAST(N'2021-07-17T11:28:53.980' AS DateTime), 3, CAST(N'2021-07-19T09:55:38.420' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 8, 1, 0, 0, 0, NULL, CAST(N'2021-07-19T09:40:32.017' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 8, 2, 0, 0, 0, NULL, CAST(N'2021-07-19T09:40:35.543' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, 9, 2, 1, 2, 3, N'demo', CAST(N'2021-07-19T09:53:25.827' AS DateTime), 3, CAST(N'2021-07-19T09:54:13.713' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, 10, 2, 1, 3, 5, NULL, CAST(N'2021-07-19T09:58:19.027' AS DateTime), 3, CAST(N'2021-07-20T08:05:00.150' AS DateTime), 5)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, 10, 1, 1, 2, 4, N'demo', CAST(N'2021-07-19T09:58:20.760' AS DateTime), 3, CAST(N'2021-07-20T08:17:26.203' AS DateTime), 5)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, 11, 2, 2, 1, 3, N'test', CAST(N'2021-07-19T10:27:57.653' AS DateTime), 3, CAST(N'2021-07-19T10:28:48.680' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (8, 11, 1, 1, 2, 4, N'demo', CAST(N'2021-07-19T10:28:00.243' AS DateTime), 3, CAST(N'2021-07-19T10:28:19.597' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, 13, 3, 1, 2, 0, N'good', CAST(N'2021-07-22T04:13:57.637' AS DateTime), 3, CAST(N'2021-07-22T05:37:22.577' AS DateTime), 0)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (13, 14, 3, 1, 3, 0, N'not good', CAST(N'2021-07-22T05:46:53.867' AS DateTime), 0, CAST(N'2021-07-22T05:47:18.907' AS DateTime), 0)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (14, 14, 2, 2, 2, 0, N'good', CAST(N'2021-07-22T05:52:48.227' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (15, 14, 1, 1, 4, 4, N'not good', CAST(N'2021-07-22T05:58:26.073' AS DateTime), 3, CAST(N'2021-07-22T05:59:02.310' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (19, 12, 3, 0, 0, 0, NULL, CAST(N'2021-07-29T07:39:24.680' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (20, 12, 2, 0, 0, 0, NULL, CAST(N'2021-07-29T07:39:26.813' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (21, 12, 1, 0, 0, 0, NULL, CAST(N'2021-07-29T07:39:28.247' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (22, 17, 3, 0, 0, 0, NULL, CAST(N'2021-07-31T10:03:09.403' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist] ([Id], [DSRId], [ChemistId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (23, 17, 2, 0, 0, 0, NULL, CAST(N'2021-07-31T10:03:11.580' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DSR_VisitedChemist] OFF
GO
SET IDENTITY_INSERT [dbo].[DSR_VisitedChemist_Products] ON 

INSERT [dbo].[DSR_VisitedChemist_Products] ([Id], [DSR_VisitedChemistId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 1, 1, CAST(2.00 AS Decimal(18, 2)), CAST(N'2021-07-17T11:31:13.303' AS DateTime), 3, CAST(N'2021-07-20T08:17:02.267' AS DateTime), 5)
INSERT [dbo].[DSR_VisitedChemist_Products] ([Id], [DSR_VisitedChemistId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 2, 2, CAST(20.00 AS Decimal(18, 2)), CAST(N'2021-07-19T09:53:51.723' AS DateTime), 3, CAST(N'2021-07-19T10:28:42.350' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist_Products] ([Id], [DSR_VisitedChemistId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 2, 1, CAST(30.00 AS Decimal(18, 2)), CAST(N'2021-07-19T09:53:56.810' AS DateTime), 3, CAST(N'2021-07-19T10:28:46.307' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedChemist_Products] ([Id], [DSR_VisitedChemistId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, 12, 3, CAST(10.00 AS Decimal(18, 2)), CAST(N'2021-07-22T05:45:19.713' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedChemist_Products] ([Id], [DSR_VisitedChemistId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, 13, 3, CAST(20.00 AS Decimal(18, 2)), CAST(N'2021-07-22T05:47:19.327' AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DSR_VisitedChemist_Products] OFF
GO
SET IDENTITY_INSERT [dbo].[DSR_VisitedDoctor] ON 

INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 1, 1, 0, 0, 0, NULL, CAST(N'2021-07-17T06:04:48.900' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 3, 1, 0, 0, 0, NULL, CAST(N'2021-07-17T06:33:17.027' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 4, 1, 0, 0, 0, NULL, CAST(N'2021-07-17T06:34:25.000' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, 6, 1, 0, 0, 0, NULL, CAST(N'2021-07-17T11:32:27.577' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, 8, 1, 0, 0, 0, NULL, CAST(N'2021-07-19T09:40:45.857' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, 8, 2, 0, 0, 0, NULL, CAST(N'2021-07-19T09:40:47.707' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (8, 9, 1, 1, 3, 4, N'demo', CAST(N'2021-07-19T09:54:58.327' AS DateTime), 3, CAST(N'2021-07-19T09:55:15.673' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (9, 10, 2, 2, 3, 3, N'demo', CAST(N'2021-07-19T09:59:21.143' AS DateTime), 3, CAST(N'2021-07-19T09:59:52.223' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10, 11, 2, 2, 1, 3, N'demo', CAST(N'2021-07-19T10:29:06.977' AS DateTime), 3, CAST(N'2021-07-19T10:29:49.527' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, 11, 1, 1, 2, 4, N'test', CAST(N'2021-07-19T10:29:08.810' AS DateTime), 3, CAST(N'2021-07-19T10:29:28.823' AS DateTime), 3)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (12, 13, 1, 1, 1, 0, N'', CAST(N'2021-07-22T05:29:05.550' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (13, 13, 1, 1, 1, 0, N'', CAST(N'2021-07-22T05:31:32.430' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (15, 14, 1, 1, 1, 0, N'', CAST(N'2021-07-22T05:53:47.697' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (16, 14, 2, 1, 1, 0, N'', CAST(N'2021-07-22T10:20:42.970' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (17, 12, 1, 2, 2, 0, N'', CAST(N'2021-07-22T10:22:57.870' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (18, 12, 1, 2, 2, 0, N'', CAST(N'2021-07-22T10:24:52.950' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (19, 15, 1, 2, 3, 0, N'not good', CAST(N'2021-07-22T10:32:07.700' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (20, 15, 1, 2, 3, 0, N'not good', CAST(N'2021-07-22T10:32:26.440' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (21, 15, 2, 2, 1, 0, N'good', CAST(N'2021-07-22T10:32:59.393' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (22, 15, 1, 2, 3, 0, N'not good', CAST(N'2021-07-22T10:36:17.307' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (23, 17, 2, 0, 0, 0, NULL, CAST(N'2021-07-31T10:03:27.880' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[DSR_VisitedDoctor] ([Id], [DSRId], [DoctorId], [CallInId], [OutcomeId], [WorkWithId], [Remarks], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (24, 17, 1, 0, 0, 0, NULL, CAST(N'2021-07-31T10:03:29.323' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DSR_VisitedDoctor] OFF
GO
SET IDENTITY_INSERT [dbo].[DSR_VisitedDoctor_Products] ON 

INSERT [dbo].[DSR_VisitedDoctor_Products] ([Id], [DSR_VisitedDoctorId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 1, 1, CAST(20.00 AS Decimal(18, 2)), CAST(N'2021-07-17T06:05:13.650' AS DateTime), 3, CAST(N'2021-07-19T10:29:27.267' AS DateTime), NULL)
INSERT [dbo].[DSR_VisitedDoctor_Products] ([Id], [DSR_VisitedDoctorId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 2, 1, CAST(20.00 AS Decimal(18, 2)), CAST(N'2021-07-19T09:59:44.310' AS DateTime), 3, CAST(N'2021-07-19T10:29:44.570' AS DateTime), NULL)
INSERT [dbo].[DSR_VisitedDoctor_Products] ([Id], [DSR_VisitedDoctorId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 2, 2, CAST(35.00 AS Decimal(18, 2)), CAST(N'2021-07-19T09:59:49.457' AS DateTime), 3, CAST(N'2021-07-19T10:29:48.230' AS DateTime), NULL)
INSERT [dbo].[DSR_VisitedDoctor_Products] ([Id], [DSR_VisitedDoctorId], [ProductId], [OrderBookedQty], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, 13, 2, CAST(0.00 AS Decimal(18, 2)), CAST(N'2021-07-22T05:31:32.850' AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DSR_VisitedDoctor_Products] OFF
GO
SET IDENTITY_INSERT [dbo].[DSRStatus] ON 

INSERT [dbo].[DSRStatus] ([Id], [Name]) VALUES (1, N'Submitted')
INSERT [dbo].[DSRStatus] ([Id], [Name]) VALUES (2, N'Not Submitted')
SET IDENTITY_INSERT [dbo].[DSRStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [IsActive], [Gender], [Mobile], [Phone], [EmergencyNumber], [Birthdate], [Anniversary], [Email], [Photo], [PermanentAddress], [CurrentAddress], [AddressProof], [DrivingLicense], [Remarks], [Qualification], [JoiningDate], [ConfirmationDate], [Role], [Headquarter], [Region], [ReportingPerson], [SalaryAmount], [PaymentMode], [BankName], [BankBranch], [BankAccountNo], [BankIFSCCode], [WebPassword], [MobilePassword], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Nehal', N'Panchal', 1, 0, N'1234567890', NULL, N'1234567880', CAST(N'2021-07-16T00:00:00.000' AS DateTime), CAST(N'2021-07-19T00:00:00.000' AS DateTime), N'mr@gmail.com', N'Storage\Employee\3\Photo.jpg', 1, 2, N'address proof', N'license', N'remarks', N'B.  Tech', CAST(N'2021-07-16T00:00:00.000' AS DateTime), CAST(N'2021-07-16T00:00:00.000' AS DateTime), 1, 1, 1, 3, CAST(20000.00 AS Decimal(18, 2)), N'bank', N'test bank', N'test branch', N'46748768346586358', N'43556456456', N'admin1234', N'admin1234', CAST(N'2021-07-16T14:10:38.750' AS DateTime), 0, CAST(N'2021-07-19T15:34:23.103' AS DateTime), 3)
INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [IsActive], [Gender], [Mobile], [Phone], [EmergencyNumber], [Birthdate], [Anniversary], [Email], [Photo], [PermanentAddress], [CurrentAddress], [AddressProof], [DrivingLicense], [Remarks], [Qualification], [JoiningDate], [ConfirmationDate], [Role], [Headquarter], [Region], [ReportingPerson], [SalaryAmount], [PaymentMode], [BankName], [BankBranch], [BankAccountNo], [BankIFSCCode], [WebPassword], [MobilePassword], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'Raj', N'Shah', 1, 0, N'8764834533', NULL, N'1234567123', CAST(N'1990-12-12T00:00:00.000' AS DateTime), CAST(N'2000-12-12T00:00:00.000' AS DateTime), N'raj@gmail.com', N'Storage\Employee\4\Photo.jpg', 11, 12, N'Storage\Employee\4\AddressProof.jpg', N'Storage\Employee\4\DrivingLicence.jpg', N'demo', N'50', CAST(N'2021-07-19T00:00:00.000' AS DateTime), CAST(N'2021-07-19T00:00:00.000' AS DateTime), 1, 2, 1, 3, CAST(20000.00 AS Decimal(18, 2)), N'demo', N'demo', N'demo', N'demo', N'demo', N'Raj@299', N'Raj@299', CAST(N'2021-07-19T08:02:54.747' AS DateTime), 3, CAST(N'2021-07-19T08:03:21.950' AS DateTime), 3)
INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [IsActive], [Gender], [Mobile], [Phone], [EmergencyNumber], [Birthdate], [Anniversary], [Email], [Photo], [PermanentAddress], [CurrentAddress], [AddressProof], [DrivingLicense], [Remarks], [Qualification], [JoiningDate], [ConfirmationDate], [Role], [Headquarter], [Region], [ReportingPerson], [SalaryAmount], [PaymentMode], [BankName], [BankBranch], [BankAccountNo], [BankIFSCCode], [WebPassword], [MobilePassword], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'Nirdosh', N'Vachhani', 1, 0, N'9427922314', NULL, N'9904187041', CAST(N'1993-01-01T00:00:00.000' AS DateTime), CAST(N'2010-01-01T00:00:00.000' AS DateTime), N'nirdoshvachhani@gmail.com', N'Storage\Employee\5\Photo.jpg', 16, 17, N'Storage\Employee\5\AddressProof.jpg', N'Storage\Employee\5\DrivingLicence.jpg', N'demo
test
dfg', N'B. Pharm', CAST(N'2017-01-01T00:00:00.000' AS DateTime), CAST(N'2017-01-10T00:00:00.000' AS DateTime), 1, 2, 1, 3, CAST(20000.00 AS Decimal(18, 2)), N'bank', N'demo', N'demo bank', N'876768767698', N'DEMO87867', N'Nirdosh@773', N'Nirdosh@773', CAST(N'2021-07-19T10:46:33.470' AS DateTime), 3, CAST(N'2021-07-19T15:45:00.580' AS DateTime), 3)
INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [IsActive], [Gender], [Mobile], [Phone], [EmergencyNumber], [Birthdate], [Anniversary], [Email], [Photo], [PermanentAddress], [CurrentAddress], [AddressProof], [DrivingLicense], [Remarks], [Qualification], [JoiningDate], [ConfirmationDate], [Role], [Headquarter], [Region], [ReportingPerson], [SalaryAmount], [PaymentMode], [BankName], [BankBranch], [BankAccountNo], [BankIFSCCode], [WebPassword], [MobilePassword], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'Sanjay Bhai', N'Patel', 0, 0, N'7878588884', NULL, N'1234567890', CAST(N'1998-01-13T00:00:00.000' AS DateTime), CAST(N'2021-07-20T00:00:00.000' AS DateTime), N'abc@gmail.com', NULL, 18, 19, NULL, NULL, NULL, N'54', CAST(N'2021-07-31T00:00:00.000' AS DateTime), CAST(N'2021-07-31T00:00:00.000' AS DateTime), 1, 1, 1, 6, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, N'Sanjay Bhai@307', N'Sanjay Bhai@307', CAST(N'2021-07-20T12:11:14.250' AS DateTime), 5, CAST(N'2021-07-31T05:08:02.730' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
SET IDENTITY_INSERT [dbo].[EmployeeRegion] ON 

INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (20, 5, 16, CAST(N'2021-07-31T16:59:07.777' AS DateTime), 5)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (21, 5, 17, CAST(N'2021-07-31T16:59:07.777' AS DateTime), 5)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (22, 5, 18, CAST(N'2021-07-31T16:59:07.780' AS DateTime), 5)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (23, 6, 7, CAST(N'2021-07-31T17:01:31.370' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (24, 6, 3, CAST(N'2021-07-31T17:01:31.370' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (25, 6, 1, CAST(N'2021-07-31T17:01:31.370' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (26, 6, 2, CAST(N'2021-07-31T17:01:31.370' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (27, 6, 5, CAST(N'2021-07-31T17:01:31.373' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (28, 6, 8, CAST(N'2021-07-31T17:01:31.373' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (29, 6, 13, CAST(N'2021-07-31T17:01:31.373' AS DateTime), 6)
INSERT [dbo].[EmployeeRegion] ([Id], [UserId], [RegionId], [CreatedDate], [CreatedBy]) VALUES (30, 6, 25, CAST(N'2021-07-31T17:01:31.377' AS DateTime), 6)
SET IDENTITY_INSERT [dbo].[EmployeeRegion] OFF
GO
SET IDENTITY_INSERT [dbo].[FieldName] ON 

INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (1, N'Product Category', 1)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (2, N'Product Type', 1)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (3, N'Mfg. Name', 1)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (4, N'Doctor / Chemist Category', 3)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (5, N'City', 2)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (6, N'State', 2)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (7, N'Qualification', 3)
INSERT [dbo].[FieldName] ([Id], [Name], [MasterId]) VALUES (8, N'Specialization', 3)
SET IDENTITY_INSERT [dbo].[FieldName] OFF
GO
SET IDENTITY_INSERT [dbo].[Headquarter] ON 

INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Satelite', 2, 1, 1, 3, CAST(N'2021-07-16T18:05:11.583' AS DateTime), 0, CAST(N'2021-07-18T13:09:44.620' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Bopal', 1, 1, 1, 5, CAST(N'2021-07-16T18:06:24.630' AS DateTime), 0, CAST(N'2021-07-20T06:06:14.960' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Naroda', 1, 1, 1, 3, CAST(N'2021-07-17T07:12:08.647' AS DateTime), 0, CAST(N'2021-07-18T12:32:54.597' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'AMD-BHARUCH', 3, 1, 1, 3, CAST(N'2021-07-18T13:08:55.273' AS DateTime), 0, CAST(N'2021-07-18T13:11:22.450' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'SFF', 3, 1, 1, 3, CAST(N'2021-07-18T13:11:46.157' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'HEQ1', 3, 3, 1, 3, CAST(N'2021-07-18T15:39:08.790' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, N'HEQ2', 2, 3, 1, 3, CAST(N'2021-07-18T15:50:04.807' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, N'HEQ3', 1, 3, 1, 0, CAST(N'2021-07-18T15:52:29.230' AS DateTime), 0, CAST(N'2021-07-18T17:02:37.527' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, N'SURAT-AMD', 2, 2, 0, 3, CAST(N'2021-07-18T16:57:42.697' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (10, N'Demo 1', 3, 4, 1, 3, CAST(N'2021-07-19T05:47:24.457' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (11, N'Demo 2', 2, 4, 1, 3, CAST(N'2021-07-19T05:47:47.000' AS DateTime), 0, CAST(N'2021-07-19T05:48:24.277' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (12, N'Demo 3', 1, 4, 1, 3, CAST(N'2021-07-19T05:48:13.457' AS DateTime), 0, CAST(N'2021-07-19T05:48:48.680' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (13, N'Demo 4', 1, 4, 1, 4, CAST(N'2021-07-19T08:05:28.633' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (14, N'Rajkot West', 1, 5, 0, 5, CAST(N'2021-07-20T10:22:03.310' AS DateTime), 0, CAST(N'2021-07-20T10:22:35.577' AS DateTime), NULL)
INSERT [dbo].[Headquarter] ([Id], [Name], [RouteType], [RegionId], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (15, N'Rajot1', 2, 5, 0, 5, CAST(N'2021-07-20T11:51:43.070' AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Headquarter] OFF
GO
SET IDENTITY_INSERT [dbo].[JointWorking] ON 

INSERT [dbo].[JointWorking] ([Id], [TourPlanMonthId], [DoctorAndChemistId], [EmployeeIds], [CreatedDate], [CreatedBy]) VALUES (1, 1, 1, N'6', CAST(N'2021-07-25T05:35:17.480' AS DateTime), 0)
INSERT [dbo].[JointWorking] ([Id], [TourPlanMonthId], [DoctorAndChemistId], [EmployeeIds], [CreatedDate], [CreatedBy]) VALUES (2, 1, 1, N'5', CAST(N'2021-07-25T05:35:17.480' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[JointWorking] OFF
GO
SET IDENTITY_INSERT [dbo].[Masters] ON 

INSERT [dbo].[Masters] ([Id], [Name]) VALUES (1, N'Product Manager')
INSERT [dbo].[Masters] ([Id], [Name]) VALUES (2, N'Address')
INSERT [dbo].[Masters] ([Id], [Name]) VALUES (3, N'Doctors')
SET IDENTITY_INSERT [dbo].[Masters] OFF
GO
SET IDENTITY_INSERT [dbo].[MR_Presentation] ON 

INSERT [dbo].[MR_Presentation] ([Id], [EmployeeId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy], [DoctorId]) VALUES (1, 3, CAST(N'2021-07-17T09:46:22.877' AS DateTime), 0, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[MR_Presentation] ([Id], [EmployeeId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy], [DoctorId]) VALUES (2, 2, CAST(N'2021-07-17T11:23:42.483' AS DateTime), 0, NULL, NULL, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[MR_Presentation] OFF
GO
SET IDENTITY_INSERT [dbo].[MR_Presentation_Products] ON 

INSERT [dbo].[MR_Presentation_Products] ([Id], [MR_Presentation_Id], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 1, 1, CAST(N'2021-07-19T09:40:07.410' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[MR_Presentation_Products] ([Id], [MR_Presentation_Id], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 2, 2, CAST(N'2021-07-19T09:40:19.660' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[MR_Presentation_Products] ([Id], [MR_Presentation_Id], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 3, 3, CAST(N'2021-07-19T09:40:25.367' AS DateTime), 0, CAST(N'2021-07-19T09:40:34.243' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MR_Presentation_Products] OFF
GO
SET IDENTITY_INSERT [dbo].[MR_PresentationResources] ON 

INSERT [dbo].[MR_PresentationResources] ([Id], [MR_PresentationId], [PresentationId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy], [MR_Presentation_ProductsId]) VALUES (1, 1, 4, CAST(N'2021-07-17T09:46:24.457' AS DateTime), 0, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[MR_PresentationResources] ([Id], [MR_PresentationId], [PresentationId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy], [MR_Presentation_ProductsId]) VALUES (2, 2, 6, CAST(N'2021-07-17T11:23:43.207' AS DateTime), 0, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[MR_PresentationResources] ([Id], [MR_PresentationId], [PresentationId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy], [MR_Presentation_ProductsId]) VALUES (3, 0, 0, CAST(N'2021-07-31T06:51:34.273' AS DateTime), 0, NULL, NULL, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[MR_PresentationResources] OFF
GO
SET IDENTITY_INSERT [dbo].[Outcome] ON 

INSERT [dbo].[Outcome] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'Very Positive', CAST(N'2021-07-19T09:50:16.960' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[Outcome] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'Positive', CAST(N'2021-07-19T09:50:58.907' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[Outcome] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'Negative', CAST(N'2021-07-19T09:51:11.190' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[Outcome] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'Below Negative', CAST(N'2021-07-19T09:51:27.960' AS DateTime), 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[Outcome] OFF
GO
SET IDENTITY_INSERT [dbo].[Presentation] ON 

INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, N'Storage\Product\1\Presentation\3.jpg', N'3', N'0.00 MB', N'jpg', 1, CAST(N'2021-07-17T10:22:51.630' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, N'Storage\Product\2\Presentation\Lighthouse.jpg', N'Lighthouse', N'0.54 MB', N'jpg', 2, CAST(N'2021-07-19T08:46:08.407' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (8, N'Storage\Product\2\Presentation\Desert.jpg', N'Desert', N'0.81 MB', N'jpg', 2, CAST(N'2021-07-19T08:46:26.630' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (9, N'Storage\Product\3\Presentation\berrymen Visulate.pdf', N'berrymen Visulate', N'7.18 MB', N'pdf', 3, CAST(N'2021-07-20T10:56:14.350' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10, N'Storage\Product\3\Presentation\WhatsApp Image 2021-07-20 at 4.18.55 PM.jpeg', N'WhatsApp Image 2021-07-20 at 4.18.55 PM', N'0.21 MB', N'jpeg', 3, CAST(N'2021-07-20T10:56:32.607' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, N'Storage\Product\3\Presentation\WhatsApp Image 2021-07-20 at 4.18.54 PM.jpeg', N'WhatsApp Image 2021-07-20 at 4.18.54 PM', N'0.24 MB', N'jpeg', 3, CAST(N'2021-07-20T10:56:42.910' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (12, N'Storage\Product\3\Presentation\WhatsApp Image 2021-07-20 at 4.18.52 PM.jpeg', N'WhatsApp Image 2021-07-20 at 4.18.52 PM', N'0.27 MB', N'jpeg', 3, CAST(N'2021-07-20T10:56:59.127' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (13, N'string', N'string', N'string', N'string', 0, CAST(N'2021-07-28T06:32:49.277' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (14, N'string', N'string', N'string', N'string', 0, CAST(N'2021-07-29T05:27:45.487' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (15, N'string', N'string', N'string', N'string', 0, CAST(N'2021-07-30T05:07:30.823' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (16, N'string', N'string', N'string', N'string', 0, CAST(N'2021-07-30T05:07:30.980' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (17, N'string', N'string', N'string', N'string', 0, CAST(N'2021-07-30T06:42:41.797' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (18, N'Storage\Product\4\Presentation\product-13.jpg', N'product-13', N'0.01 MB', N'jpg', 4, CAST(N'2021-07-31T09:57:13.777' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (19, N'Storage\Product\4\Presentation\product-07.jpg', N'product-07', N'0.01 MB', N'jpg', 4, CAST(N'2021-07-31T09:57:21.587' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (20, N'Storage\Product\4\Presentation\product-03.jpg', N'product-03', N'0.01 MB', N'jpg', 4, CAST(N'2021-07-31T09:57:28.203' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (21, N'Storage\Product\4\Presentation\product-01.png', N'product-01', N'0.09 MB', N'png', 4, CAST(N'2021-07-31T09:57:40.973' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (22, N'Storage\Product\5\Presentation\product-13.jpg', N'product-13', N'0.01 MB', N'jpg', 5, CAST(N'2021-07-31T10:14:08.730' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Presentation] ([Id], [FileURL], [FileName], [Size], [FileType], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (23, N'Storage\Product\5\Presentation\Appoinment Manager  Beauty Book (1).pdf', N'Appoinment Manager  Beauty Book (1)', N'0.01 MB', N'pdf', 5, CAST(N'2021-07-31T10:14:35.200' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Presentation] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Id], [Name], [CategoryId], [ProductTypeId], [Quantity], [SIUnitesId], [ManufactureId], [StoreBelowTemprature], [PrescriptionId], [NRV], [Description], [Price], [SellingPrice], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'norgam atoz', 1, 1, 1, 3, 1, NULL, 1, N'12', N'<p>rwer</p>', CAST(0.00 AS Decimal(18, 2)), CAST(21.00 AS Decimal(18, 2)), 0, CAST(N'2021-07-31T10:48:17.933' AS DateTime), 3, CAST(N'2021-07-31T10:48:48.697' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Powder', CAST(N'2021-07-31T10:39:59.100' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductHighlight] ON 

INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (15, N'test 87', 2, CAST(N'2021-07-19T08:46:30.230' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (16, N' test 12', 2, CAST(N'2021-07-19T08:46:30.307' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (20, N'dmeo 2', 3, CAST(N'2021-07-20T10:57:02.063' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (21, N' demo 1', 3, CAST(N'2021-07-20T10:57:02.137' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (22, N' demo', 3, CAST(N'2021-07-20T10:57:02.213' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (26, N'asdadsas asda dsas das d', 4, CAST(N'2021-07-31T09:57:54.543' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (27, N' asdads asdsad asd as d asasd ad asd', 4, CAST(N'2021-07-31T09:57:54.557' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (28, N' asdasd', 4, CAST(N'2021-07-31T09:57:54.557' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (31, N'rwerwerr', 1, CAST(N'2021-07-31T10:48:48.720' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (32, N' 1234ref', 1, CAST(N'2021-07-31T10:48:48.730' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (33, N'   test 1', 1, CAST(N'2021-07-31T10:48:48.737' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (34, N'    test 2', 1, CAST(N'2021-07-31T10:48:48.737' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductHighlight] ([Id], [Name], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (35, N'  test 3', 1, CAST(N'2021-07-31T10:48:48.753' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductHighlight] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductImages] ON 

INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Storage\Product\1\productimg1.jpg', 1, CAST(N'2021-07-17T05:32:56.990' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'Storage\Product\1\productimg2.jpg', 1, CAST(N'2021-07-17T05:32:57.083' AS DateTime), 3, CAST(N'2021-07-31T10:48:48.767' AS DateTime), 3)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'Storage\Product\1\productimg3.jpg', 1, CAST(N'2021-07-17T05:32:57.157' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, N'Storage\Product\1\productimg4.jpg', 1, CAST(N'2021-07-17T05:32:57.233' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, N'Storage\Product\1\productimg5.jpg', 1, CAST(N'2021-07-17T05:32:57.310' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, N'Storage\Product\1\productimg6.jpg', 1, CAST(N'2021-07-17T05:32:57.400' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, N'Storage\Product\2\productimg1.jpg', 2, CAST(N'2021-07-19T08:45:37.433' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (8, N'Storage\Product\2\productimg2.jpg', 2, CAST(N'2021-07-19T08:45:37.510' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (9, N'Storage\Product\2\productimg3.jpg', 2, CAST(N'2021-07-19T08:45:37.587' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10, N'Storage\Product\2\productimg4.jpg', 2, CAST(N'2021-07-19T08:45:37.677' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, N'Storage\Product\2\productimg5.jpg', 2, CAST(N'2021-07-19T08:45:37.750' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (12, N'Storage\Product\2\productimg6.jpg', 2, CAST(N'2021-07-19T08:45:37.830' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (13, N'Storage\Product\3\productimg1.jpeg', 3, CAST(N'2021-07-20T10:55:48.213' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (14, N'Storage\Product\3\productimg2.jpeg', 3, CAST(N'2021-07-20T10:55:48.290' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (15, N'Storage\Product\3\productimg3.jpeg', 3, CAST(N'2021-07-20T10:55:48.380' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (16, N'Storage\Product\3\productimg4.jpeg', 3, CAST(N'2021-07-20T10:55:48.457' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (17, N'Storage\Product\3\productimg5.jpeg', 3, CAST(N'2021-07-20T10:55:48.530' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (18, N'Storage\Product\3\productimg6.jpeg', 3, CAST(N'2021-07-20T10:55:48.607' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (19, N'Storage\Product\4\productimg1.jpg', 4, CAST(N'2021-07-31T09:56:32.077' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (20, N'Storage\Product\4\productimg2.jpg', 4, CAST(N'2021-07-31T09:56:32.093' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (21, N'Storage\Product\4\productimg3.jpg', 4, CAST(N'2021-07-31T09:56:32.120' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (22, N'Storage\Product\4\productimg4.png', 4, CAST(N'2021-07-31T09:56:32.130' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (23, N'Storage\Product\5\productimg1.jpg', 5, CAST(N'2021-07-31T10:12:40.453' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (24, N'Storage\Product\5\productimg2.jpg', 5, CAST(N'2021-07-31T10:12:40.470' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (25, N'Storage\Product\5\productimg3.jpg', 5, CAST(N'2021-07-31T10:12:40.483' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (26, N'Storage\Product\1\productimg1.jpg', 1, CAST(N'2021-07-31T10:48:17.980' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (27, N'Storage\Product\1\productimg2.png', 1, CAST(N'2021-07-31T10:48:17.993' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (28, N'Storage\Product\1\productimg3.jpg', 1, CAST(N'2021-07-31T10:48:18.003' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductImages] ([Id], [URL], [ProductId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (29, N'Storage\Product\1\productimg4.jpg', 1, CAST(N'2021-07-31T10:48:18.020' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductImages] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductManufacture] ON 

INSERT [dbo].[ProductManufacture] ([Id], [Name], [Website], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'demo 1', N'demo@gmail.com', CAST(N'2021-07-17T05:24:20.377' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductManufacture] ([Id], [Name], [Website], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'demo 2', N'demo@gmail.com', CAST(N'2021-07-17T05:24:35.010' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductManufacture] ([Id], [Name], [Website], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'Vishwa Chemist', N'1', CAST(N'2021-07-19T13:05:27.870' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductManufacture] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductPrescription] ON 

INSERT [dbo].[ProductPrescription] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Required', CAST(N'2021-07-19T08:33:23.470' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ProductPrescription] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'Not Required', CAST(N'2021-07-19T08:33:34.687' AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductPrescription] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductType] ON 

INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Tablet', CAST(N'2021-07-31T10:40:40.153' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'Capsule', CAST(N'2021-07-31T10:40:45.640' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'Syrup', CAST(N'2021-07-31T10:40:53.177' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, N'Powder', CAST(N'2021-07-31T10:40:59.983' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, N'Sachet', CAST(N'2021-07-31T10:41:05.537' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, N'Cream', CAST(N'2021-07-31T10:41:12.133' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ProductType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, N'Other', CAST(N'2021-07-31T10:41:17.900' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductType] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductVsChemist] ON 

INSERT [dbo].[ProductVsChemist] ([Id], [ChemistId], [ProductId], [Quantity], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, 1, 1, 200, CAST(N'2021-07-17T10:55:14.000' AS DateTime), NULL, 3, NULL, NULL, 0)
INSERT [dbo].[ProductVsChemist] ([Id], [ChemistId], [ProductId], [Quantity], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, 2, 2, 1000, CAST(N'2021-07-19T08:56:40.793' AS DateTime), NULL, 3, NULL, NULL, 0)
INSERT [dbo].[ProductVsChemist] ([Id], [ChemistId], [ProductId], [Quantity], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, 2, 1, 200, CAST(N'2021-07-19T08:56:42.243' AS DateTime), NULL, 3, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[ProductVsChemist] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductVsDoctor] ON 

INSERT [dbo].[ProductVsDoctor] ([Id], [DoctorId], [ProductId], [Quantity], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, 1, 1, 20, CAST(N'2021-07-17T05:49:28.520' AS DateTime), NULL, 3, NULL, NULL, 0)
INSERT [dbo].[ProductVsDoctor] ([Id], [DoctorId], [ProductId], [Quantity], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, 2, 1, 200, CAST(N'2021-07-19T08:53:03.243' AS DateTime), NULL, 3, NULL, NULL, 0)
INSERT [dbo].[ProductVsDoctor] ([Id], [DoctorId], [ProductId], [Quantity], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, 2, 2, 1000, CAST(N'2021-07-19T08:53:05.207' AS DateTime), NULL, 3, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[ProductVsDoctor] OFF
GO
SET IDENTITY_INSERT [dbo].[Qualification] ON 

INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'BAMS', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'BDS', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'BHMS', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'BPT', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'BUMS', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'D ORTHO', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, N'DCH', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, N'DD', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, N'DGO', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (10, N'DHMS', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (11, N'DLO', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (12, N'DMC', CAST(N'2021-07-16T13:46:34.693' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (13, N'DNB', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (14, N'DOMS', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (15, N'DPM', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (16, N'DTCD', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (17, N'DTM', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (18, N'DTM &H', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (19, N'DVD', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (20, N'FRCOG', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (21, N'FRCP', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (22, N'FRSC', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (23, N'LCEH', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (24, N'LECH', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (25, N'MBBS', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (26, N'MBBS D ORTHO', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (27, N'MBBS DCH', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (28, N'MBBS DGO', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (29, N'MBBS DNB', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (30, N'MD', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (31, N'MD DCH', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (32, N'MD DGO', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (33, N'MD DM', CAST(N'2021-07-16T13:46:34.710' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (34, N'MD DNB', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (35, N'MD(DERM)', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (36, N'MD(PAED)', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (37, N'MD(PMR)', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (38, N'MDS', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (39, N'MRCOG', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (40, N'MRCP    ', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (41, N'MRCPCH	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (42, N'MRSC	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (43, N'MS		', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (44, N'MS(ENT)	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (45, N'MS(O&G)	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (46, N'MS (MCH)', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (47, N'MS ORTHO', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (48, N'MS DNB	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (49, N'MS MCH	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (50, N'OTHERS	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (51, N'RAMP	', CAST(N'2021-07-16T13:46:34.723' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (52, N'Test_Qualification1', CAST(N'2021-07-19T12:56:43.520' AS DateTime), 3, CAST(N'2021-07-19T14:24:18.787' AS DateTime), 3)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (53, N'Test_Qualifica2', CAST(N'2021-07-19T14:21:30.660' AS DateTime), 3, CAST(N'2021-07-19T14:24:07.290' AS DateTime), 3)
INSERT [dbo].[Qualification] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (54, N'Test_Qualification54', CAST(N'2021-07-19T15:16:23.250' AS DateTime), 3, CAST(N'2021-07-19T15:45:55.500' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Qualification] OFF
GO
SET IDENTITY_INSERT [dbo].[QuantitySIUnites] ON 

INSERT [dbo].[QuantitySIUnites] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Strip', NULL, NULL, NULL, NULL)
INSERT [dbo].[QuantitySIUnites] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'ML', NULL, NULL, NULL, NULL)
INSERT [dbo].[QuantitySIUnites] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'Gram', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[QuantitySIUnites] OFF
GO
SET IDENTITY_INSERT [dbo].[Region] ON 

INSERT [dbo].[Region] ([Id], [Name], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Ahmedabad', 1, 3, CAST(N'2021-07-16T17:25:35.140' AS DateTime), 0, CAST(N'2021-07-20T06:06:25.173' AS DateTime), 0)
INSERT [dbo].[Region] ([Id], [Name], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Surat', 1, 3, CAST(N'2021-07-17T06:07:42.140' AS DateTime), 0, CAST(N'2021-07-18T17:02:06.733' AS DateTime), 0)
INSERT [dbo].[Region] ([Id], [Name], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Bhavanagar', 1, 3, CAST(N'2021-07-18T15:38:22.007' AS DateTime), 0, CAST(N'2021-07-18T15:38:41.340' AS DateTime), 0)
INSERT [dbo].[Region] ([Id], [Name], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'Assam', 0, 3, CAST(N'2021-07-19T05:46:51.493' AS DateTime), 0, CAST(N'2021-07-20T05:35:10.670' AS DateTime), 0)
INSERT [dbo].[Region] ([Id], [Name], [IsActive], [ManagerId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'Rajkot', 1, 5, CAST(N'2021-07-20T10:20:34.817' AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Region] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Super Admin', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'GM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'NSM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'ZBH', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'BH', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'BM', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Name], [Details], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, N'BE', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Route] ON 

INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'South bopal', 0, N'test', N'demo', 2, N'20', CAST(40.00 AS Decimal(18, 2)), 1, CAST(N'2021-07-16T18:08:45.880' AS DateTime), 0, CAST(N'2021-07-18T13:07:46.987' AS DateTime), 3)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Ahmedabad', 1, N'demo', N'demo', 1, N'30', CAST(60.00 AS Decimal(18, 2)), 1, CAST(N'2021-07-16T18:20:38.393' AS DateTime), 3, CAST(N'2021-07-18T13:07:40.470' AS DateTime), 3)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'test route', 1, N'demo 2', N'demo', 3, N'100', CAST(200.00 AS Decimal(18, 2)), 2, CAST(N'2021-07-17T06:24:05.373' AS DateTime), 0, CAST(N'2021-07-18T13:07:34.670' AS DateTime), 3)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'AHM-AHM', 1, N'sd', N'sdf', 2, N'10', CAST(20.00 AS Decimal(18, 2)), 1, CAST(N'2021-07-17T06:54:25.437' AS DateTime), 0, CAST(N'2021-07-18T13:07:28.997' AS DateTime), 3)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'AMD-SURAT', 1, N'AMD', N'SURAT', 2, N'250', CAST(500.00 AS Decimal(18, 2)), 2, CAST(N'2021-07-17T07:31:09.290' AS DateTime), 3, CAST(N'2021-07-18T13:07:22.667' AS DateTime), 3)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'BVM-BVM', 0, N'BVM', N'BVM', 1, N'120', CAST(240.00 AS Decimal(18, 2)), 3, CAST(N'2021-07-18T16:19:55.330' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, N'BVM-MUM', 0, N'BVM', N'MUM', 3, N'750', CAST(1500.00 AS Decimal(18, 2)), 3, CAST(N'2021-07-18T16:29:32.443' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, N'Test Route', 0, N'Demo point start', N'Demo point end', 1, N'30', CAST(60.00 AS Decimal(18, 2)), 4, CAST(N'2021-07-19T05:52:46.303' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Route] ([Id], [Name], [IsActive], [RouteStart], [RouteEnd], [Type], [Distance], [TotalFair], [RegionId], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, N'Test 1', 1, N'test one', N'test two', 1, N'20', CAST(40.00 AS Decimal(18, 2)), 1, CAST(N'2021-07-19T08:06:58.593' AS DateTime), 3, CAST(N'2021-07-19T08:07:41.233' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Route] OFF
GO
SET IDENTITY_INSERT [dbo].[RouteType] ON 

INSERT [dbo].[RouteType] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'HQ Route', NULL, NULL, NULL, NULL)
INSERT [dbo].[RouteType] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Ex HQ Route', NULL, NULL, NULL, NULL)
INSERT [dbo].[RouteType] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'OS Route', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RouteType] OFF
GO
SET IDENTITY_INSERT [dbo].[Specialization] ON 

INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'CARDIO THORACIC SURGEON    ', CAST(N'2021-07-16T13:51:18.230' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'CARDIOLOGIST    ', CAST(N'2021-07-16T13:51:18.230' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'CONSULTANT PHYSICIAN    ', CAST(N'2021-07-16T13:51:18.230' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'COSMETOLOGY, DENTIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'DERMATOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'DIABETOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, N'ENDOCRINOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, N'ENT LARYNGOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, N'GASTEROENTEROLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (10, N'GENERAL PRACTITIONER    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (11, N'GYNAECOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (12, N'HAEMATOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (13, N'INTENSIVIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (14, N'NEPHROLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (15, N'NEURO SURGEON    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (16, N'NEUROLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (17, N'ONCOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (18, N'OPTHALMOLOGIST    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (19, N'ORTHOPAEDIC    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (20, N'ORTHOPAEDIC SURGEON    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (21, N'OTHERS    ', CAST(N'2021-07-16T13:51:18.243' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (22, N'PAEDIATRICIAN    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (23, N'PHYSIO THERAPIST    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (24, N'PLASTIC SURGEON    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (25, N'PSYCHIATRIST    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (26, N'PULMANOLOGIST    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (27, N'RHEUMATOLOGIST    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (28, N'SURGEON    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (29, N'UROLOGIST    ', CAST(N'2021-07-16T13:51:18.260' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Specialization] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (30, N'Test_Specialization1', CAST(N'2021-07-19T12:55:02.387' AS DateTime), 3, CAST(N'2021-07-19T14:24:29.230' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Specialization] OFF
GO
SET IDENTITY_INSERT [dbo].[State] ON 

INSERT [dbo].[State] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Gujarat', CAST(N'2021-07-19T07:55:02.690' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[State] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Maharashtra', CAST(N'2021-07-19T14:56:23.377' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[State] ([Id], [Name], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Assam', CAST(N'2021-07-19T15:46:18.063' AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[State] OFF
GO
SET IDENTITY_INSERT [dbo].[StateRegionHq] ON 

INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'Gujarat', 0, 0, 0, CAST(N'2021-07-29T05:37:16.600' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'Rajasthan', 0, 0, 0, CAST(N'2021-07-29T05:38:07.330' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'Surat', 1, 0, 0, CAST(N'2021-07-29T05:59:24.520' AS DateTime), 0, CAST(N'2021-07-29T11:22:17.893' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'Ahmedabad', 1, 0, 0, CAST(N'2021-07-29T06:22:13.753' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'Jaipur', 2, 0, 0, CAST(N'2021-07-29T06:45:25.657' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, N'Vejalpur', 0, 4, 0, CAST(N'2021-07-29T07:02:55.013' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (7, N'YogiChowk', 0, 3, 0, CAST(N'2021-07-29T07:03:25.293' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (8, N'Jagatpura', 0, 5, 0, CAST(N'2021-07-29T07:04:30.890' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (9, N'Maharastra', 0, 0, 0, CAST(N'2021-07-29T07:33:17.817' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (10, N'Pune', 9, 0, 0, CAST(N'2021-07-29T07:33:34.883' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (11, N'Mahaveer Nagar', 0, 5, 0, CAST(N'2021-07-29T09:36:40.980' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (12, N'Vaishali Nagar', 0, 5, 0, CAST(N'2021-07-29T09:36:59.967' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (13, N'Malviya Nagar', 0, 5, 0, CAST(N'2021-07-29T09:37:16.080' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (14, N'Junagadh', 1, 0, 0, CAST(N'2021-07-29T09:47:41.460' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (15, N'Jakatnaka', 0, 14, 0, CAST(N'2021-07-29T09:48:24.607' AS DateTime), 0, CAST(N'2021-07-29T09:48:31.177' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (16, N'Madhya Pradesh', 0, 0, 0, CAST(N'2021-07-29T10:01:46.337' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (17, N'Bhopal', 16, 0, 0, CAST(N'2021-07-29T10:02:05.797' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (18, N'Mayor–Council', 0, 17, 0, CAST(N'2021-07-29T10:02:29.997' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (19, N'Uttar Pradesh', 0, 0, 0, CAST(N'2021-07-29T10:56:49.283' AS DateTime), 0, CAST(N'2021-07-29T11:00:58.690' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (20, N'Bihar', 0, 0, 0, CAST(N'2021-07-29T10:57:02.697' AS DateTime), 0, CAST(N'2021-07-29T11:01:11.600' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (21, N'Xyz', 0, 0, 0, CAST(N'2021-07-31T09:36:05.963' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (22, N'Xkjjas', 21, 0, 0, CAST(N'2021-07-31T09:36:16.430' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (23, N'das', 0, 22, 0, CAST(N'2021-07-31T09:36:26.100' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (24, N'Station Road', 0, 4, 0, CAST(N'2021-07-31T16:29:56.643' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[StateRegionHq] ([Id], [Name], [ParentId], [GrandParentId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (25, N'Varacha', 0, 3, 0, CAST(N'2021-07-31T17:01:11.170' AS DateTime), 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StateRegionHq] OFF
GO
SET IDENTITY_INSERT [dbo].[TourPlan] ON 

INSERT [dbo].[TourPlan] ([Id], [Month], [SubmittedDate], [LastDate], [IsSubmitted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'August', N'31/06/2021', N'31/06/2021', 0, CAST(N'2021-07-30T12:45:59.583' AS DateTime), 0, CAST(N'2021-07-31T09:48:29.607' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[TourPlan] OFF
GO
SET IDENTITY_INSERT [dbo].[TourPlanMonth] ON 

INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (1, 1, N'07/03/2021', N'Saturday', 3, 1, N'8,7', CAST(N'2021-07-30T12:43:22.793' AS DateTime), 0)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (2, 1, N'07/04/2021', N'Sunday', 2, 2, N'7,6', CAST(N'2021-07-30T12:44:35.973' AS DateTime), 0)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (3, 1, N'07/05/2021', N'Monday', 2, 2, N'8,6', CAST(N'2021-07-31T08:43:19.160' AS DateTime), 0)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (4, 1, N'07/06/2021', N'Tuesday', 2, 2, N'7,6', CAST(N'2021-07-31T09:47:31.910' AS DateTime), 0)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (5, 1, N'07/07/2021', N'Wednesday', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (6, 1, N'07/08/2021', N'Thursday', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (7, 1, N'07/09/2021', N'Friday', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TourPlanMonth] ([Id], [TourPlanId], [Date], [Day], [WorkAtId], [WorkTypeId], [WorkRouteIds], [UpdatedDate], [UpdatedBy]) VALUES (8, 1, N'07/10/2021', N'Saturday', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TourPlanMonth] OFF
GO
SET IDENTITY_INSERT [dbo].[VisitType] ON 

INSERT [dbo].[VisitType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'Only Doctor', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[VisitType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'Only Chemist', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[VisitType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'Both', NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[VisitType] OFF
GO
SET IDENTITY_INSERT [dbo].[WorkExperience] ON 

INSERT [dbo].[WorkExperience] ([Id], [EmployeeId], [CompanyName], [Position], [JoiningDate], [LeavingDate], [Salary], [LeavingReason], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, 3, N'cvb', N'cvb', CAST(N'2021-07-17T00:00:00.000' AS DateTime), CAST(N'2021-07-17T00:00:00.000' AS DateTime), CAST(454.00 AS Decimal(18, 2)), NULL, CAST(N'2021-07-17T09:29:14.930' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[WorkExperience] ([Id], [EmployeeId], [CompanyName], [Position], [JoiningDate], [LeavingDate], [Salary], [LeavingReason], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, 5, N'Demo', N'Manager', CAST(N'2020-01-01T00:00:00.000' AS DateTime), CAST(N'2021-07-01T00:00:00.000' AS DateTime), CAST(15000.00 AS Decimal(18, 2)), N'Test
Demo', CAST(N'2021-07-19T10:47:33.303' AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[WorkExperience] OFF
GO
SET IDENTITY_INSERT [dbo].[WorkType] ON 

INSERT [dbo].[WorkType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'Field work', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[WorkType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'Office work', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[WorkType] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'Meeting', NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[WorkType] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ProductCategory]    Script Date: 01-08-2021 12:04:00 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ProductCategory] ON [dbo].[ProductCategory]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[AddressSelectById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[AddressSelectById] (
@Id int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

SELECT * FROM Address WHERE Id = @Id
GO
/****** Object:  StoredProcedure [dbo].[AddressUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddressUpsert] (
	@Id int,
	@Address1 nvarchar(max),
	@Address2 nvarchar(max),
	@Pincode varchar(10),	
	@Headquarter int,
	@Region int,
	@State int,
	@City int,	
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE [Address]
	SET
		[Address1] = @Address1,
		[Address2] = @Address2,
		[Pincode] = @Pincode,
		[Headquarter] = @Headquarter,
		[Region] = @Region,
		[State] = @State,
		[City] = @City,
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from [Address] where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].[Address]
           ([Address1]
           ,[Address2]
           ,[Pincode]
           ,[Headquarter]
           ,[Region]
           ,[State]
           ,[City]
           ,[CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Address1,
			@Address2,
			@Pincode,
			@Headquarter,
			@Region,
			@State,
			@City,
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from [Address] where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- EXEC ApplyMaster_All 0,0,''

CREATE PROCEDURE [dbo].[ApplyMaster_All]

@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from ApplyMaster
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )AND
									  Deletedby = 0
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[ApplyMaster]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)
			     AND
				 Deletedby = 0

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[ApplyMaster_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ApplyMaster Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[ApplyMaster]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ApplyMaster_Delete]

@Id BIGINT ,
	@DeletedBy BIGINT 


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update ApplyMaster set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'ApplyMaster Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[ApplyMaster]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ApplyMaster_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- TRUNCATE TABLE ApplyMaster
-- SELECT * FROM ApplyMaster
-- EXEC  ApplyMaster_Upsert 0, 'SANJAY'
CREATE PROCEDURE [dbo].[ApplyMaster_Upsert]
        @Id BIGINT = 0 ,
		@Name varchar (500) = 0  ,
	    @CreatedBy BIGINT = 0  ,
	    @UpdatedBy BIGINT  = 0 
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0

					 
					BEGIN
					
							INSERT INTO  ApplyMaster
												
											 (
												 Name,
												 CreatedDate,
												 createdBy,
												 DeletedBy
												 

												)

									VALUES (
											
											@Name,
											GETDATE(),
											@createdBy,
											0
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ApplyMaster Data Created Successfully'
              
				END	
				

			
		

				   	ELSE IF @Id > 0
					BEGIN
							UPDATE  ApplyMaster

							SET		
							        Name= Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ApplyMaster Data Updated Successfully'
					END

		
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[ApplyMaster]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CallIn_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CallIn_All]
@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from CallIn
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )AND
				                    Deletedby = 0
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[CallIn]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)AND
				                    Deletedby = 0
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[CallIn_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CallIn_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'CallIn Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[CallIn]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CallIn_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CallIn_Delete]

@Id BIGINT = 0,
	@DeletedBy BIGINT = 0


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update CallIn set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'CallIn Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[CallIn]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CallIn_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- exec CallIn_Upsert 0,'vikas'
CREATE PROCEDURE [dbo].[CallIn_Upsert]
@Id BIGINT = 0,
		@Name varchar (500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO CallIn
												
											 (
												 Name,
												 CreatedDate,
												 createdBy,
												 DeletedBy
												 

												)

									VALUES (
											
											@Name,
											GETDATE(),
											@createdBy,
											0
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'CallIn Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  CallIn

							SET		
							        Name= Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'CallIn Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[CallIn]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CategoryMaster_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--declare
--@Offset BIGINT = 0,
--@Limit BIGINT= 0,
--@Search VARCHAR(MAX) = null,
--@FieldNameId BIGINT = 0

--exec [dbo].[CategoryMaster_All] 
--@Offset,
--@Limit,
--@Search,
--@FieldNameId

CREATE PROCEDURE [dbo].[CategoryMaster_All]
@Offset BIGINT = 0,
@Limit BIGINT= 0,
@Search VARCHAR(MAX) = null,
@FieldNameId BIGINT = 0,
@MastersId BIGINT = 0
AS
BEGIN
	DECLARE @TotalRecords bigint = 0;

	SET @TotalRecords = (select count(*) from CategoryMaster CM 
						Where 
							(
								ISNULL(@Search,'') = ''
								OR
								CM.[Name] like LOWER(@Search)
							)
							AND
							(@FieldNameId = 0 OR CM.FieldNameId = @FieldNameId)
							AND
							(@MastersId = 0 OR CM.MastersId = @MastersId)
						);
	IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
		CM.Id,
		CM.FieldNameId,
		CM.MastersId,
		CM.[Name],
		CM.IsActive,
		CM.CreatedDate,
		CM.CreatedBy,
		CM.UpdatedDate,
		CM.UpdatedBy,
		FN.[Name] as FieldName,
		MS.Name as MastersName
	FROM CategoryMaster CM
		LEFT JOIN FieldName FN ON FN.Id = CM.FieldNameId
		LEFT JOIN Masters MS ON MS.Id = CM.MastersId
	WHERE
	(
		ISNULL(@Search,'') = ''
		OR
		CM.[Name] like LOWER(@Search)
	)
	AND
	(@FieldNameId = 0 OR CM.FieldNameId = @FieldNameId)
	AND
	(@MastersId = 0 OR CM.MastersId = @MastersId)
	order by 
		CM.Id 
	desc
		OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[CategoryMaster_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CategoryMaster_ById]
@Id BIGINT = 0
AS
BEGIN
	
	DECLARE @Code int = 200,
			@Message varchar(200) = 'Data Retrive Successfully...';

	SET NOCOUNT ON;

	SELECT @Code AS Code,
			@Message as [Message]

	SELECT 
		CM.Id,
		CM.FieldNameId,
		CM.MastersId,
		CM.[Name],
		CM.IsActive,
		CM.CreatedDate,
		CM.CreatedBy,
		CM.UpdatedDate,
		CM.UpdatedBy
	FROM CategoryMaster CM
	Where
		CM.Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[CategoryMaster_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CategoryMaster_Upsert]
@Id BIGINT = 0,
@FieldNameId BIGINT = 0,
@MastersId BIGINT = 0,
@Name varchar(50) = null,
@IsActive bit = null,
@CreatedBy BIGINT = 0,
@UpdatedBy BIGINT = 0
AS
BEGIN
	
	DECLARE @Code INT = 400,
			@Message varchar(200) = '';

	SET NOCOUNT ON;

	IF @FieldNameId = 0
	BEGIN
		SET @Code = 400;
		SET @Message = 'Field Name is Required';
	END
	ELSE IF @MastersId = 0
	BEGIN
		SET @Code = 400;
		SET @Message = 'Masters is Required';
	END
    ELSE IF @Name IS NULL OR @Name = ''
	BEGIN
		SET @Code = 400;
		SET @Message = 'Category is Required';
	END
	ELSE IF @IsActive IS NULL 
	BEGIN
		SET @Code = 400;
		SET @Message = 'Is Active is Required';
	END
	ELSE IF @Id = 0
	BEGIN
		
		INSERT INTO CategoryMaster
		(FieldNameId,MastersId,Name,IsActive,CreatedDate,CreatedBy)
		VALUES
		(@FieldNameId,@MastersId,@Name,@IsActive,getutcdate(),@CreatedBy)

		SET @Id = SCOPE_IDENTITY();
		SET @Code = 200;
		SET @Message = 'Category Saved Successfully...';
	END
	ELSE IF @Id > 0
	BEGIN
		
		UPDATE CategoryMaster
		SET
			FieldNameId = @FieldNameId,
			MastersId = @MastersId,
			Name = @Name,
			IsActive = @IsActive,
			UpdatedBy = @UpdatedBy
		WHERE Id = @Id

		SET @Code = 200;
		SET @Message = 'Category Updated Successfully...';
	END


	SELECT @Code AS Code,
			@Message as [Message]

	SELECT 
		CM.Id,
		CM.FieldNameId,
		CM.MastersId,
		CM.[Name],
		CM.IsActive,
		CM.CreatedDate,
		CM.CreatedBy,
		CM.UpdatedDate,
		CM.UpdatedBy
	FROM CategoryMaster CM
	Where
		CM.Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChampaignMaster_All]

@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
	@ApplyMasterId BIGINT = 0,
	@RegionId BIGINT = 0,
	@HeadquarterId BIGINT = 0
	

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords int;

		SET @TotalRecords = (
								select count(*) from [dbo].[ChampaignMaster] CM
								
							left join ApplyMaster AM
						   on CM.ApplyMasterId = AM.Id

						   left join Region R
						   on CM.RegionId = R.Id
			
								left join Headquarter H
						   on CM.HeadquarterId = H.Id
			
							
								
								where (@ApplyMasterId  = 0 or  CM.ApplyMasterId  = @ApplyMasterId ) And
									  (@HeadquarterId = 0 or  CM.HeadquarterId = @HeadquarterId)And
									  (@RegionId = 0 or  CM.RegionId = @RegionId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(AM.Name) like '%'+lower(@Search)+'%') OR
										(lower(H.Name) like '%'+lower(@Search)+'%') OR
										(lower(R.Name) like '%'+lower(@Search)+'%') 
									)
								)AND
				                CM.DeletedBy = 0
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
			CM.Id,
            CM.CampaignName, 
			CM.StartDate, 
			CM.EndDate,
			CM.ApplyMasterId,
			AM.Name as ApplyMasterName,
			CM.RegionId,
			R.Name As RegionName,
			CM.HeadquarterId,
			H.Name as HeadquarterName,
			CM.IsActive,
			CM.CreatedDate,
			CM.CreatedBy,
			CM.UpdatedDate,
			CM.UpdatedBy ,
			CM.DeletedDate,
			CM.DeletedBy
              

		from [dbo].[ChampaignMaster] CM
								
							left join ApplyMaster AM
						   on CM.ApplyMasterId = AM.Id

						     left join Region R
						   on CM.RegionId = R.Id
			
								left join Headquarter H
						   on CM.HeadquarterId = H.Id
			
							
								
								where (@ApplyMasterId  = 0 or  CM.ApplyMasterId  = @ApplyMasterId ) And
									  (@HeadquarterId = 0 or  CM.HeadquarterId = @HeadquarterId)and
									  (@RegionId = 0 or  CM.RegionId = @RegionId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(AM.Name) like '%'+lower(@Search)+'%') OR
										(lower(H.Name) like '%'+lower(@Search)+'%') OR
										(lower(R.Name) like '%'+lower(@Search)+'%') 
										
									)
								)AND
				                CM.DeletedBy = 0
								
							

			
			
			order by CM.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- exec ChampaignMaster_ById 1
CREATE PROCEDURE  [dbo].[ChampaignMaster_ById]

@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ChampaignMaster Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]
select
			
			CM.Id,
            CM.CampaignName, 
			CM.StartDate, 
			CM.EndDate,
			CM.ApplyMasterId,
			AM.Name as ApplyMasterName,
			CM.RegionId,
			R.Name As RegionName,
			CM.HeadquarterId,
			H.Name as HeadquarterName,
			CM.IsActive,
			CM.CreatedDate,
			CM.CreatedBy,
			CM.UpdatedDate,
			CM.UpdatedBy ,
			CM.DeletedDate,
			CM.DeletedBy
              

		from [dbo].[ChampaignMaster] CM
								
							left join ApplyMaster AM
						   on CM.ApplyMasterId = AM.Id

						     left join Region R
						   on CM.RegionId = R.Id
			
								left join Headquarter H
						   on CM.HeadquarterId = H.Id
			
			WHERE
				CM.Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[ChampaignMaster_Delete]

@Id BIGINT = 0,
	@DeletedBy BIGINT = 0


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update ChampaignMaster set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'ChampaignMaster Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
		      Id,
              CampaignName, 
			StartDate, 
			EndDate,
			ApplyMasterId,
			RegionId, 
			HeadquarterId,
			IsActive,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy ,
			DeletedDate,
			DeletedBy
              
			 



FROM
				[dbo].[ChampaignMaster]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ChampaignMaster_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec ChampaignMaster_Upsert 0,'sanjay','01-08-2021','01-08-2021',8,8,8
CREATE PROCEDURE [dbo].[ChampaignMaster_Upsert]


	@Id BIGINT =0,
	@CampaignName varchar(500) =null,
	@StartDate varchar(500) =null,
	@EndDate varchar(500) =null,
	@ApplyMasterId BIGINT =0,
	@RegionId BIGINT =0,
	@HeadquarterId BIGINT =0,
	@CreatedBy  BIGINT =0,
	@UpdatedBy BIGINT =0
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO  ChampaignMaster
												
											 (
												
												 CampaignName, 
												 StartDate, 
												 EndDate,
												 ApplyMasterId,
												 RegionId, 
												 HeadquarterId,
												 IsActive,
												 CreatedDate,
												 CreatedBy,
												 DeletedBy
												)

									VALUES (
									             @CampaignName, 
												 @StartDate, 
												 @EndDate,
												 @ApplyMasterId,
												 @RegionId, 
												 @HeadquarterId,
												 0,
												 GETDATE(),
												 0,
												 0
											
										
											
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ChampaignMaster Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  ChampaignMaster

							SET		
							        CampaignName= Isnull (@CampaignName,CampaignName),
									StartDate= Isnull (@StartDate,StartDate),
									EndDate= Isnull (@EndDate,EndDate),
									ApplyMasterId = @ApplyMasterId,
									RegionId = @RegionId,
									HeadquarterId = @HeadquarterId,
                                    UpdatedDate = GETDATE(),
									UpdatedBy = UpdatedBy
									

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ChampaignMaster Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		      Id,
              CampaignName, 
			StartDate, 
			EndDate,
			ApplyMasterId,
			RegionId, 
			HeadquarterId,
			IsActive,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy ,
			DeletedDate,
			DeletedBy
              
			 



FROM
				[dbo].[ChampaignMaster]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ChemistSelect]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[ChemistSelect] (
@Id int
)
AS

SET NOCOUNT ON

DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]
 
SET NOCOUNT ON

SELECT
	E.*,
	H.Name as 'HeadquarterName',
	Q.Name as 'RegionName',
	(A.Address1 + ' ' + ISNULL(A.Address2, '') + '</br>' + (select name from Headquarter where Id = A.Headquarter) + ' ' + (select name from Region where Id = A.Region) + ' ' + A.Pincode) as 'AddressString'
FROM Chemist E
	Left join Region Q on Q.Id = E.Region
	Left join Headquarter H on H.Id = E.Headquarter
	Left join Address A on A.Id = E.Address
WHERE E.Id = @Id
GO
/****** Object:  StoredProcedure [dbo].[ChemistSelectAll]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [ChemistSelectAll] '',0,0,2
CREATE PROCEDURE [dbo].[ChemistSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT,
	@TourPlanMonthId INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM Chemist E
							Left join Region Q on Q.Id = E.Region
							Left join Headquarter H on H.Id = E.Headquarter
							Left join Address A on A.Id = E.Address
							 WHERE ISNULL(@Search,'') = '' 
							 	  OR E.Mobile like '%'+@Search+'%' 
							 	  OR E.Name like '%'+@Search+'%' 
					)


					IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END



  SELECT
	E.*,
	H.Name as 'HeadquarterName',
	Q.Name as 'RegionName',
	(A.Address1 + ' ' + ISNULL(A.Address2, '') + '</br>' + (select name from Headquarter where Id = A.Headquarter) + ' ' + (select name from Region where Id = A.Region) + ' ' + A.Pincode) as 'AddressString',
	(select count(*) from DoctorAndChemist where ChemistId = E.Id and TourPlanMonthId = @TourPlanMonthId and IsDelete = 0) AS IsAdded
FROM Chemist E
	Left join Region Q on Q.Id = E.Region
	Left join Headquarter H on H.Id = E.Headquarter
	Left join Address A on A.Id = E.Address
	 WHERE ISNULL(@Search,'') = '' 
	 	  OR E.Mobile like '%'+@Search+'%' 
	 	  OR E.Name like '%'+@Search+'%' 
	 ORDER BY E.Id Desc
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[ChemistUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- EXEC [UserInsert] 'aaa','bbb','a@in.com','1234567890','123',true,admin 

CREATE PROCEDURE [dbo].[ChemistUpsert] (
	@Id int,
	@Name nvarchar(100),	
	@IsActive bit,	
	@Mobile varchar(15),
	@Phone varchar(15),		
	@Headquarter int,
	@Region int,
	@Type int,
	@ContactPerson nvarchar(max),
	@WebSite nvarchar(max),
	@Address int,	
	@BuisnessPotential nvarchar(100),
	@Remarks nvarchar(max),
	@CreatedBy int,
	@UpdatedBy int)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

-- Update
IF(@Id > 0)
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
		
			UPDATE [dbo].Chemist
				SET [Name] = @Name
				   ,[IsActive] = @IsActive
				   ,[Mobile] = @Mobile
				   ,[Phone] = @Phone				   				   
				   ,[Headquarter] = @Headquarter
				   ,[Region] = @Region
				   ,[ContactPerson] = @ContactPerson
				   ,[WebSite] = @WebSite
				   ,[Address] = @Address
				   ,[BuisnessPotential] = @BuisnessPotential
				   ,[Remarks] = @Remarks
				   ,[UpdatedAt] = GETDATE()
				   ,[UpdatedBy] = @UpdatedBy
				   ,[Type] = @Type
				WHERE Id = @Id

			SELECT
				E.*,
				H.Name as 'HeadquarterName',
				Q.Name as 'RegionName'
			FROM Chemist E
				Left join Region Q on Q.Id = E.Region
				Left join Headquarter H on H.Id = E.Headquarter
			WHERE E.Id = @Id
		END

-- Insert
ELSE		
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
	
			INSERT INTO [dbo].[Chemist]
						([Name] ,[IsActive] , [Mobile] ,[Phone]  
						,[Headquarter] ,[Region], [ContactPerson], [WebSite], [Type]
						,[Address] ,[BuisnessPotential] ,[Remarks] ,[CreatedAt] ,[CreatedBy])
			VALUES
						(@Name, @IsActive, @Mobile, @Phone, 
						 @Headquarter, @Region, @ContactPerson, @WebSite, @Type,
						 @Address, @BuisnessPotential, @Remarks, Getdate(), @CreatedBy)
	
			SELECT
				E.*,
				H.Name as 'HeadquarterName',
				Q.Name as 'RegionName'
			FROM Chemist E
				Left join Region Q on Q.Id = E.Region
				Left join Headquarter H on H.Id = E.Headquarter
			WHERE E.Id = SCOPE_IDENTITY()
		END
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- EXEC ChemistVsDoctor_All '',0,0,0,0

CREATE PROCEDURE [dbo].[ChemistVsDoctor_All]

@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
	@DoctorId bigint = 0,
	@ChemistId bigint = 0
	

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords int;

		SET @TotalRecords = (
								select count(*) from [dbo].[ChemistVsDoctor] CD
								
							left join Doctor D
						   on CD.DoctorId = D.Id
			
								left join Chemist C
						   on CD.ChemistId = C.Id
			
							
								
								where (@DoctorId  = 0 or  CD.DoctorId  = @DoctorId ) And
									  (@ChemistId = 0 or  CD.ChemistId = @ChemistId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(C.Name) like '%'+lower(@Search)+'%') OR
										(lower(D.FirstName) like '%'+lower(@Search)+'%') OR
										(lower(D.LastName) like '%'+lower(@Search)+'%') OR
										(lower(C.Mobile) like '%'+lower(@Search)+'%') OR
										(lower(D.Mobile) like '%'+lower(@Search)+'%') OR
										(lower(D.Qualification) like '%'+lower(@Search)+'%') OR
										(lower(C.Headquarter) like '%'+lower(@Search)+'%')
									)
								)AND
				                CD.DeletedBy = 0
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				CD.Id,
		        CD.DoctorId,
				D.FirstName AS DoctorFirstName,
				D.LastName  AS DoctorLastName,
				D.Mobile  AS DoctorMobileNo,
				D.Qualification AS DoctorQualification,
		        CD.ChemistId,
				C.Name AS  ChemistName,
				C.Mobile AS  ChemistMobileNo,
				C.Headquarter AS  ChemistHeadquarter,
		        CD.CreatedDate,
		        CD.CreatedBy,
		        CD.UpdatedDate,
		        CD.UpdatedBy,
		        CD.DeletedDate,
		        CD.DeletedBy

		 from [dbo].[ChemistVsDoctor] CD
								
							left join Doctor D
						   on CD.DoctorId = D.Id
			
								left join Chemist C
						   on CD.ChemistId = C.Id
			
							
								
								where (@DoctorId  = 0 or  CD.DoctorId  = @DoctorId ) And
									  (@ChemistId = 0 or  CD.ChemistId = @ChemistId)
								and
			(
				isnull(@Search,'') = '' or
				(
					(lower(C.Name) like '%'+lower(@Search)+'%') OR
										(lower(D.FirstName) like '%'+lower(@Search)+'%') OR
										(lower(D.LastName) like '%'+lower(@Search)+'%') OR
										(lower(C.Mobile) like '%'+lower(@Search)+'%') OR
										(lower(D.Mobile) like '%'+lower(@Search)+'%') OR
										(lower(D.Qualification) like '%'+lower(@Search)+'%') OR
										(lower(C.Headquarter) like '%'+lower(@Search)+'%')
				)AND
				CD.DeletedBy = 0

			)
			
			order by CD.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_ByChemistId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- EXEC ChemistVsDoctor_ByDoctorId 0,0,1

CREATE PROCEDURE [dbo].[ChemistVsDoctor_ByChemistId]
   @Offset bigint = 0,
	@Limit bigint = 0,
	@ChemistId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[ChemistVsDoctor] CD
									
									left join Chemist C on CD.ChemistId = C.Id
									left join Doctor D on CD.DoctorId = D.Id
									Left join Specialization H on H.Id = D.Specialization			
									Left join Address A on A.Id = D.Address
									Left join Address CA on A.Id = C.Address
									Left join Qualification Q on Q.Id = D.Qualification


									where (@ChemistId = 0 or  CD.ChemistId = @ChemistId)
									ANd
									CD.DeletedBy = 0
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				CD.Id,
		        CD.DoctorId,
		        CD.ChemistId,
				C.Name AS  ChemistName,
				C.Mobile AS  ChemistMobileNo,
				(CA.Address1 + '</br>' + CA.Address2 + ' ' + CA.Pincode) as 'ChemistAddressString',
				C.BuisnessPotential as ChemistBuisnessPotential,
				C.IsActive as ChemistStatus,
				C.ContactPerson as ChemistContactPerson,
				C.Headquarter AS  ChemistHeadquarter,
		        CD.CreatedDate,
		        CD.CreatedBy,
		        CD.UpdatedDate,
		        CD.UpdatedBy,
		        CD.DeletedDate,
		        CD.DeletedBy,
				D.FirstName as DoctorFirstName,
				D.LastName as DoctorLastName,
				D.Mobile as DoctorMobileNo,
				D.Photo as DoctorImage,
				H.Name as 'DoctorSpecializationName',
				(select name from Headquarter where Id = A.Headquarter) as 'DoctorHQName',
				(select name from Region where Id = A.Region) as 'DoctorRegionName',	
				(A.Address1 + '</br>' + A.Address2 + ' ' + A.Pincode) as 'DoctorAddressString',
				Q.Name as 'DoctorQualificationName',
				D.Gender as DoctorGender,
				D.IsActive as DoctorStatus

			from [dbo].[ChemistVsDoctor] CD
			left join Chemist C on CD.ChemistId = C.Id
			left join Doctor D on CD.DoctorId = D.Id
			Left join Specialization H on H.Id = D.Specialization			
			Left join Address A on A.Id = D.Address
			Left join Address CA on CA.Id = C.Address
			Left join Qualification Q on Q.Id = D.Qualification

			where (@ChemistId = 0 or  CD.ChemistId = @ChemistId)
				ANd
			CD.DeletedBy = 0
							   

			order by CD.DoctorId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_ByDoctorId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChemistVsDoctor_ByDoctorId]
   @Offset bigint = 0,
	@Limit bigint = 0,
	@DoctorId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[ChemistVsDoctor] CD
									left join Doctor D on CD.DoctorId = D.Id
									left join Chemist C on CD.ChemistId = C.Id
									Left join Address CA on CA.Id = C.Address


									where (@DoctorId = 0 or  CD.DoctorId = @DoctorId)
									ANd
									CD.DeletedBy = 0
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				CD.Id,
		        CD.DoctorId,
				C.Name AS  ChemistName,
				C.Mobile AS  ChemistMobileNo,
				(CA.Address1 + '</br>' + CA.Address2 + ' ' + CA.Pincode) as 'ChemistAddressString',
				C.BuisnessPotential as ChemistBuisnessPotential,
				C.IsActive as ChemistStatus,
				C.ContactPerson as ChemistContactPerson,
				C.Headquarter AS  ChemistHeadquarter,
				D.FirstName AS DoctorFirstName,
				D.LastName  AS DoctorLastName,
				D.Mobile  AS DoctorMobileNo,
				D.Qualification AS DoctorQualification,
		        CD.ChemistId,
		        CD.CreatedDate,
		        CD.CreatedBy,
		        CD.UpdatedDate,
		        CD.UpdatedBy,
		        CD.DeletedDate,
		        CD.DeletedBy

			from [dbo].[ChemistVsDoctor] CD

			left join Doctor D on CD.DoctorId = D.Id
			left join Chemist C on CD.ChemistId = C.Id
			Left join Address CA on CA.Id = C.Address

			where (@DoctorId = 0 or  CD.DoctorId = @DoctorId)
				ANd
			CD.DeletedBy = 0
							   

			order by CD.DoctorId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ChemistVsDoctor
--exec ChemistVsDoctor_Delete 1,1 

CREATE procedure [dbo].[ChemistVsDoctor_Delete]
	@Id bigint,
	@DeletedBy bigint
as
begin
	set nocount on;
	declare @Code int = 400,
	@Message varchar(50) = ''

	update ChemistVsDoctor
	set 
		DeletedDate = GETUTCDATE(),
		DeletedBy = @DeletedBy
	where Id = @Id
	
	set @Code = 200
	set @Message = 'ChemistVsDoctor Deleted Successfully'


	select @Code as Code,
	@Message as Message

	select 
		Id,
		DoctorId,
		ChemistId,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy

	from 
		ChemistVsDoctor
	where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[ChemistVsDoctor_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ChemistVsDoctor

--exec ChemistVsDoctor_Upsert 0,1,1,0,0

Create PROCEDURE [dbo].[ChemistVsDoctor_Upsert]

		@Id BIGINT = 0,
		@DoctorId BIGINT = 0,
		@ChemistId BIGINT = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO ChemistVsDoctor
												
											 (
												DoctorId,
												ChemistId,
												CreatedDate,
												CreatedBy,
												DeletedBy

												)

									VALUES (
											
												@DoctorId,
												@ChemistId,
												GETDATE(),
												@createdBy,
												0

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ChemistVsDoctor Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  ChemistVsDoctor

							SET		
							        DoctorId = @DoctorId,
									ChemistId = @ChemistId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ChemistVsDoctor Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DoctorId,
		ChemistId,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy
				
		FROM
				ChemistVsDoctor
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[City_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[City_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].City WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CitySelectAll]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CitySelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM City WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
   
SELECT * FROM City WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%'
	ORDER BY Id Desc
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[CitySelectByStateId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[CitySelectByStateId] (
@StateId int
)
AS

SET NOCOUNT ON
DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM City WHERE StateId = @StateId)

SELECT * FROM City WHERE StateId = @StateId

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[CityUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CityUpsert] (
	@Id int,
	@Name nvarchar(max),	
	@StateId int,	
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE City
	SET
		Name = @Name,
		StateId = @StateId,
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from City where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].City
           (Name,
            StateId
           ,[CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,
			@StateId,
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from City where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[DayTypeMaster_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec DayTypeMaster_All '',0,0 
CREATE proc [dbo].[DayTypeMaster_All]
@Search VARCHAR(MAX),
@Offset BIGINT,
@Limit BIGINT
    
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from DayTypeMaster
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				Id,
				Name
				
		FROM
				[dbo].[DayTypeMaster]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[DoctorAndChemist_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- EXEC DoctorAndChemist_All 0,10,'',1
-- select * from DoctorAndChemist
CREATE PROCEDURE [dbo].[DoctorAndChemist_All]
	@Offset INT,
	@Limit INT,
	@Search VARCHAR(MAX),
	@TourPlanMonthId INT = 0
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords INT;

		SET @TotalRecords = (
			select COUNT(*) from DoctorAndChemist DAC
			where 
			DAC.IsDelete = 0
			and
			(@TourPlanMonthId = 0 OR DAC.TourPlanMonthId = @TourPlanMonthId)
		)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

			SELECT 
				DAC.Id,
				--(select top(1) EmployeeIds from JointWorking where DoctorAndChemistId = DAC.Id)as DAC_EmployeeId,
				--(select FirstName from Employee where Id = (select top(1) EmployeeIds from JointWorking where DoctorAndChemistId = DAC.Id)) as DAC_EmployeeFirstName,
				--(select LastName from Employee where Id = (select top(1) EmployeeIds from JointWorking where DoctorAndChemistId = DAC.Id)) as DAC_EmployeeLastName,
				--(select Photo from Employee where Id = (select top(1) EmployeeIds from JointWorking where DoctorAndChemistId = DAC.Id)) as DAC_EmployeePhoto,
				DAC.TourPlanMonthId,
				DAC.DoctorId,
				(select FirstName from Doctor where Id = DAC.DoctorId) as DoctorFirstName,
				(select LastName from Doctor where Id = DAC.DoctorId) as DoctorLastName,
				(select Address from Doctor where Id = DAC.DoctorId) as DoctorAddressId,
				(select DA.Address1 + '</br>' + ISNULL(DA.Address2, '') + ' ' + DA.Pincode from Address DA 
				where 
				Id = (select Address from Doctor where Id = DAC.DoctorId) ) as DoctorAddress,
				DAC.ChemistId,
				(select Name from Chemist where Id = DAC.ChemistId) as ChemistIdName,
				(select Address from Chemist where Id = DAC.ChemistId) as ChemistAddressId,
				(select CA.Address1 + '</br>' + ISNULL(CA.Address2, '') + ' ' + CA.Pincode from Address CA 
				where 
				Id = (select Address from Chemist where Id = DAC.ChemistId) ) as ChemistAddress,
				DAC.IsDelete,
				DAC.CreatedDate,
				DAC.CreatedBy,
				DAC.UpdatedDate,
				DAC.UpdatedBy,
				DAC.DeletedDate,
				DAC.DeletedBy
				
			FROM
				[dbo].[DoctorAndChemist] DAC
			where 
				DAC.IsDelete = 0
				and
				(@TourPlanMonthId = 0 OR DAC.TourPlanMonthId = @TourPlanMonthId)

		order by   DAC.Id

		--desc 

		

		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END

GO
/****** Object:  StoredProcedure [dbo].[DoctorAndChemist_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- select * from DoctorAndChemist

-- exec [DoctorAndChemist_Delete] 3,3

CREATE PROCEDURE [dbo].[DoctorAndChemist_Delete]

@Id BIGINT = 0,
@DeletedBy BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update DoctorAndChemist set DeletedDate = GETUTCDATE() , IsDelete = 1, DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'Doctor And Chemist Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			Id,
			TourPlanMonthId,
			DoctorId,
			ChemistId,
			IsDelete,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy,
			DeletedDate,
			DeletedBy
				
		FROM
				[dbo].[DoctorAndChemist]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DoctorAndChemist_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kartik amipara
-- Create date: 07/14/2021 05:57 PM
-- Description:	This sp use DoctorAndChemist multipal data insert
-- =============================================
-- select * from DoctorAndChemist
-- truncate table DoctorAndChemist
-- exec [DoctorAndChemist_Upsert] 0,2,'','1',0,0
CREATE PROCEDURE [dbo].[DoctorAndChemist_Upsert]
        @Id INT = 0,
		@TourPlanMonthId INT = 0,
		@DoctorIds varchar(MAX) = null,
		@ChemistIds varchar(MAX) = null,
		@CreatedBy INT = 0,
		@UpdatedBy INT = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''

	
	IF @Id = 0
		BEGIN

			  IF @DoctorIds != '0'
			  BEGIN
				DELETE FROM DoctorAndChemist where ChemistId = 0 and TourPlanMonthId=@TourPlanMonthId
			  END

			  IF @ChemistIds != '0'
			  BEGIN
				DELETE FROM DoctorAndChemist where DoctorId = 0 and TourPlanMonthId=@TourPlanMonthId
			  END

			  IF @TourPlanMonthId = 0
				  BEGIN
		  			   SET @Id = 0
		  			   SET @Code = 400
		  			   SET @Message = 'TourPlan Month is required'
				  END
			  ELSE
			  BEGIN

			  DECLARE @Total INT = 0, @i int = 0
					

					IF @ChemistIds != '0' OR (select count(value) from string_split(@ChemistIds,',')) > (select count(value) from string_split(@DoctorIds,','))
					BEGIN
						SET @Total = (select count(value) from string_split(@ChemistIds,','));
					END
					ELSE IF @DoctorIds != '0' OR (select count(value) from string_split(@DoctorIds,',')) > (select count(value) from string_split(@ChemistIds,','))
					BEGIN
						SET @Total = (select count(value) from string_split(@DoctorIds,','));
					END

					PRINT @Total

					DECLARE @Row int = 0
					PRINT @Total
					WHILE @i < @Total
					BEGIN
						DECLARE @DoctorVar INT,@ChemistVar INT

						select @DoctorVar = CONVERT(INT,value) from  string_split(@DoctorIds,',') 
						ORDER BY (SELECT NULL) OFFSET @i ROWS FETCH NEXT 1 ROWS ONLY

						select @ChemistVar = CONVERT(INT,value) from  string_split(@ChemistIds,',') 
						ORDER BY (SELECT NULL) OFFSET @i ROWS FETCH NEXT 1 ROWS ONLY
						
						INSERT INTO DoctorAndChemist
						(
							TourPlanMonthId,
							DoctorId,
							ChemistId,
							IsDelete,
							CreatedDate
						)	
						VALUES
						(
							@TourPlanMonthId,
							@DoctorVar,
							@ChemistVar,
							0,
							GETUTCDATE()
						)
						SET @i = @i+1;
					END		
			   SET @Id = SCOPE_IDENTITY()
			   SET @Code = 200
			   SET @Message = 'Doctor And Chemist created successfully'
		  END 
       
	END

	SELECT @Code as Code,@Message as [Message]
		Select 
			Id,
			TourPlanMonthId,
			DoctorId,
			ChemistId,
			IsDelete,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy,
			DeletedDate,
			DeletedBy

	    FROM 
	        [dbo].[DoctorAndChemist]
		WHERE
			Id = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[DoctorCategory_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DoctorCategory_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].[DoctorCategory] WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DoctorCategorySelectAll]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DoctorCategorySelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM DoctorCategory WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
   
SELECT * FROM DoctorCategory WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%'
	ORDER BY Id Desc
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[DoctorCategoryUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DoctorCategoryUpsert] (
	@Id int,
	@Name nvarchar(max),		
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE DoctorCategory
	SET
		Name = @Name,		
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from DoctorCategory where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].DoctorCategory
           (Name,
           [CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,			
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from DoctorCategory where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[DoctorProducts_ByDoctorId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DoctorProducts_ByDoctorId]

@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'DoctorProducts_ByDoctorId Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

SELECT 
			  ID,	
              DoctorId, 
	          ProductId,
			  ExpectedSaleQty, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy
             



FROM
				[dbo].[DoctorProducts]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DoctorProducts_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DoctorProducts_Delete]

@Id BIGINT = 0,
	@DeletedBy BIGINT = 0


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update DoctorProducts set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'DoctorProducts Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			  ID,	
              DoctorId, 
	          ProductId,
			  ExpectedSaleQty, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy
             



FROM
				[dbo].[DoctorProducts]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DoctorProducts_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DoctorProducts_Upsert]
@Id BIGINT = 0,
		
		@DoctorId BIGINT = 0,
	    @ProductId BIGINT = 0,
		@ExpectedSaleQty decimal(18,2) = 0,
		@CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO  DoctorProducts
												
											 (
												
												 DoctorId, 
												 ProductId,
												 ExpectedSaleQty, 
												 CreatedDate,
												 createdBy,
												 DeletedBy
												 
												 

												)

									VALUES (
											
											@DoctorId,
											@ProductId,
											@ExpectedSaleQty,  
											GETDATE(),
											@createdBy,
											0
											
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DoctorProducts Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  DoctorProducts

							SET		
							        
									DoctorId = @DoctorId,
									ProductId = @ProductId,
									ExpectedSaleQty = @ExpectedSaleQty,
                                    UpdatedDate = GETDATE(),
									UpdatedBy = UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DoctorProducts Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
			  ID,	
              DoctorId, 
	          ProductId,
			  ExpectedSaleQty, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy
             



FROM
				[dbo].[DoctorProducts]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DoctorSelect]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[DoctorSelect] (
@Id int
)
AS

SET NOCOUNT ON

DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]
 
SET NOCOUNT ON

SELECT
	E.*,
	Q.Name as 'QualificationName',
	H.Name as 'SpecializationName',
	Rg.Name as 'CategoryName',
	(select name from Headquarter where Id = A.Headquarter) as 'HeadquarterName',
	(select name from Region where Id = A.Region) as 'RegionName',
	(A.Address1 + ' ' + ISNULL(A.Address2, '') + ' ' + A.Pincode) as 'AddressString'
FROM Doctor E
	Left join Qualification Q on Q.Id = E.Qualification
	Left join Specialization H on H.Id = E.Specialization
	Left join DoctorCategory Rg on Rg.Id = E.Category
	Left join Address A on A.Id = E.Address
WHERE E.Id = @Id
GO
/****** Object:  StoredProcedure [dbo].[DoctorSelectAll]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [DoctorSelectAll] '',0,0,1
CREATE PROCEDURE [dbo].[DoctorSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT,
	@TourPlanMonthId INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM Doctor E
						Left join Qualification Q on Q.Id = E.Qualification
						Left join Specialization H on H.Id = E.Specialization
						Left join DoctorCategory Rg on Rg.Id = E.Category
						Left join Address A on A.Id = E.Address
						WHERE ISNULL(@Search,'') = '' 
							  OR Mobile like '%'+@Search+'%' 
							  OR FirstName like '%'+@Search+'%' 
							  OR LastName like '%'+@Search+'%'
					)


					IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END



   select 
		E.*, 
		Q.Name as 'QualificationName',
		H.Name as 'SpecializationName',
		Rg.Name as 'CategoryName',
		(select name from Headquarter where Id = A.Headquarter) as 'HeadquarterName',
		(select name from Region where Id = A.Region) as 'RegionName',	
		--(A.Address1 + ' ' + A.Address2 + '</br>' + (select name from Headquarter where Id = A.Headquarter) + ' ' + (select name from Region where Id = A.Region) + ' ' + A.Pincode) as 'AddressString'
		(A.Address1 + '</br>' + ISNULL(A.Address2, '') + ' ' + A.Pincode) as 'AddressString',
		(select count(*) from DoctorAndChemist where DoctorId = E.Id and TourPlanMonthId = @TourPlanMonthId and IsDelete = 0) AS IsAdded

   FROM Doctor E
	Left join Qualification Q on Q.Id = E.Qualification
	Left join Specialization H on H.Id = E.Specialization
	Left join DoctorCategory Rg on Rg.Id = E.Category
	Left join Address A on A.Id = E.Address
	 WHERE ISNULL(@Search,'') = '' 
	 	  OR Mobile like '%'+@Search+'%' 
	 	  OR FirstName like '%'+@Search+'%' 
	 	  OR LastName like '%'+@Search+'%'
	 ORDER BY E.Id Desc
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[DoctorUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- EXEC [UserInsert] 'aaa','bbb','a@in.com','1234567890','123',true,admin 

CREATE PROCEDURE [dbo].[DoctorUpsert] (
	@Id int,
	@FirstName nvarchar(100),
	@LastName nvarchar(100),
	@IsActive bit,
	@Gender bit,
	@Mobile varchar(15),
	@Phone varchar(15),	
	@Birthdate datetime,
	@Anniversary datetime,
	@Photo nvarchar(max),
	@Specialization int,
	@Qualification int,
	@Category int,
	@WebSite nvarchar(max),
	@Address int,	
	@BuisnessPotential nvarchar(100),
	@Remarks nvarchar(max),
	@CreatedBy int,
	@UpdatedBy int)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

-- Update
IF(@Id > 0)
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
		
			UPDATE [dbo].Doctor
				SET [FirstName] = @FirstName
				   ,[LastName] = @LastName
				   ,[IsActive] = @IsActive
				   ,[Gender] = @Gender
				   ,[Mobile] = @Mobile
				   ,[Phone] = @Phone				   
				   ,[Birthdate] = @Birthdate
				   ,[Anniversary] = @Anniversary
				   ,[Photo] = @Photo
				   ,[Specialization] = @Specialization
				   ,[Qualification] = @Qualification
				   ,[Category] = @Category
				   ,[WebSite] = @WebSite
				   ,[Address] = @Address
				   ,[BuisnessPotential] = @BuisnessPotential
				   ,[Remarks] = @Remarks
				   ,[UpdatedAt] = GETDATE()
				   ,[UpdatedBy] = @UpdatedBy
				WHERE Id = @Id

			SELECT
				E.*,
				Q.Name as 'QualificationName',
				H.Name as 'SpecializationName',
				Rg.Name as 'CategoryName'
			FROM Doctor E
				Left join Qualification Q on Q.Id = E.Qualification
				Left join Specialization H on H.Id = E.Specialization
				Left join DoctorCategory Rg on Rg.Id = E.Category
			WHERE E.Id = @Id
		END

-- Insert
ELSE		
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
	
			INSERT INTO [dbo].[Doctor]
						([FirstName] ,[LastName] ,[IsActive] ,[Gender] ,[Mobile] ,[Phone] ,[Birthdate], [Anniversary] ,[Photo] 
						,[Specialization] ,[Qualification], [Category], [WebSite]
						,[Address] ,[BuisnessPotential] ,[Remarks] ,[CreatedAt] ,[CreatedBy])
			VALUES
						(@FirstName, @LastName, @IsActive, @Gender, @Mobile, @Phone, @Birthdate, @Anniversary, @Photo, 
						 @Specialization, @Qualification, @Category, @WebSite,
						 @Address, @BuisnessPotential, @Remarks, Getdate(), @CreatedBy)
	
			SELECT
				E.*,
				Q.Name as 'QualificationName',
				H.Name as 'SpecializationName',
				Rg.Name as 'CategoryName'
			FROM Doctor E
				Left join Qualification Q on Q.Id = E.Qualification
				Left join Specialization H on H.Id = E.Specialization
				Left join DoctorCategory Rg on Rg.Id = E.Category
			WHERE E.Id = SCOPE_IDENTITY()
		END
GO
/****** Object:  StoredProcedure [dbo].[DSR_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [DSR_All] 0,0,'',0,0,0,0,0,0,0,0,0,0,''
CREATE PROCEDURE [dbo].[DSR_All]
@Offset BIGINT = 0,
@Limit BIGINT = 0,
@Search VARCHAR(MAX),
@RouteTypeId BIGINT =0,
@WorkTypeId BIGINT = 0,
@RouteId BIGINT = 0,
@VisitTypeId BIGINT = 0,
@DoctorId BIGINT = 0,
@ChemistId BIGINT = 0,
@EmployeeId INT =0,
@StatusId BIGINT = 0,
@HeadquaterId int = 0,
@RegionId int = 0,
@Date varchar(500) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;
	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    from 
			[dbo].[DSR] d

			LEFT JOIN [RouteType] RT
			on D.RouteTypeId = RT.Id

			LEFT JOIN [WorkType] WT
			on D.WorkTypeId = WT.Id

			LEFT JOIN [Route] R
			on D.RouteId = R.Id

			LEFT JOIN [VisitType] VT
			on D.VisitTypeId = VT.Id

			LEFT JOIN [Doctor] DI
			on D.DoctorId = DI.Id

			LEFT JOIN [Chemist] C
			on D.ChemistId = C.Id

			LEFT JOIN DSRStatus DS
			ON DS.Id = d.StatusId
		
			LEFT JOIN Employee E
			ON D.EmployeeId = E.Id

		Where 
		    (@RouteTypeId = 0 OR D.RouteTypeId = @RouteTypeId)
			AND
			(@WorkTypeId = 0 OR D.WorkTypeId = @WorkTypeId)
			AND
			(@RouteId  = 0 OR D.RouteId = @RouteId)
			AND
			(@VisitTypeId = 0 OR D.VisitTypeId = @VisitTypeId)
			AND
			(@DoctorId  = 0 OR D.DoctorId = @DoctorId)
			AND
			(@ChemistId = 0 OR D.ChemistId = @ChemistId)
            AND
			(@EmployeeId = 0 OR D.EmployeeId = @EmployeeId)
			AND
			(@HeadquaterId = 0 OR E.Headquarter = @HeadquaterId)
			AND
			(@RegionId = 0 OR E.Region = @RegionId)
			AND
			((@Date iS null or @Date = '') or convert(date,D.CreatedDate,103) = convert(date,@Date,103))
			AND
			(@StatusId = 0 OR D.StatusId = @StatusId)
			AND
		
			(
					isnull(@Search,'') = ''
					or
					(
						lower(WT.Name) like lower('%'+@Search+'%')
						or
						lower(RT.Name) like lower('%'+@Search+'%')
						or
						lower(VT.Name) like lower('%'+@Search+'%')
						or
						lower(R.Name) like lower('%'+@Search+'%')
						or
						lower(DI.FirstName) like lower('%'+@Search+'%')
						or
						lower(C.Name) like lower('%'+@Search+'%')

					)
				)
				AND
				D.DeletedBy = 0	
		)


		IF @Limit = 0
			BEGIN
			     SET @Limit = @TotalRecords
				 IF @Limit = 0
				 BEGIN
				     SET @Limit = 10
				 END
			END
	 
		SELECT 
		      D.Id,
		      D.Date, 
			  D.RouteTypeId,
			  RT.Name as RouteTypeName,
			  D.WorkTypeId,
			  WT.Name AS WorkTypeName,
			  D.RouteId,
			  R.Name as RouteName,
			  D.VisitTypeId,
			  VT.Name AS VisitTypeName,
			  D.DoctorId,
			  DI.FirstName AS DoctorFirstName,
			  D.ChemistId, 
			  C.Name AS ChemistName,
			  D.EmployeeId,
			  E.FirstName +' '+E.LastName as EmployeeName,
			  E.Photo as EmployeeProfile,
			  E.Gender as Gender,
			  (select Name from Role where Id = E.Role) as 'RoleName',
			  E.IsActive,
			  D.CreatedDate,
			  D.createdBy,
			  D.UpdatedDate,
              D.UpdatedBy,
              D.DeletedDate,
              D.DeletedBy,
			  DS.Name as [Status],
			  D.StatusId,
			  (select Name from Region where Id = E.Region) as Region,
			  (select Name from Headquarter where Id = E.Headquarter) as Headquarter,
			  (select count(*) from [dbo].[DSR_VisitedDoctor] where DSRId = D.Id) as VisitedDoctor,
			  (select count(*) from [dbo].[DSR_VisitedChemist] where DSRId = D.Id) as VisitedChemist
		from 
			[dbo].[DSR] d

			LEFT JOIN [RouteType] RT
			on D.RouteTypeId = RT.Id

			LEFT JOIN [WorkType] WT
			on D.WorkTypeId = WT.Id

			LEFT JOIN [Route] R
			on D.RouteId = R.Id

			LEFT JOIN [VisitType] VT
			on D.VisitTypeId = VT.Id

			LEFT JOIN [Doctor] DI
			on D.DoctorId = DI.Id

			LEFT JOIN [Chemist] C
			on D.ChemistId = C.Id

			LEFT JOIN DSRStatus DS
			ON DS.Id = d.StatusId

			LEFT JOIN Employee E
			ON D.EmployeeId = E.Id
			

		Where 
		    (@RouteTypeId = 0 OR D.RouteTypeId = @RouteTypeId)
			AND
			(@WorkTypeId = 0 OR D.WorkTypeId = @WorkTypeId)
			AND
			(@RouteId  = 0 OR D.RouteId = @RouteId)
			AND
			(@VisitTypeId = 0 OR D.VisitTypeId = @VisitTypeId)
			AND
			(@DoctorId  = 0 OR D.DoctorId = @DoctorId)
			AND
			(@ChemistId = 0 OR D.ChemistId = @ChemistId)
            AND
			(@EmployeeId = 0 OR D.EmployeeId = @EmployeeId)
			AND
			(@HeadquaterId = 0 OR E.Headquarter = @HeadquaterId)
			AND
			(@RegionId = 0 OR E.Region = @RegionId)
			AND
			(@StatusId = 0 OR D.StatusId = @StatusId)
			AND
			((@Date iS null or @Date = '') or convert(date,D.CreatedDate,103) = convert(date,@Date,103))
			AND
		
			(
					isnull(@Search,'') = ''
					or
					(
						lower(WT.Name) like lower('%'+@Search+'%')
						or
						lower(RT.Name) like lower('%'+@Search+'%')
						or
						lower(VT.Name) like lower('%'+@Search+'%')
						or
						lower(R.Name) like lower('%'+@Search+'%')
						or
						lower(DI.FirstName) like lower('%'+@Search+'%')
						or
						lower(C.Name) like lower('%'+@Search+'%')

					)
					AND
					D.DeletedBy = 0	
				)	
		

		ORDER BY 
		      D.Id
		DESC
			  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 


	select @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'DSR Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

SELECT 
		      D.Id,
		      D.Date, 
			  D.RouteTypeId,
			  RT.Name as RouteTypeName,
			  D.WorkTypeId,
			  WT.Name AS WorkTypeName,
			  D.RouteId,
			  R.Name as RouteName,
			  D.VisitTypeId,
			  VT.Name AS VisitTypeName,
			  D.DoctorId,
			  DI.FirstName AS DoctorFirstName,
			  D.ChemistId, 
			  C.Name AS ChemistName,
			  D. EmployeeId,
			  D.CreatedDate,
			  D.createdBy,
			  D.UpdatedDate,
              D.UpdatedBy,
              D.DeletedDate,
              D.DeletedBy


			
		from 
			[dbo].[DSR] d

			LEFT JOIN [RouteType] RT
			on D.RouteTypeId = RT.Id

			LEFT JOIN [WorkType] WT
			on D.WorkTypeId = WT.Id

			LEFT JOIN [Route] R
			on D.RouteId = R.Id

			LEFT JOIN [VisitType] VT
			on D.VisitTypeId = VT.Id

			LEFT JOIN [Doctor] DI
			on D.DoctorId = DI.Id

			LEFT JOIN [Chemist] C
			on D.ChemistId = C.Id
		WHERE
				D.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_Delete]

@Id BIGINT = 0,
	@DeletedBy BIGINT = 0


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update DSR set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'DSR Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			  Id,	
              Date, 
			  RouteTypeId,
			  WorkTypeId, 
			  RouteId,
			  VisitTypeId,
			  DoctorId, 
			  ChemistId,
			   EmployeeId,
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
              DeletedDate,
              DeletedBy



FROM
				[dbo].[DSR]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_Upsert]
@Id BIGINT = 0,
		@Date varchar (500) = null,
		@RouteTypeId BIGINT = 0,
	    @WorkTypeId BIGINT = 0,
		@RouteId BIGINT = 0,
	    @VisitTypeId BIGINT = 0,
		@DoctorId BIGINT = 0,
	    @ChemistId BIGINT = 0,
		@EmployeeId INT= 0,
		@StatusId bigint = 0,
		@CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO  DSR
												
											 (
												
												 Date, 
												 RouteTypeId,
												 WorkTypeId, 
												 RouteId,
												 VisitTypeId,
												 DoctorId, 
												 ChemistId,
												 EmployeeId,
												 StatusId,
												 CreatedDate,
												 CreatedBy,
												 DeletedBy


												
												 

												)

									VALUES (
											
											@Date, 
											@RouteTypeId,
											@WorkTypeId, 
											@RouteId,
											@VisitTypeId,
											@DoctorId, 
											@ChemistId, 
											@EmployeeId,
											@StatusId,
											GETDATE(),
											@createdBy,
											0
											
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  DSR

							SET		
							        Date= Isnull (@Date,Date),
									RouteTypeId = @RouteTypeId,
									WorkTypeId = @WorkTypeId,
									RouteId = @RouteId,
									VisitTypeId = @VisitTypeId,
									DoctorId = @DoctorId,
									ChemistId =@ChemistId,
									EmployeeId =@EmployeeId,
                                    UpdatedDate = GETDATE(),
									UpdatedBy = UpdatedBy,
									StatusId = @StatusId
									

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		      Id,
              Date, 
			  RouteTypeId,
			  WorkTypeId, 
			  RouteId,
			  VisitTypeId,
			  DoctorId, 
			  ChemistId,
			   EmployeeId,
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
              DeletedDate,
              DeletedBy
			 



FROM
				[dbo].[DSR]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_ByDSRId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- select * from DSR_VisitedChemist

CREATE PROCEDURE [dbo].[DSR_VisitedChemist_ByDSRId]

	@Offset BIGINT,
	@Limit BIGINT,
	@DSRId BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[DSR_VisitedChemist] DSRV
								
								left join [DSR] D
								on DSRV.DSRId = D.Id

								left join [Chemist] C1
								on DSRV.ChemistId = C1.Id

								left join [CallIn] C2
								on DSRV.CallInId = C2.Id

								left join [Outcome] O
								on DSRV.OutcomeId = O.Id

								
								where (@DSRId = 0 or  DSRV.DSRId = @DSRId)
								
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				DSRV.Id,
				DSRV.DSRId,
				DSRV.ChemistId,
				C1.Name As ChemistName,
				C1.Mobile as ChemistMobile,
				DSRV.CallInId,
				C2.Name As CallInName,
				DSRV.OutcomeId,
				O.Name as OutcomeName,
				DSRV.WorkWithId,
				DSRV.CreatedDate,
				DSRV.CreatedBy,
				DSRV.UpdatedDate,
				DSRV.UpdatedBy,
			    M.FirstName +' '+M.LastName as WorkWithName,
				DSRV.Remarks

			FROM [dbo].[DSR_VisitedChemist] DSRV
			
			left join [DSR] D
			on DSRV.DSRId = D.Id

			left join [Chemist] C1
			on DSRV.ChemistId = C1.Id

			left join [CallIn] C2
			on DSRV.CallInId = C2.Id

			left join [Outcome] O
			on DSRV.OutcomeId = O.Id

			left join Employee M on DSRV.WorkWithId = M.Id

			
			where (@DSRId = 0 or  DSRV.DSRId = @DSRId)
			
			order by DSRV.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[DSR_VisitedChemist_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'DSR_VisitedChemist Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT 
			V.Id,
			V.DSRId,
			D.Date as DSRDate,
			V.ChemistId,
			C1.Name As ChemistName,
			C1.Mobile as ChemistMobile,
			V.CallInId,
			C2.Name As CallInName,
			V.OutcomeId,
			O.Name as OutcomeName,
			V.WorkWithId,
			M.Name as WorkWithName,
			V.CreatedDate,
			V.CreatedBy,
			V.UpdatedDate,
			V.UpdatedBy,
			V.Remarks

	FROM
				[DSR_VisitedChemist] V
			
								left join [DSR] D
								on V.DSRId = D.Id

								left join [Chemist] C1
								on V.ChemistId = C1.Id

								left join [CallIn] C2
								on V.CallInId = C2.Id

								left join [Outcome] O
								on V.OutcomeId = O.Id

								left join WorkWithMaster M
								on V.WorkWithId = M.Id

								
		WHERE
				V.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_VisitedChemist_Delete]

@Id BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From DSR_VisitedChemist

		where Id = @Id

		SET @Code = 200
		SET @Message = 'DSR_VisitedChemist Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[DSRId],
				[ChemistId],
				[CallInId],
				[OutcomeId],
				[WorkWithId],
				[Remarks],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				DSR_VisitedChemist
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Products_ByDSR_VisitedChemistId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- select * from DSR_VisitedChemist_Products

CREATE PROCEDURE [dbo].[DSR_VisitedChemist_Products_ByDSR_VisitedChemistId]

	@Offset BIGINT,
	@Limit BIGINT,
	@DSR_VisitedChemistId BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[DSR_VisitedChemist_Products] DVP
								
								left join [DSR_VisitedChemist] DV
								on DVP.DSR_VisitedChemistId = DV.Id
			
								left join [Product] P
								on DVP.ProductId = P.Id
								
								where (@DSR_VisitedChemistId = 0 or  DVP.DSR_VisitedChemistId = @DSR_VisitedChemistId)
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END
		
		select
				DVP.Id,
				DVP.DSR_VisitedChemistId,
				DVP.ProductId,
				P.Name AS ProductName,
				P.Price AS ProductPrice,
				P.Quantity As ProductQuantity,
				(select Name from ProductCategory where Id = P.CategoryId) as CategoryName,
				(select Name from ProductManufacture where Id = P.ManufactureId) as MfgName,
				P.Quantity as Qty,
				DVP.OrderBookedQty,
				DVP.CreatedDate,
				DVP.CreatedBy,
				DVP.UpdatedDate,
				DVP.UpdatedBy,
				DV.CallInId,
			    C2.Name As CallInName,
			    DV.OutcomeId,
			    O.Name as OutcomeName,
			    DV.WorkWithId,
			    M.Name as WorkWithName,
				(select top(1) URL from ProductImages where ProductId = P.Id) as ProductImage

			FROM [dbo].[DSR_VisitedChemist_Products] DVP			
			left join [DSR_VisitedChemist] DV on DVP.DSR_VisitedChemistId = DV.Id			
			left join [Product] P on DVP.ProductId = P.Id
			left join [Chemist] C1 on DV.ChemistId = C1.Id
			left join [CallIn] C2 on DV.CallInId = C2.Id
			left join [Outcome] O on DV.OutcomeId = O.Id
			left join WorkWithMaster M on DV.WorkWithId = M.Id
			
			where (@DSR_VisitedChemistId = 0 or  DVP.DSR_VisitedChemistId = @DSR_VisitedChemistId)
			
			order by DVP.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Products_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DSR_VisitedChemist_Products_Delete]

@Id BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From DSR_VisitedChemist_Products

		where Id = @Id

		SET @Code = 200
		SET @Message = 'DSR_VisitedChemist_Products Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[DSR_VisitedChemistId],
				[ProductId],
				[OrderBookedQty],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				DSR_VisitedChemist_Products
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Products_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DSR_VisitedChemist_Products_Upsert]
		@Id BIGINT = 0,
		@DSR_VisitedChemistId BIGINT = 0,
		@ProductId BIGINT = 0,
		@OrderBookedQty decimal = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
AS
BEGIN
			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF Exists(select 1 from DSR_VisitedChemist_Products where DSR_VisitedChemistId = @DSR_VisitedChemistId and ProductId = @ProductId)
				BEGIN
					SET @Id = (select Id from DSR_VisitedChemist_Products where DSR_VisitedChemistId = @DSR_VisitedChemistId and ProductId = @ProductId)
				END
               
			IF @Id = 0
					BEGIN
							INSERT INTO DSR_VisitedChemist_Products
												
											 (
												DSR_VisitedChemistId,
												ProductId,
												OrderBookedQty,
												CreatedDate,
												CreatedBy
											  )
									VALUES (
											
												@DSR_VisitedChemistId,
												@ProductId,
												@OrderBookedQty,
												GETDATE(),
												@createdBy
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR_VisitedChemist_Products Data Created Successfully'
					END

			 ELSE If @Id > 0
					BEGIN
							UPDATE  DSR_VisitedChemist_Products

							SET		
							        DSR_VisitedChemistId = @DSR_VisitedChemistId,
									ProductId = @ProductId,
									OrderBookedQty = @OrderBookedQty,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR_VisitedChemist_Products Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DSR_VisitedChemistId,
		ProductId,
		OrderBookedQty,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy
				
		FROM
				DSR_VisitedChemist_Products
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedChemist_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DSR_VisitedChemist_Upsert]

		@Id BIGINT = 0,
		@DSRId BIGINT = 0,
		@ChemistId BIGINT = 0,
		@CallInId BIGINT = 0,
		@OutcomeId BIGINT = 0,
		@WorkWithId BIGINT = 0,
		@Remarks varchar(500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO DSR_VisitedChemist
												
											 (
												DSRId,
												ChemistId,
												CallInId,
												OutcomeId,
												WorkWithId,
												Remarks,
												CreatedDate,
												CreatedBy
												

												)

									VALUES (
											
												@DSRId,
												@ChemistId,
												@CallInId,
												@OutcomeId,
												@WorkWithId,
												@Remarks,
												GETDATE(),
												@createdBy
												

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR_VisitedChemist Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  DSR_VisitedChemist

							SET		
							        DSRId = @DSRId,
									ChemistId = @ChemistId,
									CallInId = @CallInId,
									OutcomeId = @OutcomeId,
									WorkWithId = @WorkWithId,
									Remarks = ISNULL(Remarks,@Remarks),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR_VisitedChemist Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DSRId,
		ChemistId,
		CallInId,
		OutcomeId,
		WorkWithId,
		Remarks,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy
				
		FROM
				DSR_VisitedChemist
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_ByDSRId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [DSR_VisitedDoctor_ByDSRId] 0,0,4
CREATE PROCEDURE [dbo].[DSR_VisitedDoctor_ByDSRId]

	@Offset BIGINT,
	@Limit BIGINT,
	@DSRId BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[DSR_VisitedDoctor] DSRV
								
								left join [DSR] D
								on DSRV.DSRId = D.Id

								left join [Doctor] C1
								on DSRV.DoctorId = C1.Id

								left join [CallIn] C2
								on DSRV.CallInId = C2.Id

								left join [Outcome] O
								on DSRV.OutcomeId = O.Id

								
								where (@DSRId = 0 or  DSRV.DSRId = @DSRId)
								
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				DSRV.Id,
				DSRV.DSRId,
				DSRV.DoctorId,
				C1.Photo,
				C1.FirstName+' '+C1.LastName AS DoctorName,
				C1.Address AS DoctorAddress,
				C1.Mobile AS DoctorMobile,
				C1.Phone As DoctorPhone,
				DSRV.CallInId,
				C2.Name As CallInName,
				DSRV.OutcomeId,
				O.Name as OutcomeName,
				DSRV.WorkWithId,
				DSRV.CreatedDate,
				DSRV.CreatedBy,
				DSRV.UpdatedDate,
				DSRV.UpdatedBy,
				M.FirstName +' '+M.LastName as WorkWithName

			FROM [dbo].[DSR_VisitedDoctor] DSRV
			
			left join [DSR] D
			on DSRV.DSRId = D.Id

			left join [Doctor] C1
			on DSRV.DoctorId = C1.Id

			left join [CallIn] C2
			on DSRV.CallInId = C2.Id

			left join [Outcome] O
			on DSRV.OutcomeId = O.Id

			left join Employee M on DSRV.WorkWithId = M.Id

			
			where (@DSRId = 0 or  DSRV.DSRId = @DSRId)
			
			order by DSRV.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Exec [dbo].[DSR_VisitedDoctor_ById] 1
CREATE proc [dbo].[DSR_VisitedDoctor_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'DSR_VisitedDoctor Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT 
			
			DV.Id,
	        DV.DSRId,
	        DV.DoctorId,
	        DV.CallInId,
	        DV.OutcomeId,
	        DV.WorkWithId,
	        DV.Remarks,
			DV.CreatedDate,
			DV.CreatedBy,
			DV.UpdatedDate,
			DV.UpdatedBy

	FROM
				[dbo].[DSR_VisitedDoctor] DV
			
			                    left join [DSR] DS
								on DV.DSRId = DS.Id

								left join [Doctor] D
								on DV.DoctorId = D.Id

								left join [CallIn] C
								on DV.CallInId = C.Id

								left join [Outcome] O
								on DV.OutcomeId = O.Id

								
		WHERE
				DV.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@DSR_VisitedDoctorId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [DSR_VisitedDoctor_Campaign] DVC

									left join [DSR_VisitedDoctor] DV
									on DVC.DSR_VisitedDoctorId = DV.Id
									
									where 
										(@DSR_VisitedDoctorId = 0 or  @DSR_VisitedDoctorId = DVC.DSR_VisitedDoctorId)
										and DVC.DeletedBy = 0
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
			DVC.Id,
	        DVC.DSR_VisitedDoctorId,
			DV.DoctorId,
			Dv.CallInId,
			DVC.ChampaignMasterId,
			DVC.CreatedDate,
			DVC.CreatedBy,
			DVC.UpdatedDate,
			DVC.UpdatedBy,
			DVC.DeletedDate,
			DVC.DeletedBy

			FROM [DSR_VisitedDoctor_Campaign] DVC

									left join [DSR_VisitedDoctor] DV
									on DVC.DSR_VisitedDoctorId = DV.Id
									
									where 
										(@DSR_VisitedDoctorId = 0 or  @DSR_VisitedDoctorId = DVC.DSR_VisitedDoctorId)
										and DVC.DeletedBy = 0

			order by DVC.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[DSR_VisitedDoctor_Campaign_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'DSR_VisitedDoctor_Campaign Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT 
			
			DVC.Id,
	        DVC.DSR_VisitedDoctorId,
			DV.DoctorId,
			Dv.CallInId,
			DVC.ChampaignMasterId,
			C.CampaignName,
			C.StartDate,
			C.EndDate,
			DVC.CreatedDate,
			DVC.CreatedBy,
			DVC.UpdatedDate,
			DVC.UpdatedBy,
			DVC.DeletedDate,
			DVC.DeletedBy

	FROM
				[DSR_VisitedDoctor_Campaign] DVC
			
			                    left join [DSR_VisitedDoctor] DV
								on DVC.DSR_VisitedDoctorId = DV.Id

								left join [ChampaignMaster] C
								on DVC.ChampaignMasterId = C.Id

								
		WHERE
				DVC.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [DSR_VisitedDoctor_Campaign_Delete]5
--select * from DSR_VisitedDoctor_Campaign

CREATE PROCEDURE [dbo].[DSR_VisitedDoctor_Campaign_Delete]

@Id BIGINT,
@DeletedBy BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update DSR_VisitedDoctor_Campaign set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'DSR_VisitedDoctor_Campaign Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			 Id,
				DSR_VisitedDoctorId,
				ChampaignMasterId,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy



FROM
				[dbo].[DSR_VisitedDoctor_Campaign]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedCampaignId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedCampaignId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@DSR_VisitedDoctor_CampaignId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [DSR_VisitedDoctor_Campaign_Gifts] DVCG

									left join [DSR_VisitedDoctor_Campaign] DVC
									on DVCG.DSR_VisitedDoctor_CampaignId = DVC.Id
									
									where 
										(@DSR_VisitedDoctor_CampaignId = 0 or  @DSR_VisitedDoctor_CampaignId = DVCG.DSR_VisitedDoctor_CampaignId)
										and DVCG.DeletedBy = 0
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
			DVC.Id,
	        DVCG.DSR_VisitedDoctor_CampaignId,
			DVC.DSR_VisitedDoctorId,
			DVC.ChampaignMasterId,
			DVC.CreatedDate,
			DVC.CreatedBy,
			DVC.UpdatedDate,
			DVC.UpdatedBy,
			DVC.DeletedDate,
			DVC.DeletedBy

			FROM  [DSR_VisitedDoctor_Campaign_Gifts] DVCG

									left join [DSR_VisitedDoctor_Campaign] DVC
									on DVCG.DSR_VisitedDoctor_CampaignId = DVC.Id
									
									where 
										(@DSR_VisitedDoctor_CampaignId = 0 or  @DSR_VisitedDoctor_CampaignId = DVCG.DSR_VisitedDoctor_CampaignId)
										and DVCG.DeletedBy = 0

			order by DVCG.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[DSR_VisitedDoctor_Campaign_Gifts_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'DSR_VisitedDoctor_Campaign_Gifts Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT 
			
			DVCG.Id,
	        DVCG.DSR_VisitedDoctor_CampaignId,
			DVC.DSR_VisitedDoctorId,
			DvC.ChampaignMasterId,
			DVCG.GiftId,
			G.GiftName,
			G.GiftQty,
			DVCG.CreatedDate,
			DVCG.CreatedBy,
			DVCG.UpdatedDate,
			DVCG.UpdatedBy,
			DVCG.DeletedDate,
			DVCG.DeletedBy

	FROM
				[DSR_VisitedDoctor_Campaign_Gifts] DVCG
			
			                    left join [DSR_VisitedDoctor_Campaign] DVC
								on DVCG.DSR_VisitedDoctor_CampaignId = DVC.Id

								left join [Gift] G
								on DVCG.GiftId = G.Id

								
		WHERE
				DVCG.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [DSR_VisitedDoctor_Campaign_Gifts_Delete]2
--select * from DSR_VisitedDoctor_Campaign_Gifts

CREATE PROCEDURE [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Delete]

@Id BIGINT,
@DeletedBy BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update DSR_VisitedDoctor_Campaign_Gifts set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'DSR_VisitedDoctor_Campaign_Gifts Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			 Id,
				DSR_VisitedDoctor_CampaignId,
				GiftId,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy



FROM
				[dbo].[DSR_VisitedDoctor_Campaign_Gifts]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DSR_VisitedDoctor_Campaign_Gifts_Upsert]

		@Id BIGINT = 0,
		@DSR_VisitedDoctor_CampaignId BIGINT = 0,
		@GiftId BIGINT = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO DSR_VisitedDoctor_Campaign_Gifts
												
											 (
												DSR_VisitedDoctor_CampaignId,
												GiftId,
												CreatedDate,
												CreatedBy,
												DeletedBy

												)

									VALUES (
											
												@DSR_VisitedDoctor_CampaignId,
												@GiftId,
												GETDATE(),
												@createdBy,
												0

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor_Campaign_Gifts Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  DSR_VisitedDoctor_Campaign_Gifts

							SET		
							        DSR_VisitedDoctor_CampaignId = @DSR_VisitedDoctor_CampaignId,
									GiftId = @GiftId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor_Campaign_Gifts Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DSR_VisitedDoctor_CampaignId,
		GiftId,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy
				
		FROM
				DSR_VisitedDoctor_Campaign_Gifts
		WHERE
				Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Campaign_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[DSR_VisitedDoctor_Campaign_Upsert]

		@Id BIGINT = 0,
		@DSR_VisitedDoctorId BIGINT = 0,
		@ChampaignMasterId BIGINT = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO DSR_VisitedDoctor_Campaign
												
											 (
												DSR_VisitedDoctorId,
												ChampaignMasterId,
												CreatedDate,
												CreatedBy,
												DeletedBy

												)

									VALUES (
											
												@DSR_VisitedDoctorId,
												@ChampaignMasterId,
												GETDATE(),
												@createdBy,
												0

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor_Campaign Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  DSR_VisitedDoctor_Campaign

							SET		
							        DSR_VisitedDoctorId = @DSR_VisitedDoctorId,
									ChampaignMasterId = @ChampaignMasterId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor_Campaign Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DSR_VisitedDoctorId,
		ChampaignMasterId,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy
				
		FROM
				DSR_VisitedDoctor_Campaign
		WHERE
				Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from DSR_VisitedDoctor
--Exec [dbo].[DSR_VisitedDoctor_Delete] 5
CREATE proc [dbo].[DSR_VisitedDoctor_Delete]
@Id BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From DSR_VisitedDoctor

		where ID = @ID

		SET @Code = 200
		SET @Message = 'DSR_VisitedDoctor Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
			 Id,
	        DSRId,
	        DoctorId,
	        CallInId,
	        OutcomeId,
	        WorkWithId,
	        Remarks,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy
				
		FROM
				DSR_VisitedDoctor
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId]

	@Offset BIGINT,
	@Limit BIGINT,
	@DSR_VisitedDoctorId BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[DSR_VisitedDoctor_Products] DVP
								
								left join [DSR_VisitedDoctor] DV
								on DVP.DSR_VisitedDoctorId = DV.Id
			
								left join [Product] P
								on DVP.ProductId = P.Id
								
								where (@DSR_VisitedDoctorId = 0 or  DVP.DSR_VisitedDoctorId = @DSR_VisitedDoctorId)
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				DVP.Id,
				DVP.DSR_VisitedDoctorId,
				DVP.ProductId,
				P.Name AS ProductName,
				P.Price AS ProductPrice,
				(select Name from ProductCategory where Id = P.CategoryId) as CategoryName,
				(select Name from ProductManufacture where Id = P.ManufactureId) as MfgName,
				P.Quantity as Qty,
				P.Quantity As ProductQuantity,
				DVP.OrderBookedQty,
				DVP.CreatedDate,
				DVP.CreatedBy,
				DVP.UpdatedDate,
				DVP.UpdatedBy,
				DV.CallInId,
			    C2.Name As CallInName,
			    DV.OutcomeId,
			    O.Name as OutcomeName,
			    DV.WorkWithId,
			    M.Name as WorkWithName,
				(select top(1) URL from ProductImages where ProductId = P.Id) as ProductImage

			FROM [dbo].[DSR_VisitedDoctor_Products] DVP
			
			left join [DSR_VisitedDoctor] DV on DVP.DSR_VisitedDoctorId = DV.Id			
			left join [Product] P on DVP.ProductId = P.Id
			left join [Doctor] C1 on DV.DoctorId = C1.Id
			left join [CallIn] C2 on DV.CallInId = C2.Id
			left join [Outcome] O on DV.OutcomeId = O.Id
			left join WorkWithMaster M on DV.WorkWithId = M.Id
			
			where (@DSR_VisitedDoctorId = 0 or  DVP.DSR_VisitedDoctorId = @DSR_VisitedDoctorId)
			
			order by DVP.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Products_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_VisitedDoctor_Products_Delete]
@ID BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductManufacture

		where ID = @ID

		SET @Code = 200
		SET @Message = 'DSR_VisitedDoctor_Products Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
			  ID,	
              DSR_VisitedDoctorId, 
	          ProductId,
			  OrderBookedQty, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy
             



FROM
				[dbo].[DSR_VisitedDoctor_Products]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Products_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DSR_VisitedDoctor_Products_Upsert]

@Id BIGINT = 0,
		
		@DSR_VisitedDoctorId BIGINT = 0,
	    @ProductId BIGINT = 0,
		@OrderBookedQty decimal(18,2) = 0,
		@CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			   IF Exists(select 1 from DSR_VisitedDoctor_Products where DSR_VisitedDoctorId = @DSR_VisitedDoctorId and ProductId = @ProductId)
				BEGIN
					SET @Id = (select Id from DSR_VisitedDoctor_Products where DSR_VisitedDoctorId = @DSR_VisitedDoctorId and ProductId = @ProductId)
				END
			
			IF @Id = 0
					BEGIN
							INSERT INTO  DSR_VisitedDoctor_Products
												
											 (
												
												 DSR_VisitedDoctorId, 
												 ProductId,
												 OrderBookedQty, 
												 CreatedDate,
												 createdBy												
												 

												)

									VALUES (
											
											@DSR_VisitedDoctorId,
											@ProductId,
											@OrderBookedQty,  
											GETDATE(),
											@createdBy
											
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor_Products Data Created Successfully'
				END	
				
				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  DSR_VisitedDoctor_Products

							SET		
							        
									DSR_VisitedDoctorId = @DSR_VisitedDoctorId,
									ProductId = @ProductId,
									OrderBookedQty = @OrderBookedQty,
                                    UpdatedDate = GETDATE(),
									UpdatedBy = UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor_Products Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
			  ID,	
              DSR_VisitedDoctorId, 
	          ProductId,
			  OrderBookedQty, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy
             



FROM
				[dbo].[DSR_VisitedDoctor_Products]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSR_VisitedDoctor_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Select * from DSR_VisitedDoctor
--Exec  [dbo].[DSR_VisitedDoctor_Upsert] 3,6,7,4,6,4,"13",0,0
CREATE proc [dbo].[DSR_VisitedDoctor_Upsert]
	
	    @Id bigint ,
		@DSRId bigint,
		@DoctorId bigint,
		@CallInId bigint,
        @OutcomeId bigint,
        @WorkWithId bigint,
		@Remarks varchar(500),
		@CreatedBy bigint,
		@UpdatedBy Bigint
		

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code bigint = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT into DSR_VisitedDoctor (
														  
                                                           DSRId,
                                                           DoctorId,
                                                           CallInId,
                                                           OutcomeId,
                                                           WorkWithId, 
                                                           Remarks, 
														   CreatedDate,
														   CreatedBy
														   )

									VALUES (
									        
									        @DSRId,
									        @DoctorId,
									        @CallInId,
									        @OutcomeId,
									        @WorkWithId,
									        @Remarks, 
											GETDATE(),
											@CreatedBy )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE DSR_VisitedDoctor

							SET		
							          
							        DSRId     =  @DSRId,     
							        DoctorId  =  @DoctorId,  							        
									CallInId  =  @CallInId,       
							        OutcomeId =  @OutcomeId, 					        
									WorkWithId=  @WorkWithId,          
 							         								
									Remarks = ISNULL(@Remarks,Remarks),
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'DSR_VisitedDoctor Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 


	        Id,
	        DSRId,
	        DoctorId,
	        CallInId,
	        OutcomeId,
	        WorkWithId,
	        Remarks,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy
			
		FROM
				[dbo].[DSR_VisitedDoctor]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[DSRStatus_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [DSRStatus_All]
CREATE PROCEDURE [dbo].[DSRStatus_All]
@Offset BIGINT = 0,
@Limit BIGINT = 0,
@Search VARCHAR(MAX) = null
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;
	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    from 
			[dbo].[DSRStatus] 
		)

	IF @Limit = 0
			BEGIN
			     SET @Limit = @TotalRecords
				 IF @Limit = 0
				 BEGIN
				     SET @Limit = 10
				 END
			END


	Select 
		Id,
		Name

	from DsrStatus
	ORDER BY 
		      Id
		DESC
			  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 

	select @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[Employee_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Employee_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Employee Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[FirstName],
				[LastName],
				[IsActive],
				[Gender],
				[Mobile],
				[Phone],
				[EmergencyNumber],
				[Birthdate],
				[Anniversary],
				[Email],
				[Photo],
				[PermanentAddress],
				[CurrentAddress],
				[AddressProof],
				[DrivingLicense],
				[Remarks],
				[Qualification],
				[JoiningDate],
				[ConfirmationDate],
				[Role],
				[Headquarter],
				[Region],
				[ReportingPerson],
				[SalaryAmount],
				[PaymentMode],
				[BankName],
				[BankBranch],
				[BankAccountNo],
				[BankIFSCCode],
				[WebPassword],
				[MobilePassword],
				[CreatedAt],
				[CreatedBy],
				[UpdatedAt],
				[UpdatedBy]
				
		FROM
				[dbo].[Employee]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Employee_ChangePassword]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


---  exec  [Employee_ChangePassword]  2,'Vikas@201','Vikas@101'


CREATE PROCEDURE [dbo].[Employee_ChangePassword]
@Id int,
@OldPassword nvarchar(max),
@NewPassword nvarchar(max)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	    @Message VARCHAR(255) = 'Invalid email or password'

		IF (SELECT count(*) FROM Employee WHERE Id=@Id and WebPassword=@OldPassword) > 0
		BEGIN
		     UPDATE 
			 Employee
			 SET WebPassword = @NewPassword
			 WHERE Id=@Id

			 SET @Code = 200
			     SET @Message = 'Password Updated Successfully'
		END
		ELSE
		BEGIN
				SET @Id = 0
			     SET @Code = 400
			     SET @Message = 'Invalid Old Password'
		END 

		 SELECT @Code AS Code, @Message AS [Message]

		SELECT
		E.*,
		R.Name as 'RoleName',
		Q.Name as 'QualificationName',
		H.Name as 'HeadquarterName',
		Rg.Name as 'RegionName',
		(select FirstName + ' ' + LastName from Employee where Id = E.ReportingPerson) as 'ReportingPersonName'
	FROM Employee E
		Left join Role R on R.Id = E.Role
		Left join Qualification Q on Q.Id = E.Qualification
		Left join Headquarter H on H.Id = E.Headquarter
		Left join Region Rg on Rg.Id = E.Region
	WHERE E.Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Employee_Login]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [Employee_Login] 'mr@gmail.com','admin1234'
CREATE PROCEDURE [dbo].[Employee_Login]
@Email varchar(256),
@Password nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	    @Message VARCHAR(255) = 'Invalid email or password',
		@Id INT = 0


		SELECT @Id=Id FROM Employee WHERE lower(Email) = @Email

		IF @Id > 0
		BEGIN
		     IF (select COUNT(*) FROM Employee where Id=@Id and WebPassword=@Password) > 0
			 BEGIN   
			     SET @Code = 200
			     SET @Message = 'Employee verified successfully'
			 END
			 ELSE
			 BEGIN
			     SET @Id = 0
			     SET @Code = 400
			     SET @Message = 'Invalid Password'
			 END 
		END 
		ELSE
		BEGIN
		    SET @Id = 0
			SET @Code = 400
			SET @Message = 'Invalid Email'
		END 

        SELECT @Code AS Code, @Message AS [Message]

		SELECT
		E.*,
		R.Name as 'RoleName',
		H.Name as 'HeadquarterName',
		Rg.Name as 'RegionName',
		(select FirstName + ' ' + LastName from Employee where Id = E.ReportingPerson) as 'ReportingPersonName'
	FROM Employee E
		Left join Role R on R.Id = E.Role
		Left join Headquarter H on H.Id = E.Headquarter
		Left join Region Rg on Rg.Id = E.Region
	WHERE E.Id = @Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[EmployeeDelete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EmployeeDelete] (
	@Id int
	)
AS

SET NOCOUNT ON
	DELETE FROM Address WHERE Id in (select CurrentAddress from Employee WHERE Id = @Id)
	DELETE FROM Address WHERE Id in (select PermanentAddress from Employee WHERE Id = @Id)
	DELETE FROM Employee WHERE Id = @Id
SELECT 1
GO
/****** Object:  StoredProcedure [dbo].[EmployeeRegion_ByUserId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [EmployeeRegion_All] '',0,0,0,0
--  exec [EmployeeRegion_ByUserId] '',0,0,6
CREATE PROCEDURE [dbo].[EmployeeRegion_ByUserId]

	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@UserId int
    

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
				select COUNT(*) from EmployeeRegion
				where 
				(@UserId = 0 OR @UserId = UserId) 
		   )

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
			Id,
			UserId,
			RegionId,
			CreatedDate,
			CreatedBy

		from  [dbo].[EmployeeRegion]

		where 
		(@UserId = 0 OR @UserId = UserId)  
		
		order by Id

		desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[EmployeeRegion_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		kartik amipara
-- Create date: 07/31/2021
-- Description:	This sp use assign employee regions
-- =============================================
-- select * from EmployeeRegion
-- truncate table EmployeeRegion
-- exec EmployeeRegion_Upsert 0,3,'1,2,9,3',0
CREATE PROCEDURE [dbo].[EmployeeRegion_Upsert]
        @Id INT = 0,
		@UserId INT = 0,
		@RegionIds varchar(max) = null,
		@CreatedBy INT = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''


	-- IF Id = 0 add new OrderProducts data inserted

	IF @UserId > 0
		BEGIN
			  IF @RegionIds IS NULL OR @RegionIds = ''
				  BEGIN
		  			   SET @Id = 0
		  			   SET @Code = 400
		  			   SET @Message = 'Regions is required'
				  END
			  ELSE
			  BEGIN

			  -- DELETE REGION IN USER ID

			  delete from EmployeeRegion where UserId = @UserId

			  DECLARE @Total INT = 0, @i INT = 0

					SET @Total = (select count(value) from string_split(@RegionIds,','))
					DECLARE @Row INT = 0

					WHILE @i < @Total
					BEGIN
						DECLARE @RegionIdsVar int 
							
						select @RegionIdsVar = CONVERT(int,value) from  string_split(@RegionIds,',') 
						ORDER BY (SELECT 0) OFFSET @i ROWS FETCH NEXT 1 ROWS ONLY

						INSERT INTO EmployeeRegion
						(
							UserId,
							RegionId,
							CreatedDate,	
							CreatedBy
						)	
						VALUES
						(
							@UserId,
							@RegionIdsVar,
							GETUTCDATE(),
							@UserId
						)
						SET @i = @i+1;
					END		
			   SET @Id = SCOPE_IDENTITY()
			   SET @Code = 200
			   SET @Message = 'Employee region created'
		  END 
       
	END
	ELSE
	BEGIN
		SET @Code = 200
		SET @Message = 'Please select employee'
	END

	SELECT @Code as Code,@Message as [Message]
		Select 
			Id,
			UserId,
			RegionId,
			CreatedDate,
			CreatedBy
	    FROM 
	        [dbo].[EmployeeRegion]
		WHERE
			Id = @Id 
END


GO
/****** Object:  StoredProcedure [dbo].[EmployeeSelect]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[EmployeeSelect] (
@Id int
)
AS

SET NOCOUNT ON

DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]
 
SET NOCOUNT ON

SELECT
		E.*,
		R.Name as 'RoleName',
		H.Name as 'HeadquarterName',
		Rg.Name as 'RegionName',
		(select FirstName + ' ' + LastName from Employee where Id = E.ReportingPerson) as 'ReportingPersonName',
		(Select A.Address1 + ' ' + ISNULL(A.Address2, '') + ' ' + A.Pincode from Address A Where A.Id = E.CurrentAddress) as 'CurrentAddressString',
		(Select A.Address1 + ' ' + ISNULL(A.Address2, '') + ' ' + A.Pincode from Address A Where A.Id = E.PermanentAddress) as 'PermanentAddressString'

	FROM Employee E
		Left join Role R on R.Id = E.Role
		Left join Headquarter H on H.Id = E.Headquarter
		Left join Region Rg on Rg.Id = E.Region
	WHERE E.Id = @Id
GO
/****** Object:  StoredProcedure [dbo].[EmployeeSelectAll]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EmployeeSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM Employee E
						Left join Role R on R.Id = E.Role
						WHERE ISNULL(@Search,'') = '' 
							  OR Email like '%'+@Search+'%' 
							  OR Mobile like '%'+@Search+'%' 
							  OR FirstName like '%'+@Search+'%' 
							  OR LastName like '%'+@Search+'%'
							  OR R.Name like '%'+@Search+'%'
					)

   select 
		E.*, 
		R.Name as 'RoleName' 
   FROM Employee E
	 Left join Role R on R.Id = E.Role
	 WHERE ISNULL(@Search,'') = '' 
	 	  OR Email like '%'+@Search+'%' 
	 	  OR Mobile like '%'+@Search+'%' 
	 	  OR FirstName like '%'+@Search+'%' 
	 	  OR LastName like '%'+@Search+'%'
	 	  OR R.Name like '%'+@Search+'%'
	 ORDER BY E.Id Desc
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[EmployeeUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- EXEC [UserInsert] 'aaa','bbb','a@in.com','1234567890','123',true,admin 

CREATE PROCEDURE [dbo].[EmployeeUpsert] (
	@Id int,
	@FirstName nvarchar(100),
	@LastName nvarchar(100),
	@IsActive bit,
	@Gender bit,
	@Mobile varchar(15),
	@Phone varchar(15),
	@EmergencyNumber varchar(15),
	@Birthdate datetime,
	@Anniversary datetime,
	@Email nvarchar(256),
	@Photo nvarchar(max),
	@PermanentAddress int,
	@CurrentAddress int,
	@AddressProof nvarchar(max),
	@DrivingLicense nvarchar(max),
	@Remarks nvarchar(max),
	@Qualification nvarchar(max),
	@JoiningDate datetime,
	@ConfirmationDate datetime,
	@Role int,
	@Headquarter int,
	@Region int,
	@ReportingPerson int,
	@SalaryAmount decimal(18,2),
	@PaymentMode nvarchar(max),
	@BankName nvarchar(100),
	@BankBranch nvarchar(100),
	@BankAccountNo nvarchar(20),
	@BankIFSCCode nvarchar(20),
	@CreatedBy int,
	@UpdatedBy int,
	@WebPassword nvarchar(max),
	@MobilePassword nvarchar(max))
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

-- Update
IF(@Id > 0)
	IF exists (select Id from Employee where Id!=@Id AND (Mobile=@Mobile or Email=@Email))
		BEGIN
			SET @Message='Mobile Number or Email already exists'
			SELECT @Code AS Code, @Message AS [Message]
		END
	ELSE
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
		
			UPDATE [dbo].[Employee]
				SET [FirstName] = @FirstName
				   ,[LastName] = @LastName
				   ,[IsActive] = @IsActive
				   ,[Gender] = @Gender
				   ,[Mobile] = @Mobile
				   ,[Phone] = @Phone
				   ,[EmergencyNumber] = @EmergencyNumber
				   ,[Birthdate] = @Birthdate
				   ,[Anniversary] = @Anniversary
				   ,[Email] = @Email
				   ,[Photo] = @Photo
				   ,[PermanentAddress] = @PermanentAddress
				   ,[CurrentAddress] = @CurrentAddress
				   ,[AddressProof] = @AddressProof
				   ,[DrivingLicense] = @DrivingLicense
				   ,[Remarks] = @Remarks
				   ,[Qualification] = @Qualification
				   ,[JoiningDate] = @JoiningDate
				   ,[ConfirmationDate] = @ConfirmationDate
				   ,[Role] = @Role
				   ,[Headquarter] = @Headquarter
				   ,[Region] = @Region
				   ,[ReportingPerson] = @ReportingPerson
				   ,[SalaryAmount] = @SalaryAmount
				   ,[PaymentMode] = @PaymentMode
				   ,[BankName] = @BankName
				   ,[BankBranch] = @BankBranch
				   ,[BankAccountNo] = @BankAccountNo
				   ,[BankIFSCCode] = @BankIFSCCode
				   ,[UpdatedAt] = GETDATE()
				   ,[UpdatedBy] = @UpdatedBy
				   ,[WebPassword] = ISNULL(@WebPassword, [WebPassword])
				   ,[MobilePassword] = ISNULL(@MobilePassword, [MobilePassword])
				WHERE Id = @Id

			SELECT
				E.*,
				R.Name as 'RoleName',
				H.Name as 'HeadquarterName',
				Rg.Name as 'RegionName',
				(select FirstName + ' ' + LastName from Employee where Id = E.ReportingPerson) as 'ReportingPersonName'
			FROM Employee E
				Left join Role R on R.Id = E.Role
				Left join Headquarter H on H.Id = E.Headquarter
				Left join Region Rg on Rg.Id = E.Region
			WHERE E.Id = @Id
		END

-- Insert
ELSE	
	IF exists (select Id from Employee where Mobile=@Mobile or Email=@Email)
		BEGIN
			SET @Message='Mobile Number or Email is already exists.'
			SELECT @Code AS Code, @Message AS [Message]
		END
	ELSE
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
	
			INSERT INTO [dbo].[Employee]
						([FirstName] ,[LastName] ,[IsActive] ,[Gender] ,[Mobile] ,[Phone] ,[EmergencyNumber] ,[Birthdate], [Anniversary] ,[Email] ,[Photo] ,[PermanentAddress] ,[CurrentAddress]
						,[AddressProof] ,[DrivingLicense] ,[Remarks] ,[Qualification] ,[JoiningDate],[ConfirmationDate] ,[Role] ,[Headquarter] ,[Region] ,[ReportingPerson] ,[SalaryAmount]
						,[PaymentMode] ,[BankName] ,[BankBranch] ,[BankAccountNo] ,[BankIFSCCode] ,[CreatedAt] ,[CreatedBy],[MobilePassword], [WebPassword])
			VALUES
						(@FirstName, @LastName, @IsActive, @Gender, @Mobile, @Phone, @EmergencyNumber, @Birthdate, @Anniversary, @Email, @Photo, @PermanentAddress, @CurrentAddress,
						@AddressProof, @DrivingLicense, @Remarks, @Qualification, @JoiningDate, @ConfirmationDate, @Role, @Headquarter, @Region, @ReportingPerson, @SalaryAmount,
						@PaymentMode, @BankName, @BankBranch, @BankAccountNo ,@BankIFSCCode, Getdate(), @CreatedBy,  @MobilePassword, @WebPassword)
	
			SELECT
				E.*,
				R.Name as 'RoleName',
				H.Name as 'HeadquarterName',
				Rg.Name as 'RegionName',
				(select FirstName + ' ' + LastName from Employee where Id = E.ReportingPerson) as 'ReportingPersonName'
			FROM Employee E
				Left join Role R on R.Id = E.Role
				Left join Headquarter H on H.Id = E.Headquarter
				Left join Region Rg on Rg.Id = E.Region
			WHERE E.Id = SCOPE_IDENTITY()
		END
GO
/****** Object:  StoredProcedure [dbo].[FieldName_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FieldName_All]
@Offset BIGINT = 0,
@Limit BIGINT= 0,
@Search VARCHAR(MAX) = null,
@MasterId BIGINT= 0
AS
BEGIN
	DECLARE @TotalRecords bigint = 0;

	SET @TotalRecords = (select count(*) from FieldName FN	where MasterId = @MasterId);
	IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
		*
	FROM
		FieldName FN where MasterId = @MasterId
	order by 
		FN.Id 
	desc
		OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[Gift_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[Gift_ById] 3
CREATE proc [dbo].[Gift_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Gift Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT         Id, 
			           ChampaignMasterId,
                       GiftName,
                       GiftQty,
                       CreatedDate,
					   CreatedBy,
					   UpdatedDate,
					   UpdatedBy,
					   DeletedDate,
					   DeletedBy

	FROM
				[dbo].[Gift] 
			
			                    

								

								
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Gift_ChampaignMasterId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[Gift_ChampaignMasterId] 
CREATE proc [dbo].[Gift_ChampaignMasterId]


@Offset bigint = 0,
	@Limit bigint = 0,
	@ChampaignMasterId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[Gift] G

									left join ChampaignMaster CM
									on G.ChampaignMasterId = CM.Id


									where (@ChampaignMasterId = 0 or  G.ChampaignMasterId = @ChampaignMasterId)
									ANd
									G.DeletedBy = 0
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select             
	
	                   G.Id,
				       G.ChampaignMasterId,
					   CM.CampaignName as ChampaignMasterName ,
                       G.GiftName,
                       G.GiftQty,
                       G.CreatedDate,
					   G.CreatedBy,
					   G.UpdatedDate,
					   G.UpdatedBy,
					   G.DeletedDate,
					   G.DeletedBy

			from [dbo].[Gift] G

									left join ChampaignMaster CM
									on G.ChampaignMasterId = CM.Id


									where (@ChampaignMasterId = 0 or  G.ChampaignMasterId = @ChampaignMasterId)
									ANd
									G.DeletedBy = 0

			order by G.ChampaignMasterId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[Gift_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Gift_Delete]

@Id BIGINT = 0,
@DeletedBy BIGINT = 0


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update Gift set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'Gift Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			           ChampaignMasterId,
                       GiftName,
                       GiftQty,
                       CreatedDate,
					   CreatedBy,
					   UpdatedDate,
					   UpdatedBy,
					   DeletedDate,
					   DeletedBy
				
		FROM
				[dbo].[Gift] 
			
			                   
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Gift_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from Gift 
--[dbo].[Gift_Upsert]0,4,"lkdf","fngj",0,0

CREATE proc [dbo].[Gift_Upsert]

	    @Id bigint ,
		@ChampaignMasterId bigint,
		@GiftName varchar(500),
	    @GiftQty varchar(500),
		@CreatedBy bigint,
		@UpdatedBy Bigint
		

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code bigint = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT into Gift (
														  
                                                           ChampaignMasterId,
                                                           GiftName,
                                                           GiftQty,
                                                           CreatedDate,
														   CreatedBy,
														   
														   DeletedBy

														   )

									VALUES (
									@ChampaignMasterId,
									@GiftName,
									@GiftQty,
									GETDATE(),
									@CreatedBy,
									
									0
									        
									         )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'Gift Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE Gift

							SET		
							
									ChampaignMasterId=  @ChampaignMasterId,          
 							         								
									GiftName = ISNULL(@GiftName,GiftName),
									GiftQty = ISNULL(@GiftQty,GiftQty),
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Gift Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
	                   Id,
                       ChampaignMasterId,
                       GiftName,
                       GiftQty,
                       CreatedDate,
					   CreatedBy,
					   UpdatedDate,
					   UpdatedBy,
					   DeletedDate,
					   DeletedBy

			
		FROM
				[dbo].[Gift]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Headquarter_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hassan Mansuri
-- Create date: 20 May 2021 01:00 PM
-- Description:	This sp is used for get the Admin data by id
-- =============================================
-- exec [Headquarter_ById] 1
CREATE PROCEDURE [dbo].[Headquarter_ById] 
	@Id BIGINT
AS
BEGIN

	SET NOCOUNT ON;

	Declare 
	        @Code BIGINT = 200,
            @Message varchar(200) = ''

	SET @Message = 'Data retrived successfully'

	       Select @Code as Code,
			      @Message as [Message]

		-- Admin data get for id

		Select 
			 H.Id,
              H.Name, 
			  H.RouteType,
			  R.Name AS RouteTypeName,
			  H.RegionId, 
			  H.IsActive,
			  H.CreatedAt,
			  H.CreatedBy,
			  H.UpdatedAt,
              H.UpdatedBy,
			  H.ManagerId
		from [dbo].[Headquarter] H
				left join RouteType R
				on H.RouteType = R.Id       
		WHERE
			H.Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[HeadquarterSelectAll]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [HeadquarterSelectAll] '',0,10,2
CREATE PROCEDURE [dbo].[HeadquarterSelectAll]
	@Search varchar(100),
	@Offset INT,
	@Limit INT,
	@RouteType INT = 0,
	@RegionId INT =0
	
AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[Headquarter] H

									left join RouteType R
									on H.RouteType = R.Id


									where (@RouteType = 0 or  H.RouteType = @RouteType)
									and
									(@RegionId = 0 or  H.RegionId = @RegionId)
										  
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(R.Name) like '%'+lower(@Search)+'%') 
										
									)
							   )
            )
		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
			  H.Id,
              H.Name, 
			  H.RouteType,
			  R.Name AS RouteTypeName,
			  H.RegionId, 
			  H.IsActive,
			  H.CreatedAt,
			  H.CreatedBy,
			  H.UpdatedAt,
              H.UpdatedBy,
			  H.ManagerId,
			  E.FirstName + ' ' + E.LastName as ManagerName,
			  E.Photo as ManagerPhoto
                          from [dbo].[Headquarter] H

									left join RouteType R
									on H.RouteType = R.Id
									LEFT JOIN Employee E ON E.Id = H.ManagerId


									where (@RouteType = 0 or  H.RouteType = @RouteType)
									and
									(@RegionId = 0 or  H.RegionId = @RegionId)
							   		  
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(R.Name) like '%'+lower(@Search)+'%') 
										
									)
							   )
            

			order by H.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[HeadquarterUpsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[HeadquarterUpsert] 
	@Id int,
	@Name nvarchar(max),
	@RouteType int,
	@RegionId int,
	@ManagerId int = 0,
	@IsActive bit,
	@CreatedBy int,
	@UpdatedBy int
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO  Headquarter
											 (												
												 Name, 
												 RouteType,
												 RegionId, 
												 IsActive,
												 CreatedAt,
												 CreatedBy,
												 ManagerId)
									VALUES (
												@Name, 
												 @RouteType,
												 @RegionId, 
												 @IsActive,
												 GETDATE(),
												 @CreatedBy,
												 @ManagerId											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'Headquarter Data Created Successfully'
				END	
			   ELSE	IF @Id > 0
					BEGIN
							UPDATE  Headquarter

							SET		
							        Name= Isnull (@Name,Name),
									RouteType = @RouteType,
									RegionId = @RegionId,
									IsActive = @IsActive,
									ManagerId = @ManagerId,
                                    UpdatedAt = GETDATE(),
									UpdatedBy = UpdatedBy
									

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Headquarter Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM
				[dbo].[Headquarter]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[JointWorking_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [JointWorking_All] 0,0,'',1,2

CREATE PROCEDURE [dbo].[JointWorking_All]
	@Offset INT,
	@Limit  INT,
    @Search VARCHAR(MAX),
	@TourPlanMonthId int = 0,
	@DoctorAndChemistId int = 0
	as
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
				select COUNT(*) from JointWorking
				where 
				(@TourPlanMonthId = 0 OR TourPlanMonthId = @TourPlanMonthId)
				and
				(@DoctorAndChemistId = 0 OR DoctorAndChemistId = @DoctorAndChemistId)
				)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		SELECT 
			Id,
			TourPlanMonthId,
			DoctorAndChemistId,
			EmployeeIds,
			CreatedDate,
			CreatedBy
		FROM
			[dbo].[JointWorking]

		where 
		(@TourPlanMonthId = 0 OR TourPlanMonthId = @TourPlanMonthId)
		and
		(@DoctorAndChemistId = 0 OR DoctorAndChemistId = @DoctorAndChemistId)


		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[JointWorking_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kartik amipara
-- Create date: 07/14/2021 05:57 PM
-- Description:	This sp use JointWorking multipal data insert
-- =============================================
-- select * from JointWorking
-- truncate table JointWorking
-- exec [JointWorking_Upsert] 0,1,1,'6,5',0
CREATE PROCEDURE [dbo].[JointWorking_Upsert]
        @Id INT = 0,
		@TourPlanMonthId INT = 0,
		@DoctorAndChemistId INT = 0,
		@EmployeeIds varchar(MAX) = null,
		@CreatedBy INT = 0
		
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''

	IF @Id = 0
		BEGIN
			  IF @TourPlanMonthId = 0
				  BEGIN
		  			   SET @Id = 0
		  			   SET @Code = 400
		  			   SET @Message = 'TourPlan Month is required'
				  END
				  ELSE IF @DoctorAndChemistId = 0
				  BEGIN
		  			   SET @Id = 0
		  			   SET @Code = 400
		  			   SET @Message = 'Doctor Or Chemist is required'
				  END
				  ELSE IF @EmployeeIds IS NULL OR @EmployeeIds = ''
				  BEGIN
		  			   SET @Id = 0
		  			   SET @Code = 400
		  			   SET @Message = 'Employees is required'
				  END
			  ELSE
			  BEGIN
			  DECLARE @Total INT = 0, @i int = 0
						SET @Total = (select count(value) from string_split(@EmployeeIds,','));

					PRINT @Total

					DECLARE @Row int = 0
					PRINT @Total
					WHILE @i < @Total
					BEGIN
						DECLARE @EmployeeIdsVar INT

						select @EmployeeIdsVar = CONVERT(INT,value) from  string_split(@EmployeeIds,',') 
						ORDER BY (SELECT NULL) OFFSET @i ROWS FETCH NEXT 1 ROWS ONLY

						
						
						INSERT INTO JointWorking
						(
							TourPlanMonthId,
							DoctorAndChemistId,
							EmployeeIds,
							CreatedDate,
							CreatedBy
						)	
						VALUES
						(
							@TourPlanMonthId,
							@DoctorAndChemistId,
							@EmployeeIdsVar,
							GETUTCDATE(),
							@CreatedBy
						)
						SET @i = @i+1;
					END		
			   SET @Id = SCOPE_IDENTITY()
			   SET @Code = 200
			   SET @Message = 'Joint Working created successfully'
		  END 
       
	END

	SELECT @Code as Code,@Message as [Message]
		Select 
			Id,
			TourPlanMonthId,
			DoctorAndChemistId,
			EmployeeIds,
			CreatedDate,
			CreatedBy
	    FROM 
	        [dbo].[JointWorking]
		WHERE
			Id = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[Leave_ByEmployeeId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE  [dbo].[Leave_ByEmployeeId]
   @Offset bigint = 0,
	@Limit bigint = 0,
	@EmployeeId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [Leave] L

									left join [Employee] E
									on L.EmployeeId = E.Id


									where (@EmployeeId = 0 or  L.EmployeeId = @EmployeeId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select

						L.Id,
						L.EmployeeId,
						E.FirstName + E.LastName as EmployeeFullName,
						E.Mobile,
						E.Email,
						E.EmergencyNumber,
						L.LeaveTypeId,
						L.DayTypeId,
						L.FromDate,
						L.ToDate,
						L.LeaveReason,
						L.CreatedDate,
						L.CreatedBy,
						L.UpdatedDate,
						L.UpdatedBy
				

			from [Leave] L

									left join [Employee] E
									on L.EmployeeId = E.Id


									where (@EmployeeId = 0 or  L.EmployeeId = @EmployeeId)
							   

			order by L.EmployeeId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[Leave_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[Leave_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Leave Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	select  
	                    L.Id,
						L.EmployeeId,
						E.FirstName + E.LastName as EmployeeFullName,
						E.Mobile,
						E.Email,
						E.EmergencyNumber,
						L.LeaveTypeId,
						LT.Name,
						L.DayTypeId,
						D.Name,
						L.FromDate,
						L.ToDate,
						L.LeaveReason,
						L.CreatedDate,
						L.CreatedBy,
						L.UpdatedDate,
						L.UpdatedBy

		from  [Leave] L

		left join [Employee] E
		on L.EmployeeId = E.Id

		left join [LeaveTypeMaster] LT
		on L.LeaveTypeId = LT.Id

		left join [DayTypeMaster] D
		on L.DayTypeId = D.Id

		WHERE
				L.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Leave_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [Leave_Upsert] 0,1,1,1,'demo','demo','test',0,0

CREATE PROCEDURE  [dbo].[Leave_Upsert]

		@Id BIGINT = 0,
		@EmployeeId BIGINT = 0,
		@LeaveTypeId BIGINT = 0,
		@DayTypeId BIGINT = 0,
		@FromDate varchar(250) = null,
		@ToDate varchar(250) = null,
		@LeaveReason varchar(250) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO Leave
												
											 (
												EmployeeId,
												LeaveTypeId,
												DayTypeId,
												FromDate,
												ToDate,
												LeaveReason,
												CreatedDate,
												CreatedBy
											

												)

									VALUES (
												@EmployeeId,
												@LeaveTypeId,
												@DayTypeId,
												@FromDate,
												@ToDate,
												@LeaveReason,
												GETDATE(),
												@createdBy
												

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'Leave Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  Leave

							SET		
							        EmployeeId = @EmployeeId,
									LeaveTypeId = @LeaveTypeId,
									DayTypeId = @DayTypeId,
									FromDate = ISNULL(@FromDate,FromDate),
									ToDate = ISNULL(@ToDate,ToDate),
									LeaveReason = ISNULL(@LeaveReason,LeaveReason),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Leave Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		EmployeeId,
		LeaveTypeId,
		DayTypeId,
		FromDate,
		ToDate,
		LeaveReason,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy
		
		
				
		FROM
				Leave
		WHERE
				Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[LeaveTypeMaster_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Exec LeaveTypeMaster_All '',0,0 
CREATE PROC [dbo].[LeaveTypeMaster_All]
@Search VARCHAR(MAX),
@Offset BIGINT,
@Limit BIGINT
    
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LeaveTypeMaster
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name]
				
		FROM
				[dbo].[LeaveTypeMaster]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[Masters_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Masters_All]
@Offset BIGINT = 0,
@Limit BIGINT= 0,
@Search VARCHAR(MAX) = null
AS
BEGIN
	DECLARE @TotalRecords bigint = 0;

	SET @TotalRecords = (select count(*) from Masters MS
						
						);
	IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
		MS.Id,
		MS.[Name]
	FROM
		Masters MS
	order by 
		MS.Id 
	desc
		OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[MasterWorkAt_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[MasterWorkAt_All]
@Offset BIGINT = 0,
@Limit BIGINT= 0,
@Search VARCHAR(MAX) = null
AS
BEGIN
	DECLARE @TotalRecords bigint = 0;

	SET @TotalRecords = (select count(*) from MasterWorkAt);
	IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
		Id,
		Name,
		CreatedBy,
		CreatedDate
	FROM MasterWorkAt

	order by 
		Id 
	desc
		OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[Mr_Doctor_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Mr_Doctor_All]

@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Doctor
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(FirstName) like '%'+lower(@Search)+'%')and
											(lower(LastName) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				Id,
	            FirstName,
	            LastName,
	            IsActive,
	            Gender,
	            Mobile,
	            Phone,	
	            Birthdate,
	            Anniversary,
	            Photo,
	            Specialization,
	            Qualification,
	            Category,
	            WebSite,
	            Address,	
	            BuisnessPotential,
	            Remarks,
	            CreatedBy,
	            UpdatedBy 
				
		FROM
				[dbo].[Doctor]

		where ( isnull(@Search,'') = '' or
					(
						(lower(FirstName) like '%'+lower(@Search)+'%')and
						(lower(LastName) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)
			    

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- exec [MR_Presentation_All]  '',0,0,0,0

CREATE PROCEDURE [dbo].[MR_Presentation_All]

	@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
	@EmployeeId bigint = 0,
	@DoctorId bigint = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[MR_Presentation] MP

								left join Employee E
								on MP.EmployeeId = E.Id

								left join Doctor D
								on MP.DoctorId = D.Id

								where (@EmployeeId = 0 or  MP.EmployeeId = @EmployeeId) AND
									  (@DoctorId = 0 or  MP.DoctorId= @DoctorId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(E.FirstName) like '%'+lower(@Search)+'%') or
										(lower(E.LastName) like '%'+lower(@Search)+'%') or
										(lower(E.Email) like '%'+lower(@Search)+'%') or
										(lower(E.Mobile) like '%'+lower(@Search)+'%') or
										(lower(E.Phone) like '%'+lower(@Search)+'%') or
										(lower(D.FirstName) like '%'+lower(@Search)+'%') or
										(lower(D.LastName) like '%'+lower(@Search)+'%')
									)
								)
								And
								MP.DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				MP.Id,
				MP.EmployeeId,
				E.FirstName As EmployeeFirstName,
				E.LastName As EmployeeLastName,
				E.Email AS EmployeeEmail,
				E.Mobile As EmployeeMobile,
				E.Phone As EmployeePhone,
				MP.DoctorId,
				D.FirstName As DoctorFirstName,
				D.LastName As DoctorLastName,
				MP.CreatedDate,
				MP.CreatedBy,
				MP.UpdatedDate,
				MP.UpdatedBy,
				MP.DeletedDate,
				MP.DeletedBy
 from [dbo].[MR_Presentation] MP

								left join Employee E
								on MP.EmployeeId = E.Id

								left join Doctor D
								on MP.DoctorId = D.Id

								where (@EmployeeId = 0 or  MP.EmployeeId = @EmployeeId) AND
									  (@DoctorId = 0 or  MP.DoctorId= @DoctorId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(E.FirstName) like '%'+lower(@Search)+'%') or
										(lower(E.LastName) like '%'+lower(@Search)+'%') or
										(lower(E.Email) like '%'+lower(@Search)+'%') or
										(lower(E.Mobile) like '%'+lower(@Search)+'%') or
										(lower(E.Phone) like '%'+lower(@Search)+'%') or
										(lower(D.FirstName) like '%'+lower(@Search)+'%') or
										(lower(D.LastName) like '%'+lower(@Search)+'%')
									)
								)
								And
								MP.DeletedBy = 0

			order by MP.Id

		desc
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  exec  [MR_Presentation_ById] 1

CREATE PROCEDURE [dbo].[MR_Presentation_ById]

	@Id bigint

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
            @Message varchar(200) = ''


			SET @Message = 'MR_Presentation Data Retrived Successfully'

	       SELECT @Code as Code,
				  @Message as [Message]

	select
			
				MP.Id,
				MP.EmployeeId,
				E.FirstName As EmployeeFirstName,
				E.LastName As EmployeeLastName,
				E.Email AS EmployeeEmail,
				E.Mobile As EmployeeMobile,
				E.Phone As EmployeePhone,
				MP.DoctorId,
				D.FirstName As DoctorFirstName,
				D.LastName As DoctorLastName,
				MP.CreatedDate,
				MP.CreatedBy,
				MP.UpdatedDate,
				MP.UpdatedBy,
				MP.DeletedDate,
				MP.DeletedBy
 from [dbo].[MR_Presentation] MP

								left join Employee E
								on MP.EmployeeId = E.Id

								left join Doctor D
								on MP.DoctorId = D.Id
			
		WHERE
				MP.Id = @Id
		
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MR_Presentation_Delete]

		@Id bigint = 0,
		@DeletedBy bigint = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update MR_Presentation 
		
		set DeletedDate = GETDATE() , 
			DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'MR_Presentation Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		select
				
				[Id],
				[EmployeeId],
				[DoctorId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		from
				[dbo].[MR_Presentation]
		where
				Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Products_ByMR_Presentation_Id]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[MR_Presentation_Products_ByMR_Presentation_Id]
@Offset bigint = 0,
	@Limit bigint = 0,
	@MR_Presentation_Id bigint = 0,
	@ProductId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[MR_Presentation_Products] MPR
									left join MR_Presentation MP on MPR.MR_Presentation_Id = MP.Id
									left join Product P on MPR.ProductId = P.Id
									


									where (@MR_Presentation_Id = 0 or  MPR.MR_Presentation_Id = @MR_Presentation_Id)and
									       (@ProductId = 0 or  MPR.ProductId = @ProductId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				MPR.Id,
				MPR.MR_Presentation_Id,
				MP.DoctorId AS MR_PresentationDoctorId,
				MPR.ProductId,
				P.Name AS ProductName,
				MPR.CreatedDate,
				MPR.CreatedBy,
				MPR.UpdatedDate,
				MPR.UpdatedBy
				

			from [dbo].[MR_Presentation_Products] MPR
									left join MR_Presentation MP on MPR.MR_Presentation_Id = MP.Id
									left join Product P on MPR.ProductId = P.Id

			where (@MR_Presentation_Id = 0 or  MPR.MR_Presentation_Id = @MR_Presentation_Id)and
									       (@ProductId = 0 or  MPR.ProductId = @ProductId)
									
							   

			order by MPR.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Products_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[MR_Presentation_Products_Upsert]

 @Id BIGINT = 0 ,
		 @MR_Presentation_Id BIGINT = 0,
	    @ProductId BIGINT  = 0,
	    @CreatedBy BIGINT = 0  ,
	    @UpdatedBy BIGINT  = 0 
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0

					 
					BEGIN
					
							INSERT INTO  MR_Presentation_Products
												
											 (
												MR_Presentation_Id,
												ProductId,
												 CreatedDate,
												 createdBy
												 
												 

												)

									VALUES (
											
											@MR_Presentation_Id,
										    @ProductId,
											GETDATE(),
											@createdBy
											
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'MR_Presentation_Products Data Created Successfully'
              
				END	
				

			
		

				   	ELSE IF @Id > 0
					BEGIN
							UPDATE  MR_Presentation_Products

							SET		
							        MR_Presentation_Id = @MR_Presentation_Id,
									ProductId = @ProductId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'MR_Presentation_Products Data Updated Successfully'
					END

		
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[MR_Presentation_Id],
				[ProductId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
				
		FROM
				[dbo].[MR_Presentation_Products]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[MR_Presentation_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MR_Presentation_Upsert]

		@Id bigint = 0,
		@EmployeeId bigint = 0,
		@DoctorId bigint = 0,
		@CreatedBy int = 0,
		@UpdatedBy int = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT INTO MR_Presentation (EmployeeId,
												         DoctorId,
												         CreatedDate,
												         CreatedBy,
														 DeletedBy )
									VALUES (@EmployeeId,
											@DoctorId,
											GETDATE(),
											@CreatedBy,
											0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'MR_Presentation Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN
					
					BEGIN
							UPDATE MR_Presentation

							SET		EmployeeId = @EmployeeId,
							        DoctorId = @DoctorId,
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'MR_Presentation Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[EmployeeId],
				[DoctorId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[MR_Presentation]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_ByMR_Presentation_ProductsId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[MR_PresentationResources_ByMR_Presentation_ProductsId]

@Offset bigint = 0,
	@Limit bigint = 0,
	@MR_Presentation_ProductsId bigint = 0
	

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[MR_PresentationResources] MPR
									left join Product P on MPR.MR_Presentation_ProductsId = P.Id
									
									


									where (@MR_Presentation_ProductsId = 0 or  MPR.MR_Presentation_ProductsId= @MR_Presentation_ProductsId)
									
							   and
							   DeletedBy = 0
                            )
		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
			    MPR.Id,
				MPR.MR_PresentationId,
				MPR.MR_Presentation_ProductsId,
				P.Name As ProductName,
				MPR.PresentationId,
				MPR.CreatedDate,
				MPR.CreatedBy,
				MPR.UpdatedDate,
				MPR.UpdatedBy,
				MPR.DeletedDate,
				MPR.DeletedBy
				

			from [dbo].[MR_PresentationResources] MPR
									left join Product P on MPR.MR_Presentation_ProductsId = P.Id
									
									


									where (@MR_Presentation_ProductsId = 0 or  MPR.MR_Presentation_ProductsId= @MR_Presentation_ProductsId)
									
							   and
							   DeletedBy = 0
							   

			order by MPR.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_ByMR_PresentationId]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  exec  [MR_PresentationResources_ByMR_PresentationId] 0,0,0

CREATE PROCEDURE [dbo].[MR_PresentationResources_ByMR_PresentationId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@MR_PresentationId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[MR_PresentationResources] MPR

									left join MR_Presentation MP
									on MPR.MR_PresentationId = MP.Id

									left join Presentation P
									on MPR.PresentationId = P.Id

									where (@MR_PresentationId = 0 or  MPR.MR_PresentationId = @MR_PresentationId)
									ANd
									MPR.DeletedBy = 0
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				MPR.Id,
				MPR.MR_PresentationId,
				MP.PresentationName,
				MPR.PresentationId,
				P.FileName AS PresentationFileName,
				P.FileURL AS PresentationFileURL,
				P.FileType As PresentationFileType,
				MPR.CreatedDate,
				MPR.CreatedBy,
				MPR.UpdatedDate,
				MPR.UpdatedBy,
				MPR.DeletedDate,
				MPR.DeletedBy

			FROM [dbo].[MR_PresentationResources] MPR

			left join MR_Presentation MP
			on MPR.MR_PresentationId = MP.Id

			left join Presentation P
			on MPR.PresentationId = P.Id
			
			where (@MR_PresentationId = 0 or  MPR.MR_PresentationId = @MR_PresentationId)
			ANd
		    MPR.DeletedBy = 0

			order by MPR.MR_PresentationId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--  exec  [MR_PresentationResources_Delete]  1,1

CREATE PROCEDURE [dbo].[MR_PresentationResources_Delete]

		@Id bigint = 0,
		@DeletedBy bigint = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update MR_PresentationResources 
		
		set DeletedDate = GETDATE() , 
			DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'MR_PresentationResources Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		select
				[Id],
				[MR_PresentationId],
				[PresentationId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		from
				[dbo].[MR_PresentationResources]
		where
				Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[MR_PresentationResources_Upsert]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  exec  [MR_PresentationResources_Upsert]  1,2,'pqr',1

CREATE PROCEDURE [dbo].[MR_PresentationResources_Upsert]

		@Id bigint = 0,
		@MR_PresentationId bigint = 0,
		@PresentationId bigint = 0,
		@CreatedBy int = 0,
		@UpdatedBy int = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT INTO MR_PresentationResources (MR_PresentationId,
												                  PresentationId,
												                  CreatedDate,
												                  CreatedBy,
														          DeletedBy )
									VALUES (@MR_PresentationId,
											@PresentationId,
											GETDATE(),
											@CreatedBy,
											0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'MR_PresentationResources Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN
					
					BEGIN
							UPDATE MR_PresentationResources

							SET		MR_PresentationId = @MR_PresentationId,
									PresentationId = @PresentationId,
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'MR_PresentationResources Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[MR_PresentationId],
				[PresentationId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[MR_PresentationResources]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Outcome_All]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Outcome_All]


	@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Outcome
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				Outcome

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[Outcome_ById]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Outcome_ById]

	@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Outcome Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				Outcome
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Outcome_Delete]    Script Date: 01-08-2021 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Outcome_Delete]
	@Id bigint,
	@DeletedBy bigint
as
begin
	set nocount on;
	declare @Code int = 400,
	@Message varchar(50) = ''

	update Outcome
	set 
		DeletedDate = GETUTCDATE(),
		DeletedBy = @DeletedBy
	where Id = @Id
	
	set @Code = 200
	set @Message = 'Outcome Deleted Successfully'


	select @Code as Code,
	@Message as Message

	select 
		Id,
		Name,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy
	from Outcome
	where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[Outcome_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from Outcome

--exec [Outcome_Upsert] 2,'veer',0,0

CREATE PROCEDURE [dbo].[Outcome_Upsert]

		@Id BIGINT = 0,
		@Name varchar (500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO Outcome
												
											 (
												 Name,
												 CreatedDate,
												 createdBy,
												 DeletedBy

												)

									VALUES (
											
											@Name,
											GETDATE(),
											@createdBy,
											0

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'Outcome Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  Outcome

							SET		
							        Name= Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Outcome Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				Outcome
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Prescriber_ByDoctorId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE  [dbo].[Prescriber_ByDoctorId]
   @Offset bigint = 0,
	@Limit bigint = 0,
	@DoctorId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[Prescriber] P

									left join Doctor D
									on P.DoctorId = D.Id


									where (@DoctorId = 0 or  P.DoctorId = @DoctorId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select

	    P.Id,
		P.DoctorId,
		D.FirstName AS DoctorFirstName,
	    D.LastName  AS DoctorLastName,
		D.Mobile  AS DoctorMobileNo,
		D.Qualification AS DoctorQualification,
		P.EmployeeId,
		P.CreatedDate,
		P.CreatedBy,
		P.UpdatedDate,
		P.UpdatedBy
				

			from [dbo].[Prescriber] P

									left join Doctor D
									on P.DoctorId = D.Id


						where (@DoctorId = 0 or  P.DoctorId = @DoctorId)
							   

			order by P.DoctorId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[Prescriber_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Prescriber_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Prescriber Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[DoctorId],
				[EmployeeId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
				
		FROM
				[dbo].[Prescriber]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Prescriber_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE  [dbo].[Prescriber_Upsert]

		@Id BIGINT = 0,
		@DoctorId BIGINT = 0,
		@EmployeeId BIGINT = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO Prescriber
												
											 (
												DoctorId,
												EmployeeId,
												CreatedDate,
												CreatedBy
											

												)

									VALUES (
											
												@DoctorId,
												@EmployeeId,
												GETDATE(),
												@createdBy
												

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'Prescriber Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  Prescriber

							SET		
							        DoctorId = @DoctorId,
									EmployeeId = @EmployeeId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Prescriber Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DoctorId,
		EmployeeId,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy
		
		
				
		FROM
				Prescriber
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[PrescriberProducts_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE  [dbo].[PrescriberProducts_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'PrescriberProducts Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		PrescriberId,
		ProductId,
		Price,
		Qty,
		TotalBusiness,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy
		
		
				
		FROM
				PrescriberProducts
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[PrescriberProducts_ByPrescriberId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- select * from Prescriber
-- select * from Doctor
-- select * from Employee

-- exec [PrescriberProducts_ByPrescriberId] 0,0,0

CREATE PROCEDURE  [dbo].[PrescriberProducts_ByPrescriberId]
   @Offset bigint = 0,
	@Limit bigint = 0,
	@PrescriberId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
			@TotalRecords bigint,
            @Message varchar(200) = ''

			SET @TotalRecords = (
									select count(*) from [dbo].[PrescriberProducts] PP

									left join Prescriber P
									on PP.PrescriberId = P.Id

									left join Product PPP
									on PP.ProductId = PPP.Id


									where (@PrescriberId = 0 or  PP.PrescriberId = @PrescriberId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select

	   	PP.Id,
		PP.PrescriberId,
		P.DoctorId AS PrescriberDoctorId,
		P.EmployeeId AS PrescriberEmployeeId,
		PP.ProductId,
		PPP.Name AS ProductName,
		PPP.Quantity AS ProductQuantity,
		PPP.Description As ProductDescription,
		PPP.Price AS ProductPrice,
		PP.Price,
		PP.Qty,
		PP.TotalBusiness,
		PP.CreatedDate,
		PP.CreatedBy,
		PP.UpdatedDate,
		PP.UpdatedBy
		
		
				
		from [dbo].[PrescriberProducts] PP

									left join Prescriber P
									on PP.PrescriberId = P.Id

									left join Product PPP
									on PP.ProductId = PPP.Id


									where (@PrescriberId = 0 or  PP.PrescriberId = @PrescriberId)

						
							   

			order by PP.PrescriberId

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[PrescriberProducts_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE  [dbo].[PrescriberProducts_Upsert]

		@Id BIGINT = 0,
		@PrescriberId BIGINT = 0,
		@ProductId BIGINT = 0,
		@Price Varchar(500) = null,
		@Qty Varchar(500) = null,
		@TotalBusiness Varchar(500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
				
					BEGIN
							INSERT INTO PrescriberProducts
												
											 (
												PrescriberId,
												ProductId,
												Price,
												Qty,
												TotalBusiness,
												CreatedDate,
												CreatedBy
											

												)

									VALUES (
											
												@PrescriberId,
												@ProductId,
												@Price,
												@Qty,
												@TotalBusiness,
												GETDATE(),
												0
												

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'PrescriberProducts Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  PrescriberProducts

							SET		
							        PrescriberId = @PrescriberId,
									ProductId = @ProductId,
                                    Price = ISNULL(@Price,Price),
									Qty = ISNULL(@Qty,Qty),
									TotalBusiness = ISNULL(@TotalBusiness,TotalBusiness),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'PrescriberProducts Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		PrescriberId,
		ProductId,
		Price,
		Qty,
		TotalBusiness,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy
		
		
				
		FROM
				PrescriberProducts
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Presentation_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from Presentation
--Exec [dbo].[Presentation_All] '',0,0
CREATE Proc [dbo].[Presentation_All]


	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT,
	@ProductId int

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Presentation PP

								left join [Product] P
								on PP.ProductId = P.Id
								
								where (@ProductId = 0 or  PP.ProductId = @ProductId)

								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(PP.FileURL) like '%'+lower(@Search)+'%') OR
										(lower(PP.FileName) like '%'+lower(@Search)+'%') OR
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(P.Price) like '%'+lower(@Search)+'%')
									)
								)
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
	                    PP.Id,
						PP.FileURL,
						PP.FileName,
						PP.Size,
						PP.FileType,
						PP.ProductId,
						P.Name AS ProductName,
						P.Price AS ProductPrice,
						P.Quantity AS ProductQuantity,
						PP.CreatedDate,
						PP.CreatedBy,
						PP.UpdatedDate,
						PP.UpdatedBy

		from  [dbo].[Presentation] PP

		left join [Product] P
								on PP.ProductId = P.Id
								
								where (@ProductId = 0 or  PP.ProductId = @ProductId)

								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(PP.FileURL) like '%'+lower(@Search)+'%') OR
										(lower(PP.FileName) like '%'+lower(@Search)+'%') OR
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(P.Price) like '%'+lower(@Search)+'%')
									)
								)

		order by PP.Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[Presentation_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[Presentation_ById] 3
CREATE Proc [dbo].[Presentation_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Presentation Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	select  
	                    PP.Id,
						PP.FileURL,
						PP.FileName,
						PP.Size,
						PP.FileType,
						PP.ProductId,
						P.Name AS ProductName,
						P.Price AS ProductPrice,
						P.Quantity AS ProductQuantity,
						PP.CreatedDate,
						PP.CreatedBy,
						PP.UpdatedDate,
						PP.UpdatedBy

		from  [dbo].[Presentation] PP

		left join [Product] P
		on PP.ProductId = P.Id

		WHERE
				PP.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Presentation_ByProductId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Presentation_ByProductId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@ProductId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [Presentation] P1

									left join [Product] P2
									on P1.ProductId = P2.Id
									
									where (@ProductId = 0 or  @ProductId = P1.ProductId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				P1.Id,
				P1.FileURL,
				P1.FileName,
				P1.Size,
				P1.FileType,
				P1.ProductId,
				P2.Name As ProductName,
				P2.Price As ProductPrice,
				P2.Quantity As ProductQuantity,
				P1.CreatedBy,
				P1.UpdatedDate,
				P1.UpdatedBy

			FROM [dbo].[Presentation] P1

			left join [Product] P2
			on P1.ProductId = P2.Id
			
			where (@ProductId = 0 or  @ProductId = P1.ProductId)
									

			order by P1.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[Presentation_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from  Presentation
--Exec Presentation_Delete 2
CREATE Proc [dbo].[Presentation_Delete]
@ID BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From Presentation

		where ID = @ID

		SET @Code = 200
		SET @Message = 'Presentation Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
	                    Id,
						FileURL,
						FileName,
						Size,
						FileType,
						ProductId,
						CreatedDate,
						CreatedBy,
						UpdatedDate,
						UpdatedBy
				
		FROM
				[dbo].[Presentation]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Presentation_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from Presentation
--Exec Presentation_Upsert 2,"bcd234","Sanjay","14","a2",0,0
CREATE PROCEDURE [dbo].[Presentation_Upsert]
(
	
		@Id BIGINT,
		@FileURL varchar(MAX),
		@FileName varchar(500),
		@Size varchar(500),
		@FileType varchar(500),
		@ProductId int = 0,
		@CreatedBy bigint,
		@UpdatedBy bigint
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE Presentation
	SET
									FileURL = ISNULL(@FileURL,FileURL),
									FileName = ISNULL(@FileURL,FileURL),
									Size = ISNULL(@Size,Size),
									FileType = ISNULL (@FileType,FileType),
									ProductId = @ProductId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @Id
	WHERE
		[Id] = @Id

	SELECT * from Presentation where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].Presentation
           (         
					FileURL,
					FileName,
					Size,
					FileType,
					ProductId,
					CreatedDate,
					CreatedBy)
     VALUES(

					@FileURL,
					@FileName,
					@Size,
					@FileType,
					@ProductId,
					GETDATE(),
					@CreatedBy)


		
	
	SELECT 
	Id,
												FileURL,
												FileName,
												Size,
												FileType,
												ProductId,
												CreatedDate,
												CreatedBy,
												UpdatedDate,
												UpdatedBy
	from Presentation where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[Product_ActInact]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Product_ActInact]
	@Id bigint
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(200) = '',	
			@IsActive BIT

	SELECT @IsActive = IsActive FROM Product WHERE Id = @Id

	IF @IsActive = 1
	BEGIN
	     UPDATE Product SET IsActive = 0 WHERE Id = @Id

		 SET @Code = 200
		 SET @Message = 'Product In-Activated Successfully'
	END
	ELSE
	BEGIN
	    UPDATE Product SET IsActive = 1 WHERE Id = @Id

		 SET @Code = 200
		 SET @Message = 'Product Activated Successfully'
	END
    
	SELECT @Code as Code,
		   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CategoryId],
				[ProductTypeId],
				[Quantity],
				[SIUnitesId],
				[ManufactureId],
				[StoreBelowTemprature],
				[PrescriptionId],
				[NRV],
				[Description],
				[Price],
				[SellingPrice],
				[IsActive],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
		FROM
				[dbo].[Product]
		WHERE
				Id = @Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[Product_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Product_All]

	@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
	@CategoryId bigint = 0,
	@ProductTypeId bigint = 0,
	@SIUnitesId bigint = 0,
	@ManufactureId bigint = 0,
	@PrescriptionId bigint = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords int;

		SET @TotalRecords = (
								select count(*) from [dbo].[Product] P
								
								left join [ProductCategory] PC
								on P.CategoryId = PC.Id
			
								left join [ProductType] PT
								on P.ProductTypeId = PT.Id
			
								left join [QuantitySIUnites] Q
								on P.SIUnitesId = Q.Id
			
								left join [ProductManufacture] PM
								on P.ManufactureId = PM.Id

								left join [ProductPrescription] PP
								on P.PrescriptionId = PP.Id
								
								where (@CategoryId = 0 or  P.CategoryId = @CategoryId) And
									  (@ProductTypeId = 0 or  P.ProductTypeId = @ProductTypeId) AND
									  (@SIUnitesId = 0 or  P.SIUnitesId = @SIUnitesId) AND
									  (@ManufactureId = 0 or  P.ManufactureId = @ManufactureId) AND
									  (@PrescriptionId = 0 or  P.PrescriptionId = @PrescriptionId)

								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(PC.Name) like '%'+lower(@Search)+'%') OR
										(lower(PT.Name) like '%'+lower(@Search)+'%') OR
										(lower(Q.Name) like '%'+lower(@Search)+'%') OR
										(lower(PM.Name) like '%'+lower(@Search)+'%') OR
										(lower(PP.Name) like '%'+lower(@Search)+'%')
									)
								)
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				P.Id,
				P.Name,
				P.CategoryId,
				PC.Name AS CategoyName,
				P.ProductTypeId,
				PT.Name AS ProductTypeName,
				P.Quantity,
				P.SIUnitesId,
				Q.Name AS QuantitySIUnites,
				P.ManufactureId,
				PM.Name As ManufactureName,
				P.StoreBelowTemprature,
				P.PrescriptionId,
				PP.Name As PrescriptionName,
				P.NRV,
				P.Description,
				P.Price,
				P.SellingPrice,
				P.IsActive,
				P.CreatedDate,
				P.CreatedBy,
				P.UpdatedDate,
				P.UpdatedBy,
				(select top(1) [URL] from ProductImages where ProductId = P.Id) as 'ImagePath'

			FROM [dbo].[Product] P
			
			left join [ProductCategory] PC
			on P.CategoryId = PC.Id
			
			left join [ProductType] PT
			on P.ProductTypeId = PT.Id
			
			left join [QuantitySIUnites] Q
			on P.SIUnitesId = Q.Id
			
			left join [ProductManufacture] PM
			on P.ManufactureId = PM.Id

			left join [ProductPrescription] PP
			on P.PrescriptionId = PP.Id
			
			where (@CategoryId = 0 or  P.CategoryId = @CategoryId) And
				  (@ProductTypeId = 0 or  P.ProductTypeId = @ProductTypeId) AND
				  (@SIUnitesId = 0 or  P.SIUnitesId = @SIUnitesId) AND
				  (@ManufactureId = 0 or  P.ManufactureId = @ManufactureId) AND
				  (@PrescriptionId = 0 or  P.PrescriptionId = @PrescriptionId)

			and
			(
				isnull(@Search,'') = '' or
				(
					(lower(P.Name) like '%'+lower(@Search)+'%') OR
					(lower(PC.Name) like '%'+lower(@Search)+'%') OR
					(lower(PT.Name) like '%'+lower(@Search)+'%') OR
					(lower(Q.Name) like '%'+lower(@Search)+'%') OR
					(lower(PM.Name) like '%'+lower(@Search)+'%') OR
					(lower(PP.Name) like '%'+lower(@Search)+'%')
				)
			)
			
			order by P.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[Product_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [Product_ById] 1

CREATE PROCEDURE [dbo].[Product_ById]

	@Id bigint

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
            @Message varchar(200) = ''


			SET @Message = 'Product Data Retrived Successfully'

	       SELECT @Code as Code,
				  @Message as [Message]

	select
				P.Id,
				P.Name,
				P.CategoryId,
				PC.Name AS CategoyName,
				P.ProductTypeId,
				PT.Name AS ProductTypeName,
				P.Quantity,
				P.SIUnitesId,
				Q.Name AS QuantitySIUnites,
				P.ManufactureId,
				PM.Name As ManufactureName,
				P.StoreBelowTemprature,
				P.PrescriptionId,
				PP.Name As PrescriptionName,
				P.NRV,
				P.Description,
				P.Price,
				P.SellingPrice,
				P.IsActive,
				P.CreatedDate,
				P.CreatedBy,
				P.UpdatedDate,
				P.UpdatedBy,
				(select top(1) [URL] from ProductImages where ProductId = P.Id) as 'ImagePath'

			FROM [dbo].[Product] P
			
			left join [ProductCategory] PC
			on P.CategoryId = PC.Id
			
			left join [ProductType] PT
			on P.ProductTypeId = PT.Id
			
			left join [QuantitySIUnites] Q
			on P.SIUnitesId = Q.Id
			
			left join [ProductManufacture] PM
			on P.ManufactureId = PM.Id

			left join [ProductPrescription] PP
			on P.PrescriptionId = PP.Id
			
		WHERE
				P.Id = @Id
		
END
GO
/****** Object:  StoredProcedure [dbo].[Product_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Product_Delete]
	@Id bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''
 
               
				DELETE 
				FROM
					Product
				WHERE
					Id = @Id

				SET @Code = 200
			    SET @Message = 'Product Deleted Successfully'

		  SELECT @Code as Code,
				 @Message as [Message]
   SELECT 
				[Id],
				[Name],
				[CategoryId],
				[ProductTypeId],
				[Quantity],
				[SIUnitesId],
				[ManufactureId],
				[StoreBelowTemprature],
				[PrescriptionId],
				[NRV],
				[Description],
				[Price],
				[SellingPrice],
				[IsActive],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
		FROM
				[dbo].[Product]
		WHERE
				Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Product_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Product_Upsert]

		@Id bigint = 0,
		@Name varchar(500) = null,
		@CategoryId bigint = 0,
		@ProductTypeId bigint = 0,
		@Quantity bigint = 0,
		@SIUnitesId bigint = 0,
		@ManufactureId bigint = 0,
		@StoreBelowTemprature varchar(500) = null,
		@PrescriptionId bigint = 0,
		@NRV varchar(50) = null,
		@Description varchar(MAX) = null,
		@Price decimal(18,2) = 0,
		@SellingPrice decimal(18,2) = 0,
		@CreatedBy bigint = 0,
		@UpdatedBy bigint = 0,
		@IsActive bit

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code bigint = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT into Product (Name,
												 CategoryId,
												 ProductTypeId,
												 Quantity,
												 SIUnitesId,
												 ManufactureId,
												 StoreBelowTemprature,
												 PrescriptionId,
												 NRV,
												 Description,
												 Price,
												 SellingPrice,
												 IsActive,
												 CreatedDate,
												 CreatedBy )

									VALUES (@Name,
											@CategoryId,
											@ProductTypeId,
											@Quantity,
											@SIUnitesId,
											@ManufactureId,
											@StoreBelowTemprature,
											@PrescriptionId,
											@NRV,
											@Description,
											@Price,
											@SellingPrice,
											@IsActive,
											GETDATE(),
											@CreatedBy )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'Product Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE Product

							SET		Name = ISNULL(@Name,Name),
									CategoryId = @CategoryId,
									ProductTypeId = @ProductTypeId,
									Quantity = ISNULL(@Quantity,Quantity),
									SIUnitesId = @SIUnitesId,
									ManufactureId = @ManufactureId,
									StoreBelowTemprature = ISNULL(@StoreBelowTemprature,StoreBelowTemprature),
									PrescriptionId = @PrescriptionId,
									NRV = ISNULL(@NRV,NRV),
									Description = ISNULL(@Description,Description),
									Price = ISNULL(@Price,Price),
									SellingPrice = ISNULL(@SellingPrice,SellingPrice),
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy),
									IsActive = @IsActive

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Product Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CategoryId],
				[ProductTypeId],
				[Quantity],
				[SIUnitesId],
				[ManufactureId],
				[StoreBelowTemprature],
				[PrescriptionId],
				[NRV],
				[Description],
				[Price],
				[SellingPrice],
				[IsActive],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
		FROM
				[dbo].[Product]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Select * from ProductCategory


-- exec [ProductCategory_All]  'Div',0,0

CREATE PROCEDURE [dbo].[ProductCategory_All]

	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from ProductCategory
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]

		from  [dbo].[ProductCategory]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC  [ProductCategory_ById]
--SELECT * from ProductCategory
CREATE PROCEDURE [dbo].[ProductCategory_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ProductCategory Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductCategory]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[ProductCategory_Delete]

@ID BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductCategory

		where ID = @ID

		SET @Code = 200
		SET @Message = 'ProductCategory Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductCategory]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductCategory_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--exec  [dbo].[ProductCategory_Upsert]0,'GO22A'

--select * from ProductCategory


CREATE PROCEDURE [dbo].[ProductCategory_Upsert]

		@Id BIGINT = 0,
		@Name varchar(50) = null,
		@CreatedBy   BIGINT = 0,
		@UpdatedBy BIGINT = 0
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					
					BEGIN
							INSERT INTO ProductCategory(Name,CreatedDate,CreatedBy)
									
									VALUES (@Name,GETDATE(),@CreatedBy)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductCategory Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE ProductCategory
							SET		
									Name = ISNULL(@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductCategory Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]

		FROM
				[dbo].[ProductCategory]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[ProductHighlight_All] '',0,0,0
CREATE Proc [dbo].[ProductHighlight_All]
@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
    @ProductId  bigint
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[ProductHighlight] PH
								
								left join [Product] PI
								on PH.ProductId = PI.Id

								where (@ProductId = 0 or  PH.ProductId = @ProductId) 
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(PH.Name) like '%'+lower(@Search)+'%') 
										
									)
								)
									
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		SELECT 
			PH.Id,
			PH.Name,
			PH.ProductId,
			PI.Name As ProdutName,
			PH.CreatedDate,
			PH.CreatedBy,
			PH.UpdatedDate,
			PH.UpdatedBy

	FROM
				[dbo].[ProductHighlight] PH
			
								left join [Product] PI
								on PH.ProductId = PI.Id
								
								where (@ProductId = 0 or  PH.ProductId = @ProductId) And
			(
				isnull(@Search,'') = '' or
									(
										(lower(PH.Name) like '%'+lower(@Search)+'%') 
										

				)
			)
		
			order by PH.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from ProductHighlight
--Exec [dbo].[ProductHighlight_ById] 4
CREATE Proc [dbo].[ProductHighlight_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Presentation Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT 
			PH.Id,
			PH.Name,
			PH.ProductId,
			PI.Name As ProdutName,
			PH.CreatedDate,
			PH.CreatedBy,
			PH.UpdatedDate,
			PH.UpdatedBy

	FROM
				[dbo].[ProductHighlight] PH
			
								left join [Product] PI
								on PH.ProductId = PI.Id
		WHERE
				PH.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from ProductHighlight
--Exec [dbo].[ProductHighlight_Delete] 2
CREATE Proc [dbo].[ProductHighlight_Delete]
@Id bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''
 
               
				DELETE 
				FROM
					ProductHighlight
				WHERE
					Id = @Id

				SET @Code = 200
			    SET @Message = 'ProductHighlight Deleted Successfully'

		  SELECT @Code as Code,
				 @Message as [Message]
   SELECT 
	Id,
	Name,
	ProductId,
	CreatedDate,
	CreatedBy,
	UpdatedDate,
	UpdatedBy
		FROM
				[dbo].[ProductHighlight]
		WHERE
				Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_DeleteByProductId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from ProductHighlight
--Exec [dbo].[ProductHighlight_Delete] 2
CREATE Proc [dbo].[ProductHighlight_DeleteByProductId]
@Id bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''
 
               
				DELETE 
				FROM
					ProductHighlight
				WHERE
					ProductId = @Id

				SET @Code = 200
			    SET @Message = 'Product Highlight Deleted Successfully'

		  SELECT @Code as Code,
				 @Message as [Message]
   SELECT 
	Id,
	Name,
	ProductId,
	CreatedDate,
	CreatedBy,
	UpdatedDate,
	UpdatedBy
		FROM
				[dbo].[ProductHighlight]
		WHERE
				ProductId = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[ProductHighlight_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from ProductHighlight

--Exec [dbo].[ProductHighlight_Upsert] 4,DineshBhai,4,0,0
CREATE Proc [dbo].[ProductHighlight_Upsert]

		@Id bigint ,
		@Name varchar(500),
		@ProductId bigint,
		@CreatedBy bigint,
		@UpdatedBy bigint

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code bigint = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT into ProductHighlight (Name,
												          ProductId,
												          CreatedDate,
												          CreatedBy )

									VALUES (
									        @Name,
											@ProductId,
											GETDATE(),
											@CreatedBy )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductHighlight Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE ProductHighlight

							SET		Name = ISNULL(@Name,Name),
									ProductId = @ProductId,
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductHighlight Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
	Id,
	Name,
	ProductId,
	CreatedDate,
	CreatedBy,
	UpdatedDate,
	UpdatedBy
		FROM
				[dbo].[ProductHighlight]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductHightlights_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductHightlights
--exec ProductHightlights_All 0,0,'d'

CREATE PROCEDURE [dbo].[ProductHightlights_All]


	@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from ProductHightlights
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductHightlights

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ProductHightLights_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductHightLights
--exec [ProductHightLights_ById] 1


CREATE PROCEDURE [dbo].[ProductHightLights_ById]

	@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ProductHightlights Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductHightlights
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductHightlights_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductHightlights
--exec [ProductHightlights_Delete] 3

CREATE PROCEDURE [dbo].[ProductHightlights_Delete]

@Id BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductHightlights

		where ID = @ID

		SET @Code = 200
		SET @Message = 'ProductHightlights Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductHightlights
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductHightlights_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductHightlights
--exec ProductHightlights_Upsert 3,'dev',0,0

CREATE PROCEDURE [dbo].[ProductHightlights_Upsert]

		@Id BIGINT = 0,
		@Name varchar (500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
					
					BEGIN
							INSERT INTO ProductHightlights
												
											 (
												 Name,
												 CreatedDate,
												 createdBy

												)

									VALUES (
											
											@Name,
											GETDATE(),
											@createdBy

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductHightlights Data Created Successfully'
					END
				

			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  ProductHightlights

							SET		
							        Name= Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductHightlights Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductHightlights
		WHERE
				Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[ProductImages_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--EXEC [ProductImages_All]0,0,'',0,0,0

--SELECT * from MasterProductBrand

CREATE PROCEDURE [dbo].[ProductImages_All] 

@Offset BIGINT = 0,
@Limit BIGINT = 0,
@Search VARCHAR(MAX) = null,
@ProductId  BIGINT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;
	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    from 
			[dbo].[ProductImages] P

			LEFT JOIN [Product] PP
			on P.ProductId = PP.Id

			

		Where 
			(@ProductId = 0 OR P.ProductId = @ProductId)
			AND
			
			(
					isnull(@Search,'') = ''
					or
					(
						lower(PP.Name) like lower('%'+@Search+'%')
						
					)
				)
		)


		IF @Limit = 0
			BEGIN
			     SET @Limit = @TotalRecords
				 IF @Limit = 0
				 BEGIN
				     SET @Limit = 10
				 END
			END
	 
		SELECT 

		    P.Id,
			P.URL,
			P.ProductId,
			PP.Name as ProductName,
			P.CreatedBy,
			P.UpdatedDate,
			P.UpdatedBy

		
		from 
			[dbo].[ProductImages] P

			LEFT JOIN [Product] PP
			on P.ProductId = PP.Id

		Where 
			(@ProductId = 0 OR P.ProductId = @ProductId)
			AND
		
			(
					isnull(@Search,'') = ''
					or
					(
						lower(PP.Name) like lower('%'+@Search+'%')
						
					)
				)
		

		ORDER BY 
		      P.Id
		--DESC
			  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 


	select @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select* from ProductImages

--EXEC [ProductImages_ById]6

CREATE PROCEDURE [dbo].[ProductImages_ById]

@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''
	SET @Code = 200
							SET @Message = 'ProductImages Data Retrive Successfully'
					

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 

		    P.Id,
			P.URL,
			P.ProductId,
			PP.Name as ProductName,
			P.CreatedBy,
			P.UpdatedDate,
			P.UpdatedBy

		
		from 
			[dbo].[ProductImages] P

			LEFT JOIN [Product] PP
			on P.ProductId = PP.Id

		Where 
			 P.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC  [ProductImages_Delete]2
--SELECT * FROM ProductImages

CREATE PROCEDURE [dbo].[ProductImages_Delete]

@Id BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductImages

		where Id = @Id

		SET @Code = 200
		SET @Message = 'ProductImages Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[URL],
				[ProductId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductImages]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductImages_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--exec  [dbo].[ProductImages_Upsert]0,'GO22A'

--select * from ProductImages


CREATE PROCEDURE [dbo].[ProductImages_Upsert]

		@Id BIGINT = 0,
		@URL varchar(50) = null,
		@ProductId BIGINT = 0,
		@CreatedBy   BIGINT = 0,
		@UpdatedBy BIGINT = 0
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					
					BEGIN
							INSERT INTO ProductImages(URL,ProductId,CreatedDate,CreatedBy)
									
									VALUES (@URL,@ProductId,GETDATE(),@CreatedBy)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductImages Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE ProductImages
							SET		
									URL = ISNULL(@URL,URL),
									ProductId = @ProductId,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductImages Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[URL],
				[ProductId],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]

		FROM
				[dbo].[ProductImages]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductManufacture_All]


	@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from ProductManufacture
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') OR
											(lower(Website) like '%'+lower(@Search)+'%') 
											

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[Website],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductManufacture]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%') OR
						(lower(Website) like '%'+lower(@Search)+'%') 
						
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductManufacture_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ProductManufacture Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[Website],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductManufacture]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductManufacture_Delete]

@ID BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductManufacture

		where ID = @ID

		SET @Code = 200
		SET @Message = 'ProductManufacture Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[Name],
				[Website],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductManufacture]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductManufacture_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- eXEC ProductManufacture_Upsert 0,'AJAY','WWW.ANJAYSATHWARA.COM'
--- SELECT * FROM ProductManufacture


CREATE PROCEDURE [dbo].[ProductManufacture_Upsert]

@Id BIGINT = 0,
		@Name varchar (500) = null,
		@Website varchar (500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO ProductManufacture
												
											 (
												 Name,
												 Website,
												 CreatedDate,
												 createdBy
												 

												)

									VALUES (
											
											@Name,
											@Website,
											GETDATE(),
											@createdBy
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductManufacture Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  ProductManufacture

							SET		
							        Name= Isnull (@Name,Name),
									Website= Isnull (@Website,Website),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductManufacture Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[Website],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductManufacture]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductPrescription
--exec ProductPrescription_All 0,0,'u'

CREATE PROCEDURE [dbo].[ProductPrescription_All]


	@Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from ProductPrescription
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductPrescription

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductPrescription
--exec ProductPrescription_ById 1


CREATE PROCEDURE [dbo].[ProductPrescription_ById]

	@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ProductPrescription Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductPrescription
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductPrescription
--exec ProductPrescription_Delete 3

CREATE PROCEDURE [dbo].[ProductPrescription_Delete]

@Id BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductPrescription

		where ID = @ID

		SET @Code = 200
		SET @Message = 'ProductPrescription Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductPrescription
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductPrescription_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPrescription_Upsert]

		@Id BIGINT = 0,
		@Name varchar (500) = null,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			IF @Id = 0
					
					BEGIN
							INSERT INTO ProductPrescription
												
											 (
												 Name,
												 CreatedDate,
												 createdBy

												)

									VALUES (
											
											@Name,
											GETDATE(),
											@createdBy

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductPrescription Data Created Successfully'
					END
				

			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  ProductPrescription

							SET		
							        Name= Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductPrescription Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				ProductPrescription
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductType_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Select * from ProductType


-- exec [ProductType_All]  'Div',0,0

CREATE PROCEDURE [dbo].[ProductType_All]

	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from ProductType
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]

		from  [dbo].[ProductType]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ProductType_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC  [ProductType_ById]1
--SELECT * from ProductType
CREATE PROCEDURE [dbo].[ProductType_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'ProductType Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductType_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [ProductType_Delete] 3
--SELECT * FROM ProductType
CREATE PROCEDURE [dbo].[ProductType_Delete]

@ID BIGINT
		

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		Delete  From ProductType

		where ID = @ID

		SET @Code = 200
		SET @Message = 'ProductType Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductType_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--exec  [dbo].[ProductType_Upsert]0,'GO22A'

--select * from ProductType


CREATE PROCEDURE [dbo].[ProductType_Upsert]

		@Id BIGINT = 0,
		@Name varchar(50) = null,
		@CreatedBy   BIGINT = 0,
		@UpdatedBy BIGINT = 0
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					
					BEGIN
							INSERT INTO ProductType(Name,CreatedDate,CreatedBy)
									
									VALUES (@Name,GETDATE(),@CreatedBy)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductType Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE ProductType
							SET		
									Name = ISNULL(@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductType Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]

		FROM
				[dbo].[ProductType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from ProductVsChemist
--Exec [dbo].[ProductVsChemist_All] '',0,0,3,3

CREATE Proc [dbo].[ProductVsChemist_All]


	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT,
	@ProductId Bigint,
	@ChemistId bigint

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from [ProductVsChemist] PC

								left join [Chemist] C 
								on PC.ChemistId = C.Id

								left join [Product] P
								on PC.ProductId = P.Id
								
								where (@ProductId = 0 or  PC.ProductId = @ProductId)And
									(@ChemistId = 0 or  PC.ChemistId = @ChemistId)

								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(PC.ChemistId) like '%'+lower(@Search)+'%') OR
										(lower(PC.ProductId) like '%'+lower(@Search)+'%') OR
										(lower(C.Name) like '%'+lower(@Search)+'%') OR
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(P.Price) like '%'+lower(@Search)+'%')
									)
								) and PC.DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
	                    PC.Id,
						PC.ChemistId,
						C.Name as ChemistName,
						C.Mobile as ChemistMobileNo,
						C.Phone as ChemistPhone,
						C.Address as ChemistAddress,
						PC.ProductId,
						P.Name AS ProductName,
						P.Price AS ProductPrice,
						P.Quantity AS ProductQuantity,
						PC.CreatedDate,
						PC.CreatedBy,
						PC.UpdatedDate,
						PC.UpdatedBy,
						PC.DeletedDate,
						PC.DeletedBy

		from  [ProductVsChemist] PC

								left join [Chemist] C 
								on PC.ChemistId = C.Id

								left join [Product] P
								on PC.ProductId = P.Id
								
								where (@ProductId = 0 or  PC.ProductId = @ProductId)And
									(@ChemistId = 0 or  PC.ChemistId = @ChemistId)

								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(PC.ChemistId) like '%'+lower(@Search)+'%') OR
										(lower(PC.ProductId) like '%'+lower(@Search)+'%') OR
										(lower(C.Name) like '%'+lower(@Search)+'%') OR
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(P.Price) like '%'+lower(@Search)+'%')
									)
								) and PC.DeletedBy = 0

		order by PC.Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_ByChemistId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductVsChemist_ByChemistId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@ChemistId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [ProductVsChemist] P1

									left join [Chemist] C on P1.ChemistId = C.Id
									left join [Product] P on P1.ProductId = P.Id
									
									where (@ChemistId = 0 or  @ChemistId = P1.ChemistId)
									And P1.DeletedBy = 0
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				P1.Id,
				P1.ChemistId,
				C.Name as ChemistName,
				C.Mobile as ChemistMobileNo,
				C.Phone as ChemistPhone,
				C.Address as ChemistAddress,
				C.ContactPerson as ChemistContactPerson,
				P1.ProductId,
				P1.CreatedDate,
				P1.CreatedBy,
				P1.UpdatedDate,
				P1.UpdatedBy,
				P1.DeletedDate,
				P1.DeletedBy,
				P.Name as 'ProductName',			
				(Select name from ProductCategory where Id = P.CategoryId) as 'CategoyName',
				(Select name from ProductManufacture where Id = P.ManufactureId) as 'ManufactureName',
				(Select name from ProductType where Id = P.ProductTypeId) as 'ProductTypeName',				
				(Select top(1) URL from ProductImages where Id = P.Id) as 'ImagePath'

			FROM [dbo].[ProductVsChemist] P1

			left join [Chemist] C on P1.ChemistId = C.Id
			left join [Product] P on P1.ProductId = P.Id
									
			where (@ChemistId = 0 or  @ChemistId = P1.ChemistId)
				And P1.DeletedBy = 0					

			order by P1.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_ByProductId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductVsChemist_ByProductId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@ProductId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [ProductVsChemist] P1

									left join [Product] P2
									on P1.ProductId = P2.Id
									
									where (@ProductId = 0 or  @ProductId = P1.ProductId)
									And P1.DeletedBy = 0
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				P1.Id,
				P1.ChemistId,
				P1.ProductId,
				P2.Name As ProductName,
				P2.Price As ProductPrice,
				P2.Quantity As ProductQuantity,
				P1.CreatedDate,
				P1.CreatedBy,
				P1.UpdatedDate,
				P1.UpdatedBy,
				P1.DeletedDate,
				P1.DeletedBy,
				P2.Name as 'ProductName',			
				(Select name from ProductCategory where Id = P2.CategoryId) as 'CategoyName',
				(Select name from ProductType where Id = P2.ProductTypeId) as 'ProductTypeName',
				(Select name from ProductManufacture where Id = P2.ManufactureId) as 'ManufactureName',				
				(Select top(1) URL from ProductImages where Id = P2.Id) as 'ImagePath'

			FROM [dbo].[ProductVsChemist] P1

			left join [Product] P2
			on P1.ProductId = P2.Id
			
			where (@ProductId = 0 or  @ProductId = P1.ProductId)
				And P1.DeletedBy = 0					

			order by P1.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductVsChemist
--exec ProductVsChemist_Delete 1,1 

Create procedure [dbo].[ProductVsChemist_Delete]
	@Id bigint,
	@DeletedBy bigint
as
begin
	set nocount on;
	declare @Code int = 400,
	@Message varchar(50) = ''

	update ProductVsChemist
	set 
		DeletedDate = GETUTCDATE(),
		DeletedBy = @DeletedBy
	where Id = @Id
	
	set @Code = 200
	set @Message = 'ProductVsChemist Deleted Successfully'


	select @Code as Code,
	@Message as Message

	select 
		Id,
		ChemistId,
		ProductId,
		Quantity,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy

	from 
		ProductVsChemist
	where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[ProductVsChemist_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductVsChemist

--exec ProductVsChemist_Upsert 3,3,3,3,0,0

CREATE PROCEDURE [dbo].[ProductVsChemist_Upsert]

		@Id BIGINT = 0,
		@ChemistId BIGINT = 0,
		@ProductId BIGINT = 0,
		@Quantity BIGINT = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF Exists(select 1 from ProductVsChemist where ChemistId = @ChemistId and ProductId = @ProductId and (DeletedBy is null or DeletedBy = 0))
				BEGIN
					SET @Id = (select Id from ProductVsChemist where ChemistId = @ChemistId and ProductId = @ProductId and (DeletedBy is null or DeletedBy = 0))
				END  
				
			IF @Id = 0
				
					BEGIN
							INSERT INTO ProductVsChemist
												
											 (
												ChemistId,
												ProductId,
												Quantity,
												CreatedDate,
												CreatedBy,
												DeletedBy

												)

									VALUES (
											
												@ChemistId,
												@ProductId,
												@Quantity,
												GETDATE(),
												@createdBy,
												0

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductVsChemist Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  ProductVsChemist

							SET		
							        ChemistId = @ChemistId,
									ProductId = @ProductId,
									Quantity = @Quantity,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductVsChemist Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		ChemistId,
		ProductId,
		Quantity,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy
				
		FROM
				ProductVsChemist
		WHERE
				Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- EXEC ProductVsDoctor_All '',0,0,0,0

CREATE PROCEDURE [dbo].[ProductVsDoctor_All]

@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
	@DoctorId  bigint = 0,
	@ProductId  bigint = 0
	

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords int;

		SET @TotalRecords = (
								select count(*) from [dbo].[ProductVsDoctor] PD
								
									left join [Doctor] D
									on PD.DoctorId = D.Id

									left join [Product] P
									on PD.ProductId = P.Id
			
							
								
								where (@DoctorId = 0 or  @DoctorId = PD.DoctorId)And
									  	(@ProductId = 0 or  @ProductId = PD.ProductId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(D.FirstName) like '%'+lower(@Search)+'%') OR
										(lower(D.LastName) like '%'+lower(@Search)+'%') OR
										(lower(P.Quantity) like '%'+lower(@Search)+'%') OR
										(lower(D.Mobile) like '%'+lower(@Search)+'%') OR
										(lower(D.Qualification) like '%'+lower(@Search)+'%') OR
										(lower(P.StoreBelowTemprature) like '%'+lower(@Search)+'%')or
										(lower(P.Price) like '%'+lower(@Search)+'%')or
										(lower(P.SellingPrice) like '%'+lower(@Search)+'%')
									)
								)AND
				                PD.DeletedBy = 0
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				PD.Id,
				PD.DoctorId,
				PD.ProductId,
				PD.Quantity,
				D.FirstName As DoctorFirstName,
				D.LastName As DoctorLastName,
				D.Gender As DoctorGender,
				D.Mobile as DoctorMobile,
				D.Birthdate As DoctorBirthday,
				D.Phone As DoctorPhone,
				D.Specialization As DoctorSpecialization,
				D.Qualification as DoctorQualification,
				P.Name As ProductName,
				P.Quantity As ProductQuantity,
				P.StoreBelowTemprature As ProductStoreBelowTemprature,
				P.Price as ProductPrice,
				P.SellingPrice As ProductSellingPrice,
				PD.CreatedBy,
				PD.UpdatedDate,
				PD.UpdatedBy,
				PD.DeletedDate,
				PD.DeletedBy

		from [dbo].[ProductVsDoctor] PD
								
									left join [Doctor] D
									on PD.DoctorId = D.Id

									left join [Product] P
									on PD.ProductId = P.Id
			
							
								
								where (@DoctorId = 0 or  @DoctorId = PD.DoctorId)And
									  	(@ProductId = 0 or  @ProductId = PD.ProductId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(D.FirstName) like '%'+lower(@Search)+'%') OR
										(lower(D.LastName) like '%'+lower(@Search)+'%') OR
										(lower(P.Quantity) like '%'+lower(@Search)+'%') OR
										(lower(D.Mobile) like '%'+lower(@Search)+'%') OR
										(lower(D.Qualification) like '%'+lower(@Search)+'%') OR
										(lower(P.StoreBelowTemprature) like '%'+lower(@Search)+'%')or
										(lower(P.Price) like '%'+lower(@Search)+'%')or
										(lower(P.SellingPrice) like '%'+lower(@Search)+'%')
									)
									AND
				PD.DeletedBy = 0

			)
			
			order by PD.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_ByDoctorId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from Doctor

--EXEC [ProductVsDoctor_ByDoctorId]0,0,1

CREATE PROCEDURE [dbo].[ProductVsDoctor_ByDoctorId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@DoctorId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [ProductVsDoctor] PD

									left join [Doctor] D
									on PD.DoctorId = D.Id
									
									where (@DoctorId = 0 or  @DoctorId = PD.DoctorId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				PD.Id,
				PD.DoctorId,
				PD.ProductId,
				PD.Quantity,
				P.Name as 'ProductName',			
				(Select name from ProductCategory where Id = P.CategoryId) as 'CategoyName',
				(Select name from ProductType where Id = P.ProductTypeId) as 'ProductTypeName',
				(Select name from ProductManufacture where Id = P.ManufactureId) as 'ManufactureName',				
				(Select top(1) URL from ProductImages where Id = P.Id) as 'ImagePath',
				D.FirstName As DoctorFirstName,
				D.LastName As DoctorLastName,
				D.Gender As DoctorGender,
				D.Mobile as DoctorMobile,
				D.Birthdate As DoctorBirthday,
				D.Phone As DoctorPhone,
				D.Specialization As DoctorSpecialization,
				D.Qualification as DoctorQualification,
				PD.CreatedBy,
				PD.UpdatedDate,
				PD.UpdatedBy,
				PD.DeletedDate,
				PD.DeletedBy

			FROM [dbo].[ProductVsDoctor] PD

			left join [Doctor] D on PD.DoctorId = D.Id
			left join [Product] P on P.Id = PD.ProductId
									
									where (@DoctorId = 0 or  @DoctorId = PD.DoctorId)			

			order by PD.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_ByProductId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Product

--SELECT * from ProductVsDoctor

--EXEC [ProductVsDoctor_ByProductId]0,0,3

CREATE PROCEDURE [dbo].[ProductVsDoctor_ByProductId]

	@Offset bigint = 0,
	@Limit bigint = 0,
	@ProductId bigint = 0

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	       
			@TotalRecords bigint
          

			SET @TotalRecords = (
									select count(*) from [ProductVsDoctor] PD

									left join [Product] P on PD.ProductId = P.Id
									left join [Doctor] D on PD.DoctorId = D.Id
									Left join Qualification Q on Q.Id = D.Qualification
									Left join Specialization H on H.Id = D.Specialization
									
									where (@ProductId = 0 or  @ProductId = PD.ProductId)
									
							   )

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END


	select
				PD.Id,
				PD.DoctorId,
				PD.ProductId,
				PD.Quantity,
				P.Name as 'ProductName',			
				(Select name from ProductCategory where Id = P.CategoryId) as 'CategoyName',
				(Select name from ProductManufacture where Id = P.ProductTypeId) as 'ProductTypeName',
				(Select name from ProductCategory where Id = P.ManufactureId) as 'ManufactureName',				
				(Select top(1) URL from ProductImages where Id = P.Id) as 'ImagePath',
				P.Name As ProductName,
				P.Quantity As ProductQuantity,
				P.StoreBelowTemprature As ProductStoreBelowTemprature,
				P.Price as ProductPrice,
				P.SellingPrice As ProductSellingPrice,
				PD.CreatedBy,
				PD.UpdatedDate,
				PD.UpdatedBy,
				PD.DeletedDate,
				PD.DeletedBy,
				D.FirstName As DoctorFirstName,
				D.LastName As DoctorLastName,
				D.Gender As DoctorGender,
				D.Mobile as DoctorMobile,
				D.Birthdate As DoctorBirthday,
				D.Phone As DoctorPhone,
				D.Specialization As DoctorSpecialization,
				H.Name As DoctorSpecializationName,
				D.Qualification as DoctorQualification,
				Q.Name As DoctorQualificationName,
				D.IsActive as DoctorStatus,
				D.Photo as DoctorImage

				from [ProductVsDoctor] PD

									left join [Product] P on PD.ProductId = P.Id
									left join [Doctor] D on PD.DoctorId = D.Id
									Left join Qualification Q on Q.Id = D.Qualification
									Left join Specialization H on H.Id = D.Specialization			
									
									where (@ProductId = 0 or  @ProductId = PD.ProductId)
									
			order by PD.Id

			OFFSET @offset rows fetch next @Limit rows only

		   select @TotalRecords as TotalRecords
		
END
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductVsDoctor
--exec ProductVsDoctor_Delete 1,1 

CREATE procedure [dbo].[ProductVsDoctor_Delete]
	@Id bigint,
	@DeletedBy bigint
as
begin
	set nocount on;
	declare @Code int = 400,
	@Message varchar(50) = ''

	--update ProductVsDoctor
	--set 
	--	DeletedDate = GETUTCDATE(),
	--	DeletedBy = @DeletedBy
	Delete from ProductVsDoctor where Id = @Id
	
	set @Code = 200
	set @Message = 'ProductVsDoctor Deleted Successfully'


	select @Code as Code,
	@Message as Message

	select 
		Id,
		DoctorId,
		ProductId,
		Quantity,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy

	from 
		ProductVsDoctor
	where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[ProductVsDoctor_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from ProductVsDoctor

--exec ProductVsDoctor_Upsert 0,2,2,0,0

CREATE PROCEDURE [dbo].[ProductVsDoctor_Upsert]

		@Id BIGINT = 0,
		@DoctorId BIGINT = 0,
		@ProductId BIGINT = 0,
		@Quantity BIGINT = 0,
	    @CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF Exists(select 1 from ProductVsDoctor where DoctorId = @DoctorId and ProductId = @ProductId and (DeletedBy is null or DeletedBy = 0))
				BEGIN
					SET @Id = (select Id from ProductVsDoctor where DoctorId = @DoctorId and ProductId = @ProductId and (DeletedBy is null or DeletedBy = 0))
				END
				
			IF @Id = 0
				
					BEGIN
							INSERT INTO ProductVsDoctor
												
											 (
												DoctorId,
												ProductId,
												Quantity,
												CreatedDate,
												CreatedBy,
												DeletedBy

												)

									VALUES (
											
												@DoctorId,
												@ProductId,
												@Quantity,
												GETDATE(),
												@createdBy,
												0

											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'ProductVsDoctor Data Created Successfully'
					END
				
				
			 ELSE If @Id > 0
		

					
					BEGIN
							UPDATE  ProductVsDoctor

							SET		
							        DoctorId = @DoctorId,
									ProductId = @ProductId,
									Quantity = @Quantity,
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'ProductVsDoctor Data Updated Successfully'
					END

			

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		Id,
		DoctorId,
		ProductId,
		Quantity,
		CreatedDate,
		CreatedBy,
		UpdatedDate,
		UpdatedBy,
		DeletedDate,
		DeletedBy
				
		FROM
				ProductVsDoctor
		WHERE
				Id = @Id

END

GO
/****** Object:  StoredProcedure [dbo].[Qualification_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Qualification_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].Qualification WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[QualificationSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[QualificationSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM Qualification WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
   
SELECT * FROM Qualification WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%'
	ORDER BY Id Desc
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[QualificationUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[QualificationUpsert] (
	@Id int,
	@Name nvarchar(max),		
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE Qualification
	SET
		Name = @Name,		
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from Qualification where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].Qualification
           (Name,
           [CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,			
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from Qualification where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[QuantitySIUnites_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[QuantitySIUnites_All]

   @Offset BIGINT,
	@Limit BIGINT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from QuantitySIUnites
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
											

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[QuantitySIUnites]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[QuantitySIUnites_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[QuantitySIUnites_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'QuantitySIUnites_ById Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[ProductManufacture]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[QuantitySIUnites_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC QuantitySIUnites_Upsert 3,'RAHUL'

CREATE PROCEDURE [dbo].[QuantitySIUnites_Upsert]

@Id BIGINT = 0,
		@Name varchar (500) = null,
	    @CreatedBy int = 0,
	    @UpdatedBy int = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO QuantitySIUnites
												
											 (
												 Name,
												 CreatedDate,
												 createdBy
												 

												)

									VALUES (
											
											@Name,
											GETDATE(),
											@createdBy
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'QuantitySIUnites Data Created Successfully'
				END	
				

			
		

					ELSE IF @Id > 0
					BEGIN
							UPDATE  QuantitySIUnites

							SET		
							        Name= Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'QuantitySIUnites Data Updated Successfully'
					END

			
			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
		FROM
				[dbo].[QuantitySIUnites]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Region_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hassan Mansuri
-- Create date: 20 May 2021 01:00 PM
-- Description:	This sp is used for get the Admin data by id
-- =============================================
-- exec [Admin_ById] 1
CREATE PROCEDURE [dbo].[Region_ById] 
	@Id BIGINT
AS
BEGIN

	SET NOCOUNT ON;

	Declare 
	        @Code BIGINT = 200,
            @Message varchar(200) = ''

	SET @Message = 'Data retrived successfully'

	       Select @Code as Code,
			      @Message as [Message]

		-- Admin data get for id

		Select 
			R.*,
			ISNULL(E.FirstName,'') + ' ' + ISNULL(E.LastName,'') as ManagerName,
			(select count(*) from Headquarter where RegionId = R.Id) as NumberOfHeadquarter,
			(select count(*) from Doctor D left join Address A on A.Id = D.Address where A.Region =  R.Id) as NumberOfDoctor,
			(select count(*) from Chemist where Region = R.Id) as NumberOfChemist
		FROM [dbo].[Region] R
		LEFT JOIN Employee E ON E.Id = R.ManagerId
		WHERE R.Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[RegionSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Exec EmailMarketing_All 0,0,'',0

CREATE  PROCEDURE [dbo].[RegionSelectAll]
@Offset BIGINT = 0,
@Limit BIGINT = 0,
@Search VARCHAR(MAX) = null

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;
	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    from 
			[dbo].[Region] EM
			Where 
			(ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
		)
		
        IF @Limit = 0
			BEGIN
			     SET @Limit = @TotalRecords
				 IF @Limit = 0
				 BEGIN
				     SET @Limit = 10
				 END
			END
	 
		Select 
			R.*,
			ISNULL(E.FirstName,'') + ' ' + ISNULL(E.LastName,'') as ManagerName,
			(select count(*) from Headquarter where RegionId = R.Id) as NumberOfHeadquarter,
			(select count(*) from Doctor D left join Address A on A.Id = D.Address where A.Region =  R.Id) as NumberOfDoctor,
			(select count(*) from Chemist where Region = R.Id) as NumberOfChemist
		FROM [dbo].[Region] R
		LEFT JOIN Employee E ON E.Id = R.ManagerId
		Where 
			(ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
		ORDER BY 
		     Id
		--DESC
			  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 


	select @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[RegionUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from Region
-- truncate table Region
-- exec [RegionUpsert] 3,'Vadodara',0,0,0
CREATE PROCEDURE [dbo].[RegionUpsert]
	@Id int = 0,
	@Name nvarchar(max) = null,		
	@CreatedBy int = 0,
	@IsActive bit,
	@ManagerId int = 0,
	@UpdatedBy int = 0
		
AS
BEGIN

			SET NOCOUNT ON;

	DECLARE @Code int = 400,
			@Message varchar(500) = ''
               
	IF @Id = 0
		BEGIN
			INSERT INTO Region
				(
					Name,
					IsActive,
					CreatedAt,
					CreatedBy,
					ManagerId
				)
				VALUES (
					@Name,	
					1,
					GETDATE(),
					@CreatedBy,
					@ManagerId
				)

				SET @Id = SCOPE_IDENTITY()
				SET @Code = 200
				SET @Message = 'Region Data Created Successfully'
		END
				
				
	ELSE If @Id > 0
	BEGIN 			
		UPDATE  Region

		SET		
			Name = @Name,
			IsActive  = @IsActive,  
			UpdatedAt = GETDATE(),
			UpdatedBy = @UpdatedBy,
			ManagerId = @ManagerId

		WHERE   
			Id = @Id

		SET @Code = 200
		SET @Message = 'Region Data Updated Successfully'
	END

			

	SELECT @Code as Code,
		   @Message as [Message]

	SELECT 
		*
	FROM
		Region
	WHERE
		Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[RoleSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [RoleSelectAll] '',0,0
CREATE PROCEDURE [dbo].[RoleSelectAll]

	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Role
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
				Id,
				Name,
				Details,
				CreatedAt,
				CreatedBy,
				UpdatedAt,
				UpdatedBy

		from  [dbo].[Role]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[RoleUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RoleUpsert] (
	@Id int,
	@Name nvarchar(max),		
	@Details nvarchar(max),
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE Role
	SET
		Name = @Name,
		Details = @Details,
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from Role where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].Role
           (Name,
		   Details,
           [CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,
			@Details,
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from Role where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[RouteSelect]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[RouteSelect] (
@Id int
)
AS

SET NOCOUNT ON

DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]
 
SET NOCOUNT ON

SELECT
		R.*,
		Rt.Name as 'RouteTypeName'	
	FROM Route R
		Left join RouteType Rt on Rt.Id = R.Type
	WHERE R.Id = @Id
GO
/****** Object:  StoredProcedure [dbo].[RouteSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RouteSelectAll]

	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@RegionId INT =0,
	@RouteTypeId INT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Route
								
								where (@RegionId = 0 or  RegionId = @RegionId) and
									  (@RouteTypeId = 0 or  Type = @RouteTypeId) and
										( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  Id,
				Name,
				IsActive,
				RouteStart,
				RouteEnd,
				Type,
				(select Name from RouteType where Id = Type) as RouteTypeName,
				Distance,
				TotalFair,
				CreatedAt,
				CreatedBy,
				UpdatedAt,
				UpdatedBy

		from  [dbo].[Route]

		where (@RegionId = 0 or  RegionId = @RegionId) and
				(@RouteTypeId = 0 or  Type = @RouteTypeId) and
				( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[RouteTypeSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [RouteTypeSelectAll] '',0,0
CREATE PROCEDURE [dbo].[RouteTypeSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM RouteType WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')

IF @Limit = 0
			BEGIN
			     SET @Limit = @TotalRecords
				 IF @Limit = 0
				 BEGIN
				     SET @Limit = 10
				 END
			END

   
SELECT * FROM RouteType WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%'
	ORDER BY Id Desc
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[RouteTypeUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RouteTypeUpsert] (
	@Id int,
	@Name nvarchar(max),		
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE RouteType
	SET
		Name = @Name,		
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from RouteType where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].RouteType
           (Name,
           [CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,			
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from RouteType where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[RouteUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RouteUpsert] (
	@Id int,
	@Name nvarchar(max),
	@IsActive bit,
	@RouteStart nvarchar(max),
	@RouteEnd nvarchar(max),
	@Type int,
	@Distance nvarchar(50),
	@TotalFair decimal(18,2),
	@RegionId int,
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE Route
	SET
		Name = @Name,	
		IsActive = @IsActive,
		RouteStart = @RouteStart,
		RouteEnd = @RouteEnd,
		Type = @Type,
		Distance = @Distance,
		TotalFair = @TotalFair,
		RegionId = @RegionId,
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from Route where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].Route
           (Name,
		   IsActive,
		   RouteStart,
		   RouteEnd,
		   Type,
		   Distance,
		   TotalFair,
		   RegionId,
           [CreatedAt],
           [CreatedBy])
     VALUES(
			@Name,
			@IsActive,
			@RouteStart,
			@RouteEnd,
			@Type,
			@Distance,
			@TotalFair,
			@RegionId,
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from Route where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[Specialization_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Specialization_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].Specialization WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[SpecializationSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpecializationSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM Specialization WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
   
SELECT * FROM Specialization WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%'
	ORDER BY Id Desc
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[SpecializationUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpecializationUpsert] (
	@Id int,
	@Name nvarchar(max),		
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE Specialization
	SET
		Name = @Name,		
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from Specialization where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].Specialization
           (Name,
           [CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,			
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from Specialization where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[State_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[State_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].State WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [StateRegionHq_All] '',0,0,0,0
-- select * from StateRegionHq 
CREATE PROCEDURE [dbo].[StateRegionHq_All]

	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@ParentId int,
    @GrandParentId int

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
				select COUNT(*) from StateRegionHq
				where 
				(@ParentId = ParentId) 
				and
		        (@GrandParentId = GrandParentId)
				and 
				IsDeleted = 0
		   )

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
			Id,
			Name,
			ParentId,
			GrandParentId,
			IsDeleted,
            CreatedDate,
            CreatedBy,
            UpdatedDate,
            UpdatedBy,
            DeletedDate,
            DeletedBy

		from  [dbo].[StateRegionHq]

		where 
		(@ParentId = ParentId) 
		and
		(@GrandParentId = GrandParentId)
		and 
		IsDeleted = 0

		order by Id

		desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_AllRegion]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [StateRegionHq_AllRegion]
CREATE PROCEDURE [dbo].[StateRegionHq_AllRegion]

AS
BEGIN
SET NOCOUNT ON;
DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
				select COUNT(*) from StateRegionHq
				where 
				
				IsDeleted = 0
		   )


		 
declare 
@TempTable table
(
	Id int,
	Name varchar(200),
	ParentId int,
	GrandParentId int,
	IsDeleted bit,
	CReatedDate Datetime,
	CreatedBy int,
	UpdatedDate datetime,
	UpdatedBy int,
	DeletedDate datetime,
	DeletedBy int
)
declare @StateId int = 0
declare @CityId int = 0
declare @AreaId int = 0
declare StateCursor CURSOR FOR  select Id from StateRegionHq where ParentId = 0 and GrandParentId = 0

OPen StateCursor
	Fetch next from StateCursor into @StateId
	
	while(@@FETCH_STATUS=0)  
	BEGIN
		insert into @TempTable
		select * from StateRegionHq where Id = @StateId

		declare CityCursor CURSOR FOR  select Id from StateRegionHq where ParentId  = @StateId and GrandParentId = 0

		OPen CityCursor
		Fetch next from CityCursor into @CityId
		while(@@FETCH_STATUS=0)  
		BEGIN	
			insert into @TempTable
			select * from StateRegionHq where Id = @CityId	

			declare AreaCursor CURSOR FOR  select Id from StateRegionHq where ParentId  = 0 and GrandParentId = @CityId
			OPen AreaCursor
			Fetch next from AreaCursor into @AreaId
			while(@@FETCH_STATUS=0)  
			BEGIN	
				insert into @TempTable
				select * from StateRegionHq where Id = @AreaId	
				Fetch next from AreaCursor into @AreaId
			END
			CLOSE AreaCursor
			DEALLOCATE  AreaCursor
			Fetch next from CityCursor into @CityId
		END
		CLOSE CityCursor
		DEALLOCATE  CityCursor
		Fetch next from StateCursor into @StateId
	END


CLOSE StateCursor
DEALLOCATE StateCursor

select * from @TempTable

select  @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [StateRegionHq_ById] 4
CREATE PROCEDURE [dbo].[StateRegionHq_ById] 
	@Id int
AS
BEGIN

	SET NOCOUNT ON;

	Declare 
	        @Code BIGINT = 200,
            @Message varchar(200) = ''

	SET @Message = ' StateRegionHq Data retrived successfully'

	       Select @Code as Code,
			      @Message as [Message]

		-- Admin data get for id

		select 
	     	Id,
			Name,
			ParentId,
			GrandParentId,
			IsDeleted,
            CreatedDate,
            CreatedBy,
            UpdatedDate,
            UpdatedBy,
            DeletedDate,
            DeletedBy
	 
	 from
	      [dbo].[StateRegionHq] 
	      where
		  Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[StateRegionHq_Delete]
	@Id bigint,
	@DeletedBy bigint
as
begin
	set nocount on;
	declare @Code int = 400,
	@Message varchar(50) = ''

	update StateRegionHq
	set 
		DeletedDate = GETUTCDATE(),
		DeletedBy = @DeletedBy,
		IsDeleted = 1
	where Id = @Id
	
	set @Code = 200
	set @Message = 'StateRegionHq Deleted Successfully'


	select @Code as Code,
	@Message as Message

	SELECT 
				[Id],
			    [Name],
				[ParentId],
				[GrandParentId],
				[IsDeleted],
             [CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
	from 
		StateRegionHq
	where Id = @Id
end

GO
/****** Object:  StoredProcedure [dbo].[StateRegionHq_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [StateRegionHq_Upsert] 0,'Surat',1,0
-- select * from StateRegionHq
CREATE PROCEDURE [dbo].[StateRegionHq_Upsert]

		@Id int = 0,
		@Name  varchar (500) = null,
		@ParentId int = 0,
		@GrandParentId int = 0,
		@CreatedBy int = 0,
	    @UpdatedBy int = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message  nvarchar(500) = ''

			IF @Id = 0

			BEGIN  
			        IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
				    
			    	ELSE
					BEGIN
							INSERT INTO StateRegionHq(
							                     Name,
												 ParentId,
												 GrandParentId,
												CreatedDate,
                                                 CreatedBy,
												 IsDeleted
                                               )
									VALUES ( @Name,
											@ParentId,
											@GrandParentId,
											GETDATE(),
                                              @CreatedBy,
											  0
                                                ) 

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' Region created successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN
				
				    IF @Name IS null or @Name = ''
					BEGIN
							
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
				
					ELSE
					BEGIN
							UPDATE StateRegionHq
							SET		
									  Name = ISNULL(@Name,Name),
									  ParentId = @ParentId,
									  GrandParentId = @GrandParentId,

									UpdatedDate = GETDATE(),
									UpdatedBy  = @UpdatedBy

									
							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Region updated successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
			    [Name],
				[ParentId],
				[GrandParentId],
				[IsDeleted],
             [CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				StateRegionHq
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[StateSelectAll]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[StateSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM [State] WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%')
   
SELECT * FROM [State] WHERE ISNULL(@Search,'') = '' OR Name like '%'+@Search+'%'
	ORDER BY Id Desc
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[StateUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[StateUpsert] (
	@Id int,
	@Name nvarchar(max),		
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE State
	SET
		Name = @Name,		
		[UpdatedBy] = @UpdatedBy,
		[UpdatedAt] = getdate()
	WHERE
		[Id] = @Id

	SELECT * from State where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].State
           (Name,
           [CreatedAt]
           ,[CreatedBy])
     VALUES(
			@Name,			
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from State where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TourPlan_All]

@Offset INT,
	@Limit  INT,
    @Search VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from TourPlan
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Month) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
				[Id],
				[Month],
				[SubmittedDate],
				[LastDate],
				[IsSubmitted],
			    [CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
				
		FROM
				[dbo].[TourPlan]
		


		where ( isnull(@Search,'') = '' or
					(
						(lower(Month) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)
			     

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TourPlan_ById]
@Id INT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'TourPlan Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

		SELECT 
				[Id],
				[Month],
				[SubmittedDate],
				[LastDate],
				[IsSubmitted],
			    [CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
				
				
		FROM
				[dbo].[TourPlan]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_IsSubmitted]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [TourPlan_IsSubmitted] 1,1
CREATE PROCEDURE [dbo].[TourPlan_IsSubmitted]
	@Id INT = 0,
	@IsSubmitted bit = 0
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Code INT = 400,
	        @Message VARCHAR(200) = ''

			IF @IsSubmitted = 1 AND (select count(*) from TourPlanMonth where WorkAtId IS NUll) > 0
			BEGIN
		  	    SET @Code = 400
		  	    SET @Message = 'Please create all tour plan !'
			END
			ELSE
				BEGIN
				UPDATE TourPlan SET IsSubmitted  = @IsSubmitted 
				WHERE 
				Id = @Id
				SET @Code = 200
				--SET @Message = 'Status updated successfully'
			END
		
    
	SELECT @Code as Code,@Message as [Message]

	SELECT
		[Id],
		[Month],
		[SubmittedDate],
		[LastDate],
		[IsSubmitted],
		[CreatedDate],
		[CreatedBy],
		[UpdatedDate],
		[UpdatedBy]
	FROM
	    [dbo].[TourPlan]
	WHERE 
	    Id = @Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[TourPlan_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC TourPlan_Upsert 0,'June','18-07-2021', '20-07-2021'
-- truncate table TourPlan
CREATE PROCEDURE [dbo].[TourPlan_Upsert]
@Id INT = 0 ,
@Month varchar (100) = NULL  ,
@SubmittedDate varchar (100) = NULL  ,
@LastDate varchar (100) = NULL  ,
@CreatedBy INT = 0,
@UpdatedBy INT  = 0 
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
			IF @Id = 0
				BEGIN
					INSERT INTO  TourPlan
					(
						Month,
						SubmittedDate,
						LastDate,
						IsSubmitted,
						CreatedDate,
						CreatedBy
					)
					VALUES (
											
						@Month,
						@SubmittedDate,
						@LastDate,
						0,
						GETDATE(),
						@CreatedBy
					)

				SET @Id = SCOPE_IDENTITY()
				SET @Code = 200
				SET @Message = 'TourPlan Created Successfully'
			END	
			ELSE IF @Id > 0
				BEGIN
					UPDATE  TourPlan
					SET		
						Month= Isnull (@Month,Month),
						SubmittedDate= Isnull (@SubmittedDate,SubmittedDate),
						LastDate= Isnull (@LastDate,LastDate),
						UpdatedDate = GETDATE(),
						UpdatedBy = @UpdatedBy

					WHERE   Id = @Id

					SET @Code = 200
					SET @Message = 'TourPlan Updated Successfully'
				END

		
	SELECT @Code as Code,@Message as [Message]
	SELECT 
		[Id],
		[Month],
		[SubmittedDate],
		[LastDate],
		[IsSubmitted],
		[CreatedDate],
		[CreatedBy],
		[UpdatedDate],
		[UpdatedBy]
	FROM
		[dbo].[TourPlan]
	WHERE
		Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[TourPlanMonth_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [TourPlanMonth_All] 0,0,'',1

CREATE PROCEDURE [dbo].[TourPlanMonth_All]
	@Offset INT,
	@Limit  INT,
    @Search VARCHAR(MAX),
	@TourPlanId int = 0
	as
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
				select COUNT(*) from TourPlanMonth
								
				where 
				(@TourPlanId = 0 OR TourPlanId = @TourPlanId)
				and
				( isnull(@Search,'') = '' or
						(
							(lower(Day) like '%'+lower(@Search)+'%') 
						)	
					)
				)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		SELECT 
			Id,
			(select top(1)EmployeeIds from JointWorking where TourPlanMonthId = Id) as TPM_EmployeeId,
			(select FirstName from Employee where Id = (select top(1) EmployeeIds from JointWorking where TourPlanMonthId = Id)) as TPM_EmployeeFirstName,
			(select LastName from Employee where Id = (select top(1) EmployeeIds from JointWorking where TourPlanMonthId = Id)) as TPM_EmployeeLastName,
			(select Photo from Employee where Id = (select top(1) EmployeeIds from JointWorking where TourPlanMonthId = Id)) as TPM_EmployeePhoto,
			TourPlanId,
			Date,
			Day,
			WorkAtId,
			(select Name from RouteType where Id = WorkAtId) as WorkAtName,
			WorkTypeId,
			(select Name from WorkType where Id = WorkTypeId) as WorkTypeName,
			WorkRouteIds,
			(select STRING_AGG(Name,', </br>') from Route where Id in (select value from STRING_SPLIT(WorkRouteIds,','))) as WorkRouteIdsName,
			UpdatedDate,
			UpdatedBy
		FROM
			[dbo].[TourPlanMonth]

		where 
		(@TourPlanId = 0 OR TourPlanId = @TourPlanId)
		and
		( isnull(@Search,'') = '' or
					(
						(lower(Day) like '%'+lower(@Search)+'%') 
					)
			 )
			     

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[TourPlanMonth_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hassan Mansuri
-- Create date: 20 May 2021 01:00 PM
-- Description:	This sp is used for get the Admin data by id
-- =============================================
-- exec [TourPlanMonth_ById] 1
CREATE PROCEDURE [dbo].[TourPlanMonth_ById] 
	@Id BIGINT
AS
BEGIN

	SET NOCOUNT ON;

	Declare 
	        @Code BIGINT = 200,
            @Message varchar(200) = ''

	SET @Message = 'Data retrived successfully'

	       Select @Code as Code,
			      @Message as [Message]

		-- Admin data get for id

		Select 
			Id,
			(select top(1)EmployeeIds from JointWorking where TourPlanMonthId = Id) as TPM_EmployeeId,
			(select FirstName from Employee where Id = (select top(1) EmployeeIds from JointWorking where TourPlanMonthId = Id)) as TPM_EmployeeFirstName,
			(select LastName from Employee where Id = (select top(1) EmployeeIds from JointWorking where TourPlanMonthId = Id)) as TPM_EmployeeLastName,
			(select Photo from Employee where Id = (select top(1) EmployeeIds from JointWorking where TourPlanMonthId = Id)) as TPM_EmployeePhoto,
			TourPlanId,
			Date,
			Day,
			WorkAtId,
			(select Name from RouteType where Id = WorkAtId) as WorkAtName,
			WorkTypeId,
			(select Name from WorkType where Id = WorkTypeId) as WorkTypeName,
			WorkRouteIds,
			(select STRING_AGG(Name,', ') from Route where Id in (select value from STRING_SPLIT(WorkRouteIds,','))) as WorkRouteIdsName,
			UpdatedDate,
			UpdatedBy
		FROM [dbo].[TourPlanMonth]
		
		WHERE Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[TourPlanMonth_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [TourPlanMonth_Upsert] 1,2,2,'7,8',0
CREATE PROCEDURE [dbo].[TourPlanMonth_Upsert]
		@Id INT = 0 ,
		--@TourPlanId int = 0 ,
		--@Date varchar (100) = NULL  ,
		--@Day varchar (100) = NULL  ,
		@WorkAtId int = 0 ,
		@WorkTypeId int = 0 ,
		@WorkRouteIds varchar(max) = null,
	    @UpdatedBy INT  = 0 
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
					
				
                   IF @Id > 0
					BEGIN
							UPDATE  TourPlanMonth

							SET		
							        --TourPlanId = @TourPlanId,
									--Date= Isnull (@Date,Date),
									--Day= Isnull (@Day,Day),
									WorkAtId = @WorkAtId,
									WorkTypeId = @WorkTypeId,
									WorkRouteIds = Isnull(@WorkRouteIds,WorkRouteIds),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'TourPlanMonth Data Updated Successfully'
					END

		
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				Id,
				TourPlanId,
				Date,
				Day,
				WorkAtId,
				WorkTypeId,
				WorkRouteIds,
			    UpdatedDate,
				UpdatedBy
				
				
		FROM
				[dbo].[TourPlanMonth]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[UserPlans_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[UserPlans_All] '',0,0,0,0
CREATE Proc  [dbo].[UserPlans_All]
    @Search varchar(MAX),
	@Offset bigint,
	@Limit bigint,
	@UserId bigint = 0,
	@PlansId bigint = 0
	

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords BIGINT;

		SET @TotalRecords = (
								select count(*) from [UserPlans] UP
								
							left join [User] U
						   on UP.UserId = U.Id
			
								left join [Plans] P
						   on UP.PlansId = P.Id
			
							
								
								where (@UserId  = 0 or  UP.UserId  = @UserId ) And
									  (@PlansId = 0 or  UP.PlansId = @PlansId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(U.UserPlansName) like '%'+lower(@Search)+'%') 
									)
								)
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				UP.Id,
				UP.UserId,
				UP.PlansId,
				UP.UserPlansName,
				UP.UserPlansPrice,
				UP.CreatedDate,
				UP.CreatedBy,
				UP.UpdatedDate,
				UP.UpdatedBy

		 from [UserPlans] UP
								
							left join [User] U
						   on UP.UserId = U.Id
			
								left join [Plans] P
						   on UP.PlansId = P.Id
			
							
								
								where (@UserId  = 0 or  UP.UserId  = @UserId ) And
									  (@PlansId = 0 or  UP.PlansId = @PlansId)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(P.Name) like '%'+lower(@Search)+'%') OR
										(lower(U.UserPlansName) like '%'+lower(@Search)+'%') 
									)
								)

			
			
			order by UP.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[UserPlans_ByUserId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc  [dbo].[UserPlans_ByUserId]
As
Select * From UserPlans
Go;
GO
/****** Object:  StoredProcedure [dbo].[UserPlans_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[UserPlans_Upsert] 0,4,5,gyhgdf,421,0,0
CREATE Proc  [dbo].[UserPlans_Upsert]
        
	    @Id BIGINT ,
		@UserId BIGINT,
		@PlansId  BIGINT,
		@UserPlansName Varchar(500),
		@UserPlansPrice Varchar(500),
	    @CreatedBy BIGINT  ,
	    @UpdatedBy BIGINT 
		

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code bigint = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					BEGIN
							INSERT into UserPlans (
														  
                                                          
												 UserId,
												 PlansId,
												 UserPlansName,
												 UserPlansPrice,
												 CreatedDate ,
												 CreatedBy
														   )

									VALUES (
									        
									        @UserId,
											@PlansId,
											@UserPlansName,
											@UserPlansPrice,
											GETDATE(),
											@CreatedBy
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'UserPlans Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN

					BEGIN
							UPDATE UserPlans

							SET		
							          
							        UserId = @UserId,
									PlansId = @PlansId,
									UserPlansName= Isnull (@UserPlansName,UserPlansName),
									UserPlansPrice= Isnull (@UserPlansPrice,UserPlansPrice),
								    UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy


							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'UserPlans Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 

	            Id,
				UserId,
				PlansId,
				UserPlansName,
				UserPlansPrice,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy
			
		FROM
				[dbo].[UserPlans]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[VisitType_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Select * from VisitType


-- exec [VisitType_All]  '',0,0

CREATE PROCEDURE [dbo].[VisitType_All]

	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from VisitType
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[VisitType]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[VisitType_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- EXEC  [VisitType_ById]
--SELECT * from VisitType

CREATE PROCEDURE [dbo].[VisitType_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'VisitType Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[VisitType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[VisitType_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [VisitType_Delete]2
--select * from VisitType

CREATE PROCEDURE [dbo].[VisitType_Delete]

@Id BIGINT,
@DeletedBy BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update VisitType set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'VisitType Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			  Id,
              Name, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
              DeletedDate,
              DeletedBy



FROM
				[dbo].[VisitType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[VisitType_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT *FROM VisitType

--EXEC  [VisitType_Upsert]0,'MASjjTI'

CREATE PROCEDURE [dbo].[VisitType_Upsert]

		@Id BIGINT = 0,
		@Name  varchar (500) = null,
		@CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO VisitType
												
											 (
												
												 Name, 
												 CreatedDate,
												 createdBy,
												 DeletedBy
												 

												)

									VALUES (
											
											@Name, 
											GETDATE(),
											@createdBy,
											0
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'VisitType Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  VisitType

							SET		
							        Name = Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'VisitType Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
			  Id,
              Name, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
              DeletedDate,
              DeletedBy



FROM
				[dbo].[VisitType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[WorkExperienceByEmployeeId]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec UserSelect 34
CREATE PROCEDURE [dbo].[WorkExperienceByEmployeeId] (
@EmployeeId int
)
AS

SET NOCOUNT ON
DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM WorkExperience WHERE EmployeeId = @EmployeeId)

SELECT * FROM WorkExperience WHERE EmployeeId = @EmployeeId

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[WorkExperienceDeleteByEmpID]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WorkExperienceDeleteByEmpID]
	@Id bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''
 
               
				DELETE 
				FROM
					WorkExperience
				WHERE
					Id = @Id

				SET @Code = 200
			    SET @Message = 'Work Experience Successfully.'

		  SELECT @Code as Code,
				 @Message as [Message]
   
   SELECT * FROM [dbo].WorkExperience WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[WorkExperienceUpsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WorkExperienceUpsert] (
	@Id int,
	@EmployeeId int,
	@CompanyName nvarchar(max),
	@Position nvarchar(100),
	@JoiningDate datetime,	
	@LeavingDate datetime,
	@Salary decimal(18,2),
	@LeavingReason nvarchar(max),
	@CreatedBy int,
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE [dbo].[WorkExperience]
	   SET [EmployeeId] = @EmployeeId,
		   [CompanyName] = @CompanyName,
		   [Position] = @Position,
		   [JoiningDate] = @JoiningDate,
		   [LeavingDate] = @LeavingDate,
		   [Salary] = @Salary,
		   [LeavingReason] = @LeavingReason,
		   [UpdatedAt] = GETDATE(),
		   [UpdatedBy] = @UpdatedBy
		WHERE
		[Id] = @Id

	SELECT * from [WorkExperience] where Id = @Id
	END
--INSERT
ELSE
	BEGIN
	INSERT INTO [dbo].[WorkExperience]
           ([EmployeeId]
           ,[CompanyName]
           ,[Position]
           ,[JoiningDate]
           ,[LeavingDate]
           ,[Salary]
           ,[LeavingReason]
           ,[CreatedAt]
           ,[CreatedBy])
     VALUES(
			@EmployeeId,
			@CompanyName,
			@Position,
			@JoiningDate,
			@LeavingDate,
			@Salary,
			@LeavingReason,
			GETDATE(),
			@CreatedBy
		)
	
	SELECT * from [WorkExperience] where Id = SCOPE_IDENTITY()
	END
GO
/****** Object:  StoredProcedure [dbo].[WorkType_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Select * from WorkType


-- exec [WorkType_All]  'Div',0,0

CREATE PROCEDURE [dbo].[WorkType_All]

	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from WorkType
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[WorkType]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[WorkType_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC  [WorkType_ById]
--SELECT * from WorkType

CREATE PROCEDURE [dbo].[WorkType_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'WorkType Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
				
		FROM
				[dbo].[WorkType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[WorkType_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [WorkType_Delete]5
--select * from WorkType

CREATE PROCEDURE [dbo].[WorkType_Delete]

@Id BIGINT,
@DeletedBy BIGINT = 0

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update WorkType set DeletedDate = GETDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'WorkType Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
			  Id,
              Name, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
              DeletedDate,
              DeletedBy



FROM
				[dbo].[WorkType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[WorkType_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT *FROM WorkType

--EXEC  [WorkType_Upsert]3,'MASTI'

CREATE PROCEDURE [dbo].[WorkType_Upsert]

		@Id BIGINT = 0,
		@Name  varchar (500) = null,
		@CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO WorkType
												
											 (
												
												 Name, 
												 CreatedDate,
												 createdBy,
												 DeletedBy
												 

												)

									VALUES (
											
											@Name, 
											GETDATE(),
											@createdBy,
											0
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'WorkType Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  WorkType

							SET		
							        Name = Isnull (@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'WorkType Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
			  Id,
              Name, 
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
              DeletedDate,
              DeletedBy



FROM
				[dbo].[WorkType]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_All]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WorkWithMaster_All]

	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from WorkWithMaster
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%')
										)	
									  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  Id,
				Name,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy

		from  [dbo].[WorkWithMaster]

		where ( isnull(@Search,'') = '' or
					(
						(lower(Name) like '%'+lower(@Search)+'%')
					)
			  )

		order by Id

		desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_ById]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WorkWithMaster_ById]

	@Id Int

AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
            @Message varchar(200) = ''


			SET @Message = 'WorkWith Data Retrived Successfully'

	       SELECT @Code as Code,
				  @Message as [Message]

	select
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

			FROM [dbo].[WorkWithMaster]
			
		WHERE
				Id = @Id
		
END
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_Delete]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WorkWithMaster_Delete]

		@Id int,
		@DeletedBy int

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update WorkWithMaster 
		
		set DeletedDate = GETDATE(),
		DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'WorkWith Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		select
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		from
				[dbo].[WorkWithMaster]
		where
				Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[WorkWithMaster_Upsert]    Script Date: 01-08-2021 12:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WorkWithMaster_Upsert]

		@Id int = 0,
		@Name varchar(500) = null,
		@CreatedBy int = 0,
		@UpdatedBy int = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					
					BEGIN
							INSERT INTO WorkWithMaster (Name,
														CreatedDate,
														CreatedBy,
														DeletedBy)

									VALUES (@Name,
											GETDATE(),
											@CreatedBy,
											0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'WorkWith Created Successfully'
					END
			END
			
			ELSE IF @Id > 0
			BEGIN
					
					BEGIN
							UPDATE WorkWithMaster

							SET		Name = ISNULL(@Name,Name),
									UpdatedDate = GETDATE(),
									UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'WorkWith Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[WorkWithMaster]
		WHERE
				Id = @Id

END
GO
USE [master]
GO
ALTER DATABASE [MR-App] SET  READ_WRITE 
GO
