﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class CallInV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCallInServices abstractCallInServices;
        #endregion

        #region Cnstr
        public CallInV1Controller(AbstractCallInServices abstractCallInServices)
        {
            this.abstractCallInServices = abstractCallInServices;
        }
        #endregion


        //CallIn_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("CallIn_All")]
        public async Task<IHttpActionResult> CallIn_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCallInServices.CallIn_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        //CallIn_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("CallIn_ById")]
        public async Task<IHttpActionResult> CallIn_ById(long Id)
        {
            var quote = abstractCallInServices.CallIn_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // CallIn_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("CallIn_Upsert")]
        public async Task<IHttpActionResult> CallIn_Upsert(CallIn CallIn)
        {
            var quote = abstractCallInServices.CallIn_Upsert(CallIn);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        // CallIn_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("CallIn_Delete")]
        public async Task<IHttpActionResult> CallIn_Delete(long Id, long DeletedBy)
        {
            var quote = abstractCallInServices.CallIn_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




    }
}
