﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class ChampaignMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractChampaignMasterServices abstractChampaignMasterServices;
        #endregion

        #region Cnstr
        public ChampaignMasterV1Controller(AbstractChampaignMasterServices abstractChampaignMasterServices)
        {
            this.abstractChampaignMasterServices = abstractChampaignMasterServices;
        }
        #endregion


        //ChampaignMaster_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChampaignMaster_All")]
        public async Task<IHttpActionResult> ChampaignMaster_All(PageParam pageParam, string search = "", long ApplyMasterId =0, long RegionId = 0, long HeadquarterId = 0)
        {
            var quote = abstractChampaignMasterServices.ChampaignMaster_All(pageParam, search, ApplyMasterId, RegionId, HeadquarterId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ChampaignMaster_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChampaignMaster_ById")]
        public async Task<IHttpActionResult> ChampaignMaster_ById(long Id)
        {
            var quote = abstractChampaignMasterServices.ChampaignMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // ChampaignMaster_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChampaignMaster_Upsert")]
        public async Task<IHttpActionResult> ChampaignMaster_Upsert(ChampaignMaster ChampaignMaster)
        {
            var quote = abstractChampaignMasterServices.ChampaignMaster_Upsert(ChampaignMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        // ChampaignMaster_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChampaignMaster_Delete")]
        public async Task<IHttpActionResult> ChampaignMaster_Delete(long Id, long DeletedBy)
        {
            var quote = abstractChampaignMasterServices.ChampaignMaster_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




    }
}
