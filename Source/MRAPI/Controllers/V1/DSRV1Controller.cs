﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DSRV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSRServices abstractDSRServices;
        #endregion

        #region Cnstr
        public DSRV1Controller(AbstractDSRServices abstractDSRServices)
        {
            this.abstractDSRServices = abstractDSRServices;
        }
        #endregion


        //DSR_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_All")]
        public async Task<IHttpActionResult> DSR_All(AbstractDSR abstractDSR, PageParam pageParam, string search, long RouteTypeId = 0, long WorkTypeId = 0, long RouteId = 0, long VisitTypeId = 0, long DoctorId = 0, long ChemistId = 0, int EmployeeId = 0)
        {
            var quote = abstractDSRServices.DSR_All(pageParam, search, RouteTypeId, WorkTypeId, RouteId, VisitTypeId, DoctorId, ChemistId, EmployeeId, abstractDSR);
            return this.Content((HttpStatusCode)200, quote);
        }


        //DSR_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_ById")]
        public async Task<IHttpActionResult> DSR_ById(long Id)
        {
            var quote = abstractDSRServices.DSR_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // DSR_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_Upsert")]
        public async Task<IHttpActionResult> DSR_Upsert(DSR DSR)
        {
            var quote = abstractDSRServices.DSR_Upsert(DSR);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

       

        // DSR_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_Delete")]
        public async Task<IHttpActionResult> DSR_Delete(long Id, long DeletedBy)
        {
            var quote = abstractDSRServices.DSR_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        

    }
}
