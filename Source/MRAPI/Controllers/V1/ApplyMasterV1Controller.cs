﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class ApplyMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractApplyMasterServices abstractApplyMasterServices;
        #endregion

        #region Cnstr
        public ApplyMasterV1Controller(AbstractApplyMasterServices abstractApplyMasterServices)
        {
            this.abstractApplyMasterServices = abstractApplyMasterServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("ApplyMaster_All")]
        public async Task<IHttpActionResult> ApplyMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractApplyMasterServices.ApplyMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ApplyMaster_ById")]
        public async Task<IHttpActionResult> ApplyMaster_ById(long Id)
        {
            var quote = abstractApplyMasterServices.ApplyMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // ApplyMaster_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ApplyMaster_Upsert")]
        public async Task<IHttpActionResult> ApplyMaster_Upsert(ApplyMaster ApplyMaster)
        {
            var quote = abstractApplyMasterServices.ApplyMaster_Upsert(ApplyMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ApplyMaster_Delete")]
        public async Task<IHttpActionResult> ApplyMaster_Delete(long Id, long DeletedBy)
        {
            var quote = abstractApplyMasterServices.ApplyMaster_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}
