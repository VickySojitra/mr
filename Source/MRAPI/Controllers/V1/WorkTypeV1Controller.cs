﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class WorkTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractWorkTypeServices abstractWorkTypeServices;
        #endregion

        #region Cnstr
        public WorkTypeV1Controller(AbstractWorkTypeServices abstractWorkTypeServices)
        {
            this.abstractWorkTypeServices = abstractWorkTypeServices;
        }
        #endregion


        //WorkType_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("WorkType_All")]
        public async Task<IHttpActionResult> WorkType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractWorkTypeServices.WorkType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        //WorkType_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("WorkType_ById")]
        public async Task<IHttpActionResult> WorkType_ById(long Id)
        {
            var quote = abstractWorkTypeServices.WorkType_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // WorkType_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("WorkType_Upsert")]
        public async Task<IHttpActionResult> WorkType_Upsert(WorkType WorkType)
        {
            var quote = abstractWorkTypeServices.WorkType_Upsert(WorkType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        // WorkType_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("WorkType_Delete")]
        public async Task<IHttpActionResult> WorkType_Delete(long Id, long DeletedBy)
        {
            var quote = abstractWorkTypeServices.WorkType_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


       
    }
}
