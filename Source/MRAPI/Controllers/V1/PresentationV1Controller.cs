﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class PresentationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPresentationServices abstractPresentationServices;
        #endregion

        #region Cnstr
        public PresentationV1Controller(AbstractPresentationServices abstractPresentationServices)
        {
            this.abstractPresentationServices = abstractPresentationServices;
        }
        #endregion


        //Presentation_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Presentation_All")]
        public async Task<IHttpActionResult> Presentation_All(PageParam pageParam, string search = "", int ProductId = 0)
        {
            var quote = abstractPresentationServices.Presentation_All(pageParam, search, ProductId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Presentation_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Presentation_ByProductId")]
        public async Task<IHttpActionResult> Presentation_ByProductId(PageParam pageParam, long ProductId = 0)
        {
            var quote = abstractPresentationServices.Presentation_ByProductId(pageParam, ProductId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Presentation_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Presentation_ById")]
        public async Task<IHttpActionResult> Presentation_ById(long Id)
        {
            var quote = abstractPresentationServices.Presentation_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // Presentation_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Presentation_Upsert")]
        public async Task<IHttpActionResult> Presentation_Upsert(Presentation Presentation)
        {
            var quote = abstractPresentationServices.Presentation_Upsert(Presentation);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Presentation_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Presentation_Delete")]
        public async Task<IHttpActionResult> Presentation_Delete(long ID)
        {
            var quote = abstractPresentationServices.Presentation_Delete(ID);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
