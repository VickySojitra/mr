﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class MR_PresentationResourcesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMR_PresentationResourcesServices abstractMR_PresentationResourcesServices;
        #endregion

        #region Cnstr
        public MR_PresentationResourcesV1Controller(AbstractMR_PresentationResourcesServices abstractMR_PresentationResourcesServices)
        {
            this.abstractMR_PresentationResourcesServices = abstractMR_PresentationResourcesServices;
        }
        #endregion


        //MR_PresentationResources_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_PresentationResources_ByMR_PresentationId")]
        public async Task<IHttpActionResult> MR_PresentationResources_ByMR_PresentationId(PageParam pageParam, long MR_PresentationId = 0)
        {
            var quote = abstractMR_PresentationResourcesServices.MR_PresentationResources_ByMR_PresentationId(pageParam, MR_PresentationId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // MR_PresentationResources_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_PresentationResources_Upsert")]
        public async Task<IHttpActionResult> MR_PresentationResources_Upsert(MR_PresentationResources MR_PresentationResources)
        {
            var quote = abstractMR_PresentationResourcesServices.MR_PresentationResources_Upsert(MR_PresentationResources);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // MR_PresentationResources_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_PresentationResources_Delete")]
        public async Task<IHttpActionResult> MR_PresentationResources_Delete(long Id, long DeletedBy)
        {
            var quote = abstractMR_PresentationResourcesServices.MR_PresentationResources_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
