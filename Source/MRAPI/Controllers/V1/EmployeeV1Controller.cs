﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class EmployeeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractEmployeeService abstractEmployeeService;
        #endregion

        #region Cnstr
        public EmployeeV1Controller(AbstractEmployeeService abstractEmployeeService)
        {
            this.abstractEmployeeService = abstractEmployeeService;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Employee_Login")]
        public async Task<IHttpActionResult> Employee_Login(string Email, string WebPassword)
        {
            var quote = abstractEmployeeService.Employee_Login(Email, WebPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Employee_ChangePassword")]
        public async Task<IHttpActionResult> Employee_ChangePassword(int Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            var quote = abstractEmployeeService.Employee_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Employee_ById")]
        public async Task<IHttpActionResult> Employee_ById(int Id)
        {
            var quote = abstractEmployeeService.Employee_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
