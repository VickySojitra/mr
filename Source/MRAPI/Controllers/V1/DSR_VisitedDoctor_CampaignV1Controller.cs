﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DSR_VisitedDoctor_CampaignV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSR_VisitedDoctor_CampaignServices abstractDSR_VisitedDoctor_CampaignServices;
        #endregion

        #region Cnstr
        public DSR_VisitedDoctor_CampaignV1Controller(AbstractDSR_VisitedDoctor_CampaignServices abstractDSR_VisitedDoctor_CampaignServices)
        {
            this.abstractDSR_VisitedDoctor_CampaignServices = abstractDSR_VisitedDoctor_CampaignServices;
        }
        #endregion




        //DSR_VisitedDoctor_Campaign_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId = 0)
        {
            var quote = abstractDSR_VisitedDoctor_CampaignServices.DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId(pageParam, DSR_VisitedDoctorId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //DSR_VisitedDoctor_Campaign_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_ById")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_ById(long Id)
        {
            var quote = abstractDSR_VisitedDoctor_CampaignServices.DSR_VisitedDoctor_Campaign_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




        // DSR_VisitedDoctor_Campaign_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_Upsert")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_Upsert(DSR_VisitedDoctor_Campaign DSR_VisitedDoctor_Campaign)
        {
            var quote = abstractDSR_VisitedDoctor_CampaignServices.DSR_VisitedDoctor_Campaign_Upsert(DSR_VisitedDoctor_Campaign);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DSR_VisitedDoctor_Campaign_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_Delete")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_Delete(long Id, long DeletedBy)
        {
            var quote = abstractDSR_VisitedDoctor_CampaignServices.DSR_VisitedDoctor_Campaign_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
