﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DoctorProductsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDoctorProductsServices abstractDoctorProductsServices;
        #endregion

        #region Cnstr
        public DoctorProductsV1Controller(AbstractDoctorProductsServices abstractDoctorProductsServices)
        {
            this.abstractDoctorProductsServices = abstractDoctorProductsServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("DoctorProducts_ByDoctorId")]
        public async Task<IHttpActionResult> DoctorProducts_ByDoctorId(long Id)
        {
            var quote = abstractDoctorProductsServices.DoctorProducts_ByDoctorId(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




        [System.Web.Http.HttpPost]
        [InheritedRoute("DoctorProducts_Upsert")]
        public async Task<IHttpActionResult> DoctorProducts_Upsert(DoctorProducts DoctorProducts)
        {
            var quote = abstractDoctorProductsServices.DoctorProducts_Upsert(DoctorProducts);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }





        [System.Web.Http.HttpPost]
        [InheritedRoute("DoctorProducts_Delete")]
        public async Task<IHttpActionResult> DoctorProducts_Delete(long Id,long DeletedBy)
        {
            var quote = abstractDoctorProductsServices.DoctorProducts_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




    }
}
