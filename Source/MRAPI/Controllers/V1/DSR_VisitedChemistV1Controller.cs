﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DSR_VisitedChemistV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSR_VisitedChemistServices abstractDSR_VisitedChemistServices;
        #endregion

        #region Cnstr
        public DSR_VisitedChemistV1Controller(AbstractDSR_VisitedChemistServices abstractDSR_VisitedChemistServices)
        {
            this.abstractDSR_VisitedChemistServices = abstractDSR_VisitedChemistServices;
        }
        #endregion

        //DSR_VisitedChemist_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_ByDSRId")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_ByDSRId(PageParam pageParam, long DSRId = 0)
        {
            var quote = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_ByDSRId(pageParam, DSRId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //DSR_VisitedChemist_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_ById")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_ById(long Id)
        {
            var quote = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // DSR_VisitedChemist_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_Upsert")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_Upsert(DSR_VisitedChemist DSR_VisitedChemist)
        {
            var quote = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_Upsert(DSR_VisitedChemist);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DSR_VisitedChemist_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_Delete")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_Delete(long Id)
        {
            var quote = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
