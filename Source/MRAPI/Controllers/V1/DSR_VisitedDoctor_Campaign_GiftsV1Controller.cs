﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRApi.Controllers.V1
{
    public class DSR_VisitedDoctor_Campaign_GiftsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSR_VisitedDoctor_Campaign_GiftsServices abstractDSR_VisitedDoctor_Campaign_GiftsServices;
        #endregion

        #region Cnstr
        public DSR_VisitedDoctor_Campaign_GiftsV1Controller(AbstractDSR_VisitedDoctor_Campaign_GiftsServices abstractDSR_VisitedDoctor_Campaign_GiftsServices)
        {
            this.abstractDSR_VisitedDoctor_Campaign_GiftsServices = abstractDSR_VisitedDoctor_Campaign_GiftsServices;
        }
        #endregion


        

        //DSR_VisitedDoctor_Campaign_Gifts_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId(PageParam pageParam, long DSR_VisitedDoctor_CampaignId = 0)
        {
            var quote = abstractDSR_VisitedDoctor_Campaign_GiftsServices.DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId(pageParam, DSR_VisitedDoctor_CampaignId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //DSR_VisitedDoctor_Campaign_Gifts_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_Gifts_ById")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_Gifts_ById(long Id)
        {
            var quote = abstractDSR_VisitedDoctor_Campaign_GiftsServices.DSR_VisitedDoctor_Campaign_Gifts_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




        // DSR_VisitedDoctor_Campaign_Gifts_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_Gifts_Upsert")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_Gifts_Upsert(DSR_VisitedDoctor_Campaign_Gifts DSR_VisitedDoctor_Campaign_Gifts)
        {
            var quote = abstractDSR_VisitedDoctor_Campaign_GiftsServices.DSR_VisitedDoctor_Campaign_Gifts_Upsert(DSR_VisitedDoctor_Campaign_Gifts);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DSR_VisitedDoctor_Campaign_Gifts_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Campaign_Gifts_Delete")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Campaign_Gifts_Delete(long Id, long DeletedBy)
        {
            var quote = abstractDSR_VisitedDoctor_Campaign_GiftsServices.DSR_VisitedDoctor_Campaign_Gifts_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
