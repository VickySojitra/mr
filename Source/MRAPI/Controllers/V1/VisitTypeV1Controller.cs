﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class VisitTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractVisitTypeServices abstractVisitTypeServices;
        #endregion

        #region Cnstr
        public VisitTypeV1Controller(AbstractVisitTypeServices abstractVisitTypeServices)
        {
            this.abstractVisitTypeServices = abstractVisitTypeServices;
        }
        #endregion


        //VisitType_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("VisitType_All")]
        public async Task<IHttpActionResult> VisitType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractVisitTypeServices.VisitType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        //VisitType_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("VisitType_ById")]
        public async Task<IHttpActionResult> VisitType_ById(long Id)
        {
            var quote = abstractVisitTypeServices.VisitType_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // VisitType_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("VisitType_Upsert")]
        public async Task<IHttpActionResult> VisitType_Upsert(VisitType VisitType)
        {
            var quote = abstractVisitTypeServices.VisitType_Upsert(VisitType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        // VisitType_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("VisitType_Delete")]
        public async Task<IHttpActionResult> VisitType_Delete(long Id,long DeletedBy)
        {
            var quote = abstractVisitTypeServices.VisitType_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



    }
}
