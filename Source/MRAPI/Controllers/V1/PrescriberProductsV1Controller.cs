﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRApi.Controllers.V1
{
    public class PrescriberProductsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPrescriberProductsServices abstractPrescriberProductsServices;
        #endregion

        #region Cnstr
        public PrescriberProductsV1Controller(AbstractPrescriberProductsServices abstractPrescriberProductsServices)
        {
            this.abstractPrescriberProductsServices = abstractPrescriberProductsServices;
        }
        #endregion


        //PrescriberProducts_All Api

      

        //PrescriberProducts_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("PrescriberProducts_ById")]
        public async Task<IHttpActionResult> PrescriberProducts_ById( long Id)
        {
            var quote = abstractPrescriberProductsServices.PrescriberProducts_ById(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        //PrescriberProducts_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("PrescriberProducts_ByPrescriberId")]
        public async Task<IHttpActionResult> PrescriberProducts_ByPrescriberId(PageParam pageParam, long PrescriberId = 0)
        {
            var quote = abstractPrescriberProductsServices.PrescriberProducts_ByPrescriberId(pageParam, PrescriberId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // PrescriberProducts_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("PrescriberProducts_Upsert")]
        public async Task<IHttpActionResult> PrescriberProducts_Upsert(PrescriberProducts PrescriberProducts)
        {
            var quote = abstractPrescriberProductsServices.PrescriberProducts_Upsert(PrescriberProducts);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        



    }
}
