﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DSR_VisitedDoctorV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSR_VisitedDoctorServices abstractDSR_VisitedDoctorServices;
        #endregion

        #region Cnstr
        public DSR_VisitedDoctorV1Controller(AbstractDSR_VisitedDoctorServices abstractDSR_VisitedDoctorServices)
        {
            this.abstractDSR_VisitedDoctorServices = abstractDSR_VisitedDoctorServices;
        }
        #endregion


        // DSR_VisitedDoctor_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Upsert")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Upsert(DSR_VisitedDoctor DSR_VisitedDoctor)
        {
            var quote = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_Upsert(DSR_VisitedDoctor);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //DSR_VisitedDoctor_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_ById")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_ById(int Id)
        {
            var quote = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_ByDSRId")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_ByDSRId(PageParam pageParam, long DSRId = 0)
        {
            var quote = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_ByDSRId(pageParam, DSRId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // DSR_VisitedDoctor_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Delete")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Delete(long Id)
        {
            var quote = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



    }
}
