﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRApi.Controllers.V1
{
    public class ProductVsChemistV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractProductVsChemistServices abstractProductVsChemistServices;
        #endregion

        #region Cnstr
        public ProductVsChemistV1Controller(AbstractProductVsChemistServices abstractProductVsChemistServices)
        {
            this.abstractProductVsChemistServices = abstractProductVsChemistServices;
        }
        #endregion


        //ProductVsChemist_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsChemist_All")]
        public async Task<IHttpActionResult> ProductVsChemist_All(PageParam pageParam, string search = "", long ProductId = 0, long ChemistId = 0)
        {
            var quote = abstractProductVsChemistServices.ProductVsChemist_All(pageParam, search, ProductId, ChemistId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ProductVsChemist_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsChemist_ByProductId")]
        public async Task<IHttpActionResult> ProductVsChemist_ByProductId(PageParam pageParam, long ProductId = 0)
        {
            var quote = abstractProductVsChemistServices.ProductVsChemist_ByProductId(pageParam, ProductId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ProductVsChemist_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsChemist_ByChemistId")]
        public async Task<IHttpActionResult> ProductVsChemist_ByChemistId(PageParam pageParam, long ChemistId = 0)
        {
            var quote = abstractProductVsChemistServices.ProductVsChemist_ByChemistId(pageParam, ChemistId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // ProductVsChemist_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsChemist_Upsert")]
        public async Task<IHttpActionResult> ProductVsChemist_Upsert(ProductVsChemist ProductVsChemist)
        {
            var quote = abstractProductVsChemistServices.ProductVsChemist_Upsert(ProductVsChemist);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ProductVsChemist_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsChemist_Delete")]
        public async Task<IHttpActionResult> ProductVsChemist_Delete(long Id)
        {
            var quote = abstractProductVsChemistServices.ProductVsChemist_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
