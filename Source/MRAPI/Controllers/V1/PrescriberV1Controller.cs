﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class PrescriberV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPrescriberServices abstractPrescriberServices;
        #endregion

        #region Cnstr
        public PrescriberV1Controller(AbstractPrescriberServices abstractPrescriberServices)
        {
            this.abstractPrescriberServices = abstractPrescriberServices;
        }
        #endregion


        //Prescriber_All Api



        //Prescriber_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Prescriber_ById")]
        public async Task<IHttpActionResult> Prescriber_ById(long Id)
        {
            var quote = abstractPrescriberServices.Prescriber_ById(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Prescriber_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Prescriber_ByPrescriberId")]
        public async Task<IHttpActionResult> Prescriber_ByPDoctorId(PageParam pageParam, long DoctorId = 0)
        {
            var quote = abstractPrescriberServices.Prescriber_ByDoctorId(pageParam, DoctorId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // Prescriber_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Prescriber_Upsert")]
        public async Task<IHttpActionResult> Prescriber_Upsert(Prescriber Prescriber)
        {
            var quote = abstractPrescriberServices.Prescriber_Upsert(Prescriber);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }






    }
}
