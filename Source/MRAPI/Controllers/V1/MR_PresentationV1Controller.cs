﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class MR_PresentationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMR_PresentationServices abstractMR_PresentationServices;
        #endregion

        #region Cnstr
        public MR_PresentationV1Controller(AbstractMR_PresentationServices abstractMR_PresentationServices)
        {
            this.abstractMR_PresentationServices = abstractMR_PresentationServices;
        }
        #endregion


        //MR_Presentation_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_Presentation_All")]
        public async Task<IHttpActionResult> MR_Presentation_All(PageParam pageParam, string search = "", long EmployeeId = 0, long ProductId = 0)
        {
            var quote = abstractMR_PresentationServices.MR_Presentation_All(pageParam, search, EmployeeId, ProductId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //MR_Presentation_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_Presentation_ById")]
        public async Task<IHttpActionResult> MR_Presentation_ById(long Id)
        {
            var quote = abstractMR_PresentationServices.MR_Presentation_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // MR_Presentation_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_Presentation_Upsert")]
        public async Task<IHttpActionResult> MR_Presentation_Upsert(MR_Presentation MR_Presentation)
        {
            var quote = abstractMR_PresentationServices.MR_Presentation_Upsert(MR_Presentation);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // MR_Presentation_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("MR_Presentation_Delete")]
        public async Task<IHttpActionResult> MR_Presentation_Delete(long Id, long DeletedBy)
        {
            var quote = abstractMR_PresentationServices.MR_Presentation_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
