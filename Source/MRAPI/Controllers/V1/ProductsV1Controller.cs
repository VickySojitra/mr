﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class ProductsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractProductServices abstractProductsServices;
        #endregion

        #region Cnstr
        public ProductsV1Controller(AbstractProductServices abstractProductsServices)
        {
            this.abstractProductsServices = abstractProductsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductsSelectAll")]
        public async Task<IHttpActionResult> ProductsSelectAll(PageParam pageParam, string search, long CategoryId, long ProductTypeId = 0, long SIUnitesId = 0, long ManufactureId = 0, long PrescriptionId = 0)
        {
            var quote = abstractProductsServices.Product_All(pageParam, search, CategoryId, ProductTypeId, SIUnitesId, ManufactureId, PrescriptionId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
