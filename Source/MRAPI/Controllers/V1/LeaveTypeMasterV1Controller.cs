﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;

namespace MRAPI.Controllers.V1
{
    public class LeaveTypeMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLeaveTypeMasterServices abstractLeaveTypeMasterServices;
        #endregion

        #region Cnstr
        public LeaveTypeMasterV1Controller(AbstractLeaveTypeMasterServices abstractLeaveTypeMasterServices)
        {
            this.abstractLeaveTypeMasterServices = abstractLeaveTypeMasterServices;
        }
        #endregion


        //LeaveTypeMaster_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("LeaveTypeMaster_All")]
        public async Task<IHttpActionResult> LeaveTypeMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLeaveTypeMasterServices.LeaveTypeMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        
    }
}
