﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class RouteV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRouteService abstractRouteServices;
        #endregion

        #region Cnstr
        public RouteV1Controller(AbstractRouteService abstractRouteServices)
        {
            this.abstractRouteServices = abstractRouteServices;
        }
        #endregion

        //Route_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteSelectAll")]
        public async Task<IHttpActionResult> RouteSelectAll(PageParam pageParam, string search = "")
        {
            var quote = abstractRouteServices.SelectAll(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteTypeSelectAll")]
        public async Task<IHttpActionResult> RouteTypeSelectAll(PageParam pageParam, string search = "")
        {
            var quote = abstractRouteServices.SelectAllRouteType(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
