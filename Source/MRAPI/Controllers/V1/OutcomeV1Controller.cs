﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class OutcomeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractOutcomeServices abstractOutcomeServices;
        #endregion

        #region Cnstr
        public OutcomeV1Controller(AbstractOutcomeServices abstractOutcomeServices)
        {
            this.abstractOutcomeServices = abstractOutcomeServices;
        }
        #endregion


        //Outcome_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Outcome_All")]
        public async Task<IHttpActionResult> Outcome_All(PageParam pageParam, string search = "")
        {
            var quote = abstractOutcomeServices.Outcome_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Outcome_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Outcome_ById")]
        public async Task<IHttpActionResult> Outcome_ById(long Id)
        {
            var quote = abstractOutcomeServices.Outcome_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // Outcome_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Outcome_Upsert")]
        public async Task<IHttpActionResult> Outcome_Upsert(Outcome Outcome)
        {
            var quote = abstractOutcomeServices.Outcome_Upsert(Outcome);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Outcome_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Outcome_Delete")]
        public async Task<IHttpActionResult> Outcome_Delete(long Id, long DeletedBy)
        {
            var quote = abstractOutcomeServices.Outcome_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        

    }
}
