﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DoctorsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDoctorService abstractDoctorsServices;
        #endregion

        #region Cnstr
        public DoctorsV1Controller(AbstractDoctorService abstractDoctorsServices)
        {
            this.abstractDoctorsServices = abstractDoctorsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("DoctorsSelectAll")]
        public async Task<IHttpActionResult> DoctorsSelectAll(PageParam pageParam, string search = "")
        {
            var quote = abstractDoctorsServices.SelectAll(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
