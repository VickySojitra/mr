﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class ProductVsDoctorV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractProductVsDoctorServices abstractProductVsDoctorServices;
        #endregion

        #region Cnstr
        public ProductVsDoctorV1Controller(AbstractProductVsDoctorServices abstractProductVsDoctorServices)
        {
            this.abstractProductVsDoctorServices = abstractProductVsDoctorServices;
        }
        #endregion


        //ProductVsDoctor_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsDoctor_All")]
        public async Task<IHttpActionResult> ProductVsDoctor_All(PageParam pageParam, string search = "")
        {
            var quote = abstractProductVsDoctorServices.ProductVsDoctor_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ProductVsDoctor_ByDoctorId Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsDoctor_ByDoctorId")]
        public async Task<IHttpActionResult> ProductVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            var quote = abstractProductVsDoctorServices.ProductVsDoctor_ByDoctorId(pageParam,DoctorId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // ProductVsDoctor_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsDoctor_Upsert")]
        public async Task<IHttpActionResult> ProductVsDoctor_Upsert(ProductVsDoctor ProductVsDoctor)
        {
            var quote = abstractProductVsDoctorServices.ProductVsDoctor_Upsert(ProductVsDoctor);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        // ProductVsDoctor_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsDoctor_Delete")]
        public async Task<IHttpActionResult> ProductVsDoctor_Delete(long Id, long DeletedBy)
        {
            var quote = abstractProductVsDoctorServices.ProductVsDoctor_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //ProductVsDoctor_ByProductId Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductVsDoctor_ByProductId")]
        public async Task<IHttpActionResult> ProductVsDoctor_ByProductId(PageParam pageParam, long ProductId)
        {
            var quote = abstractProductVsDoctorServices.ProductVsDoctor_ByProductId(pageParam, ProductId);
            return this.Content((HttpStatusCode)200, quote);
        }

       
    }
}
