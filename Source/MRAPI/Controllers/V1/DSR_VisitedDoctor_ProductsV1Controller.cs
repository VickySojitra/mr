﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DSR_VisitedDoctor_ProductsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSR_VisitedDoctor_ProductsServices abstractDSR_VisitedDoctor_ProductsServices;
        #endregion

        #region Cnstr
        public DSR_VisitedDoctor_ProductsV1Controller(AbstractDSR_VisitedDoctor_ProductsServices abstractDSR_VisitedDoctor_ProductsServices)
        {
            this.abstractDSR_VisitedDoctor_ProductsServices = abstractDSR_VisitedDoctor_ProductsServices;
        }
        #endregion





        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId = 0)
        {
            var quote = abstractDSR_VisitedDoctor_ProductsServices.DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(pageParam, DSR_VisitedDoctorId);
            return this.Content((HttpStatusCode)200, quote);
        }




        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Products_Upsert")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Products_Upsert(DSR_VisitedDoctor_Products DSR_VisitedDoctor_Products)
        {
            var quote = abstractDSR_VisitedDoctor_ProductsServices.DSR_VisitedDoctor_Products_Upsert(DSR_VisitedDoctor_Products);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



   

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedDoctor_Products_Delete")]
        public async Task<IHttpActionResult> DSR_VisitedDoctor_Products_Delete(long Id)
        {
            var quote = abstractDSR_VisitedDoctor_ProductsServices.DSR_VisitedDoctor_Products_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




    }
}
