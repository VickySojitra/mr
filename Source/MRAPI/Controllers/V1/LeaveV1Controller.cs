﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class LeaveV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLeaveServices abstractLeaveServices;
        #endregion

        #region Cnstr
        public LeaveV1Controller(AbstractLeaveServices abstractLeaveServices)
        {
            this.abstractLeaveServices = abstractLeaveServices;
        }
        #endregion




        //Leave_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Leave_ByEmployeeId")]
        public async Task<IHttpActionResult> Leave_ByEmployeeId(PageParam pageParam, long EmployeeId = 0)
        {
            var quote = abstractLeaveServices.Leave_ByEmployeeId(pageParam, EmployeeId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //Leave_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Leave_ById")]
        public async Task<IHttpActionResult> Leave_ById(long Id)
        {
            var quote = abstractLeaveServices.Leave_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




        // Leave_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Leave_Upsert")]
        public async Task<IHttpActionResult> Leave_Upsert(Leave Leave)
        {
            var quote = abstractLeaveServices.Leave_Upsert(Leave);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

    }
}
