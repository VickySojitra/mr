﻿using MR.APICommon;
using MR.Common;
using MRApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DayTypeMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDayTypeMasterServices abstractDayTypeMasterServices;
        #endregion

        #region Cnstr
        public DayTypeMasterV1Controller(AbstractDayTypeMasterServices abstractDayTypeMasterServices)
        {
            this.abstractDayTypeMasterServices = abstractDayTypeMasterServices;
        }
        #endregion


        //DayTypeMaster_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DayTypeMaster_All")]
        public async Task<IHttpActionResult> DayTypeMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractDayTypeMasterServices.DayTypeMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
