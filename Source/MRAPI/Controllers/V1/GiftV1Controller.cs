﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class GiftV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractGiftServices abstractGiftServices;
        #endregion

        #region Cnstr
        public GiftV1Controller(AbstractGiftServices abstractGiftServices)
        {
            this.abstractGiftServices = abstractGiftServices;
        }
        #endregion


        //Gift_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Gift_ChampaignMasterId")]
        public async Task<IHttpActionResult> Gift_ChampaignMasterId(PageParam pageParam,  long ChampaignMasterId = 0)
        {
            var quote = abstractGiftServices.Gift_ChampaignMasterId(pageParam, ChampaignMasterId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Gift_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Gift_ById")]
        public async Task<IHttpActionResult> Gift_ById(long Id)
        {
            var quote = abstractGiftServices.Gift_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // Gift_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Gift_Upsert")]
        public async Task<IHttpActionResult> Gift_Upsert(Gift Gift)
        {
            var quote = abstractGiftServices.Gift_Upsert(Gift);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        // Gift_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Gift_Delete")]
        public async Task<IHttpActionResult> Gift_Delete(long Id, long DeletedBy)
        {
            var quote = abstractGiftServices.Gift_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




    }
}
