﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class DSR_VisitedChemist_ProductsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDSR_VisitedChemist_ProductsServices abstractDSR_VisitedChemist_ProductsServices;
        #endregion

        #region Cnstr
        public DSR_VisitedChemist_ProductsV1Controller(AbstractDSR_VisitedChemist_ProductsServices abstractDSR_VisitedChemist_ProductsServices)
        {
            this.abstractDSR_VisitedChemist_ProductsServices = abstractDSR_VisitedChemist_ProductsServices;
        }
        #endregion


        //DSR_VisitedChemist_Products_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_Products_ByDSR_VisitedChemistId")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(PageParam pageParam, long DSR_VisitedChemistId = 0)
        {
            var quote = abstractDSR_VisitedChemist_ProductsServices.DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(pageParam, DSR_VisitedChemistId);
            return this.Content((HttpStatusCode)200, quote);
        }



        // DSR_VisitedChemist_Products_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_Products_Upsert")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_Products_Upsert(DSR_VisitedChemist_Products DSR_VisitedChemist_Products)
        {
            var quote = abstractDSR_VisitedChemist_ProductsServices.DSR_VisitedChemist_Products_Upsert(DSR_VisitedChemist_Products);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DSR_VisitedChemist_Products_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("DSR_VisitedChemist_Products_Delete")]
        public async Task<IHttpActionResult> DSR_VisitedChemist_Products_Delete(long Id)
        {
            var quote = abstractDSR_VisitedChemist_ProductsServices.DSR_VisitedChemist_Products_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
