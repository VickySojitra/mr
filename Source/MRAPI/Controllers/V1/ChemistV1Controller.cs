﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRAPI.Controllers.V1
{
    public class ChemistV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractChemistService abstractChemistServices;
        #endregion

        #region Cnstr
        public ChemistV1Controller(AbstractChemistService abstractChemistServices)
        {
            this.abstractChemistServices = abstractChemistServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChemistSelectAll")]
        public async Task<IHttpActionResult> ChemistSelectAll(PageParam pageParam, string search = "")
        {
            var quote = abstractChemistServices.SelectAll(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
