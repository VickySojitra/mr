﻿using MR.APICommon;
using MR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;
using MR.Entities.V1;
using System.IO;
namespace MRApi.Controllers.V1
{
    public class ChemistVsDoctorV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractChemistVsDoctorServices abstractChemistVsDoctorServices;
        #endregion

        #region Cnstr
        public ChemistVsDoctorV1Controller(AbstractChemistVsDoctorServices abstractChemistVsDoctorServices)
        {
            this.abstractChemistVsDoctorServices = abstractChemistVsDoctorServices;
        }
        #endregion


        //ChemistVsDoctor_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChemistVsDoctor_All")]
        public async Task<IHttpActionResult> ChemistVsDoctor_All(PageParam pageParam, string search = "",long DoctorId = 0, long ChemistId = 0)
        {
            var quote = abstractChemistVsDoctorServices.ChemistVsDoctor_All(pageParam, search, DoctorId, ChemistId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ChemistVsDoctor_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChemistVsDoctor_ByDoctorId")]
        public async Task<IHttpActionResult> ChemistVsDoctor_ByDoctorId(PageParam pageParam,  long DoctorId = 0)
        {
            var quote = abstractChemistVsDoctorServices.ChemistVsDoctor_ByDoctorId(pageParam, DoctorId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ChemistVsDoctor_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChemistVsDoctor_ByChemistId")]
        public async Task<IHttpActionResult> ChemistVsDoctor_ByChemistId(PageParam pageParam,  long ChemistId = 0)
        {
            var quote = abstractChemistVsDoctorServices.ChemistVsDoctor_ByChemistId(pageParam, ChemistId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // ChemistVsDoctor_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChemistVsDoctor_Upsert")]
        public async Task<IHttpActionResult> ChemistVsDoctor_Upsert(ChemistVsDoctor ChemistVsDoctor)
        {
            var quote = abstractChemistVsDoctorServices.ChemistVsDoctor_Upsert(ChemistVsDoctor);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // ChemistVsDoctor_Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("ChemistVsDoctor_Delete")]
        public async Task<IHttpActionResult> ChemistVsDoctor_Delete(long Id, long DeletedBy)
        {
            var quote = abstractChemistVsDoctorServices.ChemistVsDoctor_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }




    }
}
