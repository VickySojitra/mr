﻿using MR.Common;
using MR.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MR.Services.Contract;

namespace MR.Infrastructure
{
    public class BaseController : Controller
    {
        //#region Fields

        //private readonly AbstractUsersServices usersService;

        //#endregion

        //#region Ctor

        //public BaseController(AbstractUsersServices usersService)
        //{
        //    this.usersService = usersService;
        //}

        //#endregion

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                //bool isAllow = false;
                //if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName == Actions.LogIn)
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName == Actions.ForgotPassword)
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName == Actions.EmployeeResetPassword)
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName.ToLower() == Actions.Error.ToLower())
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName.ToLower() == Actions.PrivacyPolicy.ToLower())
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName.ToLower() == Actions.SignUp.ToLower())
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID == 0 && filterContext.ActionDescriptor.ActionName.ToLower() == Actions.Connect.ToLower())
                //{
                //    isAllow = true;
                //}
                //else if (ProjectSession.AdminUserID > 0)
                //{
                //    isAllow = true;
                //}
                //else
                //{
                //    isAllow = false;
                //}

                //if (!isAllow)
                //{
                //    filterContext.Result = new RedirectResult("~/Account/LogIn");
                //    return;
                //}

                if (ProjectSession.AdminUserID == 0)
                {
                    //if (Request.Cookies.Get("AdminUserLogin").Value[0].ToString() != string.Empty)
                    //{
                    //    //var user = usersService.Select(Convert.ToInt32(Request.Cookies.Get("AdminUserLogin").Value[0].ToString())).Item;
                    //    //ProjectSession.AdminUserID = user.Id;
                    //    //ProjectSession.UserName = user.Fname + " " + user.Lname;
                    //    return;

                    //}
                    //else
                    //{
                    filterContext.Result = new RedirectResult("~/Account/LogIn");
                    return;
                    //}
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                throw ex;
            }
        }
    }
}