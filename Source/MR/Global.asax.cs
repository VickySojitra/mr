﻿using Autofac;
using MR.Common;
using MR.Entities.Contract;
using MR.Entities.V1;
using System;
using System.Security.Claims;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MR
{
	public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ModelBinders.Binders.DefaultBinder = new TupleModelBinder();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            ModelMetadataProviders.Current = new CachedDataAnnotationsModelMetadataProvider();
            ContainerBuilder builder = new ContainerBuilder();

            Bootstrapper.Resolve(builder);
        }

        /// <summary>
        /// This method is called on application end request
        /// </summary>
        public void Application_EndRequest()
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Code that runs when an unhandled error occurs
            Exception ErrorInfo = Server.GetLastError().GetBaseException();
            CommonHelper.LogError(Server.MapPath("~/ErrorLog/ErrorLog.txt"), ErrorInfo);
            //Infrastructure.ErrorLogHelper.Log(ErrorInfo);
            //Server.ClearError();
            //Response.Redirect(ConfigItems.HostURL + Pages.Controllers.Account + "/" + Pages.Actions.Error);
        }

        //Custom Model Binder
        public class TupleModelBinder : DefaultModelBinder
        {
            protected override object CreateModel(ControllerContext controllerContext,
                      ModelBindingContext bindingContext, Type modelType)
            {
                if (modelType == typeof(AbstractAddress))
                {
                    return new Address();
                }

                if (modelType == typeof(AbstractProductImages))
                {
                    return new ProductImages();
                }
                
                return base.CreateModel(controllerContext, bindingContext, modelType);
            }
        }
    }
}
