﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Pages;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.EmployeeManager
{
    public class EmployeeController : BaseController
    {

        #region Fields
        private readonly AbstractEmployeeService _AbstractEmployeeService;
        private readonly AbstractRoleService _AbstractRole;
        private readonly AbstractAddressService _AddressServices;
        private readonly AbstractStateRegionHqServices abstractStateRegionHqServices;
        private readonly AbstractEmployeeRegionServices abstractEmployeeRegionServices;
        #endregion

        #region Ctor
        public EmployeeController(AbstractEmployeeService abstractEmployeeService, 
                                    AbstractRoleService abstractRoleService, 
                                    AbstractAddressService AddressServices,
                                    AbstractStateRegionHqServices abstractStateRegionHqServices,
                                    AbstractEmployeeRegionServices abstractEmployeeRegionServices
            )
        {
            this._AbstractEmployeeService = abstractEmployeeService;
            this._AbstractRole = abstractRoleService;
            this._AddressServices = AddressServices;
            this.abstractStateRegionHqServices = abstractStateRegionHqServices;
            this.abstractEmployeeRegionServices = abstractEmployeeRegionServices;

        }
        #endregion

        // GET: EmployeeManager
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        public ActionResult AddEmployeeState(string EmployeeId = "MA==",string role=null)
        {
            ViewBag.EmployeeId = ConvertTo.Integer(ConvertTo.Base64Decode(EmployeeId));
            ViewBag.RoleName = ConvertTo.Base64Decode(role);
            return View();
        }
        public ActionResult AddEmployeeRegion(string EmployeeId = "MA==" , string StateId = "MA==",string RoleName = null)
        {
            ViewBag.EmployeeId = ConvertTo.Integer(ConvertTo.Base64Decode(EmployeeId));
            ViewBag.Role =ConvertTo.Base64Decode(RoleName);
            ViewBag.EmployeeIdEncode = EmployeeId;
            ViewBag.StateIdEncode = StateId;
            ViewBag.StateId = ConvertTo.Integer(ConvertTo.Base64Decode(StateId)); 
            return View();
        }

        public ActionResult AddEmployeeHq(string EmployeeId = "MA==", string RegionId = "MA==" , string StateId = "MA==")
        {
            ViewBag.EmployeeId = ConvertTo.Integer(ConvertTo.Base64Decode(EmployeeId));
            ViewBag.EmployeeIdEncode = EmployeeId;
            ViewBag.RegionIdEncode = RegionId;
            ViewBag.StateIdEncode = StateId;
            ViewBag.RegionId = ConvertTo.Integer(ConvertTo.Base64Decode(RegionId));
            return View();
        }

        [HttpGet]
        [ActionName(Actions.Profile)]
        public ActionResult Profile(int Id)
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            var model = _AbstractEmployeeService.Select(Id);
            return View(model.Item);
        }

        [HttpGet]
        [ActionName(Actions.ManagePassword)]
        public ActionResult ManagePassword()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            return View();
        }

        [HttpGet]
        public ActionResult ManageEmployee(string empId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            ViewBag.HeadQuarterDD = HeadQuarterDropdownList();
            ViewBag.ReportingPersonDD = ReportingPersonDropdown();
            ViewBag.RoleDD = RoleDropdownList();
            ViewBag.RegionDD = RegionsDropdownList();
            ViewBag.StateDD = StateDropdownList();
            ViewBag.CityDD = CityDropdownList();

            AbstractEmployee employee = null;
            if (string.IsNullOrWhiteSpace(empId) == false)
            {

                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(empId));
                if (decryptedId > 0)
                {
                    employee = _AbstractEmployeeService.Select(decryptedId).Item;

                    employee.PermanentAddressData = (Address)(_AddressServices.Select(employee.PermanentAddress).Item);
                    employee.CurrentAddressData = (Address)(_AddressServices.Select(employee.CurrentAddress).Item);
                    //employee.workExperience = (WorkExperience)(_AbstractEmployeeService.WorkExperienceByEmployeeId(employee.Id).Values.FirstOrDefault());
                }
            }

            if (employee == null)
            {
                employee = new Employee();
                employee.PermanentAddressData = new Address();
                employee.CurrentAddressData = new Address();
            }

            employee.workExperience = new WorkExperience();
            return View(employee);
        }


        //ReportingPerson username drp
        [HttpPost]
        public JsonResult ReportingPersonDrp(int RoleId = 0, bool IsTourPlanDropDown = false)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = _AbstractEmployeeService.Employee_ReportingPerson(pageParam, "", RoleId, IsTourPlanDropDown);
            return Json(result.Values, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEmployee(Employee employee, HttpPostedFileBase avatar,
                                        HttpPostedFileBase addresProof, HttpPostedFileBase drivinLicense)
        {
            try
            {
                employee.CreatedBy = employee.UpdatedBy = ProjectSession.AdminUserID;
                employee.PermanentAddressData.CreatedBy = ProjectSession.AdminUserID;
                employee.PermanentAddressData.UpdatedBy = ProjectSession.AdminUserID;
                employee.CurrentAddressData.CreatedBy = ProjectSession.AdminUserID;
                employee.CurrentAddressData.UpdatedBy = ProjectSession.AdminUserID;
                SuccessResult<AbstractAddress> resultP = _AddressServices.InsertUpdateAddress(employee.PermanentAddressData);
                SuccessResult<AbstractAddress> resultC = _AddressServices.InsertUpdateAddress(employee.CurrentAddressData);

                employee.PermanentAddress = resultP.Item.Id;
                employee.CurrentAddress = resultC.Item.Id;

                bool newEmployee = false;
                if (employee.Id == 0)
                {
                    newEmployee = true;
                    employee.MobilePassword = employee.WebPassword = employee.FirstName + "@" + DateTime.Now.Millisecond.ToString();
                }

                SuccessResult<AbstractEmployee> result = _AbstractEmployeeService.InsertUpdate(employee);

                if (result.Item == null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.AlreadyExist);
                    return RedirectToAction("ManageEmployee", "Employee", new { Area = "", empId = ConvertTo.Base64Encode(Convert.ToString(employee.Id)) });
                }

                bool IsfileUploaded = false;
                if (avatar != null && avatar.ContentLength > 0)
                {
                    string profilePath = Server.MapPath("~/" + Path.Combine(Utility.getStoragePathOfEmployee(), result.Item.Id.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    string fileName = Path.Combine(profilePath, "Photo" + Path.GetExtension(avatar.FileName));
                    avatar.SaveAs(fileName);
                    result.Item.Photo = Path.Combine(Utility.getStoragePathOfEmployee(), result.Item.Id.ToString(), "Photo" + Path.GetExtension(avatar.FileName));
                    IsfileUploaded = true;
                }

                if (addresProof != null && addresProof.ContentLength > 0)
                {
                    string addressProofPath = Server.MapPath("~/" + Path.Combine(Utility.getStoragePathOfEmployee(), result.Item.Id.ToString()));

                    if (Directory.Exists(addressProofPath) == false)
                    {
                        Directory.CreateDirectory(addressProofPath);
                    }

                    string fileName = Path.Combine(addressProofPath, "AddressProof" + Path.GetExtension(addresProof.FileName));
                    addresProof.SaveAs(fileName);
                    result.Item.AddressProof = Path.Combine(Utility.getStoragePathOfEmployee(), result.Item.Id.ToString(), "AddressProof" + Path.GetExtension(addresProof.FileName));
                    IsfileUploaded = true;
                }

                if (drivinLicense != null && drivinLicense.ContentLength > 0)
                {
                    string drivingLicenseFilePath = Server.MapPath("~/" + Path.Combine(Utility.getStoragePathOfEmployee(), result.Item.Id.ToString()));

                    if (Directory.Exists(drivingLicenseFilePath) == false)
                    {
                        Directory.CreateDirectory(drivingLicenseFilePath);
                    }

                    string fileName = Path.Combine(drivingLicenseFilePath, "DrivingLicence" + Path.GetExtension(drivinLicense.FileName));
                    drivinLicense.SaveAs(fileName);
                    result.Item.DrivingLicense = Path.Combine(Utility.getStoragePathOfEmployee(), result.Item.Id.ToString(), "DrivingLicence" + Path.GetExtension(drivinLicense.FileName));
                    IsfileUploaded = true;
                }
                if (IsfileUploaded == true)
                {
                    _AbstractEmployeeService.InsertUpdate(result.Item);
                }

                try
                {
                    if (newEmployee == true)
                    {
                        string text = "Welcome to MR-App, now you can able to access the Web & mobile app. \nYour default credential is \nUserId:" + result.Item.Email + "\nPassword:" + result.Item.WebPassword;
                        string url = @"http://smsp.omrchecker.com/submitsms.jsp";
                        string parameters = @"?user=sunnyR&key=e39d451d68XX&mobile=" + result.Item.Mobile + "&message=" + HttpUtility.UrlEncode(text) + "&senderid=INFOTP&accusage=1";
                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(url);

                        HttpResponseMessage response = client.GetAsync(parameters).Result;
                    }
                }
                catch (Exception ex)
                {
                    GlobalLogger.Current.Error(ex);
                }

                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.RecordSavedSuccessfully);
                return RedirectToAction("Index", "Employee", new { Area = "" });
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction("ManageEmployee", "Employee", new { Area = "", empId = ConvertTo.Base64Encode(Convert.ToString(employee.Id)) });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddWorkExperiance(Employee employee, int Id)
        {
            try
            {
                employee.workExperience.EmployeeId = Id;
                SuccessResult<AbstractWorkExperience> result = _AbstractEmployeeService.InsertUpdateWorkExperience(employee.workExperience);

                if (result.Item != null)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }            
        }

        [HttpPost]
        [ActionName(Actions.BindEmployee)]
        //[ValidateAntiForgeryToken]
        public JsonResult BindEmployee([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _AbstractEmployeeService.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Employee>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.DeleteWorkExperience)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkExperience(string Id)
        {
            try
            {
                bool isdeleted = false;
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
                if (decryptedId > 0)
                {
                    isdeleted = _AbstractEmployeeService.DeleteWorkExperience(decryptedId);
                }

                return Json(isdeleted, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.BindWorkExperiance)]
        //[ValidateAntiForgeryToken]
        public JsonResult BindWorkExperiance([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Id)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                if (string.IsNullOrWhiteSpace(Id) == false)
                {
                    int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
                    string search = requestModel.Search.Value;
                    var model = _AbstractEmployeeService.WorkExperienceByEmployeeId(decryptedId);

                    totalRecord = (int)model.TotalRecords;
                    filteredRecord = (int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }

                return Json(new DataTablesResponse(requestModel.Draw, new List<WorkExperience>(), filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<WorkExperience>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.GetEmployee)]
        public JsonResult GetEmployee(int Id)
        {
            var model = _AbstractEmployeeService.Select(Id);
            return Json(model.Item, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> HeadQuarterDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.HeadquarterSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> RegionsDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.RegionSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> StateDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.StateSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> CityDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.CitySelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> RoleDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AbstractRole.SelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> QualificationDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var qualification = _AbstractEmployeeService.QualificationSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in qualification.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> ReportingPersonDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var employee = _AbstractEmployeeService.SelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in employee.Values)
            {
                items.Add(new SelectListItem() { Text = category.FirstName.ToString() + ' ' + category.LastName.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }


        //Add Employee Region
        [HttpPost]
        public JsonResult BindEmployeeRegion(int ParentId = 0, int GrandParentId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;
            string search = "";

            var result = abstractStateRegionHqServices.StateRegionHq_All(pageParam, search, ParentId, GrandParentId);
            return Json(result.Values, JsonRequestBehavior.AllowGet);
        }


        //Employee Region add
        [HttpPost]
        public JsonResult EmployeeAddRegion(int Id = 0, int UserId = 0, string RegionIds = "")
        {
            EmployeeRegion model = new EmployeeRegion();
            model.Id = Id;
            model.UserId = UserId;
            model.RegionIds = RegionIds;

            var result = abstractEmployeeRegionServices.EmployeeRegion_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Employee Region add
        public JsonResult GetEmployeeRegion(int UserId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractEmployeeRegionServices.EmployeeRegion_ByUserId(pageParam, "", UserId);
            return Json(result.Values, JsonRequestBehavior.AllowGet);
        }


    }
}