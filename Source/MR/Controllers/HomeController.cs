﻿using MR.Common;
using MR.Infrastructure;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers
{
	public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}