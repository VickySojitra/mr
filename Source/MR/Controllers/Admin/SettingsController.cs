﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MR.Controllers.Admin
{
	public class SettingsController : BaseController
	{
		private readonly AbstractCategoryMasterServices _abstractCategoryMasterDao = null;
		private readonly AbstractProductCategoryServices _abstractProductCategoryServices;
		private readonly AbstractProductManufactureServices _abstractProductManufactureServices;
		private readonly AbstractProductTypeServices _abstractProductTypeServices;
		private readonly AbstractDoctorService _abstractDoctorServices;
		private readonly AbstractProductPrescriptionServices _abstractProductPrescriptionServices;
		private readonly AbstractQuantitySIUnitesServices _abstractQuantitySIUnitesServices;
		private readonly AbstractEmployeeService _EmployeeService;
		private readonly AbstractAddressService _AddressServices;
		private readonly AbstractRoleService _AbstractRoleService;


		public SettingsController(AbstractCategoryMasterServices abstractCategoryMasterDao,
								  AbstractProductCategoryServices abstractProductCategoryServices,
								  AbstractProductTypeServices abstractProductTypeServices,
								  AbstractProductManufactureServices abstractProductManufactureServices,
								  AbstractDoctorService abstractDoctorServices,
								  AbstractEmployeeService EmployeeService,
								  AbstractAddressService AddressServices,
								  AbstractRoleService AbstractRoleService)
		{
			_abstractCategoryMasterDao = abstractCategoryMasterDao;
			_abstractProductCategoryServices = abstractProductCategoryServices;
			_abstractProductTypeServices = abstractProductTypeServices;
			_abstractProductManufactureServices = abstractProductManufactureServices;
			_abstractDoctorServices = abstractDoctorServices;
			_EmployeeService = EmployeeService;
			_AddressServices = AddressServices;
			_AbstractRoleService = AbstractRoleService;
		}

		// GET: Settings
		public ActionResult Index()
		{
			ViewBag.MasterDD = BindMasterName();
			return View();
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string FieldName = "")
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);
				
				switch (FieldName)
				{
					case "Product Category":
						var model = _abstractProductCategoryServices.ProductCategory_All(pageParam,search);
						totalRecord = (int)model.TotalRecords;
						filteredRecord = (int)model.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
						
					case "Product Type":
						var model1 = _abstractProductTypeServices.ProductType_All(pageParam, search);

						totalRecord = (int)model1.TotalRecords;
						filteredRecord = (int)model1.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model1.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					case "Mfg. Name":
						var model2 = _abstractProductManufactureServices.ProductManufacture_All(pageParam, search);

						totalRecord = (int)model2.TotalRecords;
						filteredRecord = (int)model2.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model2.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					case "Doctor / Chemist Category":
						var model3 = _abstractDoctorServices.SelectAllDoctorCategory(pageParam, search);

						totalRecord = (int)model3.TotalRecords;
						filteredRecord = (int)model3.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model3.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					case "City":
						var model4 = _AddressServices.CitySelectAll(pageParam, search);

						totalRecord = (int)model4.TotalRecords;
						filteredRecord = (int)model4.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model4.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					case "State":
						var model5 = _AddressServices.StateSelectAll(pageParam, search);

						totalRecord = (int)model5.TotalRecords;
						filteredRecord = (int)model5.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model5.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					case "Qualification":
						var model6 = _EmployeeService.QualificationSelectAll(pageParam,search);

						totalRecord = (int)model6.TotalRecords;
						filteredRecord = (int)model6.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model6.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					case "Specialization":
						var model7 = _EmployeeService.SpecializationSelectAll(pageParam,search);

						totalRecord = (int)model7.TotalRecords;
						filteredRecord = (int)model7.TotalRecords;

						return Json(new DataTablesResponse(requestModel.Draw, model7.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

					default:
						return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception)
			{
				return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult GetCategoryDetails(int Id = 0, string FieldName = "")
		{
			try
			{
				switch (FieldName)
				{
					case "Product Category":
						var model = _abstractProductCategoryServices.ProductCategory_ById(Id);
						return Json(model, JsonRequestBehavior.AllowGet);

					case "Product Type":
						var model1 = _abstractProductTypeServices.ProductType_ById(Id);
						return Json(model1, JsonRequestBehavior.AllowGet);

					case "Mfg. Name":
						var model2 = _abstractProductManufactureServices.ProductManufacture_ById(Id);
						return Json(model2, JsonRequestBehavior.AllowGet);

					case "Doctor / Chemist Category":
						var model3 = _abstractDoctorServices.DoctorCategory_ById(Id);
						return Json(model3, JsonRequestBehavior.AllowGet);

					case "City":
						var model4 = _AddressServices.City_ById(Id);
						return Json(model4, JsonRequestBehavior.AllowGet);

					case "State":
						var model5 = _AddressServices.State_ById(Id);
						return Json(model5, JsonRequestBehavior.AllowGet);

					case "Qualification":
						var model6 = _EmployeeService.Qualification_ById(Id);
						return Json(model6, JsonRequestBehavior.AllowGet);

					case "Specialization":
						var model7 = _EmployeeService.Specialization_ById(Id);
						return Json(model7, JsonRequestBehavior.AllowGet);

					default:
						return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception ex)
			{
				return Json(new { Code = 400, Message = ex.Message }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult SaveCatgory(long Id = 0, string fieldName = "", string Name = "", string website = "", int stateId = 0)
		{
			try
			{
				object model = null;

				switch (fieldName)
				{
					case "Product Category":
						AbstractProductCategory productCategory = new ProductCategory()
						{
							Id = Id,
							Name = Name,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _abstractProductCategoryServices.ProductCategory_Upsert(productCategory);
						break;
					case "Product Type":
						AbstractProductType productType = new ProductType()
						{
							Id = Id,
							Name = Name,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _abstractProductTypeServices.ProductType_Upsert(productType);
						break;
					case "Mfg. Name":
						AbstractProductManufacture productManufacture = new ProductManufacture()
						{
							Id = Id,
							Name = Name,
							Website = website,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _abstractProductManufactureServices.ProductManufacture_Upsert(productManufacture);
						break;
					case "Doctor / Chemist Category":
						AbstractDoctorCategory doctorCategory = new DoctorCategory()
						{
							Id = (int)Id,
							Name = Name,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _abstractDoctorServices.InsertUpdateDoctorCategory(doctorCategory);
						break;
					case "City":
						AbstractCity city = new City()
						{
							Id = (int)Id,
							Name = Name,
							StateId = stateId,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _AddressServices.InsertUpdateCity(city);
						break;
					case "State":
						AbstractState state = new State()
						{
							Id = (int)Id,
							Name = Name,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _AddressServices.InsertUpdateState(state);
						break;
					case "Qualification":
						AbstractQualification qualification = new Qualification()
						{
							Id = (int)Id,
							Name = Name,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _EmployeeService.InsertUpdateQualification(qualification);
						break;
					case "Specialization":
						AbstractSpecialization specialization = new Specialization()
						{
							Id = (int)Id,
							Name = Name,
							CreatedBy = ProjectSession.AdminUserID,
							UpdatedBy = ProjectSession.AdminUserID
						};

						model = _EmployeeService.InsertUpdateSpecialization(specialization);
						break;
					default:
						break;
				}

				return Json(model, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				//ErrorLogHelper.Log(ex);
				return Json(new { Code = 400, Message = ex.Message }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult SaveRole(int Id = 0, string Name = "", string Detail = "")
		{
			try
			{
				AbstractRole abstractRole = new Role()
				{
					Id = Id,
					Name = Name,
					Details = Detail,
					CreatedBy = ProjectSession.AdminUserID,
					UpdatedBy = ProjectSession.AdminUserID
				};

				var model = _AbstractRoleService.InsertUpdate(abstractRole);
				return Json(model, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				//ErrorLogHelper.Log(ex);
				return Json(new { Code = 400, Message = ex.Message }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult BindFieldName(int masterId)
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = int.MaxValue;

				var models = _abstractCategoryMasterDao.FieldName_All(pageParam, "", masterId);

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}
			}
			catch (Exception)
			{
			}

			return Json(items);
		}		

		[HttpPost]
		public JsonResult BindState()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = int.MaxValue;

				var models = _AddressServices.StateSelectAll(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}
			}
			catch (Exception)
			{
			}

			return Json(items);
		}

		private IList<SelectListItem> BindMasterName()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = int.MaxValue;

				var models = _abstractCategoryMasterDao.Master_All(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}

				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}
	}
}