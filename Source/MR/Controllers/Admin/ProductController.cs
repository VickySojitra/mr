﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Pages;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.Admin
{
	public class ProductController : BaseController
	{
		AbstractPresentationServices _abstractPresentationServices;
		AbstractProductCategoryServices _abstractProductCategoryServices;
		AbstractProductHighlightServices _abstractProductHighlightServices;
		AbstractProductImagesServices _abstractProductImagesServices;
		AbstractProductManufactureServices _abstractProductManufactureServices;
		AbstractProductPrescriptionServices _abstractProductPrescriptionServices;
		AbstractProductServices _abstractProductServices;
		AbstractProductTypeServices _abstractProductTypeServices;
		AbstractQuantitySIUnitesServices _abstractQuantitySIUnitesServices;
		AbstractProductVsDoctorServices _ProductVsDoctorServices;

		public ProductController(AbstractPresentationServices _abstractPresentationServices,
										AbstractProductCategoryServices _abstractProductCategoryServices,
										AbstractProductHighlightServices _abstractProductHighlightServices,
										AbstractProductImagesServices _abstractProductImagesServices,
										AbstractProductManufactureServices _abstractProductManufactureServices,
										AbstractProductPrescriptionServices _abstractProductPrescriptionServices,
										AbstractProductServices _abstractProductServices,
										AbstractProductTypeServices _abstractProductTypeServices,
										AbstractQuantitySIUnitesServices _abstractQuantitySIUnitesServices,
										AbstractProductVsDoctorServices ProductVsDoctorServices)
		{
			this._abstractPresentationServices = _abstractPresentationServices;
			this._abstractProductCategoryServices = _abstractProductCategoryServices;
			this._abstractProductHighlightServices = _abstractProductHighlightServices;
			this._abstractProductImagesServices = _abstractProductImagesServices;
			this._abstractProductManufactureServices = _abstractProductManufactureServices;
			this._abstractProductPrescriptionServices = _abstractProductPrescriptionServices;
			this._abstractProductServices = _abstractProductServices;
			this._abstractProductTypeServices = _abstractProductTypeServices;
			this._abstractQuantitySIUnitesServices = _abstractQuantitySIUnitesServices;
			_ProductVsDoctorServices = ProductVsDoctorServices;
		}

		[HttpGet]
		[ActionName(Actions.Index)]
		public ActionResult Index()
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
		}

		[HttpGet]
		[ActionName(Actions.ProductDetail)]
		public ActionResult ProductDetail(int Id)
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            var model = _abstractProductServices.Product_ById(Id);

			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};
			model.Item.ProductImages = _abstractProductImagesServices.ProductImages_All(pageParam, "", model.Item.Id).Values;
			model.Item.ProductHighlight = _abstractProductHighlightServices.ProductHighlight_All(pageParam, "", model.Item.Id).Values;
			return View(model.Item);
		}

		[HttpGet]
		[ActionName(Actions.ManageProduct)]
		public ActionResult ManageProduct(string prodId = "")
		{
			if (TempData["openPopup"] != null)
			{
				ViewBag.openPopup = TempData["openPopup"];
			}

			AbstractProduct product = null;
			if (string.IsNullOrWhiteSpace(prodId) == false)
			{
				int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(prodId));
				if (decryptedId > 0)
				{
					product = _abstractProductServices.Product_ById(decryptedId).Item;

					PageParam pageParam = new PageParam()
					{
						Limit = int.MaxValue,
						Offset = 0
					};

					product.ProductImages = _abstractProductImagesServices.ProductImages_All(pageParam, "", product.Id).Values;
					var values = _abstractProductHighlightServices.ProductHighlight_All(pageParam, "", product.Id).Values;
					List<string> prodHighlight = new List<string>();
					foreach (var value in values)
					{
						prodHighlight.Add(value.Name);
					}

					product.ProductHighlightString = string.Join(", ", prodHighlight.ToArray());
				}
			}

			if (product == null)
			{
				product = new Product();
				product.ProductImages = new List<AbstractProductImages>();
			}

			ViewBag.Category = ProductCategoryDropdown();
			ViewBag.Type = ProductTypeDropdown();
			ViewBag.QuantSIUnit = SIUnitDropdown();
			ViewBag.Manufacture = ManufactureDropdown();
			ViewBag.Priscription = PriscriptionDropdown();
			return View(product);
		}

		[HttpPost]
		[ActionName(Actions.BindPresentation)]
		//[ValidateAntiForgeryToken]
		public JsonResult BindPresentation([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int productId = -1)
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = requestModel.Search.Value;
				productId = (productId == 0) ? -1 : productId;
				var model = _abstractPresentationServices.Presentation_All(pageParam, search, productId);

				totalRecord = (int)model.TotalRecords;
				filteredRecord = (int)model.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Json(new DataTablesResponse(requestModel.Draw, new List<Presentation>(), 0, 0), JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ActionName(Actions.BindDoctors)]
		[ValidateAntiForgeryToken]
		public JsonResult BindDoctors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int productId = -1)
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				productId = (productId == 0) ? -1 : productId;
				var model = _ProductVsDoctorServices.ProductVsDoctor_ByProductId(pageParam, productId);

				totalRecord = (int)model.TotalRecords;
				filteredRecord = (int)model.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Json(new DataTablesResponse(requestModel.Draw, new List<ProductVsDoctor>(), 0, 0), JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ActionName(Actions.BindProduct)]
		//[ValidateAntiForgeryToken]
		public JsonResult BindProduct([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = requestModel.Search.Value;
				var model = _abstractProductServices.Product_All(pageParam, search, 0, 0, 0, 0, 0);

				totalRecord = (int)model.TotalRecords;
				filteredRecord = (int)model.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Json(new DataTablesResponse(requestModel.Draw, new List<Product>(), 0, 0), JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ValidateInput(false)]
		//[ValidateAntiForgeryToken]
		[ActionName(Actions.AddProduct)]
		public ActionResult AddProduct(Product product, HttpPostedFileBase productimg1,
										HttpPostedFileBase productimg2,
										HttpPostedFileBase productimg3,
										HttpPostedFileBase productimg4,
										HttpPostedFileBase productimg5,
										HttpPostedFileBase productimg6)
		{
			try
			{
				// Add product
				product.CreatedBy = product.UpdatedBy = ProjectSession.AdminUserID;
				SuccessResult<AbstractProduct> result = _abstractProductServices.Product_Upsert(product);

				if (result?.Item != null)
				{
					// Add product highlight
					if (product.Id > 0)
					{
						_abstractProductHighlightServices.ProductHighlight_DeleteByProductId(product.Id);
					}

                    if (string.IsNullOrWhiteSpace(product.ProductHighlightString) == false)
                    {
                        var prodHighlights = product.ProductHighlightString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < prodHighlights.Length; i++)
                        {
                            var prodhigh = new ProductHighlight()
                            {
                                ProductId = (int)result.Item.Id,
                                Name = prodHighlights[i],
                                CreatedBy = ProjectSession.AdminUserID
                            };

                            _abstractProductHighlightServices.ProductHighlight_Upsert(prodhigh);

                        } 
                    }

					// Add product images
					string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Product", result.Item.Id.ToString()));

					if (Directory.Exists(profilePath) == false)
					{
						Directory.CreateDirectory(profilePath);
					}

					ProductImages productImages = new ProductImages();
					productImages.ProductId = result.Item.Id;
					productImages.ProductName = result.Item.Name;
					productImages.CreatedBy = productImages.UpdatedBy = ProjectSession.AdminUserID;

					if (productimg1 != null && productimg1.ContentLength > 0)
					{
						string fileName = Path.Combine(profilePath, "productimg1" + Path.GetExtension(productimg1.FileName));
						productimg1.SaveAs(fileName);

						productImages.URL = Path.Combine("Storage", "Product", result.Item.Id.ToString(), "productimg1" + Path.GetExtension(productimg1.FileName));

						if (product.ProductImages != null && product.ProductImages.Count > 0 && product.ProductImages[0] != null)
						{
							productImages.Id = product.ProductImages[0].Id;
						}

						_abstractProductImagesServices.ProductImages_Upsert(productImages);
					}

					if (productimg2 != null && productimg2.ContentLength > 0)
					{
						string fileName = Path.Combine(profilePath, "productimg2" + Path.GetExtension(productimg2.FileName));
						productimg2.SaveAs(fileName);

						productImages.URL = Path.Combine("Storage", "Product", result.Item.Id.ToString(), "productimg2" + Path.GetExtension(productimg2.FileName));

						if (product.ProductImages != null && product.ProductImages.Count > 1 && product.ProductImages[1] != null)
						{
							productImages.Id = product.ProductImages[1].Id;
						}

						_abstractProductImagesServices.ProductImages_Upsert(productImages);
					}

					if (productimg3 != null && productimg3.ContentLength > 0)
					{
						string fileName = Path.Combine(profilePath, "productimg3" + Path.GetExtension(productimg3.FileName));
						productimg3.SaveAs(fileName);

						productImages.URL = Path.Combine("Storage", "Product", result.Item.Id.ToString(), "productimg3" + Path.GetExtension(productimg3.FileName));

						if (product.ProductImages != null && product.ProductImages.Count > 2 && product.ProductImages[2] != null)
						{
							productImages.Id = product.ProductImages[2].Id;
						}

						_abstractProductImagesServices.ProductImages_Upsert(productImages);
					}

					if (productimg4 != null && productimg4.ContentLength > 0)
					{
						string fileName = Path.Combine(profilePath, "productimg4" + Path.GetExtension(productimg4.FileName));
						productimg4.SaveAs(fileName);

						productImages.URL = Path.Combine("Storage", "Product", result.Item.Id.ToString(), "productimg4" + Path.GetExtension(productimg4.FileName));

						if (product.ProductImages != null && product.ProductImages.Count > 3 && product.ProductImages[3] != null)
						{
							productImages.Id = product.ProductImages[3].Id;
						}

						_abstractProductImagesServices.ProductImages_Upsert(productImages);
					}

					if (productimg5 != null && productimg5.ContentLength > 0)
					{
						string fileName = Path.Combine(profilePath, "productimg5" + Path.GetExtension(productimg5.FileName));
						productimg5.SaveAs(fileName);

						productImages.URL = Path.Combine("Storage", "Product", result.Item.Id.ToString(), "productimg5" + Path.GetExtension(productimg5.FileName));

						if (product.ProductImages != null && product.ProductImages.Count > 4 && product.ProductImages[4] != null)
						{
							productImages.Id = product.ProductImages[4].Id;
						}

						_abstractProductImagesServices.ProductImages_Upsert(productImages);
					}

					if (productimg6 != null && productimg6.ContentLength > 0)
					{
						string fileName = Path.Combine(profilePath, "productimg6" + Path.GetExtension(productimg6.FileName));
						productimg6.SaveAs(fileName);

						productImages.URL = Path.Combine("Storage", "Product", result.Item.Id.ToString(), "productimg6" + Path.GetExtension(productimg6.FileName));

						if (product.ProductImages != null && product.ProductImages.Count > 5 && product.ProductImages[5] != null)
						{
							productImages.Id = product.ProductImages[5].Id;
						}

						_abstractProductImagesServices.ProductImages_Upsert(productImages);
					}
				}

				if (result.Item != null)
				{
					TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.RecordSavedSuccessfully);
					return RedirectToAction(Actions.Index, Pages.Controllers.Product, new { Area = "" });
				}
				else
				{
					TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.AlreadyExist);
					return RedirectToAction(Actions.ManageProduct, Pages.Controllers.Product, new { Area = "", prodId = ConvertTo.Base64Encode(Convert.ToString(product.Id)) });
				}
			}
			catch (Exception ex)
			{
				TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
				return RedirectToAction(Actions.ManageProduct, Pages.Controllers.Product, new { Area = "", prodId = ConvertTo.Base64Encode(Convert.ToString(product.Id)) });
			}
		}

		[HttpPost]
		//[ValidateAntiForgeryToken]
		[ValidateInput(false)]
		[ActionName(Actions.AddPresentation)]
		public JsonResult AddPresentation(int ProductId, HttpPostedFileBase productimg1)
		{
			try
			{
				Presentation presentation = new Presentation();
				presentation.ProductId = ProductId;
				presentation.CreatedBy = presentation.UpdatedBy = ProjectSession.AdminUserID;

				string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Product", ProductId.ToString(), "Presentation"));

				if (Directory.Exists(profilePath) == false)
				{
					Directory.CreateDirectory(profilePath);
				}

				SuccessResult<AbstractPresentation> result = null;
				if (productimg1 != null && productimg1.ContentLength > 0)
				{
					string fileName = Path.Combine(profilePath, productimg1.FileName);
					productimg1.SaveAs(fileName);

					presentation.FileName = Path.GetFileNameWithoutExtension(productimg1.FileName);
					presentation.FileType = Path.GetExtension(productimg1.FileName)?.TrimStart('.');
					presentation.Size = Math.Round(((decimal)productimg1.ContentLength / (decimal)(1024 * 1024)),2) + " MB";
					presentation.FileURL = Path.Combine("Storage", "Product", ProductId.ToString(), "Presentation", productimg1.FileName);
					result = _abstractPresentationServices.Presentation_Upsert(presentation);
				}

                return Json(result.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ValidateInput(false)]
		[ActionName(Actions.DeletePresentation)]
		public JsonResult DeletePresentation(int Id)
		{
			try
			{
				var model = _abstractPresentationServices.Presentation_Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ActionName(Actions.AddCategory)]
		public JsonResult AddCategory(string Name)
		{
			try
			{
				if (string.IsNullOrEmpty(Name))
				{
					return Json(null, JsonRequestBehavior.AllowGet);
				}

				var category = new ProductCategory()
				{
					Name = Name,
					CreatedBy = ProjectSession.AdminUserID
				};
				var model = _abstractProductCategoryServices.ProductCategory_Upsert(category);
				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ActionName(Actions.AddProductType)]
		public JsonResult AddProductType(string Name)
		{
			try
			{
				if (string.IsNullOrEmpty(Name))
				{
					return Json(null, JsonRequestBehavior.AllowGet);
				}

				var category = new ProductType()
				{
					Name = Name,
					CreatedBy = ProjectSession.AdminUserID
				};
				var model = _abstractProductTypeServices.ProductType_Upsert(category);
				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ActionName(Actions.AddManufacture)]
		public JsonResult AddManufacture(string Name, string WebSite)
		{
			try
			{
				if (string.IsNullOrEmpty(Name))
				{
					return Json(null, JsonRequestBehavior.AllowGet);
				}

				var category = new ProductManufacture()
				{
					Name = Name,
					Website = WebSite,
					CreatedBy = ProjectSession.AdminUserID
				};
				var model = _abstractProductManufactureServices.ProductManufacture_Upsert(category);
				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ActionName(Actions.GetProduct)]
		public JsonResult GetProduct(int Id)
		{
			try
			{
				var model = _abstractProductServices.Product_ById(Id);
				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		private List<SelectListItem> PriscriptionDropdown()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var category = _abstractProductPrescriptionServices.ProductPrescription_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category1 in category.Values)
			{
				items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> ManufactureDropdown()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var category = _abstractProductManufactureServices.ProductManufacture_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category1 in category.Values)
			{
				items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> SIUnitDropdown()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var category = _abstractQuantitySIUnitesServices.QuantitySIUnites_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category1 in category.Values)
			{
				items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> ProductTypeDropdown()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var category = _abstractProductTypeServices.ProductType_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category1 in category.Values)
			{
				items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> ProductCategoryDropdown()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var category = _abstractProductCategoryServices.ProductCategory_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category1 in category.Values)
			{
				items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.Id.ToString() });
			}

			return items;
		}
	}
}