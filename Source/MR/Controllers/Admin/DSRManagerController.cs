﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.Admin
{
	public class DSRManagerController : BaseController
	{

		private readonly AbstractDSRServices abstractDSRServices = null;
		private readonly AbstractAddressService abstractAddressService = null;
		private readonly AbstractDSRStatusServices abstractDSRStatusServices = null;
		private readonly AbstractDSR_VisitedChemistServices abstractDSR_VisitedChemistServices = null;
		private readonly AbstractDSR_VisitedDoctorServices abstractDSR_VisitedDoctorServices = null;
		private readonly AbstractEmployeeService abstractEmployeeService = null;
		private readonly AbstractChemistService abstractChemistService = null;
		private readonly AbstractDoctorService abstractDoctorService = null;
		private readonly AbstractDSR_VisitedChemist_ProductsServices abstractDSR_VisitedChemist_ProductsServices = null;
		private readonly AbstractDSR_VisitedDoctor_ProductsServices abstractDSR_VisitedDoctor_ProductsServices = null;
		private readonly AbstractRouteService abstractRouteService = null;
		private readonly AbstractWorkTypeServices abstractWorkTypeServices = null;
		private readonly AbstractVisitTypeServices abstractVisitTypeServices;
		private readonly AbstractCallInServices abstractCallInServices;
		private readonly AbstractOutcomeServices abstractOutcomeServices;
		private readonly AbstractEmployeeService _AbstractEmployeeService;

		public DSRManagerController(AbstractDSRServices abstractDSRServices,
			AbstractDSRStatusServices abstractDSRStatusServices,
			AbstractAddressService abstractAddressService,
			AbstractDSR_VisitedChemistServices abstractDSR_VisitedChemistServices,
			AbstractDSR_VisitedDoctorServices abstractDSR_VisitedDoctorServices,
			AbstractEmployeeService abstractEmployeeService,
			AbstractChemistService abstractChemistService,
			AbstractDSR_VisitedDoctor_ProductsServices abstractDSR_VisitedDoctor_ProductsServices,
			AbstractDSR_VisitedChemist_ProductsServices abstractDSR_VisitedChemist_ProductsServices,
			 AbstractDoctorService abstractDoctorService,
			 AbstractRouteService abstractRouteService,
			 AbstractWorkTypeServices abstractWorkTypeServices,
			 AbstractVisitTypeServices abstractVisitTypeServices,
			 AbstractCallInServices abstractCallInServices,
			 AbstractOutcomeServices abstractOutcomeServices,
             AbstractEmployeeService abstractWorkWithMasterServices)
		{
			this.abstractDSR_VisitedDoctor_ProductsServices = abstractDSR_VisitedDoctor_ProductsServices;
			this.abstractDSR_VisitedChemist_ProductsServices = abstractDSR_VisitedChemist_ProductsServices;
			this.abstractEmployeeService = abstractEmployeeService;
			this.abstractDSRServices = abstractDSRServices;
			this.abstractDSRStatusServices = abstractDSRStatusServices;
			this.abstractAddressService = abstractAddressService;
			this.abstractDSR_VisitedChemistServices = abstractDSR_VisitedChemistServices;
			this.abstractDSR_VisitedDoctorServices = abstractDSR_VisitedDoctorServices;
			this.abstractDoctorService = abstractDoctorService;
			this.abstractChemistService = abstractChemistService;
			this.abstractRouteService = abstractRouteService;
			this.abstractWorkTypeServices = abstractWorkTypeServices;
			this.abstractVisitTypeServices = abstractVisitTypeServices;
			this.abstractCallInServices = abstractCallInServices;
			this.abstractOutcomeServices = abstractOutcomeServices;
			this._AbstractEmployeeService = abstractWorkWithMasterServices;
		}

		[HttpGet]
		public ActionResult Index()
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.Headquatar = BindHeadquatar();
			ViewBag.Region = BindRegion();
			ViewBag.Status = BindStatus();
			return View();
		}

		[HttpGet]
		public ActionResult DSRReport()
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            PageParam pageParam = new PageParam();
			pageParam.Offset = 0;
			pageParam.Limit = int.MaxValue;
			ViewBag.SubmitId = abstractDSRStatusServices.DSRStatus_All(pageParam, "").Values.Where(a => a.Name.StartsWith("Submitted")).Select(a => a.Id).FirstOrDefault();
			return View();
		}

		[HttpGet]
		public ActionResult SubmitDSRReport(int dsrId = 0)
		{
            try
			{
				if (dsrId == 0)
				{
					return RedirectToAction(MR.Pages.Actions.ManageDSR1);
				}

				var model = abstractDSRServices.DSR_ById(dsrId);

				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = int.MaxValue;
				model.Item.StatusId = abstractDSRStatusServices.DSRStatus_All(pageParam, "").Values.Where(a => a.Name.StartsWith("Submitted")).Select(a => a.Id).FirstOrDefault();
				model.Item.UpdatedBy = ProjectSession.AdminUserID;
				abstractDSRServices.DSR_Upsert(model.Item);
			}
			catch (Exception)
			{
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(MR.Pages.Actions.ManageDSR1);
			}
			return RedirectToAction(MR.Pages.Actions.DSRReport);
		}

		[HttpGet]
		public ActionResult DraftDSRReport(int dsrId = 0)
		{
			try
			{
				if (dsrId == 0)
				{
					return RedirectToAction(MR.Pages.Actions.ManageDSR1);
				}

				var model = abstractDSRServices.DSR_ById(dsrId);

				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = int.MaxValue;
				model.Item.StatusId = abstractDSRStatusServices.DSRStatus_All(pageParam, "").Values.Where(a => a.Name.StartsWith("Not")).Select(a => a.Id).FirstOrDefault();
				model.Item.UpdatedBy = ProjectSession.AdminUserID;
				abstractDSRServices.DSR_Upsert(model.Item);
			}
			catch (Exception)
			{
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(MR.Pages.Actions.ManageDSR1);
			}
			return RedirectToAction(MR.Pages.Actions.DSRReport);
		}

		[HttpGet]
		public ActionResult ManageDSR1(int dsrId = 0)
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.WorkAtDD = BindWorkAt();
			ViewBag.WorkTypeDD = BindWorkType();
			ViewBag.WorkRouteDD = RouteDropdownList();
			ViewBag.VisitTypeDD = VisitTypeDropdownList();

			AbstractDSR abstractDSR = null;
			if (dsrId > 0)
			{
				abstractDSR = abstractDSRServices.DSR_ById(dsrId).Item;
			}
			else
			{
				abstractDSR = new DSR();
                abstractDSR.EmployeeId = ProjectSession.AdminUserID;
            }
			return View(abstractDSR);
		}

		[HttpGet]
		public ActionResult ManageDSR2(int dsrId)
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.CallInDD = CallInDropdownList();
			ViewBag.OutComeDD = OutComeDropdownList();
			ViewBag.WorkInPersonDD = WorkWithDropdownList();
			if (dsrId == 0)
			{
				return RedirectToAction(MR.Pages.Actions.ManageDSR1);
			}

			ViewBag.DSRId = dsrId;
			return View();
		}

		[HttpGet]
		public ActionResult ManageDSR3(int dsrId)
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.CallInDD = CallInDropdownList();
			ViewBag.OutComeDD = OutComeDropdownList();
			ViewBag.WorkInPersonDD = WorkWithDropdownList();

			if (dsrId == 0)
			{
				return RedirectToAction(MR.Pages.Actions.ManageDSR1);
			}

			ViewBag.DSRId = dsrId;
			return View();
		}

		[HttpGet]
		public ActionResult ViewDSR(string ri = "MA==")
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            try
			{
				long Id = ConvertTo.Long(ConvertTo.Base64Decode(ri));
				if (Id == 0)
				{
					throw new Exception("");
				}
				ViewBag.DSRId = Id;
				var model = abstractDSRServices.DSR_ById(Id);
				return View(model.Item);
			}
			catch (Exception ex)
			{
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(MR.Pages.Actions.DSRReport);
			}
		}

		[HttpPost]
		public ActionResult AddDSR(DSR DSR)
		{
			try
			{
                DSR.CreatedBy = DSR.UpdatedBy = ProjectSession.AdminUserID;
                var model = abstractDSRServices.DSR_Upsert(DSR);

				if (model != null || model.Item != null)
				{
					return RedirectToAction(MR.Pages.Actions.ManageDSR2, new { dsrId = model.Item.Id });
				}

                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to add DSR.");
                return RedirectToAction(MR.Pages.Actions.DSRReport);
			}
			catch (Exception ex)
			{
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(MR.Pages.Actions.DSRReport);
			}
		}

		[HttpPost]
		public JsonResult AddChemist(DSR_VisitedChemist abstractDSR_VisitedChemist)
		{
			try
			{
				abstractDSR_VisitedChemist.CreatedBy = abstractDSR_VisitedChemist.UpdatedBy = ProjectSession.AdminUserID;

				var model = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_Upsert(abstractDSR_VisitedChemist);

				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult AddChemistProduct(int chemistId, int productId, int prodQuantity)
		{
			try
			{
				AbstractDSR_VisitedChemist_Products abstractDSR_VisitedChemist_Products = new DSR_VisitedChemist_Products()
				{
					DSR_VisitedChemistId = chemistId,
					ProductId = productId,
					OrderBookedQty = prodQuantity,
					CreatedBy = ProjectSession.AdminUserID,
					UpdatedBy = ProjectSession.AdminUserID
				};

				var model = abstractDSR_VisitedChemist_ProductsServices.DSR_VisitedChemist_Products_Upsert(abstractDSR_VisitedChemist_Products);

				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult RemoveChemist(int visibleChemistId)
		{
			try
			{
				var model = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_Delete(visibleChemistId);
				return Json(model.IsSuccessStatusCode, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult GetChemist(int visibleChemistId)
		{
			try
			{
				var model = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_ById(visibleChemistId);
				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult AddDoctor(DSR_VisitedDoctor abstractDSR_Visiteddoctor)
		{
			try
			{
				abstractDSR_Visiteddoctor.CreatedBy = abstractDSR_Visiteddoctor.UpdatedBy = ProjectSession.AdminUserID;

				var model = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_Upsert(abstractDSR_Visiteddoctor);

				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult AddDoctorProduct(int chemistId, int productId, int prodQuantity)
		{
			try
			{
				AbstractDSR_VisitedDoctor_Products abstractDSR_VisitedChemist_Products = new DSR_VisitedDoctor_Products()
				{
					DSR_VisitedDoctorId = chemistId,
					ProductId = productId,
					OrderBookedQty = prodQuantity,
					CreatedBy = ProjectSession.AdminUserID,
					UpdatedBy = ProjectSession.AdminUserID
				};

				var model = abstractDSR_VisitedDoctor_ProductsServices.DSR_VisitedDoctor_Products_Upsert(abstractDSR_VisitedChemist_Products);

				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult RemoveDoctor(int visibleChemistId)
		{
			try
			{
				var model = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_Delete(visibleChemistId);
				return Json(model.IsSuccessStatusCode, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult GetDoctor(int visibleChemistId)
		{
			try
			{
				var model = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_ById(visibleChemistId);
				return Json(model.Item, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult ViewProfile(long Id, string type = "")
		{
			if (type == "Employee")
			{
				var response = abstractEmployeeService.Employee_ById(ConvertTo.Integer(Id));
				return Json(response, JsonRequestBehavior.AllowGet);
			}
			else if (type == "Chemist")
			{
				var response = abstractChemistService.Select(ConvertTo.Integer(Id));
				return Json(response, JsonRequestBehavior.AllowGet);
			}
			else if (type == "Doctor")
			{
				var response = abstractDoctorService.Select(ConvertTo.Integer(Id));
				return Json(response, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json("", JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string type = "", string Date = "", int HeadquaterId = 0, int RegionId = 0, long StatusId = 0, long DSRId = 0, long Id = 0)
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);

				if (type == "ManageDSR")
				{
					AbstractDSR abstractDSR = new DSR();
					abstractDSR.HeadquaterId = HeadquaterId;
					abstractDSR.RegionId = RegionId;
					abstractDSR.StatusId = StatusId;
					abstractDSR.Date = Date;
					var model = abstractDSRServices.DSR_All(pageParam, search, 0, 0, 0, 0, 0, 0, 0, abstractDSR);

					totalRecord = (int)model.TotalRecords;
					filteredRecord = (int)model.TotalRecords;

					return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
				}
				else if (type == "ChemistProduct")
				{
					var response = abstractDSR_VisitedChemist_ProductsServices.DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(pageParam, Id);
					totalRecord = (int)response.TotalRecords;
					filteredRecord = (int)response.TotalRecords;

					return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
				}
				else if (type == "DoctorProduct")
				{
					var response = abstractDSR_VisitedDoctor_ProductsServices.DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(pageParam, Id);

					totalRecord = (int)response.TotalRecords;
					filteredRecord = (int)response.TotalRecords;
					return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
				}
				else if (type == "DSRChemist")
				{
					var model = abstractDSR_VisitedChemistServices.DSR_VisitedChemist_ByDSRId(pageParam, DSRId);

					totalRecord = (int)model.TotalRecords;
					filteredRecord = (int)model.TotalRecords;

					return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
				}
				else if (type == "DSRDoctor")
				{
					var model = abstractDSR_VisitedDoctorServices.DSR_VisitedDoctor_ByDSRId(pageParam, DSRId);

					totalRecord = (int)model.TotalRecords;
					filteredRecord = (int)model.TotalRecords;

					return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception ex)
			{
				//ErrorLogHelper.Log(ex);
				return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult BindDSR([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);
				var model = abstractDSRServices.DSR_All(pageParam, search, 0, 0, 0, 0, 0, 0, 0, null);

				totalRecord = (int)model.TotalRecords;
				filteredRecord = (int)model.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				//ErrorLogHelper.Log(ex);
				return Json(new DataTablesResponse(requestModel.Draw, new List<DSR>(), 0, 0), JsonRequestBehavior.AllowGet);
			}
		}

		private IList<SelectListItem> BindHeadquatar()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var models = abstractAddressService.HeadquarterSelectAll(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}


				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}
		private IList<SelectListItem> BindRegion()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var models = abstractAddressService.RegionSelectAll(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}


				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}
		private IList<SelectListItem> BindStatus()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var models = abstractDSRStatusServices.DSRStatus_All(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}


				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		private IList<SelectListItem> BindWorkAt()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var models = abstractRouteService.SelectAllRouteType(pageParam);

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}

				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		private IList<SelectListItem> BindWorkType()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var models = abstractWorkTypeServices.WorkType_All(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}

				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		private List<SelectListItem> RouteDropdownList()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var Headquarters = abstractRouteService.SelectAll(pageParam);

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category in Headquarters.Values)
			{
				items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> VisitTypeDropdownList()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var Headquarters = abstractVisitTypeServices.VisitType_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category in Headquarters.Values)
			{
				items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> CallInDropdownList()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var Headquarters = abstractCallInServices.CallIn_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category in Headquarters.Values)
			{
				items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> OutComeDropdownList()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var Headquarters = abstractOutcomeServices.Outcome_All(pageParam, "");

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category in Headquarters.Values)
			{
				items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
			}

			return items;
		}

		private List<SelectListItem> WorkWithDropdownList()
		{
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var employee = _AbstractEmployeeService.SelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in employee.Values)
            {
                items.Add(new SelectListItem() { Text = category.FirstName.ToString() + ' ' + category.LastName.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }
	}
}