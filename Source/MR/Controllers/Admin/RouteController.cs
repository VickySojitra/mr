﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Pages;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.Admin
{
    public class RouteController : BaseController
    {
        private readonly AbstractRouteService _RouteServices;
        private readonly AbstractAddressService _AddressService;

        public RouteController(AbstractRouteService RouteServices, AbstractAddressService AddressService)
        {
            _RouteServices = RouteServices;
            _AddressService = AddressService;
        }

        [HttpGet]
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.RouteType = RouteTypeDropdownList();
            ViewBag.RegionDD = RegionDropdownList();
            return View(new Route());
        }

        [HttpPost]
        [ActionName(Actions.GetRoute)]
        public JsonResult GetRoute(int Id)
        {
            var model = _RouteServices.Select(Id);
            return Json(model.Item);
        }

        [HttpPost]
        [ActionName(Actions.BindRoute)]
        //[ValidateAntiForgeryToken]
        public JsonResult BindRoute([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int RegionId = 0, int RouteTypeId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _RouteServices.SelectAll(pageParam, search, RegionId, RouteTypeId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.AddRoute)]
        public ActionResult AddRoute(Route users)
        {
            try
            {
                users.CreatedBy = users.UpdatedBy = ProjectSession.AdminUserID;
                SuccessResult<AbstractRoute> result = _RouteServices.InsertUpdate(users);

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.RecordSavedSuccessfully);
                    return RedirectToAction(Actions.Index, MR.Pages.Controllers.Route, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.AlreadyExist);
                    return RedirectToAction(Actions.Index, MR.Pages.Controllers.Route, new { Area = "" });
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(Actions.Index, MR.Pages.Controllers.Route, new { Area = "" });
            }
        }

        private List<SelectListItem> RouteTypeDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _RouteServices.SelectAllRouteType(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> RegionDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressService.RegionSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }
    }
}