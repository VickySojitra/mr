﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MR.Controllers.Admin
{
	public class TourPlanManagerController : BaseController
	{
		private readonly AbstractTourPlanManagerService abstractTPMServices = null;
		private readonly AbstractDSRStatusServices abstractDSRStatusServices = null;
		private readonly AbstractEmployeeService abstractEmployeeService = null;
		private readonly AbstractTourPlanMonthServices abstractTourPlanMonthServices;
		private readonly AbstractRouteService _RouteServices;
		private readonly AbstractWorkTypeServices abstractWorkTypeServices;
		private readonly AbstractTourPlanServices abstractTourPlanServices;
		private readonly AbstractDoctorAndChemistServices abstractDoctorAndChemistService;
		private readonly AbstractAddressService _AbstractAddressService;

		public TourPlanManagerController(AbstractTourPlanManagerService abstractTPMServices, AbstractDSRStatusServices abstractDSRStatusServices, AbstractEmployeeService abstractEmployeeService, AbstractTourPlanMonthServices abstractTourPlanMonthServices, AbstractRouteService _RouteServices, AbstractWorkTypeServices abstractWorkTypeServices, AbstractTourPlanServices abstractTourPlanServices, AbstractDoctorAndChemistServices abstractDoctorAndChemistService, AbstractAddressService abstractAddressService)
		{
			this.abstractTPMServices = abstractTPMServices;
			this.abstractDSRStatusServices = abstractDSRStatusServices;
			this.abstractEmployeeService = abstractEmployeeService;
			this.abstractTourPlanMonthServices = abstractTourPlanMonthServices;
			this._RouteServices = _RouteServices;
			this.abstractWorkTypeServices = abstractWorkTypeServices;
			this.abstractTourPlanServices = abstractTourPlanServices;
			this.abstractDoctorAndChemistService = abstractDoctorAndChemistService;
			this._AbstractAddressService = abstractAddressService;
		}
		public ActionResult Index()
		{
			ViewBag.Status = BindStatus();
			ViewBag.Headquarter = BindHeadquarterDrp();
			ViewBag.Region = BindRegionsDrp();
			return View();
		}
		[HttpPost]
		//[ValidateAntiForgeryToken]
		public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int IsSubmitted = 2, string Month = "", int Headquarter = 0, int Region = 0)
		{
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);
				
				var model = abstractTPMServices.TourPlanManager_All(pageParam, search,IsSubmitted,Month,Headquarter,Region);

				totalRecord = (int)model.TotalRecords;
				filteredRecord = (int)model.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);


			}
			catch (Exception ex)
			{
				
				return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
			}
		}
		[HttpPost]
		public JsonResult ViewProfile(long Id, string type = "")
		{
			if (type == "Employee")
			{
				var response = abstractEmployeeService.Employee_ById(ConvertTo.Integer(Id));
    //            if (response.Item !=null)
    //            {
				//	response.Item.Photo = "http://localhost:7531/TourPlanManager/Storage/Employee/3/Photo.jpg"; 

				//}
				return Json(response, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json("", JsonRequestBehavior.AllowGet);
			}
		}

		//public ActionResult ViewTourPlan(string Id)
		//{
		//	return View();
		//}
		public ActionResult CreateTourPlan(string TourPlanId = "MA==", int IsSubmitted = 1)
		{
			ViewBag.TourPlansId = ConvertTo.Integer(ConvertTo.Base64Decode(TourPlanId));
			ViewBag.IsSubmitted = IsSubmitted;
			return View();
		}
		//Tour Plan Month Grid =======================================
		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult TourPlanMonthData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TourPlanId = 0)
		{
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);
				var response = abstractTourPlanMonthServices.TourPlanMonth_All(pageParam, search, TourPlanId);

				totalRecord = (int)response.TotalRecords;
				filteredRecord = (int)response.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
		}
		public ActionResult CreateTourplanSingle(string TourPlanId = "MA==")
		{
			ViewBag.TourPlanDayId = ConvertTo.Integer(ConvertTo.Base64Decode(TourPlanId));
			ViewBag.WorkAtName = BindWorkAtDrp();
			ViewBag.WorkTypeName = BindWorkTypeDrp();
			ViewBag.RouteName = BindRouteDrp();
			ViewBag.EmployeeeDD = BindEmployeeDrp();

			return View();
		}

		[HttpPost]
		//[ValidateAntiForgeryToken]
		public JsonResult CallsList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TourPlanMonthId = 0, int EmployeeId = 0)
		{
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);
				var response = abstractDoctorAndChemistService.DoctorAndChemist_All(pageParam, search, TourPlanMonthId, EmployeeId);

				totalRecord = (int)response.TotalRecords;
				filteredRecord = (int)response.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
		}


		[HttpPost]
		public JsonResult CreatetourPlan(int Id = 0, string Month = "", string SubmittedDate = "", string LastDate = "")
		{
			TourPlan model = new TourPlan();
			model.Id = Id;
			model.Month = Month;
			model.SubmittedDate = SubmittedDate;
			model.LastDate = LastDate;
			model.EmployeeId = ProjectSession.AdminUserID;

			var result = abstractTourPlanServices.TourPlan_Upsert(model);
			return Json(result, JsonRequestBehavior.AllowGet);
		}
		[HttpPost]
		public JsonResult IsSubmittedTour(int Id = 0, bool IsSubmitted = false)
		{
			var result = abstractTourPlanServices.TourPlan_IsSubmitted(Id, IsSubmitted);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetTourPlanData(int Id = 0)
		{
			SuccessResult<AbstractTourPlan> successResult = abstractTourPlanServices.TourPlan_ById(Id);
			return Json(successResult, JsonRequestBehavior.AllowGet);
		}

		private IList<SelectListItem> BindStatus()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var models = abstractDSRStatusServices.DSRStatus_All(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Value) });
				}


				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		private List<SelectListItem> BindEmployeeDrp()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var result = abstractEmployeeService.Employee_ReportingPerson(pageParam, "", ProjectSession.AdminRoleID, true);

				foreach (var employee in result.Values)
				{
					items.Add(new SelectListItem() { Text = employee.FirstName.ToString(), Value = employee.Id.ToString() });
				}

				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		public IList<SelectListItem> BindWorkAtDrp()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;


				var models = _RouteServices.SelectAllRouteType(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}
				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		public IList<SelectListItem> BindWorkTypeDrp()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;


				var models = abstractWorkTypeServices.WorkType_All(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}
				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		public IList<SelectListItem> BindRouteDrp()
		{
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;


				var models = _RouteServices.SelectAll(pageParam, "");

				foreach (var master in models.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}
				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IList<SelectListItem> BindHeadquarterDrp()
		{			
			List<SelectListItem> items = new List<SelectListItem>();
			try
			{
				PageParam pageParam = new PageParam();
				pageParam.Offset = 0;
				pageParam.Limit = 0;

				var response = _AbstractAddressService.HeadquarterSelectAll(pageParam, null, 0, 0);

				foreach (var master in response.Values)
				{
					items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
				}
				return items;
			}
			catch (Exception)
			{
				return items;
			}
		}

		private List<SelectListItem> BindRegionsDrp()
		{
			PageParam pageParam = new PageParam()
			{
				Limit = int.MaxValue,
				Offset = 0
			};

			var Headquarters = _AbstractAddressService.RegionSelectAll(pageParam);

			List<SelectListItem> items = new List<SelectListItem>();
			foreach (var category in Headquarters.Values)
			{
				items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
			}

			return items;
		}

	}
}