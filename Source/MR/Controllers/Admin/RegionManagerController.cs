﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.Admin
{
    public class RegionManagerController : BaseController
	{

		#region Fields
		private readonly AbstractRouteService _RouteServices;
		private readonly AbstractAddressService _AbstractAddressService;
        private readonly AbstractEmployeeService _AbstractEmployeeService;
        #endregion

        #region Ctor
        public RegionManagerController(AbstractAddressService abstractAddressService, AbstractRouteService routeServices, AbstractEmployeeService AbstractEmployeeService)
		{
			this._AbstractAddressService = abstractAddressService;
			this._RouteServices = routeServices;
            this._AbstractEmployeeService = AbstractEmployeeService;

        }
		#endregion

		// GET: RegionManager
		public ActionResult Index()
        {
            ViewBag.RouteType = RouteTypeDropdownList();
            ViewBag.ManagerDD = ManagerDropdown();

            return View();
        }

		[HttpPost]
		public JsonResult RegionUpsert(int Id = 0, string Name = "", bool IsActive = true, int ManagerId = 0)
		{
			Region model = new Region();
			model.Id = Id;
			model.Name = Name;
			model.IsActive = IsActive;
            model.ManagerId = ManagerId;

            var result = _AbstractAddressService.InsertUpdateRegion(model);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult BindRegionList()
		{
			PageParam pageParam = new PageParam();
			pageParam.Offset = 0;
			pageParam.Limit = 0;

			var result = _AbstractAddressService.RegionSelectAll(pageParam, "");
			return Json(result.Values, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetRegionData(int Id = 0)
		{
			SuccessResult<AbstractRegion> successResult = _AbstractAddressService.Region_ById(Id);
			return Json(successResult, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult BindHeadquarterGrid([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,int RouteType = 0,int RegionId =0)
		{
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = Convert.ToString(requestModel.Search.Value);
				var response = _AbstractAddressService.HeadquarterSelectAll(pageParam, search, RouteType, RegionId);

				totalRecord = (int)response.TotalRecords;
				filteredRecord = (int)response.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult HeadquarterUpsert(int Id = 0, string Name = "", int RouteTypeId = 0, int RegionId = 0, bool IsActive = true, int ManagerId = 0)
		{
			Headquarter model = new Headquarter();
			model.Id = Id;
			model.Name = Name;
			model.RouteType = RouteTypeId;
			model.RegionId = RegionId;
			model.IsActive = IsActive;
            model.ManagerId = ManagerId;

            var result = _AbstractAddressService.InsertUpdateHeadquarter(model);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetHeadquarterData(int Id = 0)
		{
			SuccessResult<AbstractHeadquarter> successResult = _AbstractAddressService.Headquarter_ById(Id);
			return Json(successResult, JsonRequestBehavior.AllowGet);
		}


		[HttpPost]
		public JsonResult addRouteData(int Id = 0, string Name = "", int Type = 0, string RouteStart="",string RouteEnd="", decimal Distance =0, int RegionId = 0)
		{
			Route model = new Route();
			model.Id = Id;
			model.Name = Name;
			model.Type = Type;
			model.RouteStart = RouteStart;
			model.RouteEnd = RouteEnd;
			model.Distance = Distance;
            model.RegionId = RegionId;
            model.TotalFair = Distance * 2;

            var result = _RouteServices.InsertUpdate(model);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

        private List<SelectListItem> RouteTypeDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _RouteServices.SelectAllRouteType(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> ManagerDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var employee = _AbstractEmployeeService.SelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in employee.Values)
            {
                items.Add(new SelectListItem() { Text = category.FirstName.ToString() + ' ' + category.LastName.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }
    }
}