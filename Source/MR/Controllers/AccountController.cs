﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MR.Common;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Pages;
using MR.Services.Contract;
using static MR.Infrastructure.Enums;

namespace MR.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly AbstractEmployeeService employeeService;
        #endregion

        #region Ctor
        public AccountController(AbstractEmployeeService usersService)
        {
            this.employeeService = usersService;
        }
        #endregion

        #region Methods
        [HttpGet]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

		[HttpPost]
		[ValidateAntiForgeryToken]
		[ActionName(Actions.LogIn)]
		public ActionResult LogIn(string email, string password)
		    {
			SuccessResult<AbstractEmployee> userData = employeeService.Employee_Login(email, password);
			if (userData != null && userData.Code == 200 && userData.Item != null)
			{
				if (userData.Item.IsActive == false)
				{
					ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.InActiveAccount);
				}
				Session.Clear();
				ProjectSession.AdminUserID = ProjectSession.UserID = userData.Item.Id;
				ProjectSession.AdminUserName = ProjectSession.UserName = userData.Item.FirstName + " " + userData.Item.LastName;
				ProjectSession.AdminRoleID = userData.Item.Role;
				//ProjectSession.Menu = userData.Item.Permission;
				//ProjectSession.IsAdmin = (userData.Item.RoleName == "Admin");
				HttpCookie cookie = new HttpCookie("AdminUserLogin");
				cookie.Values.Add("Id", userData.Item.Id.ToString());
				cookie.Values.Add("LoginUserImg", userData.Item.Photo);
				cookie.Expires = DateTime.Now.AddHours(1);

				Response.Cookies.Add(cookie);
				return RedirectToAction(Actions.Index, Pages.Controllers.Home, new { Area = "" });
			}
			else
			{
				ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.InValidCredential);
			}
			return View();
		}

		[AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction(Actions.LogIn, Pages.Controllers.Account, new { Area = ""});
        }

		//[HttpPost]
		//[ActionName(Actions.ForgotPassword)]
		//public ActionResult ForgotPassword(string email)
		//{
		//	var user = employeeService.VerifyEmail(objmodel.Email).Item;
		//	if (user != null)
		//	{
		//		string body = string.Empty;

		//		using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ForgotPasswordAdmin.html")))
		//		{
		//			body = reader.ReadToEnd();
		//		}

		//		body = body.Replace("userId", ConvertTo.Base64Encode(user.Id.ToString()));

		//		EmailHelper.Send(objmodel.Email, "", "", "Forgot Password", body);

		//		Email email = new Email();
		//		email.CustomerId = user.Id;
		//		email.Text = "Forgot Password";
		//		SuccessResult<AbstractEmail> Response = emailServices.InsertUpdateEmail(email);

		//		TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.Mailsend);
		//	}
		//	else
		//	{
		//		TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.InvalidEmail);
		//	}
		//	return RedirectToAction(Actions.LogIn, Pages.Controllers.Account, new { Area = "" });
		//}

		[HttpGet]
        [ActionName(Actions.ResetPSW)]
        public ActionResult ResetPSW(string Id = null)
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.userid = ConvertTo.Base64Decode(Id);
            return View();
        }

        //[HttpPost]
        //[ValidateInput(false)]
        //[ValidateAntiForgeryToken]
        //[ActionName(Actions.AdminUserResetPassword)]
        //public ActionResult ResetPassword(Users Users, int id)
        //{
        //    if (Users.Password == Users.ConfirmPassword)
        //    {
        //        int userId = 0;
        //        Users.Id = id;

        //        userId = usersService.UserPasswordUpdate(Users);

        //        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.PasswordReSet);
        //        return RedirectToAction(Actions.LogIn, Pages.Controllers.Account, new { Area = ""});
        //    }
        //    else
        //    {
        //        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.PasswordUnmatch);
        //        return RedirectToAction(Actions.ResetPSW, Pages.Controllers.Account, new { Area = "", Id = ConvertTo.Base64Encode(id.ToString()) });
        //    }            

        //}
        #endregion

    }
}