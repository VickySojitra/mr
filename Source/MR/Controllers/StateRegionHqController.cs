﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MR.Controllers
{
	public class StateRegionHqController : Controller
    {

        #region Fields
        private readonly AbstractStateRegionHqServices abstractStateRegionHqServices;
        #endregion 

        #region Ctor
        public StateRegionHqController(
            AbstractStateRegionHqServices abstractStateRegionHqServices
            )
        {
            this.abstractStateRegionHqServices = abstractStateRegionHqServices;
        }
        #endregion


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StateRegionParent(string RegionSWQ = "MA==")
        {
            ViewBag.regionId = Convert.ToInt32(ConvertTo.Base64Decode(RegionSWQ));
            return View();
        }

        public ActionResult StateRegionGrandParent(string ParentRegionSWQ = "MA==", string RegionSWQ = "MA==")
        {
            ViewBag.parentRegionId = Convert.ToInt32(ConvertTo.Base64Decode(ParentRegionSWQ));
            ViewBag.regionSWQ = RegionSWQ;
            return View();
        }


        //Get all region data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult StateRegionHqData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ParentId = 0, int GrandParentId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractStateRegionHqServices.StateRegionHq_All(pageParam, search, ParentId, GrandParentId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        //Create Region
        [HttpPost]
        public JsonResult StateRegionAdd(int Id = 0, string Name = "", int ParentId = 0, int GrandParentId = 0)
        {
            StateRegionHq model = new StateRegionHq();
            model.Id = Id;
            model.Name = Name;
            model.ParentId = ParentId;
            model.GrandParentId = GrandParentId;

            var result = abstractStateRegionHqServices.StateRegionHq_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Get Byid region data
        [HttpPost]
        public JsonResult GetRegionData(string RegionId = "")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(RegionId));
            SuccessResult<AbstractStateRegionHq> successResult = abstractStateRegionHqServices.StateRegionHq_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

    }
}