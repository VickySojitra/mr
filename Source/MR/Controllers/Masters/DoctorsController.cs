﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Pages;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.Masters
{
	public class DoctorsController : BaseController
    {
        private readonly AbstractEmployeeService _EmployeeService;
        private readonly AbstractAddressService _AddressServices;
        private readonly AbstractDoctorService _DoctorServices;
        private readonly AbstractProductVsDoctorServices _DoctorVsProductServices;
        private readonly AbstractChemistVsDoctorServices _ChemistVsDoctorServices;


        public DoctorsController(AbstractAddressService AddressServices, AbstractDoctorService DoctorServices, AbstractEmployeeService EmployeeService,
            AbstractProductVsDoctorServices DoctorVsProductServices,
            AbstractChemistVsDoctorServices ChemistVsDoctorServices)
        {
            _AddressServices = AddressServices;
            _DoctorServices = DoctorServices;
            _EmployeeService = EmployeeService;
            _DoctorVsProductServices = DoctorVsProductServices;
            _ChemistVsDoctorServices = ChemistVsDoctorServices;
        }

        [HttpGet]
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpGet]
        [ActionName(Actions.Profile)]
        public ActionResult Profile(int Id)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            var model = _DoctorServices.Select(Id);
            return View(model.Item);
        }

        [HttpGet]
        [ActionName(Actions.ManageDoctors)]
        public ActionResult ManageDoctors(string DoctorId = "")
		{
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            AbstractDoctor doctor = null;
            if (string.IsNullOrWhiteSpace(DoctorId) == false)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(DoctorId));
                if (decryptedId > 0)
                {
                    doctor = _DoctorServices.Select(decryptedId).Item;
                    doctor.AddressData = _AddressServices.Select(doctor.Address).Item;
                }
            }

            if (doctor == null)
            {
                doctor = new Doctor();
                doctor.AddressData = new Address();
            }

            ViewBag.SpecializationDD = SpecializationDropdown();
            ViewBag.QualificationDD = QualificationDropdown();
            ViewBag.CategoryDD = DoctorCategoryDropdown();
            ViewBag.HeadQuarterDD = HeadQuarterDropdownList();
            ViewBag.RegionDD = RegionsDropdownList();

            return View(doctor);
        }

        [HttpPost]
        [ActionName(Actions.BindDoctors)]
        //[ValidateAntiForgeryToken]
        public JsonResult BindDoctors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _DoctorServices.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Doctor>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.BindProduct)]
        [ValidateAntiForgeryToken]
        public JsonResult BindProduct([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int doctorId = -1)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                doctorId = (doctorId == 0) ? -1 : doctorId;
                var model = _DoctorVsProductServices.ProductVsDoctor_ByDoctorId(pageParam, doctorId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<ProductVsDoctor>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.DeleteProduct)]
        public JsonResult DeleteProduct(int Id)
        {
            try
            {
                var model = _DoctorVsProductServices.ProductVsDoctor_Delete(Id, ProjectSession.AdminUserID);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.AddProduct)]
        public JsonResult AddProduct(int doctorId, int productId, int prodQuantity)
        {
            try
            {
                if (doctorId <= 0 || productId <= 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                AbstractProductVsDoctor product = new ProductVsDoctor()
                {
                    DoctorId = doctorId,
                    ProductId = productId,
                    Quantity = prodQuantity,
                    CreatedBy = ProjectSession.AdminUserID,
                    UpdatedBy = ProjectSession.AdminUserID
                };
                
                var model = _DoctorVsProductServices.ProductVsDoctor_Upsert(product);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.AddDoctor)]
        public ActionResult AddDoctor(Doctor users, HttpPostedFileBase avatar)
        {
            try
            {
                // Insert address
                if (users.AddressData != null)
                {
                    var address = _AddressServices.InsertUpdateAddress(users.AddressData);
                    users.Address = address.Item.Id;
                }

                // Insert chemist
                users.CreatedBy = users.UpdatedBy = ProjectSession.AdminUserID;
                SuccessResult<AbstractDoctor> result = _DoctorServices.InsertUpdate(users);

                if (avatar != null && avatar.ContentLength > 0)
                {
                    string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Doctors", result.Item.Id.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    string fileName = Path.Combine(profilePath, "Photo" + Path.GetExtension(avatar.FileName));
                    avatar.SaveAs(fileName);
                    result.Item.Photo = Path.Combine("Storage", "Doctors", result.Item.Id.ToString(), "Photo" + Path.GetExtension(avatar.FileName));
                    _DoctorServices.InsertUpdate(result.Item);
                }

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.RecordSavedSuccessfully);
                    return RedirectToAction(Actions.Index, Pages.Controllers.Doctors, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.AlreadyExist);
                    return RedirectToAction(Actions.ManageDoctors, Pages.Controllers.Doctors, new { Area = "", DoctorId = ConvertTo.Base64Encode(Convert.ToString(users.Id)) });
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(Actions.ManageDoctors, Pages.Controllers.Doctors, new { Area = "", DoctorId = ConvertTo.Base64Encode(Convert.ToString(users.Id)) });
            }
        }

        [HttpPost]
        [ActionName(Actions.GetDoctor)]
        public JsonResult GetDoctor(int Id)
        {
            var model = _DoctorServices.Select(Id);
            return Json(model.Item, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.BindChemists)]
        [ValidateAntiForgeryToken]
        public JsonResult BindChemists([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int doctorId = -1)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                doctorId = (doctorId == 0) ? -1 : doctorId;
                var model = _ChemistVsDoctorServices.ChemistVsDoctor_ByDoctorId(pageParam, doctorId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<ChemistVsDoctor>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        private List<SelectListItem> HeadQuarterDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.HeadquarterSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> RegionsDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.RegionSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> QualificationDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var qualification = _EmployeeService.QualificationSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in qualification.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> SpecializationDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var qualification = _EmployeeService.SpecializationSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in qualification.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> DoctorCategoryDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var qualification = _DoctorServices.SelectAllDoctorCategory(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in qualification.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }
    }
}