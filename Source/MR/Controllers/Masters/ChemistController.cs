﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Infrastructure;
using MR.Pages;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static MR.Infrastructure.Enums;

namespace MR.Controllers.Masters
{
	public class ChemistController : BaseController
    {
        private readonly AbstractAddressService _AddressServices;
        private readonly AbstractChemistService _ChemistServices;
        private readonly AbstractDoctorService _DoctorServices;
        private readonly AbstractProductVsChemistServices _ProductvsChemistServices;
        private readonly AbstractChemistVsDoctorServices _ChemistVsDoctorServices;

        public ChemistController(AbstractAddressService AddressServices, AbstractChemistService ChemistServices,
            AbstractDoctorService DoctorServices, AbstractProductVsChemistServices ProductvsChemistServices,
            AbstractChemistVsDoctorServices ChemistVsDoctorServices)
        {
            _AddressServices = AddressServices;
            _ChemistServices = ChemistServices;
            _DoctorServices = DoctorServices;
            _ProductvsChemistServices = ProductvsChemistServices;
            _ChemistVsDoctorServices = ChemistVsDoctorServices;
        }

        [HttpGet]
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpGet]
        [ActionName(Actions.Profile)]
        public ActionResult Profile(int Id)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            var model = _ChemistServices.Select(Id);            
            return View(model.Item);
        }

        [HttpGet]
        [ActionName(Actions.ManageChemist)]
        public ActionResult ManageChemist(string chemistId = "")
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.HeadQuarterDD = HeadQuarterDropdownList();
            ViewBag.RegionDD = RegionsDropdownList();
            ViewBag.TypeDD = TypeDropdownList();

            AbstractChemist chemist = null;
            if (string.IsNullOrWhiteSpace(chemistId) == false)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(chemistId));
                if (decryptedId > 0)
                {
                    chemist = _ChemistServices.Select(decryptedId).Item;
                    chemist.AddressData = _AddressServices.Select(chemist.Address).Item;
                }
            }

            if (chemist == null)
            {
                chemist = new Chemist();
                chemist.AddressData = new Address();
            }

            return View(chemist);
        }

        [HttpPost]
        [ActionName(Actions.BindChemists)]
        //[ValidateAntiForgeryToken]
        public JsonResult BindChemists([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _ChemistServices.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Chemist>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.BindProduct)]
        [ValidateAntiForgeryToken]
        public JsonResult BindProduct([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int chemistId = -1)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                chemistId = (chemistId == 0) ? -1 : chemistId;
                var model = _ProductvsChemistServices.ProductVsChemist_ByChemistId(pageParam, chemistId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<ProductVsChemist>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.DeleteProduct)]
        public JsonResult DeleteProduct(int Id)
        {
            try
            {
                var model = _ProductvsChemistServices.ProductVsChemist_Delete(Id, ProjectSession.AdminUserID);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.AddProduct)]
        public JsonResult AddProduct(int chemistId, int productId, int prodQuantity)
        {
            try
            {
                if (chemistId <= 0 || productId <= 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var product = new ProductVsChemist()
                {
                    ChemistId = chemistId,
                    ProductId = productId,
                    Quantity = prodQuantity,
                    CreatedBy = ProjectSession.AdminUserID,
                    UpdatedBy = ProjectSession.AdminUserID
                };

                var model = _ProductvsChemistServices.ProductVsChemist_Upsert(product);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.BindDoctors)]
        [ValidateAntiForgeryToken]
        public JsonResult BindDoctors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int chemistId = -1)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                chemistId = (chemistId == 0) ? -1 : chemistId;
                var model = _ChemistVsDoctorServices.ChemistVsDoctor_ByChemistId(pageParam, chemistId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<ChemistVsDoctor>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.AddDoctor)]
        public JsonResult AddDoctor(int chemistId, int doctorId)
        {
            try
            {
                if (chemistId <= 0 || doctorId <= 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var product = new ChemistVsDoctor()
                {
                    ChemistId = chemistId,
                    DoctorId = doctorId,                    
                    CreatedBy = ProjectSession.AdminUserID,
                    UpdatedBy = ProjectSession.AdminUserID
                };

                var model = _ChemistVsDoctorServices.ChemistVsDoctor_Upsert(product);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.DeleteDoctor)]
        public JsonResult DeleteDoctor(int Id)
        {
            try
            {
                var model = _ChemistVsDoctorServices.ChemistVsDoctor_Delete(Id, ProjectSession.AdminUserID);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.AddChemist)]
        public ActionResult AddChemist(Chemist users)
        {
            try
            {
                // Insert address
                if(users.AddressData != null)
                {
                    var address = _AddressServices.InsertUpdateAddress(users.AddressData);
                    users.Address = address.Item.Id;
                }

                // Insert chemist
                users.CreatedBy = users.UpdatedBy = ProjectSession.AdminUserID;
                SuccessResult<AbstractChemist> result = _ChemistServices.InsertUpdate(users);

                if (result.Item != null)
                {                    
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.RecordSavedSuccessfully);
                    return RedirectToAction(Actions.Index, Pages.Controllers.Chemist, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.AlreadyExist);
                    return RedirectToAction(Actions.ManageChemist, Pages.Controllers.Chemist, new { Area = "", chemistId = ConvertTo.Base64Encode(Convert.ToString(users.Id)) });
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.ContactToAdmin);
                return RedirectToAction(Actions.ManageChemist, Pages.Controllers.Chemist, new { Area = "", chemistId = ConvertTo.Base64Encode(Convert.ToString(users.Id)) });
            }
        }

        [HttpPost]
        [ActionName(Actions.GetChemist)]
        public JsonResult GetChemist(int Id)
        {
            var model = _ChemistServices.Select(Id);
            return Json(model.Item, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> HeadQuarterDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.HeadquarterSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> RegionsDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var Headquarters = _AddressServices.RegionSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in Headquarters.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }

        private List<SelectListItem> TypeDropdownList()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var qualification = _DoctorServices.SelectAllDoctorCategory(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category in qualification.Values)
            {
                items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.Id.ToString() });
            }

            return items;
        }
    }
}