﻿using DataTables.Mvc;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MR.Controllers
{
    public class TourPlanController : Controller
    {

        #region Fields
        private readonly AbstractTourPlanServices abstractTourPlanServices;
        private readonly AbstractTourPlanMonthServices abstractTourPlanMonthServices;
        private readonly AbstractMasterWorkAtServices abstractMasterWorkAtServices;
        private readonly AbstractWorkTypeServices abstractWorkTypeServices;
        private readonly AbstractDoctorService abstractDoctorService;
        private readonly AbstractChemistService abstractChemistService;
        private readonly AbstractDoctorAndChemistServices abstractDoctorAndChemistService;
        private readonly AbstractRouteService _RouteServices;
        private readonly AbstractEmployeeService _AbstractEmployeeService;

        #endregion

        #region Ctor
        public TourPlanController(
            AbstractTourPlanServices abstractTourPlanServices,
            AbstractTourPlanMonthServices abstractTourPlanMonthServices,
            AbstractMasterWorkAtServices abstractMasterWorkAtServices,
            AbstractWorkTypeServices abstractWorkTypeServices,
            AbstractDoctorService abstractDoctorService,
            AbstractChemistService abstractChemistService,
            AbstractDoctorAndChemistServices abstractDoctorAndChemistService,
            AbstractRouteService routeServices,
            AbstractEmployeeService abstractEmployeeService
            )
        {
            this.abstractTourPlanServices = abstractTourPlanServices;
            this.abstractTourPlanMonthServices = abstractTourPlanMonthServices;
            this.abstractMasterWorkAtServices = abstractMasterWorkAtServices;
            this.abstractWorkTypeServices = abstractWorkTypeServices;
            this.abstractDoctorService = abstractDoctorService;
            this.abstractChemistService = abstractChemistService;
            this.abstractDoctorAndChemistService = abstractDoctorAndChemistService;
            this._RouteServices = routeServices;
            this._AbstractEmployeeService = abstractEmployeeService;
        }
        #endregion


        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateTourPlan(string TourPlanId = "MA==", int IsSubmitted = 1)
        {
            ViewBag.TourPlansId = ConvertTo.Integer(ConvertTo.Base64Decode(TourPlanId));
            ViewBag.IsSubmitted = IsSubmitted;
            return View();
        }
        public ActionResult CreateTourplanSingle(string TourPlanId = "MA==")
        {
            ViewBag.TourPlanDayId = ConvertTo.Integer(ConvertTo.Base64Decode(TourPlanId));
            ViewBag.WorkAtName = BindWorkAtDrp();
            ViewBag.WorkTypeName = BindWorkTypeDrp();
            ViewBag.RouteName = BindRouteDrp();
            ViewBag.EmployeeeDD = BindEmployeeDrp();
            ViewBag.EmployeeeId = ProjectSession.AdminUserID;

            return View();
        }

        private List<SelectListItem> BindEmployeeDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var result = _AbstractEmployeeService.Employee_ReportingPerson(pageParam, "", ProjectSession.AdminRoleID, true);

                foreach (var employee in result.Values)
                {
                    items.Add(new SelectListItem() { Text = employee.FirstName.ToString(), Value = employee.Id.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> BindWorkAtDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var models = _RouteServices.SelectAllRouteType(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> BindWorkTypeDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var models = abstractWorkTypeServices.WorkType_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> BindRouteDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var models = _RouteServices.SelectAll(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }


        //Tour Plan Grid =======================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult TourPlanData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractTourPlanServices.TourPlan_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        //Tour Plan Month Grid =======================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult TourPlanMonthData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TourPlanId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractTourPlanMonthServices.TourPlanMonth_All(pageParam, search, TourPlanId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        //Dr call Grid =======================================
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult DrCallList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TourPlanMonthId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDoctorService.SelectAll(pageParam, search, TourPlanMonthId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        //Chemist call Grid =======================================
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult ChemistCallList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TourPlanMonthId = 0)

        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractChemistService.SelectAll(pageParam, search, TourPlanMonthId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult DoctorAdd(int Id = 0, int TourPlanMonthId = 0, string DoctorIds = "", string ChemistIds = "")
        {
            DoctorAndChemist model = new DoctorAndChemist();
            model.Id = Id;
            model.TourPlanMonthId = TourPlanMonthId;
            model.DoctorIds = DoctorIds;
            model.ChemistIds = ChemistIds;
            model.EmployeeId = ProjectSession.AdminUserID;

            var result = abstractDoctorAndChemistService.DoctorAndChemist_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChemistAdd(int Id = 0, int TourPlanMonthId = 0, string ChemistIds = "", string DoctorIds = "")
        {
            DoctorAndChemist model = new DoctorAndChemist();
            model.Id = Id;
            model.TourPlanMonthId = TourPlanMonthId;
            model.ChemistIds = ChemistIds;
            model.DoctorIds = DoctorIds;
            model.EmployeeId = ProjectSession.AdminUserID;

            var result = abstractDoctorAndChemistService.DoctorAndChemist_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //call Grid =======================================
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult CallsList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TourPlanMonthId = 0, int EmployeeId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = requestModel.Length;
                
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDoctorAndChemistService.DoctorAndChemist_All(pageParam, search, TourPlanMonthId, EmployeeId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        //Calls Delete
        [HttpPost]
        public JsonResult CallsDelete(int Ci = 0)
        {
            int DeletedBy = ProjectSession.AdminUserID;
            abstractDoctorAndChemistService.DoctorAndChemist_Delete(Ci, DeletedBy);
            return Json("Doctor And Chemist Data Deleted Successfully", JsonRequestBehavior.AllowGet);
        }

        //Get data Dr and Chamist
        [HttpPost]
        public JsonResult DrChamistData(int TourPlanMonthId = 0, int EmployeeId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;
            EmployeeId = ProjectSession.AdminUserID;
            var result = abstractDoctorAndChemistService.DoctorAndChemist_All(pageParam, "", TourPlanMonthId, EmployeeId);
            return Json(result.Values, JsonRequestBehavior.AllowGet);
        }


        //Work Details update
        [HttpPost]
        public JsonResult WorkDetailsUpdate(int Id = 0, int WorkAtId = 0, int WorkTypeId = 0, string WorkRouteIds = "")
        {
            TourPlanMonth model = new TourPlanMonth();
            model.Id = Id;
            model.WorkAtId = WorkAtId;
            model.WorkTypeId = WorkTypeId;
            model.WorkRouteIds = WorkRouteIds;

            var result = abstractTourPlanMonthServices.TourPlanMonth_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTourPlanMonthData(int Id = 0)
        {
            SuccessResult<AbstractTourPlanMonth> successResult = abstractTourPlanMonthServices.TourPlanMonth_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }


        //Create tour plan
        [HttpPost]
        public JsonResult CreatetourPlan(int Id = 0, string Month = "", string SubmittedDate = "", string LastDate = "")
        {
            TourPlan model = new TourPlan();
            model.Id = Id;
            model.Month = Month;
            model.SubmittedDate = SubmittedDate;
            model.LastDate = LastDate;
            model.EmployeeId = ProjectSession.UserID;

            var result = abstractTourPlanServices.TourPlan_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Submitted Tour Plan
        [HttpPost]
        public JsonResult IsSubmittedTour(int Id = 0, bool IsSubmitted = false)
        {
            var result = abstractTourPlanServices.TourPlan_IsSubmitted(Id, IsSubmitted);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetTourPlanData(int Id = 0)
        {
            SuccessResult<AbstractTourPlan> successResult = abstractTourPlanServices.TourPlan_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

    }
}