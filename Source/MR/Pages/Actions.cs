﻿namespace MR.Pages
{
    public class Actions
    {
        public const string ManageChemist = "ManageChemist";
        public const string BindChemists = "BindChemists";
        public const string AddChemist = "AddChemist";
        public const string GetChemist = "GetChemist";        
        public const string BindRoute = "BindRoute";
        public const string AddRoute = "AddRoute";
        public const string GetRoute = "GetRoute";
        public const string BindEmployee = "BindEmployee";
        public const string ManageEmployee = "ManageEmployee";
        public const string Profile = "Profile";
        public const string BindWorkExperiance = "BindWorkExperiance";
        public const string DeleteWorkExperience = "DeleteWorkExperience";
        public const string ManageDoctors = "ManageDoctors";
        public const string BindDoctors = "BindDoctors";
        public const string AddDoctor = "AddDoctor";
        public const string GetDoctor = "GetDoctor";
        public const string ManagePassword = "ManagePassword";
        public const string ManageProduct = "ManageProduct";
        public const string BindPresentation = "BindPresentation";
        public const string AddPresentation = "AddPresentation";
        public const string DeletePresentation = "DeletePresentation";
        public const string AddProductType = "AddProductType";
        public const string AddManufacture = "AddManufacture";
        public const string GetProduct = "GetProduct";
        public const string ProductDetail = "ProductDetail";
        public const string GetEmployee = "GetEmployee";
        public const string HeadquarterUpsert = "HeadquarterUpsert";
        public const string GetHeadquarterData = "GetHeadquarterData";
        public const string addRouteData = "addRouteData";
        public const string BindDSR = "BindDSR";
        public const string DSRReport = "DSRReport";
        public const string ViewDSR = "ViewDSR";
        public const string ManageDSR1 = "ManageDSR1";
        public const string ManageDSR2 = "ManageDSR2";
        public const string ManageDSR3 = "ManageDSR3";
        public const string BindData = "BindData";
        public const string AddDSR = "AddDSR";
        public const string RemoveChemist = "RemoveChemist";
        public const string TourPlanData = "TourPlanData";
        public const string TourPlanMonthData = "TourPlanMonthData";

        public const string AddChemistProduct = "AddChemistProduct";
        public const string RemoveDoctor = "RemoveDoctor";
        public const string AddDoctorProduct = "AddDoctorProduct";
        public const string SubmitDSRReport = "SubmitDSRReport";
        public const string DraftDSRReport = "DraftDSRReport";
        public const string DeleteDoctor = "DeleteDoctor";
        public const string DrCallList = "DrCallList";
        public const string ChemistCallList = "ChemistCallList";
        public const string DoctorAndChemistAdd = "DoctorAndChemistAdd";
        public const string ChemistAdd = "ChemistAdd";
        public const string DoctorAdd = "DoctorAdd";
        public const string CallsList = "CallsList";
        public const string CallsDelete = "CallsDelete";
        public const string DrChamistData = "DrChamistData";
        public const string WorkDetailsUpdate = "WorkDetailsUpdate";
        public const string GetTourPlanMonthData = "GetTourPlanMonthData";
        public const string CreatetourPlan = "CreatetourPlan";
        public const string StateRegionHqData = "StateRegionHqData";
        public const string StateRegionAdd = "StateRegionAdd";
        public const string GetRegionData = "GetRegionData";
        public const string GetTourPlanData = "GetTourPlanData";
        public const string AddEmployeeRegion = "AddEmployeeRegion";
        public const string BindEmployeeRegion = "BindEmployeeRegion";
        public const string EmployeeAddRegion = "EmployeeAddRegion";
        public const string GetEmployeeRegion = "GetEmployeeRegion";
        public const string AddEmployeeState = "AddEmployeeState";
        public const string AddEmployeeHq = "AddEmployeeHq";
        public const string AddWorkExperiance = "AddWorkExperiance";        

        #region Common
        public const string Index = "Index";
        public const string ChangeStatus = "ChangeStatus";
        #endregion Common        

        #region Dashboard
        public const string BindTopFiveOrders = "BindTopFiveOrders";
        public const string BindTopFiveOutOfStockProduct = "BindTopFiveOutOfStockProduct";
        public const string BindTopFiveFeedback = "BindTopFiveFeedback";
        public const string OutOfStockProductGrid = "OutOfStockProductGrid";
        public const string BindAllOutOfStockProduct = "BindAllOutOfStockProduct";
        public const string DashboardOrder = "DashboardOrder";
        public const string BindTopFiveSupport = "BindTopFiveSupport";
        public const string DashboardSupport = "DashboardSupport";
        public const string DashboardOrderDetails = "DashboardOrderDetails";
        public const string DashboardOrderEdit = "DashboardOrderEdit";
        public const string DashboardOrderDetailEdit = "DashboardOrderDetailEdit";
        public const string BindColor = "BindColor";
        public const string ExportExcel = "ExportExcel";
        public const string Download = "Download";
        public const string GenerateAWB = "GenerateAWB";
        public const string OutOfStockInventoryEdit = "OutOfStockInventoryEdit";
        public const string AddInventoryOfOutOfStockProduct = "AddInventoryOfOutOfStockProduct";
        #endregion

        #region Account
        public const string LogIn = "LogIn";
        public const string ForgotPassword = "ForgotPassword";
        public const string AdminUserResetPassword = "AdminUserResetPassword";
        public const string Logout = "Logout";
        public const string AccessDenied = "AccessDenied";
        public const string Error = "Error";
        public const string PrivacyPolicy = "PrivacyPolicy";
        public const string ResetPSW = "ResetPSW";
        #endregion

        #region Category
        public const string BindCategories = "BindCategories";
        public const string AddCategory = "AddCategory";
        public const string DeleteCategory = "DeleteCategory";
        public const string CategoryDetails = "CategoryDetails";
        public const string GetCategory = "GetCategory";
        #endregion     

        #region Product
        public const string BindProductMain = "BindProductMain";
        public const string AddProductMain = "AddProductMain";
        public const string DeleteProductMain = "DeleteProductMain";
        public const string ProductMainDetails = "ProductMainDetails";
        public const string CopyProduct = "CopyProduct";
        public const string BindProduct = "BindProduct";
        public const string DeleteProduct = "DeleteProduct";
        public const string _ProductGrid = "_ProductGrid";
        public const string AddProduct = "AddProduct";
        public const string ProductDetails = "ProductDetails";
        public const string Images = "Images";
        public const string BindProductSupport = "BindProductSupport";
        public const string ProductSupportDetails = "ProductSupportDetails";
        public const string Support = "Support";
        public const string DeleteMultipleProductMain = "DeleteMultipleProductMain";
        public const string ProductById = "ProductById";
        public const string BindModels = "BindModels";
        #endregion

        #region ProductImage
        public const string BindProductImage = "BindProductImage";
        public const string AddProductImage = "AddProductImage";
        public const string DeleteProductImage = "DeleteProductImage";
        public const string ProductImageDetails = "ProductImageDetails";
        public const string ChangeCover = "ChangeCover";
        #endregion

        #region Seller
        public const string SellerDetails = "SellerDetails";
        public const string AddSeller = "AddSeller";
        public const string DeleteSeller = "DeleteSeller";
        public const string BindSellers = "BindSellers";
        #endregion

        #region Color
        public const string AddEditColor = "AddEditSeller";
        public const string ColorDetails = "ColorDetails";
        public const string DeleteColor = "DeleteColor";
        public const string BindColors = "BindColors";
        #endregion

        #region User
        public const string UsersDetails = "UsersDetails";
        public const string AddUsers = "AddUsers";
        public const string DeleteUsers = "DeleteUsers";
        public const string BindUsers = "BindUsers";
        public const string StateDropdownListByCountry = "StateByCountry";
        public const string CityDropdownListByState = "CityByState";
        #endregion

        #region Cart
        public const string BindCart = "BindCart";
        public const string ApplyCoupon = "ApplyCoupon";
        #endregion

        #region Order
        public const string BindOrder = "BindOrder";
        public const string BindOrderDetails = "BindOrderDetails";
        public const string OrderDetails = "OrderDetails";
        public const string UpdateOrderDetailStatus = "UpdateOrderDetailStatus";
        public const string ChangeOrderStatus = "ChangeOrderStatus";
        public const string DownloadExcel = "DownloadExcel";
        public const string ExportOrder = "ExportOrder";
        public const string UpdateOrder = "UpdateOrder";
        public const string OrderBySeller = "OrderBySeller";
        public const string OrderByCouponCode = "OrderByCouponCode";
        public const string BindOrderBySeller = "BindOrderBySeller";
        public const string BindOrderByCouponCode = "BindOrderByCouponCode";
        public const string UpdateOrderDetails = "UpdateOrderDetails";
        public const string DashboardUpdateOrderDetailStatus = "DashboardUpdateOrderDetailStatus";
        public const string ReplaceOrder = "ReplaceOrder";
        #endregion

        #region Inventory
        public const string BindInventories = "BindInventories";
        public const string AddInventory = "AddInventory";
        public const string DeleteInventory = "DeleteInventory";
        public const string InventoryDetails = "InventoryDetails";
        public const string UpdateInventoryPrice = "UpdateInventoryPrice";
        #endregion

        #region Customer,NewCustomer
        public const string BindCustomers = "BindCustomers";
        public const string CustomerDetails = "CustomerDetails";
        public const string AddEditCustomer = "AddEditCustomer";
        public const string DeleteCustomer = "DeleteCustomer";
        public const string ViewTransaction = "ViewTransaction";
        public const string ViewTransactionByCustomer = "ViewTransactionByCustomer";
        #endregion

        #region Company
        public const string BindCompany = "BindCompany";
        public const string CompanyDetails = "CompanyDetails";
        public const string AddEditCompany = "AddEditCompany";
        #endregion

        #region Wishlist
        public const string BindWishlist = "BindWishlist";
        #endregion

        #region Feedback
        public const string BindFeeback = "BindFeeback";
        public const string FeedbackDetails = "FeedbackDetails";
        public const string InsertFeedback = "InsertFeedback";
        public const string DeleteFeedback = "DeleteFeedback";
        public const string GetColorsByProductMain = "GetColorsByProductMain";
        #endregion

        #region FeedbackImage
        public const string BindFeebackImage = "BindFeebackImage";
        #endregion

        #region Notification
        public const string BindNotification = "BindNotification";
        public const string AddNotification = "AddNotification";
        public const string NotificationDetails = "NotificationDetails";
        #endregion

        #region Support
        public const string BindSupport = "BindSupport";
        public const string BindSupportDetails = "BindSupportDetails";
        public const string SupportDetails = "SupportDetails";
        public const string AddSupportReply = "AddSupportReply";
        public const string InsertReply = "InsertReply";
        #endregion

        #region CouponCode
        public const string AddEditCouponCode = "AddEditCouponCode";
        public const string CouponCodeDetails = "CouponCodeDetails";
        public const string DeleteCouponCode = "DeleteCouponCode";
        public const string BindCouponCode = "BindCouponCode";
        public const string ChangeIsIndividual = "ChangeIsIndividual";
        #endregion

        #region Banner
        public const string AddEditBanner = "AddEditBanner";
        public const string BannerDetails = "BannerDetails";
        public const string DeleteBanner = "DeleteBanner";
        public const string BindBanner = "BindBanner";
        #endregion

        #region Testimonials
        public const string AddEditTestimonials = "AddEditTestimonials";
        public const string TestimonialsDetails = "TestimonialsDetails";
        public const string DeleteTestimonials = "DeleteTestimonials";
        public const string BindTestimonials = "BindTestimonials";
        #endregion

        #region Offers
        public const string AddEditOffers = "AddEditOffers";
        public const string OffersDetails = "OffersDetails";
        public const string DeleteOffers = "DeleteOffers";
        public const string BindOffers = "BindOffers";
        #endregion

        #region OfferProduct
        public const string AddEditOfferProduct = "AddEditOfferProduct";
        public const string OfferProductDetails = "OfferProductDetails";
        public const string DeleteOfferProduct = "DeleteOfferProduct";
        public const string BindOfferProduct = "BindOfferProduct";
        #endregion

        #region Setting
        public const string AddEditSetting = "AddEditSetting";
        public const string SettingDetails = "SettingDetails";
        public const string DeleteSetting = "DeleteSetting";
        public const string BindSetting = "BindSetting";
        #endregion

        #region Blog
        public const string AddEditBlog = "AddEditBlog";
        public const string BlogDetails = "BlogDetails";
        public const string DeleteBlog = "DeleteBlog";
        public const string BindBlog = "BindBlog";
        #endregion

        #region Deal
        public const string BindDeal = "BindDeal";
        public const string AddEditDeal = "AddEditDeal";
        public const string DealDetails = "DealDetails";
        public const string DeleteDeal = "DeleteDeal";
        #endregion

        #region DealIdVsProductMainId
        public const string AddEditDealIdVsProductMainId = "AddEditDealIdVsProductMainId";
        public const string DealIdVsProductMainIdDetails = "DealIdVsProductMainIdDetails";
        public const string DeleteDealIdVsProductMainId = "DeleteDealIdVsProductMainId";
        public const string BindDealIdVsProductMainId = "BindDealIdVsProductMainId";
        #endregion

        #region NewArrivals
        public const string AddEditNewArrivals = "AddEditNewArrivals";
        public const string NewArrivalsDetails = "NewArrivalsDetails";
        public const string DeleteNewArrivals = "DeleteNewArrivals";
        public const string BindNewArrivals = "BindNewArrivals";
        #endregion

        #region NewArrivalsProduct
        public const string AddEditNewArrivalsProduct = "AddEditNewArrivalsProduct";
        public const string NewArrivalsProductDetails = "NewArrivalsProductDetails";
        public const string DeleteNewArrivalsProduct = "DeleteNewArrivalsProduct";
        public const string BindNewArrivalsProduct = "BindNewArrivalsProduct";
        #endregion

        #region CMS
        public const string BindCMS = "BindCMS";
        public const string AddEditCMS = "AddEditCMS";
        public const string CMSDetails = "CMSDetails";
        public const string DeleteCMS = "DeleteCMS";
        #endregion

        #region ProductInventory
        public const string BindProductInventory = "BindProductInventory";
        public const string ProductInventoryDetails = "ProductInventoryDetails";
        public const string AddUpdateCurrentStock = "AddUpdateCurrentStock";
        #endregion

        #region BestProductSales
        public const string BestProduct = "BestProduct";
        public const string BestProductSelectAll = "BestProductSelectAll";
        #endregion

        #region CancelledOrder
        public const string CancelledOrder = "CancelledOrder";
        #endregion

        #region ReturnedOrder
        public const string ReturnedOrder = "ReturnedOrder";
        #endregion
        
        #region  Expense
        public const string BindExpenses = "BindExpenses";
        public const string ExpenseDetails = "ExpenseDetails";
        public const string AddExpenses = "AddExpenses";
        public const string DeleteExpenses = "DeleteExpenses";
        #endregion

        #region  ProfitLossReport
        public const string DownloadReport = "DownloadReport";
        #endregion
        
        #region BankDetails
        public const string BindBankDetails = "BindBankDetails";
        public const string ChangeMultipleStatus = "ChangeMultipleStatus";
        public const string ChangeBankDetailStatus = "ChangeBankDetailStatus";
        #endregion

        #region Contact
        public const string BindContact = "BindContact";
        #endregion
        
        #region Invoice
        public const string SendInvoice = "SendInvoice";
        #endregion

        #region Size
        public const string AddEditModel = "AddEditModel";
        public const string ModelDetails = "ModelDetails";
        public const string DeleteModel = "DeleteModel";        
        #endregion

        #region ReplacedOrder
        public const string ReplacedOrder = "ReplacedOrder";
        #endregion

        #region MonthSales
        public const string MonthSales = "MonthSales";
        public const string BindOrderForSales = "BindOrderForSales";
        #endregion

        #region OrderByState
        public const string OrderByState = "OrderByState";
        public const string BindOrderByState = "BindOrderByState";
        #endregion

        #region ProductLookbook
        public const string BindLookbook = "BindLookbook";
        public const string LookbookDetails = "LookbookDetails";
        public const string AddEditProductLookbook = "AddEditProductLookbook";
        public const string DeleteProductLookbook = "DeleteProductLookbook";
        public const string AddEditProductLookbookDetails = "AddEditProductLookbookDetails";
        public const string GenerateAWBShipRocket = "GenerateAWBShipRocket";
        public const string GetCourierCompanies = "GetCourierCompanies";
        #endregion
    }
}