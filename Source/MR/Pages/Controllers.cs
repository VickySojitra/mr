﻿namespace MR.Pages
{
    public class Controllers
    {
        public const string Employee = "Employee";
        public const string TourPlan = "TourPlan";
        public const string Route = "Route";
        public const string Home = "Home";
        public const string Chemist = "Chemist";
        public const string Doctors = "Doctors";
        public const string Account = "Account";
        public const string BankDetails = "BankDetails";
        public const string Banner = "Banner";
        public const string BestProductSales = "BestProductSales";
        public const string Blog = "Blog";
        public const string CancelledOrder = "CancelledOrder";
        public const string Cart = "Cart";
        public const string Category = "Category";
        public const string CMS = "CMS";
        public const string Color = "Color";
        public const string Contact = "Contact";
        public const string CouponCode = "CouponCode";
        public const string Customer = "Customer";
        public const string Company = "Company";
        public const string Dashboard = "Dashboard";
        public const string Deal = "Deal";
        public const string DealIdVsProductMainId = "DealIdVsProductMainId";
        public const string Expense = "Expense";
        public const string ExportOrder = "ExportOrder";
        public const string Feedback = "Feedback";
        public const string FeedbackImage = "FeedbackImage";
        public const string Inventory = "Inventory";
        public const string Invoice = "Invoice";
        public const string NewArrivals = "NewArrivals";
        public const string NewArrivalsProduct = "NewArrivalsProduct";
        public const string NewCustomer = "NewCustomer";
        public const string Notification = "Notification";
        public const string OfferProduct = "OfferProduct";
        public const string Offers = "Offers";
        public const string Order = "Order";
        public const string OrderReport = "OrderReport";
        public const string Product = "Product";
        public const string ProductImage = "ProductImage";
        public const string ProductInventory = "ProductInventory";
        public const string ProfitLossReport = "ProfitLossReport";
        public const string Seller = "Seller";
        public const string Setting = "Setting";
        public const string Model = "Model";
        public const string Support = "Support";
        public const string Testimonials = "Testimonials";
        public const string Users = "Users";
        public const string Wishlist = "Wishlist";
        public const string ReplacedOrder = "ReplacedOrder";
        public const string ReturnedOrder = "ReturnedOrder";
        public const string MonthSales = "MonthSales";
        public const string OrderByState = "OrderByState";
        public const string LookBook = "LookBook";
        public const string DSRManager = "DSRManager";
        public const string RegionManager = "RegionManager";
        public const string StateRegionHq = "StateRegionHq";
        public const string TourPlanManager = "TourPlanManager";
    }
}