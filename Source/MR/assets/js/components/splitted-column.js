const splittedColumn = (() => {

    $('[data-toggle-splitted-column]').on('click', function (e) {
        $('body').addClass('splitted-column-show');
        $('.navbar-backdrop').addClass('show');
    })

    $('.nav-cards-style .nav-link').on('click', function (e) {
        $('body').removeClass('splitted-column-show');
        $('.navbar-backdrop').removeClass('show');
    })

    $('.navbar-backdrop').on('click', function (e) {
        $('body').removeClass('splitted-column-show');
        $('.navbar-backdrop').removeClass('show');
    })

})();

export default splittedColumn;
