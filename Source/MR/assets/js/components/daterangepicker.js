const dateRangePicker = (() => {

    $('.single-date-picker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1940,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });
})();

export default dateRangePicker;
