const regionManager = (() => {
    
    if ($(window).width() <= 1200){	
        $(".column-region .nav-link").removeClass("active");

        $('.column-region .nav-link').on('click', function (e) {
            $("body").addClass("column-region-open");
        })

        $('.closer-region-content').on('click', function (e) {
            $("body").removeClass("column-region-open");
        })
    } else{
        $("body").removeClass("column-region-open");
    }
})();

export default regionManager;
