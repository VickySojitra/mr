const tooltip = (() => {

  $('[data-toggle="tooltip"]').tooltip()

})();

export default tooltip;
