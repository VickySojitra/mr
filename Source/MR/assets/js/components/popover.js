const popover = (() => {

  $('[data-toggle="popover"]').popover()

})();

export default popover;
