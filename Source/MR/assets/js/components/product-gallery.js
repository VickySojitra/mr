

const productGallery = (() => {

  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });

  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    focusOnSelect: true
  });

  // keeps thumbnails active when changing main image, via mouse/touch drag/swipe
  $('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    //remove all active class
    $('.slider-nav .slick-slide').removeClass('slick-current');
    //set active class for current slide
    $('.slider-nav .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');
  });

})();

export default productGallery;
