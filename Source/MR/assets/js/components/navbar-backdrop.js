const navbarBackdrop = (() => {

    $('.offcanvas-toggler').on('click', function (e) {
        $('.navbar-backdrop').toggleClass('show');
    })
    
    $('.navbar-backdrop').on('click', function (e) {
        $('.navbar-collapse').removeClass('show');
        $(this).removeClass('show');
    })

})();

export default navbarBackdrop;
