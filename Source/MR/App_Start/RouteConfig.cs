﻿using MR.Pages;
using System.Web.Mvc;
using System.Web.Routing;

namespace MR
{
	public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = MR.Pages.Controllers.Account, action = Actions.LogIn, id = UrlParameter.Optional }
            );
        }
    }
}
