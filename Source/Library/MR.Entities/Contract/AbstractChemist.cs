﻿using MR.Entities.V1;

namespace MR.Entities.Contract
{
    public abstract class AbstractChemist : AbstractBaseClass
	{
		public int Id { get; set; }
		public string Name { get; set; }		
		public bool IsActive { get; set; }
		public string Mobile { get; set; }
		public string Phone { get; set; }
		public int Headquarter { get; set; }
		public int Region { get; set; }
        public string ContactPerson { get; set; }
        public string WebSite { get; set; }
        public int Address { get; set; }
        public AbstractAddress AddressData { get; set; }
		public string AddressString { get; set; }
		public string BuisnessPotential { get; set; }
        public string Remarks { get; set; }
        public string HeadquarterName { get; set; }
        public string RegionName { get; set; }
		public int Type { get; set; }
		public int TourPlanMonthId { get; set; }
		public int IsAdded { get; set; }
	}
}
