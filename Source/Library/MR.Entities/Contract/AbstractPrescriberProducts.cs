﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractPrescriberProducts
    {
        public long Id { get; set; }

        public long PrescriberId { get; set; }

        public long PrescriberDoctorId { get; set; }

        public long PrescriberEmployeeId { get; set; }

        public long ProductId { get; set; }

        public string ProductName { get; set; }
        public string ProductPrice { get; set; }
        public string ProductQuantity { get; set; }
        public string ProductDescription { get; set; }


        public string Price { get; set; }
        public string Qty { get; set; }

        public string TotalBusiness { get; set; }


        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }




        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";



    }
}
