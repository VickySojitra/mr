﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractEmployeeRegion
    {
       public int Id { get; set; }
        public int UserId { get; set; }
        public int RegionId { get; set; }
        public string RegionIds { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
    }
}
