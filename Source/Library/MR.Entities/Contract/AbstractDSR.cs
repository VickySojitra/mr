﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractDSR
    {
        public long Id { get; set; }

        public string Date { get; set; }
        public long RouteTypeId { get; set; }
        public int HeadquaterId { get; set; }
        public int RegionId { get; set; }
        public string EmployeeProfile { get; set; }
        public string RouteTypeName { get; set; }
        public string Region { get; set; }
        public string Headquarter { get; set; }

        public long WorkTypeId { get; set; }
        public string WorkTypeName { get; set; }

        public long RouteId { get; set; }
        public string RouteName { get; set; }
        public long VisitTypeId { get; set; }
        public string VisitTypeName { get; set; }
        public long DoctorId { get; set; }
        public string DoctorFirstName { get; set; }
        public long ChemistId { get; set; }
        public string ChemistName { get; set; }

        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Gender { get; set; }

        public int VisitedDoctor { get; set; }
        public int VisitedChemist { get; set; }


        public long StatusId { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
       
       
        
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string EmployeeProfileUrl => EmployeeProfile != null ? Configurations.BaseUrl+EmployeeProfile: "";

    }
}
