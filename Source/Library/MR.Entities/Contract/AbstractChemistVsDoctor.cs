﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractChemistVsDoctor
    {
        public long Id { get; set; }

        public long DoctorId { get; set; }
        public string DoctorFirstName { get; set; }
        public string DoctorLastName { get; set; }
        public string DoctorMobileNo { get; set; }
        public string DoctorImage { get; set; }
        public string DoctorHQName { get; set; }
        public string DoctorRegionName { get; set; }
        public string DoctorSpecializationName { get; set; }
        public string DoctorQualificationName { get; set; }
        public string DoctorAddressString { get; set; }
        public Gender DoctorGender { get; set; }
        public bool DoctorStatus { get; set; }


        public long ChemistId { get; set; }
        public string ChemistName { get; set; }
        public string ChemistAddressString { get; set; }
        public string ChemistMobileNo { get; set; }
        public string ChemistContactPerson { get; set; }
        public bool ChemistStatus { get; set; }
        public decimal ChemistBuisnessPotential { get; set; }


        public string ChemistHeadquarter { get; set; }

        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }



        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";


    }
}
