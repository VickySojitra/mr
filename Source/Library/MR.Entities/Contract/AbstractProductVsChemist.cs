﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractProductVsChemist
    {
        public long Id { get; set; }
        public long ChemistId { get; set; }
        public string ChemistName { get; set; }
        public string ChemistMobileNo { get; set; }
        public string ChemistPhone { get; set; }
        public int ChemistAddress { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public long ProductQuantity { get; set; }
        public long Quantity { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate{ get; set; }
        public long DeletedBy { get; set; }
        public string ProductTypeName { get; set; }

        public string ManufactureName { get; set; }

        public string ImagePath { get; set; }
        public string CategoyName { get; set; }


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
    }
}
