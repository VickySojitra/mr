﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractLeave
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string EmergencyNumber { get; set; }
        public long LeaveTypeId { get; set; }
        public string LeaveTypeName { get; set; }
        public long DayTypeId { get; set; }
        public string DayTypeName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string LeaveReason { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        
    }
}
