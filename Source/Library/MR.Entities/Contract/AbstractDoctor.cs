﻿using MR.Common;
using System;

namespace MR.Entities.Contract
{
	public abstract class AbstractDoctor : AbstractBaseClass
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public bool IsActive { get; set; }
		public Gender Gender { get; set; }
		public string Mobile { get; set; }
		public string Phone { get; set; }		
		public DateTime? Birthdate { get; set; } = null;
		public DateTime? Anniversary { get; set; } = null;
		public string Photo { get; set; }
		public int Specialization { get; set; }
		public string SpecializationName { get; set; }
		public string QualificationName { get; set; }
		public string CategoryName { get; set; }
		public string HeadquarterName { get; set; }
		public string RegionName { get; set; }
		public int Qualification { get; set; }
		public int Category { get; set; }
		public string WebSite { get; set; }
		public int Address { get; set; }
		public AbstractAddress AddressData { get; set; }
		public string AddressString { get; set; }
		public string BuisnessPotential { get; set; }
		public string Remarks { get; set; }
		public int TourPlanMonthId { get; set; }
		public int IsAdded { get; set; }
	}

    public abstract class AbstractDoctorCategory : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
