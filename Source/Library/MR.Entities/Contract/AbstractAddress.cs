﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{ 
    public abstract class AbstractAddress : AbstractBaseClass
    {
        public int Id { get; set; }         
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Pincode { get; set; }
        public int Headquarter { get; set; }
        public string HeadquarterName { get; set; }
        public int Region { get; set; }
        public string RegionName { get; set; }
        public int State { get; set; }
        public string StateName { get; set; }
        public int City { get; set; }
        public string CityName { get; set; }
    }

    public abstract class AbstractCountry : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public abstract class AbstractState : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
    }

    public abstract class AbstractCity : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
    }

    public abstract class AbstractHeadquarter : AbstractBaseClass
    {
        public int Id { get; set; }
        public int ManagerId { get; set; }        
        public string ManagerName { get; set; }
        public string ManagerPhoto { get; set; }
        public string Name { get; set; }
        public int RouteType { get; set; }
        public int RegionId { get; set; }
        public bool IsActive { get; set; }
        public string RouteTypeName { get; set; }

    }

    public abstract class AbstractRegion : AbstractBaseClass
    {
        public int Id { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public AbstractHeadquarter Headquarter{get;set; }
        public AbstractRoute Route { get; set; }
        public int NumberOfHeadquarter { get; set; }
        public int NumberOfStockiest { get; set; }
        public int NumberOfDoctor { get; set; }
        public int NumberOfChemist { get; set; }
        public decimal BuisnessPotensial { get; set; }
    }
}
