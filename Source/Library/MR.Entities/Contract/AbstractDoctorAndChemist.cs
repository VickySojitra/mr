﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractDoctorAndChemist
    {
        public int Id { get; set; }
        public int DAC_EmployeeId { get; set; }
        public string DAC_EmployeeFirstName { get; set; }

        public string DAC_EmployeeLastName { get; set; }

        public string DAC_EmployeePhoto { get; set; }


        public int TourPlanMonthId { get; set; }
        public int DoctorId { get; set; }
        public string DoctorFirstName { get; set; }
        public string DoctorLastName { get; set; }

        public int ChemistId { get; set; }
        public string ChemistIdName { get; set; }
        public Boolean IsDelete { get; set; }
       
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int DeletedBy { get; set; }
        public string DoctorIds { get; set; }
        public string ChemistIds { get; set; }
        public string DoctorAddress { get; set; }
        public int DoctorAddressId { get; set; }
        public string ChemistAddress { get; set; }
        public string ChemistAddressId { get; set; }
        public int EmployeeId { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
      
    }
}
