﻿using MR.Common;
using MR.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractProduct
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
        public string CategoyName { get; set; }
        public long ProductTypeId { get; set; }
        public string ProductTypeName { get; set; }
        public long Quantity { get; set; }
        public long SIUnitesId { get; set; }
        public string QuantitySIUnites { get; set; }
        public long ManufactureId { get; set; }
        public string ManufactureName { get; set; }
        public string StoreBelowTemprature { get; set; }
        public long PrescriptionId { get; set; }
        public string PrescriptionName { get; set; }
        public string NRV { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal SellingPrice { get; set; }
        public string ImagePath { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }

        public List<AbstractProductImages> ProductImages { get; set; }
        public string ProductHighlightString { get; set; }
        public List<AbstractProductHighlight> ProductHighlight { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
    }
}
