﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractDSR_VisitedChemist
    {
        public long Id { get; set; }
        public long DSRId { get; set; }
        public string DSRDate { get; set; }
        public long ChemistId { get; set; }
        public string ChemistName { get; set; }
        public string ChemistMobile { get; set; }
        public long CallInId { get; set; }
        public string CallInName { get; set; }
        public long OutcomeId { get; set; }
        public string OutcomeName { get; set; }
        public string Photo { get; set; }
        
        public long WorkWithId { get; set; }
        public string WorkWithName { get; set; }
        public string Remarks { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string PhotoUrl => Photo != null ? Configurations.BaseUrl + Photo : "Not Found";
    }
}
