﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractCategoryMaster
    {
        public long Id { get; set; }
        public long FieldNameId { get; set; }
        public long MastersId { get; set; }
        public string MastersName { get; set; }

        public string Name { get; set; }
        public string FieldName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
    }
}
