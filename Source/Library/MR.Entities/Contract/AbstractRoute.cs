﻿namespace MR.Entities.Contract
{
    public abstract class AbstractRoute : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string RouteStart { get; set; }
        public string RouteEnd { get; set; }
        public int Type { get; set; }
        public string RouteTypeName { get; set; }
        public decimal Distance { get; set; }
        public decimal TotalFair { get; set; }
        public int RegionId { get; set; }
    }

    public abstract class AbstractRouteType : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
