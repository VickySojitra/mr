﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractTourPlanMonth
    {
        public int Id { get; set; }
        public int TPM_EmployeeId { get; set; }
        public string  TPM_EmployeeFirstName { get; set; }
        public string TPM_EmployeeLastName { get; set; }
        public string TPM_EmployeePhoto { get; set; }
        public int TourPlanId { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public int WorkAtId { get; set; }
        public string WorkAtName { get; set; }
        public int WorkTypeId { get; set; }
        public string WorkTypeName { get; set; }
        public string WorkRouteIds { get; set; }
        public string WorkRouteIdsName { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }



        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";





    }
}
