﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractChampaignMaster
    {
        public long Id { get; set; }

       
        public string CampaignName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public long ApplyMasterId { get; set; }
        public string ApplyMasterName { get; set; }

        public long RegionId { get; set; }

        public long HeadquarterId { get; set; }

       
        public string HeadquarterName { get; set; }

        public Boolean IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }



        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";


    }
}
