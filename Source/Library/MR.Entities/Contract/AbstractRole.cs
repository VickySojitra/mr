﻿namespace MR.Entities.Contract
{
	public abstract class AbstractRole : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
    }
}
