﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractJointWorking
    {
        public int Id { get; set; }
        public int TourPlanMonthId { get; set; }
      
       public int DoctorAndChemistId { get; set; }
        public int EmployeeIds { get; set; }
       
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
       

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
       

    }
}
