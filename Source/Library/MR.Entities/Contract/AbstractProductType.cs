﻿using MR.Common;
using MR.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractProductType
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";


    }
}
