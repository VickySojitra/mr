﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractDSRStatus
    {
        public long Id { get; set; }
        public string Name { get; set; }       
        public int Value { get; set; }
    }
}
