﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractDSR_VisitedDoctor_Products
    {
        public long Id { get; set; }

        public long DSR_VisitedDoctorId { get; set; }
        public long DoctorId { get; set; }
        public long DSRId { get; set; }
        public long ProductId { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string MfgName { get; set; }
        public string Qty { get; set; }
        public decimal OrderBookedQty { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public string ProductImage { get; set; }


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        


    }
}
