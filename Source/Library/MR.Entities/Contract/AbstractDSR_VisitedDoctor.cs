﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractDSR_VisitedDoctor
    {
        public long Id { get; set; }
        public long DSRId { get; set; }
        public long DoctorId { get; set; }
        public long CallInId { get; set; }
        public long OutcomeId { get; set; }
        public long WorkWithId { get; set; }
        public string WorkWithName { get; set; }
        public string Remarks { get; set; }
        public string Photo { get; set; }
        public string DoctorName { get; set; }
        public string CallInName { get; set; }
        public string DoctorAddress { get; set; }
        public string DoctorMobile { get; set; }
        public string DoctorPhone { get; set; }
        public string OutcomeName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string PhotoUrl => Photo != null ? Configurations.BaseUrl + Photo : "Not Found";

    }
}
