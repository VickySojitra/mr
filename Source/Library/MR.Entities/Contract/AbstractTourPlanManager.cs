﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
	public abstract class AbstractTourPlanManager
	{
		public int Id { get; set; }
		public string EmployeeName { get; set; }
		public int EmployeeId { get; set; }
		public string Gender { get; set; }
		public string SubmittedDate { get; set; }
		public int IsSubmitted { get; set; }
		public string RoleName { get; set; }
		public string Month { get; set; }

		//[NotMapped]
		//public string SubmittedDateStr => SubmittedDate != null ? SubmittedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
	}
}
