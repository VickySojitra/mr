﻿using MR.Common;
using MR.Entities.V1;
using System;

namespace MR.Entities.Contract
{
    public abstract class AbstractEmployee : AbstractBaseClass
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public Gender Gender { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string EmergencyNumber { get; set; }
        public DateTime? Birthdate { get; set; } = null;
        public DateTime? Anniversary { get; set; } = null;
        public string Email { get; set; }
        public string Photo { get; set; }
        public int PermanentAddress { get; set; }
        public Address PermanentAddressData { get; set; }
        public string PermanentAddressString { get; set; }

        public int CurrentAddress { get; set; }
        public WorkExperience workExperience { get; set; }
        public Address CurrentAddressData { get; set; }
        public string CurrentAddressString { get; set; }
        public string AddressProof { get; set; }
        public string DrivingLicense { get; set; }
        public string Remarks { get; set; }
        public string Qualification { get; set; }
        public DateTime JoiningDate { get; set; } = DateTime.Now;
        public DateTime ConfirmationDate { get; set; } = DateTime.Now;
        public int RoleId {get;set;} 
        public int Role { get; set; }
        public string RoleName { get; set; }
        public int Headquarter { get; set; }
        public string HeadquarterName { get; set; }
        public int Region { get; set; }
        public string RegionName { get; set; }
        public int ReportingPerson { get; set; }
        public string ReportingPersonName { get; set; }
        public decimal SalaryAmount { get; set; }
        public string PaymentMode { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountNo { get; set; }
        public string BankIFSCCode { get; set; }
        public string MobilePassword { get; set; }
        public string WebPassword { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public abstract class AbstractWorkExperience : AbstractBaseClass
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public DateTime? JoiningDate { get; set; } = null;
        public DateTime? LeavingDate { get; set; } = null;
        public decimal Salary { get; set; }
        public string LeavingReason { get; set; }
    }

    public abstract class AbstractQualification : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public abstract class AbstractSpecialization : AbstractBaseClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
