﻿using MR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Entities.Contract
{
    public abstract class AbstractProductVsDoctor
    {
        public long Id { get; set; }
        public long DoctorId { get; set; }
        public long ProductId { get; set; }
        public long Quantity { get; set; }
        public string DoctorFirstName { get; set; }
        public string DoctorLastName { get; set; }
        public string DoctorGender { get; set; }
        public string DoctorMobile { get; set; }
        public string DoctorBirthday { get; set; }
        public string DoctorPhone { get; set; }
        public string DoctorSpecialization { get; set; }
        public string DoctorQualification { get; set; }
        public string DoctorSpecializationName { get; set; }
        public string DoctorQualificationName { get; set; }
        public bool DoctorStatus { get; set; }
        public string ProductName { get; set; }
        public string CategoyName { get; set; }
        public string ProductTypeName { get; set; }
        public string DoctorImage { get; set; }
        public string ManufactureName { get; set; }

        public string ImagePath { get; set; }


        public long ProductQuantity { get; set; }
        public string ProductStoreBelowTemprature { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal ProductSellingPrice { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
       

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        
    }
}
