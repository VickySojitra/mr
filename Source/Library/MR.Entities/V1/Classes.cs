﻿using MR.Entities.Contract;

namespace MR.Entities.V1
{
    public class Employee : AbstractEmployee { }

    public class WorkExperience : AbstractWorkExperience { }

    public class Qualification : AbstractQualification { }

    public class Specialization : AbstractSpecialization { }

    public class Address : AbstractAddress { }

	public class Country : AbstractCountry { }

	public class State : AbstractState { }

	public class City : AbstractCity { }

    public class Headquarter : AbstractHeadquarter { }

    public class Region : AbstractRegion { }

    public class Role : AbstractRole { }

    public class Doctor : AbstractDoctor { }

    public class DoctorCategory : AbstractDoctorCategory { }

    public class Chemist : AbstractChemist { }

    public class Route : AbstractRoute { }

    public class RouteType : AbstractRouteType { }
}
