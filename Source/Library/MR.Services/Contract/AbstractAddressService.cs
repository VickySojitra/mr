﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractAddressService
    {
        //public abstract PagedList<AbstractAddress> SelectAll(PageParam pageParam);

        //public abstract PagedList<AbstractCountry> ContrySelectAll(PageParam pageParam, string search = "");

        public abstract PagedList<AbstractState> StateSelectAll(PageParam pageParam, string search = "");

        public abstract PagedList<AbstractCity> CitySelectAll(PageParam pageParam, string search = "");

        public abstract PagedList<AbstractHeadquarter> HeadquarterSelectAll(PageParam pageParam, string search = "", int RouteType = 0,int RegionId = 0);
        public abstract PagedList<AbstractRegion> RegionSelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractAddress> Select(int id);

        public abstract PagedList<AbstractCity> CitySelectByStateId(int stateId);

        //public abstract PagedList<AbstractState> StateSelectByCountryId(int countryId);

        public abstract SuccessResult<AbstractAddress> InsertUpdateAddress(AbstractAddress abstractAddress);

        public abstract bool Delete(int id);

        //public abstract PagedList<AbstractAddress> GetAddressByEmpId(int EmpId);

        public abstract SuccessResult<AbstractCity> InsertUpdateCity(AbstractCity abstractCity);

        public abstract SuccessResult<AbstractState> InsertUpdateState(AbstractState abstractState);

        public abstract SuccessResult<AbstractHeadquarter> InsertUpdateHeadquarter(AbstractHeadquarter abstractHeadquarter);
        public abstract SuccessResult<AbstractRegion> InsertUpdateRegion(AbstractRegion abstractRegion);
        public abstract SuccessResult<AbstractRegion> Region_ById(int id);
        public abstract SuccessResult<AbstractHeadquarter> Headquarter_ById(int id);
        public abstract SuccessResult<AbstractCity> City_ById(int id);
        public abstract SuccessResult<AbstractState> State_ById(int id);
    }
}
