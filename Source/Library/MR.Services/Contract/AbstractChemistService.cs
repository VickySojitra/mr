﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
	public abstract class AbstractChemistService
	{
		public abstract PagedList<AbstractChemist> SelectAll(PageParam pageParam, string search = "", int TourPlanMonthId = 0);

		public abstract SuccessResult<AbstractChemist> Select(int id);

		public abstract SuccessResult<AbstractChemist> InsertUpdate(AbstractChemist abstractChemist);
	}
}
