﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractWorkWithMasterServices
    {
        public abstract SuccessResult<AbstractWorkWithMaster> WorkWith_Upsert(AbstractWorkWithMaster abstractWorkType);
        public abstract PagedList<AbstractWorkWithMaster> WorkWith_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractWorkWithMaster> WorkWith_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractWorkWithMaster> WorkWith_ById(long Id);
    }
}
