﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractDoctorAndChemistServices
    {
        public abstract SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist_Upsert(AbstractDoctorAndChemist abstractDoctorAndChemist);
        public abstract PagedList<AbstractDoctorAndChemist> DoctorAndChemist_All(PageParam pageParam, string search, int TourPlanMonthId, int EmployeeId);
        public abstract SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist_Delete(long Id, long DeletedBy);
    }
}
