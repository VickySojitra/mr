﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractOutcomeServices
    {
        public abstract SuccessResult<AbstractOutcome> Outcome_Upsert(AbstractOutcome abstractHMS_Builder);
        public abstract SuccessResult<AbstractOutcome> Outcome_ById(long Id);
        public abstract PagedList<AbstractOutcome> Outcome_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractOutcome> Outcome_Delete(long Id, long DeletedBy);
    }
}
