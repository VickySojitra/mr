﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractProductServices
    {
        public abstract SuccessResult<AbstractProduct> Product_Upsert(AbstractProduct abstractProduct);
        public abstract SuccessResult<AbstractProduct> Product_ById(long Id);
        public abstract PagedList<AbstractProduct> Product_All(PageParam pageParam, string search, long CategoryId, long ProductTypeId, long SIUnitesId, long ManufactureId, long PrescriptionId);
        public abstract SuccessResult<AbstractProduct> Product_Delete(long Id);

    }
}
