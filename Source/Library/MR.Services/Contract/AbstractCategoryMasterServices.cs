﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Services.Contract
{
    public abstract class AbstractCategoryMasterServices
    {
        public abstract SuccessResult<AbstractCategoryMaster> CategoryMaster_Upsert(AbstractCategoryMaster abstractCategoryMaster);
        public abstract SuccessResult<AbstractCategoryMaster> CategoryMaster_ById(long Id);
        public abstract PagedList<AbstractCategoryMaster> CategoryMaster_All(PageParam pageParam,string Search = "",long FieldNameId = 0);
        public abstract PagedList<AbstractFieldName> FieldName_All(PageParam pageParam,string Search = "", int masterId = 0);
        public abstract PagedList<AbstractFieldName> Master_All(PageParam pageParam, string Search = "");

    }
}
