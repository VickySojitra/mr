﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractDSR_VisitedChemistServices
    {
        public abstract SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_ById(long Id);
        public abstract SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_Upsert(AbstractDSR_VisitedChemist abstractDSR_VisitedChemist);
        public abstract PagedList<AbstractDSR_VisitedChemist> DSR_VisitedChemist_ByDSRId(PageParam pageParam, long DSRId);
        public abstract SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_Delete(long Id);

    }
}
