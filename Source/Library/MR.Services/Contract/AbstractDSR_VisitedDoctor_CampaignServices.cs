﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractDSR_VisitedDoctor_CampaignServices
    {
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_Upsert(AbstractDSR_VisitedDoctor_Campaign abstractDSR_VisitedDoctor_Campaign);
        public abstract PagedList<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_ById(long Id);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_Delete(long Id, long DeletedBy);

    }
}
