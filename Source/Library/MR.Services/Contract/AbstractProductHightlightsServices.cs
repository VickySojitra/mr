﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractProductHightlightsServices
    {
        public abstract SuccessResult<AbstractProductHightlights> ProductHightlights_Upsert(AbstractProductHightlights abstractProductHightlights);
        public abstract SuccessResult<AbstractProductHightlights> ProductHightlights_ById(long Id);
        public abstract PagedList<AbstractProductHightlights> ProductHightlights_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductHightlights> ProductHightlights_Delete(long Id);

    }
}
