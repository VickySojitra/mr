﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractRoleService
    {
        public abstract PagedList<AbstractRole> SelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractRole> InsertUpdate(AbstractRole abstractRole);
    }
}
