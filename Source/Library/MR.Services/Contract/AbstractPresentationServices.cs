﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractPresentationServices
    {
        public abstract SuccessResult<AbstractPresentation> Presentation_Upsert(AbstractPresentation abstractPresentation);
        public abstract SuccessResult<AbstractPresentation> Presentation_ById(long Id);
        public abstract PagedList<AbstractPresentation> Presentation_All(PageParam pageParam, string search, int ProductId);
        public abstract SuccessResult<AbstractPresentation> Presentation_Delete(long Id);

        public abstract PagedList<AbstractPresentation> Presentation_ByProductId(PageParam pageParam, long ProductId);

    }
}
