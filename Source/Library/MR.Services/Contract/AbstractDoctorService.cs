﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
	public abstract class AbstractDoctorService
    {
		public abstract PagedList<AbstractDoctor> SelectAll(PageParam pageParam, string search = "", int TourPlanMonthId = 0);

		public abstract SuccessResult<AbstractDoctor> Select(int id);

		public abstract SuccessResult<AbstractDoctor> InsertUpdate(AbstractDoctor abstractDoctor);

		public abstract PagedList<AbstractDoctorCategory> SelectAllDoctorCategory(PageParam pageParam, string search = "");

		public abstract SuccessResult<AbstractDoctorCategory> InsertUpdateDoctorCategory(AbstractDoctorCategory abstractDoctorCategory);
		public abstract SuccessResult<AbstractDoctorCategory> DoctorCategory_ById(int id);
	}
}
