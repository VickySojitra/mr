﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractRouteService
    {
        public abstract PagedList<AbstractRoute> SelectAll(PageParam pageParam, string search = "", int RegionId = 0, int RouteTypeId= 0);

        public abstract SuccessResult<AbstractRoute> Select(int id);

        public abstract SuccessResult<AbstractRoute> InsertUpdate(AbstractRoute abstractRoute);

        public abstract PagedList<AbstractRouteType> SelectAllRouteType(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractRouteType> InsertUpdateRouteType(AbstractRouteType abstractRouteType);
    }
}
