﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractProductPrescriptionServices
    {
        public abstract SuccessResult<AbstractProductPrescription> ProductPrescription_Upsert(AbstractProductPrescription abstractProductPrescription);
        public abstract SuccessResult<AbstractProductPrescription> ProductPrescription_ById(long Id);
        public abstract PagedList<AbstractProductPrescription> ProductPrescription_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductPrescription> ProductPrescription_Delete(long Id);

    }
}
