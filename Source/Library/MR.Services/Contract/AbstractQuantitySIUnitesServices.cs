﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractQuantitySIUnitesServices
    {
        public abstract SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites_Upsert(AbstractQuantitySIUnites abstractQuantitySIUnites);
        public abstract SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites_ById(long Id);
        public abstract PagedList<AbstractQuantitySIUnites> QuantitySIUnites_All(PageParam pageParam, string search);
    }
}
