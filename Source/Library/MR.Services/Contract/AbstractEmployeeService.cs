﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractEmployeeService
    {
        public abstract PagedList<AbstractEmployee> SelectAll(PageParam pageParam, string search = "");
        public abstract PagedList<AbstractEmployee> Employee_ReportingPerson(PageParam pageParam, string search = "", int RoleId = 0, bool IsTourPlanDropDown = false);

        public abstract SuccessResult<AbstractEmployee> Select(int id);

        public abstract SuccessResult<AbstractEmployee> Employee_ById(int Id);

        public abstract SuccessResult<AbstractEmployee> InsertUpdate(AbstractEmployee abstractAddress);

        public abstract bool Delete(int id);

        public abstract PagedList<AbstractQualification> QualificationSelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractQualification> InsertUpdateQualification(AbstractQualification abstractQualification);

        public abstract PagedList<AbstractSpecialization> SpecializationSelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractSpecialization> InsertUpdateSpecialization(AbstractSpecialization abstractSpecialization);

        public abstract PagedList<AbstractWorkExperience> WorkExperienceByEmployeeId(int empId);

        public abstract SuccessResult<AbstractWorkExperience> InsertUpdateWorkExperience(AbstractWorkExperience abstractWorkExp);

        public abstract bool DeleteWorkExperience(int empId);
        public abstract SuccessResult<AbstractEmployee> Employee_Login(string Email, string WebPassword);
        public abstract SuccessResult<AbstractEmployee> Employee_ChangePassword(int Id, string OldPassword, string NewPassword,string ConfirmPassword);
        public abstract SuccessResult<AbstractQualification> Qualification_ById(int Id);
        public abstract SuccessResult<AbstractSpecialization> Specialization_ById(int Id);
    }
}
