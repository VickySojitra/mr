﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractTourPlanServices
    {
        public abstract SuccessResult<AbstractTourPlan> TourPlan_Upsert(AbstractTourPlan abstractTourPlan);
        public abstract SuccessResult<AbstractTourPlan> TourPlan_ById(int Id);
        public abstract SuccessResult<AbstractTourPlan> TourPlan_IsSubmitted(int Id, bool IsSubmitted);
        public abstract PagedList<AbstractTourPlan> TourPlan_All(PageParam pageParam, string search);

    }
}
