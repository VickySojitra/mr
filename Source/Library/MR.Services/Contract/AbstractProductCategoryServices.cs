﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractProductCategoryServices
    {
        public abstract SuccessResult<AbstractProductCategory> ProductCategory_Upsert(AbstractProductCategory abstractProductCategory);
        public abstract SuccessResult<AbstractProductCategory> ProductCategory_ById(long Id);
        public abstract PagedList<AbstractProductCategory> ProductCategory_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductCategory> ProductCategory_Delete(long Id);
    }
}
