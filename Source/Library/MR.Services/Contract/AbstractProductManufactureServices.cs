﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractProductManufactureServices
    {
        public abstract SuccessResult<AbstractProductManufacture> ProductManufacture_Upsert(AbstractProductManufacture abstractProductManufacture);
        public abstract SuccessResult<AbstractProductManufacture> ProductManufacture_ById(long Id);
        public abstract PagedList<AbstractProductManufacture> ProductManufacture_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductManufacture> ProductManufacture_Delete(long Id);
    }
}
