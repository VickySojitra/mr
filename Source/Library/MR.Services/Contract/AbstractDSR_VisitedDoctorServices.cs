﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractDSR_VisitedDoctorServices
    {
        public abstract SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_Upsert(AbstractDSR_VisitedDoctor abstractDSR_VisitedDoctor);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_ById(long Id);
        public abstract PagedList<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_ByDSRId(PageParam pageParam, long DSRId);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_Delete(long Id);

    }
}
