﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractMR_PresentationServices
    {
        public abstract SuccessResult<AbstractMR_Presentation> MR_Presentation_Upsert(AbstractMR_Presentation abstractMR_Presentation);
        public abstract SuccessResult<AbstractMR_Presentation> MR_Presentation_ById(long Id);
        public abstract PagedList<AbstractMR_Presentation> MR_Presentation_All(PageParam pageParam, string search, long EmployeeId, long ProductId);
        public abstract SuccessResult<AbstractMR_Presentation> MR_Presentation_Delete(long Id, long DeletedBy);

    }
}
