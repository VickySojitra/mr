﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractPrescriberServices
    {
        public abstract SuccessResult<AbstractPrescriber> Prescriber_Upsert(AbstractPrescriber abstractPrescriber);
        public abstract PagedList<AbstractPrescriber> Prescriber_ByDoctorId(PageParam pageParam, long DoctorId);
        public abstract SuccessResult<AbstractPrescriber> Prescriber_ById(long Id);
    }
}
