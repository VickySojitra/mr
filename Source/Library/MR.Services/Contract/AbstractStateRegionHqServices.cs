﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractStateRegionHqServices
    {
        public abstract SuccessResult<AbstractStateRegionHq> StateRegionHq_Upsert(AbstractStateRegionHq abstractHMS_Builder);
        public abstract SuccessResult<AbstractStateRegionHq> StateRegionHq_ById(int Id);
        public abstract PagedList<AbstractStateRegionHq> StateRegionHq_All(PageParam pageParam, string search, int ParentId, int GrandParentId);
        public abstract PagedList<AbstractStateRegionHq> StateRegionHq_AllRegion();
        public abstract SuccessResult<AbstractStateRegionHq> StateRegionHq_Delete(int Id, int DeletedBy);

    }
}
