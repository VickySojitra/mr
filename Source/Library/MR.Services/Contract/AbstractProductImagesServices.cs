﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractProductImagesServices
    {
        public abstract SuccessResult<AbstractProductImages> ProductImages_Upsert(AbstractProductImages abstractProductImages);
        public abstract SuccessResult<AbstractProductImages> ProductImages_ById(long Id);
        public abstract PagedList<AbstractProductImages> ProductImages_All(PageParam pageParam, string search, long ProductId);
        public abstract SuccessResult<AbstractProductImages> ProductImages_Delete(long Id);
    }
}
