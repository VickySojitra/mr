﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractMR_PresentationResourcesServices
    {
        public abstract SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources_Upsert(AbstractMR_PresentationResources abstractMR_PresentationResources);
        public abstract PagedList<AbstractMR_PresentationResources> MR_PresentationResources_ByMR_PresentationId(PageParam pageParam, long MR_PresentationId);
        public abstract SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources_Delete(long Id, long DeletedBy);

    }
}
