﻿using MR.Common.Paging;
using MR.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Services.Contract
{
	public abstract class AbstractTourPlanManagerService
	{
		public abstract PagedList<AbstractTourPlanManager> TourPlanManager_All(PageParam pageParam, string search, int IsSubmitted, string Month, int Headquarter = 0, int Region = 0);

	}
}
