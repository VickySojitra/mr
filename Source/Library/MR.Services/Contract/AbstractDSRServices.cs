﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractDSRServices
    {
        public abstract SuccessResult<AbstractDSR> DSR_Upsert(AbstractDSR abstractDSR);
        public abstract SuccessResult<AbstractDSR> DSR_ById(long Id);
        public abstract PagedList<AbstractDSR> DSR_All(PageParam pageParam, string search, long RouteTypeId, long WorkTypeId, long RouteId, long VisitTypeId, long DoctorId, long ChemistId, int EmployeeId, AbstractDSR abstractDSR);
        public abstract SuccessResult<AbstractDSR> DSR_Delete(long Id, long DeletedBy);

    }
}
