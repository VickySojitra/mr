﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractChemistVsDoctorServices
    {
        public abstract SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor_Upsert(AbstractChemistVsDoctor abstractChemistVsDoctor);
        public abstract PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_All(PageParam pageParam, string search, long DoctorId, long ChemistId);
        public abstract PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId);
        public abstract PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_ByChemistId(PageParam pageParam, long ChemistId);
        public abstract SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor_Delete(long Id, long DeletedBy);

    }
}
