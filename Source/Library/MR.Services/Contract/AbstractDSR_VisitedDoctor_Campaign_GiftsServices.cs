﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Services.Contract
{
    public abstract class AbstractDSR_VisitedDoctor_Campaign_GiftsServices
    {
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_Upsert(AbstractDSR_VisitedDoctor_Campaign_Gifts abstractDSR_VisitedDoctor_Campaign_Gifts);
        public abstract PagedList<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId(PageParam pageParam, long DSR_VisitedDoctor_CampaignId);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_ById(long Id);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_Delete(long Id, long DeletedBy);

    }
}
