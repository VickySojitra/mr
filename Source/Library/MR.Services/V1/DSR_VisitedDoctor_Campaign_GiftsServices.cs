﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DSR_VisitedDoctor_Campaign_GiftsServices : AbstractDSR_VisitedDoctor_Campaign_GiftsServices
    {
        private AbstractDSR_VisitedDoctor_Campaign_GiftsDao abstractDSR_VisitedDoctor_Campaign_GiftsDao;

        public DSR_VisitedDoctor_Campaign_GiftsServices(AbstractDSR_VisitedDoctor_Campaign_GiftsDao abstractDSR_VisitedDoctor_Campaign_GiftsDao)
        {
            this.abstractDSR_VisitedDoctor_Campaign_GiftsDao = abstractDSR_VisitedDoctor_Campaign_GiftsDao;
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_Upsert(AbstractDSR_VisitedDoctor_Campaign_Gifts abstractDSR_VisitedDoctor_Campaign_Gifts)
        {
            return this.abstractDSR_VisitedDoctor_Campaign_GiftsDao.DSR_VisitedDoctor_Campaign_Gifts_Upsert(abstractDSR_VisitedDoctor_Campaign_Gifts);
        }






        public override PagedList<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId(PageParam pageParam, long DSR_VisitedDoctor_CampaignId)
        {
            return this.abstractDSR_VisitedDoctor_Campaign_GiftsDao.DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId(pageParam, DSR_VisitedDoctor_CampaignId);

        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_ById(long Id)
        {
            return this.abstractDSR_VisitedDoctor_Campaign_GiftsDao.DSR_VisitedDoctor_Campaign_Gifts_ById(Id);

        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_Delete(long Id, long DeletedBy)
        {
            return this.abstractDSR_VisitedDoctor_Campaign_GiftsDao.DSR_VisitedDoctor_Campaign_Gifts_Delete(Id, DeletedBy);
        }

    }
}
