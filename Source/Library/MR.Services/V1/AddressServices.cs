﻿using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class AddressServices : AbstractAddressService
    {
        private AbstractAddressDao abstractAddressDao;

        public AddressServices(AbstractAddressDao abstractAddressDao)
        {
            this.abstractAddressDao = abstractAddressDao;
        }

        //public override PagedList<AbstractAddress> SelectAll(PageParam pageParam)
        //{
        //    return this.abstractAddressDao.SelectAll(pageParam);
        //}

        public override SuccessResult<AbstractAddress> Select(int id)
        {
            return this.abstractAddressDao.Select(id);
        }

        public override SuccessResult<AbstractAddress> InsertUpdateAddress(AbstractAddress abstractAddress)
        {
            return this.abstractAddressDao.InsertUpdateAddress(abstractAddress);
        }

        public override bool Delete(int id)
        {
            return this.abstractAddressDao.Delete(id);
        }

        //public override PagedList<AbstractAddress> GetAddressByEmpId(int EmpId)
        //{
        //    return this.abstractAddressDao.GetAddressByEmpId(EmpId);
        //}

        //public override PagedList<AbstractCountry> ContrySelectAll(PageParam pageParam, string search = "")
        //{
        //    return this.abstractAddressDao.ContrySelectAll(pageParam, search);
        //}

        public override PagedList<AbstractState> StateSelectAll(PageParam pageParam, string search = "")
        {
            return this.abstractAddressDao.StateSelectAll(pageParam, search);
        }

        public override PagedList<AbstractHeadquarter> HeadquarterSelectAll(PageParam pageParam, string search = "",int RouteType= 0,int RegionId=0)
        {
            return this.abstractAddressDao.HeadquarterSelectAll(pageParam, search, RouteType, RegionId);
        }

        public override PagedList<AbstractRegion> RegionSelectAll(PageParam pageParam, string search = "")
        {
            return this.abstractAddressDao.RegionSelectAll(pageParam, search);
        }

        public override PagedList<AbstractCity> CitySelectAll(PageParam pageParam, string search = "")
        {
            return this.abstractAddressDao.CitySelectAll(pageParam, search);
        }

        public override PagedList<AbstractCity> CitySelectByStateId(int stateId)
        {
            return this.abstractAddressDao.CitySelectByStateId(stateId);
        }

        public override SuccessResult<AbstractCity> InsertUpdateCity(AbstractCity abstractCity)
        {
            return this.abstractAddressDao.InsertUpdateCity(abstractCity);
        }

        public override SuccessResult<AbstractState> InsertUpdateState(AbstractState abstractState)
        {
            return this.abstractAddressDao.InsertUpdateState(abstractState);
        }

        public override SuccessResult<AbstractHeadquarter> InsertUpdateHeadquarter(AbstractHeadquarter abstractHeadquarter)
        {
            return this.abstractAddressDao.InsertUpdateHeadquarter(abstractHeadquarter);
        }

        public override SuccessResult<AbstractRegion> InsertUpdateRegion(AbstractRegion abstractRegion)
        {
            return this.abstractAddressDao.InsertUpdateRegion(abstractRegion);
        }
        public override SuccessResult<AbstractRegion> Region_ById(int id)
        {
            return this.abstractAddressDao.Region_ById(id);
        }
        public override SuccessResult<AbstractHeadquarter> Headquarter_ById(int id)
        {
            return this.abstractAddressDao.Headquarter_ById(id);
        }
        public override SuccessResult<AbstractCity> City_ById(int id)
        {
            return this.abstractAddressDao.City_ById(id);
        }
        public override SuccessResult<AbstractState> State_ById(int id)
        {
            return this.abstractAddressDao.State_ById(id);
        }
        //public override PagedList<AbstractState> StateSelectByCountryId(int countryId)
        //{
        //    return this.abstractAddressDao.StateSelectByCountryId(countryId);
        //}
    }
}
