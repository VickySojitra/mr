﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductTypeServices : AbstractProductTypeServices
    {

        private AbstractProductTypeDao abstractProductTypeDao;

        public ProductTypeServices(AbstractProductTypeDao abstractProductTypeDao)
        {
            this.abstractProductTypeDao = abstractProductTypeDao;
        }

        public override PagedList<AbstractProductType> ProductType_All(PageParam pageParam, string search)
        {
            return this.abstractProductTypeDao.ProductType_All(pageParam, search);
        }

        public override SuccessResult<AbstractProductType> ProductType_ById(long Id)
        {
            return this.abstractProductTypeDao.ProductType_ById(Id);
        }

        public override SuccessResult<AbstractProductType> ProductType_Delete(long Id)
        {
            return this.abstractProductTypeDao.ProductType_Delete(Id);
        }

        public override SuccessResult<AbstractProductType> ProductType_Upsert(AbstractProductType abstractProductType)
        {
            return this.abstractProductTypeDao.ProductType_Upsert(abstractProductType);
        }
    }
}
