﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class VisitTypeServices : AbstractVisitTypeServices
    {
        private AbstractVisitTypeDao abstractVisitTypeDao;

        public VisitTypeServices(AbstractVisitTypeDao abstractVisitTypeDao)
        {
            this.abstractVisitTypeDao = abstractVisitTypeDao;
        }

        public override SuccessResult<AbstractVisitType> VisitType_Upsert(AbstractVisitType abstractVisitType)
        {
            return this.abstractVisitTypeDao.VisitType_Upsert(abstractVisitType);
        }

        public override SuccessResult<AbstractVisitType> VisitType_ById(long Id)
        {
            return this.abstractVisitTypeDao.VisitType_ById(Id);
        }

        public override PagedList<AbstractVisitType> VisitType_All(PageParam pageParam, string search)
        {
            return this.abstractVisitTypeDao.VisitType_All(pageParam, search);
        }

        public override SuccessResult<AbstractVisitType> VisitType_Delete(long Id, long DeletedBy)
        {
            return this.abstractVisitTypeDao.VisitType_Delete(Id, DeletedBy);
        }

        


    }
}
