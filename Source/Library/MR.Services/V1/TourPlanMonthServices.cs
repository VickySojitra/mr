﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
namespace MR.Services.V1
{
    public class TourPlanMonthServices : AbstractTourPlanMonthServices
    {
        private AbstractTourPlanMonthDao abstractTourPlanMonthDao;

        public TourPlanMonthServices(AbstractTourPlanMonthDao abstractTourPlanMonthDao)
        {
            this.abstractTourPlanMonthDao = abstractTourPlanMonthDao;
        }
        public override PagedList<AbstractTourPlanMonth> TourPlanMonth_All(PageParam pageParam, string search, int TourPlanId)
        {
            return this.abstractTourPlanMonthDao.TourPlanMonth_All(pageParam, search,TourPlanId);
        }

        public override SuccessResult<AbstractTourPlanMonth> TourPlanMonth_Upsert(AbstractTourPlanMonth abstractTourPlanMonth)
        {
            return this.abstractTourPlanMonthDao.TourPlanMonth_Upsert(abstractTourPlanMonth);
        }

        public override SuccessResult<AbstractTourPlanMonth> TourPlanMonth_ById(int Id)
        {
            return this.abstractTourPlanMonthDao.TourPlanMonth_ById(Id);
        }
    }
}
