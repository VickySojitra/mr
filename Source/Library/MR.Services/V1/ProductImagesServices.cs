﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductImagesServices : AbstractProductImagesServices
    {
        private AbstractProductImagesDao abstractProductImagesDao;

        public ProductImagesServices(AbstractProductImagesDao abstractProductImagesDao)
        {
            this.abstractProductImagesDao = abstractProductImagesDao;
        }

        public override SuccessResult<AbstractProductImages> ProductImages_Upsert(AbstractProductImages abstractProductImages)
        {
            return this.abstractProductImagesDao.ProductImages_Upsert(abstractProductImages);
        }

        public override SuccessResult<AbstractProductImages> ProductImages_ById(long Id)
        {
            return this.abstractProductImagesDao.ProductImages_ById(Id);
        }

        public override PagedList<AbstractProductImages> ProductImages_All(PageParam pageParam, string search, long ProductId)
        {
            return this.abstractProductImagesDao.ProductImages_All(pageParam, search, ProductId);

        }


        public override SuccessResult<AbstractProductImages> ProductImages_Delete(long Id)
        {
            return this.abstractProductImagesDao.ProductImages_Delete(Id);
        }

    }
}
