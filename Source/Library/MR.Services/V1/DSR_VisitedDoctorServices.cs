﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DSR_VisitedDoctorServices : AbstractDSR_VisitedDoctorServices
    {
        private AbstractDSR_VisitedDoctorDao abstractDSR_VisitedDoctorDao;

        public DSR_VisitedDoctorServices(AbstractDSR_VisitedDoctorDao abstractDSR_VisitedDoctorDao)

        {
            this.abstractDSR_VisitedDoctorDao = abstractDSR_VisitedDoctorDao;

        }

        public override PagedList<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_ByDSRId(PageParam pageParam, long DSRId)
        {
            return this.abstractDSR_VisitedDoctorDao.DSR_VisitedDoctor_ByDSRId(pageParam, DSRId);
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_ById(long Id)
        {
            return this.abstractDSR_VisitedDoctorDao.DSR_VisitedDoctor_ById(Id);
        }


        public override SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_Delete(long Id)
        {
            return this.abstractDSR_VisitedDoctorDao.DSR_VisitedDoctor_Delete(Id);
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_Upsert(AbstractDSR_VisitedDoctor abstractDSR_VisitedDoctor)
        {
            return this.abstractDSR_VisitedDoctorDao.DSR_VisitedDoctor_Upsert(abstractDSR_VisitedDoctor);
        }
    }
}
