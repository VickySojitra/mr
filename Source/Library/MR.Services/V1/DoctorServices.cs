﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DoctorServices : AbstractDoctorService
    {
        private AbstractDoctorDao abstractDoctorDao;

        public DoctorServices(AbstractDoctorDao abstractDoctorDao)
        {
            this.abstractDoctorDao = abstractDoctorDao;
        }

		public override SuccessResult<AbstractDoctor> InsertUpdate(AbstractDoctor abstractDoctor)
		{
			return this.abstractDoctorDao.InsertUpdate(abstractDoctor);
		}

		public override SuccessResult<AbstractDoctorCategory> InsertUpdateDoctorCategory(AbstractDoctorCategory abstractDoctorCategory)
		{
			return this.abstractDoctorDao.InsertUpdateDoctorCategory(abstractDoctorCategory);
		}

		public override SuccessResult<AbstractDoctor> Select(int id)
		{
			return this.abstractDoctorDao.Select(id);
		}

		public override PagedList<AbstractDoctor> SelectAll(PageParam pageParam, string search = "", int TourPlanMonthId = 0)
		{
			return this.abstractDoctorDao.SelectAll(pageParam, search, TourPlanMonthId);
		}

		public override PagedList<AbstractDoctorCategory> SelectAllDoctorCategory(PageParam pageParam, string search = "")
		{
			return this.abstractDoctorDao.SelectAllDoctorCategory(pageParam, search);
		}

		public override SuccessResult<AbstractDoctorCategory> DoctorCategory_ById(int id)
		{
			return this.abstractDoctorDao.DoctorCategory_ById(id);
		}
	}
}
