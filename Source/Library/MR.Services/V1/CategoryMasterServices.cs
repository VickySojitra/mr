﻿using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Services.V1
{
    public class CategoryMasterServices : AbstractCategoryMasterServices
    {
        private AbstractCategoryMasterDao abstractCategoryMasterDao = null;

        public CategoryMasterServices(AbstractCategoryMasterDao abstractCategoryMasterDao)
        {
            this.abstractCategoryMasterDao = abstractCategoryMasterDao;
        }

        public override PagedList<AbstractCategoryMaster> CategoryMaster_All(PageParam pageParam,string Search ="", long FieldNameId = 0)
        {
            return abstractCategoryMasterDao.CategoryMaster_All(pageParam, Search, FieldNameId);
        }
        public override PagedList<AbstractFieldName> FieldName_All(PageParam pageParam, string Search = "", int masterId = 0)
        {
            return abstractCategoryMasterDao.FieldName_All(pageParam, Search, masterId);
        }

        public override PagedList<AbstractFieldName> Master_All(PageParam pageParam, string Search = "")
        {
            return abstractCategoryMasterDao.Master_All(pageParam, Search);
        }

        public override SuccessResult<AbstractCategoryMaster> CategoryMaster_ById(long Id)
        {
            return abstractCategoryMasterDao.CategoryMaster_ById(Id);
        }

        public override SuccessResult<AbstractCategoryMaster> CategoryMaster_Upsert(AbstractCategoryMaster abstractCategoryMaster)
        {
            return abstractCategoryMasterDao.CategoryMaster_Upsert(abstractCategoryMaster);
        }
    }
}
