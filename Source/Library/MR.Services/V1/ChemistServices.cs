﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
	public class ChemistServices : AbstractChemistService
    {
        private AbstractChemistDao abstractChemistDao;

        public ChemistServices(AbstractChemistDao abstractChemistDao)
        {
            this.abstractChemistDao = abstractChemistDao;
        }

		public override SuccessResult<AbstractChemist> InsertUpdate(AbstractChemist abstractDoctor)
		{
			return this.abstractChemistDao.InsertUpdate(abstractDoctor);
		}

		public override SuccessResult<AbstractChemist> Select(int id)
		{
			return this.abstractChemistDao.Select(id);
		}

		public override PagedList<AbstractChemist> SelectAll(PageParam pageParam, string search = "",int TourPlanMonthId = 0)
		{
			return this.abstractChemistDao.SelectAll(pageParam, search, TourPlanMonthId);
		}
	}
}
