﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ChampaignMasterServices : AbstractChampaignMasterServices
    {
        private AbstractChampaignMasterDao abstractChampaignMasterDao;

        public ChampaignMasterServices(AbstractChampaignMasterDao abstractChampaignMasterDao)
        {
            this.abstractChampaignMasterDao = abstractChampaignMasterDao;
        }
        public override PagedList<AbstractChampaignMaster> ChampaignMaster_All(PageParam pageParam, string search, long ApplyMasterId, long RegionId, long HeadquarterId)
        {
            return this.abstractChampaignMasterDao.ChampaignMaster_All(pageParam, search, ApplyMasterId, RegionId, HeadquarterId);
        }

        public override SuccessResult<AbstractChampaignMaster> ChampaignMaster_ById(long Id)
        {
            return this.abstractChampaignMasterDao.ChampaignMaster_ById(Id);
        }

        public override SuccessResult<AbstractChampaignMaster> ChampaignMaster_Delete(long Id, long DeletedBy)
        {
            return this.abstractChampaignMasterDao.ChampaignMaster_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractChampaignMaster> ChampaignMaster_Upsert(AbstractChampaignMaster abstractChampaignMaster)
        {
            return this.abstractChampaignMasterDao.ChampaignMaster_Upsert(abstractChampaignMaster);
        }
    }
}
