﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductServices : AbstractProductServices
    {
        private AbstractProductDao abstractProductDao;

        public ProductServices(AbstractProductDao abstractProductDao)
        {
            this.abstractProductDao = abstractProductDao;
        }

        public override SuccessResult<AbstractProduct> Product_Upsert(AbstractProduct abstractProduct)
        {
            return this.abstractProductDao.Product_Upsert(abstractProduct);
        }

        public override SuccessResult<AbstractProduct> Product_ById(long Id)
        {
            return this.abstractProductDao.Product_ById(Id);
        }

        public override PagedList<AbstractProduct> Product_All(PageParam pageParam, string search, long CategoryId, long ProductTypeId, long SIUnitesId, long ManufactureId, long PrescriptionId)
        {
            return this.abstractProductDao.Product_All(pageParam, search, CategoryId, ProductTypeId, SIUnitesId, ManufactureId, PrescriptionId);

        }


        public override SuccessResult<AbstractProduct> Product_Delete(long Id)
        {
            return this.abstractProductDao.Product_Delete(Id);
        }

    }
}
