﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ApplyMasterServices : AbstractApplyMasterServices
    {
        private AbstractApplyMasterDao abstractApplyMasterDao;

        public ApplyMasterServices(AbstractApplyMasterDao abstractApplyMasterDao)

        {
            this.abstractApplyMasterDao = abstractApplyMasterDao;

        }
        public override PagedList<AbstractApplyMaster> ApplyMaster_All(PageParam pageParam, string search)
        {
            return this.abstractApplyMasterDao.ApplyMaster_All(pageParam, search);
        }

        public override SuccessResult<AbstractApplyMaster> ApplyMaster_ById(long Id)
        {
            return this.abstractApplyMasterDao.ApplyMaster_ById(Id);
        }



        public override SuccessResult<AbstractApplyMaster> ApplyMaster_Delete(long Id, long DeletedBy)
        {
            return this.abstractApplyMasterDao.ApplyMaster_Delete(Id, DeletedBy);
        }



        public override SuccessResult<AbstractApplyMaster> ApplyMaster_Upsert(AbstractApplyMaster abstractApplyMaster)
        {
            return this.abstractApplyMasterDao.ApplyMaster_Upsert(abstractApplyMaster);
        }
    }
}
