﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class MasterWorkAtServices : AbstractMasterWorkAtServices
    {
        private AbstractMasterWorkAtDao abstractMasterWorkAtDao;

        public MasterWorkAtServices(AbstractMasterWorkAtDao abstractMasterWorkAtDao)
        {
            this.abstractMasterWorkAtDao = abstractMasterWorkAtDao;
        }

        
        public override PagedList<AbstractMasterWorkAt> MasterWorkAt_All(PageParam pageParam, string search)
        {
            return this.abstractMasterWorkAtDao.MasterWorkAt_All(pageParam, search);

        }

        
    }
}
