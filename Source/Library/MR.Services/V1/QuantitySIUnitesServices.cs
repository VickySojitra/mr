﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class QuantitySIUnitesServices : AbstractQuantitySIUnitesServices
    {

        private AbstractQuantitySIUnitesDao abstractQuantitySIUnitesDao;

        public QuantitySIUnitesServices(AbstractQuantitySIUnitesDao abstractQuantitySIUnitesDao)
        {
            this.abstractQuantitySIUnitesDao = abstractQuantitySIUnitesDao;
        }

        public override PagedList<AbstractQuantitySIUnites> QuantitySIUnites_All(PageParam pageParam, string search)
        {
            return this.abstractQuantitySIUnitesDao.QuantitySIUnites_All(pageParam, search);
        }

        public override SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites_ById(long Id)
        {
            return this.abstractQuantitySIUnitesDao.QuantitySIUnites_ById(Id);
        }

        public override SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites_Upsert(AbstractQuantitySIUnites abstractQuantitySIUnites)
        {
            return this.abstractQuantitySIUnitesDao.QuantitySIUnites_Upsert(abstractQuantitySIUnites);
        }
    }
}
