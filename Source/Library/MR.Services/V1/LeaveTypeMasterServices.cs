﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class LeaveTypeMasterServices : AbstractLeaveTypeMasterServices
    {
        private AbstractLeaveTypeMasterDao abstractLeaveTypeMasterDao;

        public LeaveTypeMasterServices(AbstractLeaveTypeMasterDao abstractLeaveTypeMasterDao)
        {
            this.abstractLeaveTypeMasterDao = abstractLeaveTypeMasterDao;
        }

       

        public override PagedList<AbstractLeaveTypeMaster> LeaveTypeMaster_All(PageParam pageParam, string search)
        {
            return this.abstractLeaveTypeMasterDao.LeaveTypeMaster_All(pageParam, search);
        }

      


    }
}
