﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductPrescriptionServices : AbstractProductPrescriptionServices
    {
        private AbstractProductPrescriptionDao abstractProductPrescriptionDao;

        public ProductPrescriptionServices(AbstractProductPrescriptionDao abstractProductPrescriptionDao)
        {
            this.abstractProductPrescriptionDao = abstractProductPrescriptionDao;
        }

        public override SuccessResult<AbstractProductPrescription> ProductPrescription_Upsert(AbstractProductPrescription abstractProductPrescription)
        {
            return this.abstractProductPrescriptionDao.ProductPrescription_Upsert(abstractProductPrescription);
        }

        public override SuccessResult<AbstractProductPrescription> ProductPrescription_ById(long Id)
        {
            return this.abstractProductPrescriptionDao.ProductPrescription_ById(Id);
        }

        public override PagedList<AbstractProductPrescription> ProductPrescription_All(PageParam pageParam, string search)
        {
            return this.abstractProductPrescriptionDao.ProductPrescription_All(pageParam, search);

        }


        public override SuccessResult<AbstractProductPrescription> ProductPrescription_Delete(long Id)
        {
            return this.abstractProductPrescriptionDao.ProductPrescription_Delete(Id);
        }

    }
}
