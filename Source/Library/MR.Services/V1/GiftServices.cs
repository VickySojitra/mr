﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class GiftServices : AbstractGiftServices
    {
        private AbstractGiftDao abstractGiftDao;

        public GiftServices(AbstractGiftDao abstractGiftDao)
        {
            this.abstractGiftDao = abstractGiftDao;
        }
        public override SuccessResult<AbstractGift> Gift_ById(long Id)
        {
            return this.abstractGiftDao.Gift_ById(Id);
        }

        public override PagedList<AbstractGift> Gift_ChampaignMasterId(PageParam pageParam, long ChampaignMasterId)
        {
            return this.abstractGiftDao.Gift_ChampaignMasterId(pageParam, ChampaignMasterId);
        }

        public override SuccessResult<AbstractGift> Gift_Delete(long Id, long DeletedBy)
        {
            return this.abstractGiftDao.Gift_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractGift> Gift_Upsert(AbstractGift abstractGift)
        {
            return this.abstractGiftDao.Gift_Upsert(abstractGift);
        }
    }
}
