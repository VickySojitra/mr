﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DSR_VisitedChemist_ProductsServices : AbstractDSR_VisitedChemist_ProductsServices
    {
        private AbstractDSR_VisitedChemist_ProductsDao abstractDSR_VisitedChemist_ProductsDao;

        public DSR_VisitedChemist_ProductsServices(AbstractDSR_VisitedChemist_ProductsDao abstractDSR_VisitedChemist_ProductsDao)
        {
            this.abstractDSR_VisitedChemist_ProductsDao = abstractDSR_VisitedChemist_ProductsDao;
        }

        public override SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_Upsert(AbstractDSR_VisitedChemist_Products abstractDSR_VisitedChemist_Products)
        {
            return this.abstractDSR_VisitedChemist_ProductsDao.DSR_VisitedChemist_Products_Upsert(abstractDSR_VisitedChemist_Products);
        }



        public override PagedList<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(PageParam pageParam, long DSR_VisitedChemistId)
        {
            return this.abstractDSR_VisitedChemist_ProductsDao.DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(pageParam, DSR_VisitedChemistId);
        }


        public override SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_Delete(long Id)
        {
            return this.abstractDSR_VisitedChemist_ProductsDao.DSR_VisitedChemist_Products_Delete(Id);
        }

    }
}
