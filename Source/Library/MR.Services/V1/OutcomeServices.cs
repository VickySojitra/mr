﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class OutcomeServices : AbstractOutcomeServices
    {
        private AbstractOutcomeDao abstractOutcomeDao;

        public OutcomeServices(AbstractOutcomeDao abstractOutcomeDao)
        {
            this.abstractOutcomeDao = abstractOutcomeDao;
        }

        public override SuccessResult<AbstractOutcome> Outcome_Upsert(AbstractOutcome abstractOutcome)
        {
            return this.abstractOutcomeDao.Outcome_Upsert(abstractOutcome);
        }

        public override SuccessResult<AbstractOutcome> Outcome_ById(long Id)
        {
            return this.abstractOutcomeDao.Outcome_ById(Id);
        }

        public override PagedList<AbstractOutcome> Outcome_All(PageParam pageParam, string search)
        {
            return this.abstractOutcomeDao.Outcome_All(pageParam, search);

        }
        

        public override SuccessResult<AbstractOutcome> Outcome_Delete(long Id, long DeletedBy)
        {
            return this.abstractOutcomeDao.Outcome_Delete(Id, DeletedBy);
        }

    }
}
