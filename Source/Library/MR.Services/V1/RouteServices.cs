﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class RouteServices : AbstractRouteService
    {
        private AbstractRouteDao abstractRouteDao;

        public RouteServices(AbstractRouteDao abstractRouteDao)
        {
            this.abstractRouteDao = abstractRouteDao;
        }

        public override SuccessResult<AbstractRoute> InsertUpdate(AbstractRoute abstractRoute)
        {
            return this.abstractRouteDao.InsertUpdate(abstractRoute);
        }

        public override SuccessResult<AbstractRouteType> InsertUpdateRouteType(AbstractRouteType abstractRouteType)
        {
            return this.abstractRouteDao.InsertUpdateRouteType(abstractRouteType);
        }

        public override SuccessResult<AbstractRoute> Select(int id)
        {
            return this.abstractRouteDao.Select(id);
        }

        public override PagedList<AbstractRoute> SelectAll(PageParam pageParam, string search = "", int RegionId = 0, int RouteTypeId = 0)
		{
            return this.abstractRouteDao.SelectAll(pageParam, search,RegionId, RouteTypeId);
        }

        public override PagedList<AbstractRouteType> SelectAllRouteType(PageParam pageParam, string search = "")
        {
            return this.abstractRouteDao.SelectAllRouteType(pageParam,search);
        }
    }
}
