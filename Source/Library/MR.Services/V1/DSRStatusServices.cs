﻿using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Services.V1
{
    public class DSRStatusServices : AbstractDSRStatusServices
    {
        private AbstractDSRStatusDao abstractDSRStatusDao = null;
        public DSRStatusServices(AbstractDSRStatusDao abstractDSRStatusDao)
        {
            this.abstractDSRStatusDao = abstractDSRStatusDao;
        }
        public override PagedList<AbstractDSRStatus> DSRStatus_All(PageParam pageParam, string search = "")
        {
            return abstractDSRStatusDao.DSRStatus_All(pageParam,search);
        }
    }
}
