﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductVsDoctorServices : AbstractProductVsDoctorServices
    {
        private AbstractProductVsDoctorDao abstractProductVsDoctorDao;

        public ProductVsDoctorServices(AbstractProductVsDoctorDao abstractProductVsDoctorDao)
        {
            this.abstractProductVsDoctorDao = abstractProductVsDoctorDao;
        }

        public override SuccessResult<AbstractProductVsDoctor> ProductVsDoctor_Upsert(AbstractProductVsDoctor abstractProductVsDoctor)
        {
            return this.abstractProductVsDoctorDao.ProductVsDoctor_Upsert(abstractProductVsDoctor);
        }

        public override PagedList<AbstractProductVsDoctor> ProductVsDoctor_ByProductId(PageParam pageParam, long ProductId)
        {
            return this.abstractProductVsDoctorDao.ProductVsDoctor_ByProductId(pageParam,ProductId);
        }

        public override PagedList<AbstractProductVsDoctor> ProductVsDoctor_All(PageParam pageParam, string search)
        {
            return this.abstractProductVsDoctorDao.ProductVsDoctor_All(pageParam, search);
        }

        public override SuccessResult<AbstractProductVsDoctor> ProductVsDoctor_Delete(long Id, long DeletedBy)
        {
            return this.abstractProductVsDoctorDao.ProductVsDoctor_Delete(Id, DeletedBy);
        }

        public override PagedList<AbstractProductVsDoctor> ProductVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            return this.abstractProductVsDoctorDao.ProductVsDoctor_ByDoctorId(pageParam,DoctorId);
        }


    }
}
