﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DayTypeMasterServices : AbstractDayTypeMasterServices
    {
        private AbstractDayTypeMasterDao abstractDayTypeMasterDao;

        public DayTypeMasterServices(AbstractDayTypeMasterDao abstractDayTypeMasterDao)

        {
            this.abstractDayTypeMasterDao = abstractDayTypeMasterDao;

        }
        public override PagedList<AbstractDayTypeMaster> DayTypeMaster_All(PageParam pageParam, string search)
        {
            return this.abstractDayTypeMasterDao.DayTypeMaster_All(pageParam, search);
        }

    }
}
