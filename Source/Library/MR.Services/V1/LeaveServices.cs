﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class LeaveServices : AbstractLeaveServices
    {
        private AbstractLeaveDao abstractLeaveDao;

        public LeaveServices(AbstractLeaveDao abstractLeaveDao)
        {
            this.abstractLeaveDao = abstractLeaveDao;
        }

        public override SuccessResult<AbstractLeave> Leave_Upsert(AbstractLeave abstractLeave)
        {
            return this.abstractLeaveDao.Leave_Upsert(abstractLeave);
        }






        public override PagedList<AbstractLeave> Leave_ByEmployeeId(PageParam pageParam, long EmployeeId)
        {
            return this.abstractLeaveDao.Leave_ByEmployeeId(pageParam, EmployeeId);

        }

        public override SuccessResult<AbstractLeave> Leave_ById(long Id)
        {
            return this.abstractLeaveDao.Leave_ById(Id);

        }

       

    }
}
