﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ChemistVsDoctorServices : AbstractChemistVsDoctorServices
    {
        private AbstractChemistVsDoctorDao abstractChemistVsDoctorDao;

        public ChemistVsDoctorServices(AbstractChemistVsDoctorDao abstractChemistVsDoctorDao)
        {
            this.abstractChemistVsDoctorDao = abstractChemistVsDoctorDao;
        }
        public override PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_All(PageParam pageParam, string search, long DoctorId, long ChemistId)
        {
            return this.abstractChemistVsDoctorDao.ChemistVsDoctor_All(pageParam, search, DoctorId, ChemistId);
        }

        public override PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            return this.abstractChemistVsDoctorDao.ChemistVsDoctor_ByDoctorId(pageParam, DoctorId);
        }

        public override PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_ByChemistId(PageParam pageParam, long ChemistId)
        {
            return this.abstractChemistVsDoctorDao.ChemistVsDoctor_ByChemistId(pageParam, ChemistId);
        }

        public override SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor_Delete(long Id, long DeletedBy)
        {
            return this.abstractChemistVsDoctorDao.ChemistVsDoctor_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor_Upsert(AbstractChemistVsDoctor abstractChemistVsDoctor)
        {
            return this.abstractChemistVsDoctorDao.ChemistVsDoctor_Upsert(abstractChemistVsDoctor);
        }
    }
}
