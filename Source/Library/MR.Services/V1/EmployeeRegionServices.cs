﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class EmployeeRegionServices : AbstractEmployeeRegionServices
    {
        private AbstractEmployeeRegionDao abstractEmployeeRegionDao;

        public EmployeeRegionServices(AbstractEmployeeRegionDao abstractEmployeeRegionDao)
        {
            this.abstractEmployeeRegionDao = abstractEmployeeRegionDao;
        }

        public override SuccessResult<AbstractEmployeeRegion> EmployeeRegion_Upsert(AbstractEmployeeRegion abstractEmployeeRegion)
        {
            return this.abstractEmployeeRegionDao.EmployeeRegion_Upsert(abstractEmployeeRegion);
        }

        public override PagedList<AbstractEmployeeRegion> EmployeeRegion_ByUserId(PageParam pageParam, string search,int UserId)
        {
            return this.abstractEmployeeRegionDao.EmployeeRegion_ByUserId(pageParam, search, UserId);
        }

    }
}
