﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DSR_VisitedDoctor_CampaignServices : AbstractDSR_VisitedDoctor_CampaignServices
    {
        private AbstractDSR_VisitedDoctor_CampaignDao abstractDSR_VisitedDoctor_CampaignDao;

        public DSR_VisitedDoctor_CampaignServices(AbstractDSR_VisitedDoctor_CampaignDao abstractDSR_VisitedDoctor_CampaignDao)
        {
            this.abstractDSR_VisitedDoctor_CampaignDao = abstractDSR_VisitedDoctor_CampaignDao;
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_Upsert(AbstractDSR_VisitedDoctor_Campaign abstractDSR_VisitedDoctor_Campaign)
        {
            return this.abstractDSR_VisitedDoctor_CampaignDao.DSR_VisitedDoctor_Campaign_Upsert(abstractDSR_VisitedDoctor_Campaign);
        }



        


        public override PagedList<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId)
        {
            return this.abstractDSR_VisitedDoctor_CampaignDao.DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId(pageParam, DSR_VisitedDoctorId);

        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_ById(long Id)
        {
            return this.abstractDSR_VisitedDoctor_CampaignDao.DSR_VisitedDoctor_Campaign_ById(Id);

        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_Delete(long Id, long DeletedBy)
        {
            return this.abstractDSR_VisitedDoctor_CampaignDao.DSR_VisitedDoctor_Campaign_Delete(Id, DeletedBy);
        }

    }
}
