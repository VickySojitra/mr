﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductHightlightsServices : AbstractProductHightlightsServices
    {
        private AbstractProductHightlightsDao abstractProductHightlightsDao;

        public ProductHightlightsServices(AbstractProductHightlightsDao abstractProductHightlightsDao)
        {
            this.abstractProductHightlightsDao = abstractProductHightlightsDao;
        }

        public override SuccessResult<AbstractProductHightlights> ProductHightlights_Upsert(AbstractProductHightlights abstractProductHightlights)
        {
            return this.abstractProductHightlightsDao.ProductHightlights_Upsert(abstractProductHightlights);
        }

        public override SuccessResult<AbstractProductHightlights> ProductHightlights_ById(long Id)
        {
            return this.abstractProductHightlightsDao.ProductHightlights_ById(Id);
        }

        public override PagedList<AbstractProductHightlights> ProductHightlights_All(PageParam pageParam, string search)
        {
            return this.abstractProductHightlightsDao.ProductHightlights_All(pageParam, search);

        }


        public override SuccessResult<AbstractProductHightlights> ProductHightlights_Delete(long Id)
        {
            return this.abstractProductHightlightsDao.ProductHightlights_Delete(Id);
        }

    }
}
