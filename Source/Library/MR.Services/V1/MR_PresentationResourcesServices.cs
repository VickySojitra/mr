﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class MR_PresentationResourcesServices : AbstractMR_PresentationResourcesServices
    {
        private AbstractMR_PresentationResourcesDao abstractMR_PresentationResourcesDao;

        public MR_PresentationResourcesServices(AbstractMR_PresentationResourcesDao abstractMR_PresentationResourcesDao)
        {
            this.abstractMR_PresentationResourcesDao = abstractMR_PresentationResourcesDao;
        }

        public override SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources_Upsert(AbstractMR_PresentationResources abstractMR_PresentationResources)
        {
            return this.abstractMR_PresentationResourcesDao.MR_PresentationResources_Upsert(abstractMR_PresentationResources);
        }


        public override PagedList<AbstractMR_PresentationResources> MR_PresentationResources_ByMR_PresentationId(PageParam pageParam, long MR_PresentationId)
        {
            return this.abstractMR_PresentationResourcesDao.MR_PresentationResources_ByMR_PresentationId(pageParam, MR_PresentationId);

        }


        public override SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources_Delete(long Id, long DeletedBy)
        {
            return this.abstractMR_PresentationResourcesDao.MR_PresentationResources_Delete(Id, DeletedBy);
        }

    }
}
