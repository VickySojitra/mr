﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class WorkTypeServices : AbstractWorkTypeServices
    {
        private AbstractWorkTypeDao abstractWorkTypeDao;

        public WorkTypeServices(AbstractWorkTypeDao abstractWorkTypeDao)
        {
            this.abstractWorkTypeDao = abstractWorkTypeDao;
        }

        public override SuccessResult<AbstractWorkType> WorkType_Upsert(AbstractWorkType abstractWorkType)
        {
            return this.abstractWorkTypeDao.WorkType_Upsert(abstractWorkType);
        }

        public override SuccessResult<AbstractWorkType> WorkType_ById(long Id)
        {
            return this.abstractWorkTypeDao.WorkType_ById(Id);
        }

        public override PagedList<AbstractWorkType> WorkType_All(PageParam pageParam, string search)
        {
            return this.abstractWorkTypeDao.WorkType_All(pageParam, search);
        }

        public override SuccessResult<AbstractWorkType> WorkType_Delete(long Id, long DeletedBy)
        {
            return this.abstractWorkTypeDao.WorkType_Delete(Id, DeletedBy);
        }
    }
}
