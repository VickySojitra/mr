﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class PresentationServices : AbstractPresentationServices
    {
        private AbstractPresentationDao abstractPresentationDao;

        public PresentationServices(AbstractPresentationDao abstractPresentationDao)

        {
            this.abstractPresentationDao = abstractPresentationDao;

        }
        public override PagedList<AbstractPresentation> Presentation_All(PageParam pageParam, string search, int ProductId)
        {
            return this.abstractPresentationDao.Presentation_All(pageParam, search, ProductId);
        }

        public override SuccessResult<AbstractPresentation> Presentation_ById(long Id)
        {
            return this.abstractPresentationDao.Presentation_ById(Id);
        }

        public override SuccessResult<AbstractPresentation> Presentation_Delete(long Id)
        {
            return this.abstractPresentationDao.Presentation_Delete(Id);
        }


        public override SuccessResult<AbstractPresentation> Presentation_Upsert(AbstractPresentation abstractPresentation)
        {
            return this.abstractPresentationDao.Presentation_Upsert(abstractPresentation);
        }
        
        public override PagedList<AbstractPresentation> Presentation_ByProductId(PageParam pageParam, long ProductId)
        {
            return this.abstractPresentationDao.Presentation_ByProductId(pageParam, ProductId);
        }
    }
}
