﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DoctorAndChemistServices : AbstractDoctorAndChemistServices
    {
        private AbstractDoctorAndChemistDao abstractDoctorAndChemistDao;

        public DoctorAndChemistServices(AbstractDoctorAndChemistDao abstractDoctorAndChemistDao)
        {
            this.abstractDoctorAndChemistDao = abstractDoctorAndChemistDao;
        }

        public override PagedList<AbstractDoctorAndChemist> DoctorAndChemist_All(PageParam pageParam, string search, int TourPlanMonthId, int EmployeeId)
        {
            return this.abstractDoctorAndChemistDao.DoctorAndChemist_All(pageParam, search, TourPlanMonthId, EmployeeId);
        }

        public override SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist_Delete(long Id, long DeletedBy)
        {
            return this.abstractDoctorAndChemistDao.DoctorAndChemist_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist_Upsert(AbstractDoctorAndChemist abstractDoctorAndChemist)
        {
            return this.abstractDoctorAndChemistDao.DoctorAndChemist_Upsert(abstractDoctorAndChemist);
        }
    }
}
