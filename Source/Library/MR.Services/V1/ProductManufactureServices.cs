﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductManufactureServices : AbstractProductManufactureServices
    {

        private AbstractProductManufactureDao abstractProductManufactureDao;

        public ProductManufactureServices(AbstractProductManufactureDao abstractProductManufactureDao)
        {
            this.abstractProductManufactureDao = abstractProductManufactureDao;
        }

        public override PagedList<AbstractProductManufacture> ProductManufacture_All(PageParam pageParam, string search)
        {
            return this.abstractProductManufactureDao.ProductManufacture_All(pageParam, search);
        }

        public override SuccessResult<AbstractProductManufacture> ProductManufacture_ById(long Id)
        {
            return this.abstractProductManufactureDao.ProductManufacture_ById(Id);
        }

        public override SuccessResult<AbstractProductManufacture> ProductManufacture_Delete(long Id)
        {
            return this.abstractProductManufactureDao.ProductManufacture_Delete(Id);
        }

        public override SuccessResult<AbstractProductManufacture> ProductManufacture_Upsert(AbstractProductManufacture abstractProductManufacture)
        {
            return this.abstractProductManufactureDao.ProductManufacture_Upsert(abstractProductManufacture);
        }
    }
}
