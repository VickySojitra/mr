﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductVsChemistServices : AbstractProductVsChemistServices
    {
        private AbstractProductVsChemistDao abstractProductVsChemistDao;

        public ProductVsChemistServices(AbstractProductVsChemistDao abstractProductVsChemistDao)
        {
            this.abstractProductVsChemistDao = abstractProductVsChemistDao;
        }

        public override SuccessResult<AbstractProductVsChemist> ProductVsChemist_Upsert(AbstractProductVsChemist abstractProductVsChemist)
        {
            return this.abstractProductVsChemistDao.ProductVsChemist_Upsert(abstractProductVsChemist);
        }

        

        public override PagedList<AbstractProductVsChemist> ProductVsChemist_All(PageParam pageParam, string search, long ProductId, long ChemistId)
        {
            return this.abstractProductVsChemistDao.ProductVsChemist_All(pageParam, search, ProductId, ChemistId);

        }


        public override PagedList<AbstractProductVsChemist> ProductVsChemist_ByProductId(PageParam pageParam, long ProductId)
        {
            return this.abstractProductVsChemistDao.ProductVsChemist_ByProductId(pageParam, ProductId);

        }

        public override PagedList<AbstractProductVsChemist> ProductVsChemist_ByChemistId(PageParam pageParam, long ChemistId)
        {
            return this.abstractProductVsChemistDao.ProductVsChemist_ByChemistId(pageParam, ChemistId);

        }

        public override SuccessResult<AbstractProductVsChemist> ProductVsChemist_Delete(long Id, int deletedby = 0)
        {
            return this.abstractProductVsChemistDao.ProductVsChemist_Delete(Id,deletedby);
        }

    }
}
