﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
namespace MR.Services.V1
{
    public class DSR_VisitedDoctor_ProductsServices : AbstractDSR_VisitedDoctor_ProductsServices
    {
        private AbstractDSR_VisitedDoctor_ProductsDao abstractDSR_VisitedDoctor_ProductsDao;

        public DSR_VisitedDoctor_ProductsServices(AbstractDSR_VisitedDoctor_ProductsDao abstractDSR_VisitedDoctor_ProductsDao)
        {
            this.abstractDSR_VisitedDoctor_ProductsDao = abstractDSR_VisitedDoctor_ProductsDao;
        }

        public override PagedList<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId)
        {
            return this.abstractDSR_VisitedDoctor_ProductsDao.DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(pageParam, DSR_VisitedDoctorId);
        }



        public override SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_Delete(long Id)
        {
            return this.abstractDSR_VisitedDoctor_ProductsDao.DSR_VisitedDoctor_Products_Delete(Id);
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_Upsert(AbstractDSR_VisitedDoctor_Products abstractDSR_VisitedDoctor_Products)
        {
            return this.abstractDSR_VisitedDoctor_ProductsDao.DSR_VisitedDoctor_Products_Upsert(abstractDSR_VisitedDoctor_Products);
        }
    }
}
