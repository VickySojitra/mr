﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductCategoryServices : AbstractProductCategoryServices
    {

        private AbstractProductCategoryDao abstractProductCategoryDao;

        public ProductCategoryServices(AbstractProductCategoryDao abstractProductCategoryDao)
        {
            this.abstractProductCategoryDao = abstractProductCategoryDao;
        }

        public override PagedList<AbstractProductCategory> ProductCategory_All(PageParam pageParam, string search)
        {
            return this.abstractProductCategoryDao.ProductCategory_All(pageParam, search);
        }

        public override SuccessResult<AbstractProductCategory> ProductCategory_ById(long Id)
        {
            return this.abstractProductCategoryDao.ProductCategory_ById(Id);
        }

        public override SuccessResult<AbstractProductCategory> ProductCategory_Delete(long Id)
        {
            return this.abstractProductCategoryDao.ProductCategory_Delete(Id);
        }

        public override SuccessResult<AbstractProductCategory> ProductCategory_Upsert(AbstractProductCategory abstractProductCategory)
        {
            return this.abstractProductCategoryDao.ProductCategory_Upsert(abstractProductCategory);
        }
    }
}
