﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class PrescriberProductsServices : AbstractPrescriberProductsServices
    {
        private AbstractPrescriberProductsDao abstractPrescriberProductsDao;

        public PrescriberProductsServices(AbstractPrescriberProductsDao abstractPrescriberProductsDao)
        {
            this.abstractPrescriberProductsDao = abstractPrescriberProductsDao;
        }
        public override SuccessResult<AbstractPrescriberProducts> PrescriberProducts_ById(long Id)
        {
            return this.abstractPrescriberProductsDao.PrescriberProducts_ById(Id);
        }

        public override PagedList<AbstractPrescriberProducts> PrescriberProducts_ByPrescriberId(PageParam pageParam, long PrescriberId)
        {
            return this.abstractPrescriberProductsDao.PrescriberProducts_ByPrescriberId(pageParam, PrescriberId);
        }

        public override SuccessResult<AbstractPrescriberProducts> PrescriberProducts_Upsert(AbstractPrescriberProducts abstractPrescriberProducts)
        {
            return this.abstractPrescriberProductsDao.PrescriberProducts_Upsert(abstractPrescriberProducts);
        }
    }
}
