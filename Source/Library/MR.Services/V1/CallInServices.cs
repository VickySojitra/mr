﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
namespace MR.Services.V1
{
    public class CallInServices : AbstractCallInServices
    {
        private AbstractCallInDao abstractCallInDao;

        public CallInServices(AbstractCallInDao abstractCallInDao)
        {
            this.abstractCallInDao = abstractCallInDao;
        }
        public override PagedList<AbstractCallIn> CallIn_All(PageParam pageParam, string search)
        {
            return this.abstractCallInDao.CallIn_All(pageParam, search);
        }

        public override SuccessResult<AbstractCallIn> CallIn_ById(long Id)
        {
            return this.abstractCallInDao.CallIn_ById(Id);
        }

        public override SuccessResult<AbstractCallIn> CallIn_Delete(long Id, long DeletedBy)
        {
            return this.abstractCallInDao.CallIn_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractCallIn> CallIn_Upsert(AbstractCallIn abstractCallIn)
        {
            return this.abstractCallInDao.CallIn_Upsert(abstractCallIn);
        }
    }
}
