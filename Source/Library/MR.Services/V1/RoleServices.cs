﻿using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class RoleServices : AbstractRoleService
    {
        private AbstractRoleDao abstractRoleDao;

        public RoleServices(AbstractRoleDao abstractRoleDao)
        {
            this.abstractRoleDao = abstractRoleDao;
        }

		public override SuccessResult<AbstractRole> InsertUpdate(AbstractRole abstractRole)
		{
			return this.abstractRoleDao.InsertUpdate(abstractRole);
		}

		public override PagedList<AbstractRole> SelectAll(PageParam pageParam, string search = "")
		{
			return this.abstractRoleDao.SelectAll(pageParam,search);
		}
	}
}
