﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class MR_PresentationServices : AbstractMR_PresentationServices
    {
        private AbstractMR_PresentationDao abstractMR_PresentationDao;

        public MR_PresentationServices(AbstractMR_PresentationDao abstractMR_PresentationDao)
        {
            this.abstractMR_PresentationDao = abstractMR_PresentationDao;
        }

        public override SuccessResult<AbstractMR_Presentation> MR_Presentation_Upsert(AbstractMR_Presentation abstractMR_Presentation)
        {
            return this.abstractMR_PresentationDao.MR_Presentation_Upsert(abstractMR_Presentation);
        }

        public override SuccessResult<AbstractMR_Presentation> MR_Presentation_ById(long Id)
        {
            return this.abstractMR_PresentationDao.MR_Presentation_ById(Id);
        }

        public override PagedList<AbstractMR_Presentation> MR_Presentation_All(PageParam pageParam, string search, long EmployeeId, long ProductId)
        {
            return this.abstractMR_PresentationDao.MR_Presentation_All(pageParam, search, EmployeeId, ProductId);

        }


        public override SuccessResult<AbstractMR_Presentation> MR_Presentation_Delete(long Id, long DeletedBy)
        {
            return this.abstractMR_PresentationDao.MR_Presentation_Delete(Id, DeletedBy);
        }

    }
}
