﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class JointWorkingServices : AbstractJointWorkingServices

    {
        private AbstractJointWorkingDao abstractJointWorkingDao;

        public JointWorkingServices(AbstractJointWorkingDao abstractJointWorkingDao)
        {
            this.abstractJointWorkingDao = abstractJointWorkingDao;
        }

        public override PagedList<AbstractJointWorking> JointWorking_All(PageParam pageParam, string search, int TourPlanMonthId, int JointWorkingId)
        {
            return this.abstractJointWorkingDao.JointWorking_All(pageParam, search, TourPlanMonthId, JointWorkingId);
        }

        public override SuccessResult<AbstractJointWorking> JointWorking_Upsert(AbstractJointWorking abstractJointWorking)
        {
            return this.abstractJointWorkingDao.JointWorking_Upsert(abstractJointWorking);
        }
    }
    
}
