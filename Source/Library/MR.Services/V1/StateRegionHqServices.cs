﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class StateRegionHqServices : AbstractStateRegionHqServices
    {
        private AbstractStateRegionHqDao abstractStateRegionHqDao;

        public StateRegionHqServices(AbstractStateRegionHqDao abstractStateRegionHqDao)
        {
            this.abstractStateRegionHqDao = abstractStateRegionHqDao;
        }

        public override SuccessResult<AbstractStateRegionHq> StateRegionHq_Upsert(AbstractStateRegionHq abstractStateRegionHq)
        {
            return this.abstractStateRegionHqDao.StateRegionHq_Upsert(abstractStateRegionHq);
        }

        public override SuccessResult<AbstractStateRegionHq> StateRegionHq_ById(int Id)
        {
            return this.abstractStateRegionHqDao.StateRegionHq_ById(Id);
        }

        public override PagedList<AbstractStateRegionHq> StateRegionHq_All(PageParam pageParam, string search, int ParentId, int GrandParentId)
        {
            return this.abstractStateRegionHqDao.StateRegionHq_All(pageParam, search, ParentId, GrandParentId);

        }

        public override PagedList<AbstractStateRegionHq> StateRegionHq_AllRegion()
        {
            return this.abstractStateRegionHqDao.StateRegionHq_AllRegion();

        }


        public override SuccessResult<AbstractStateRegionHq> StateRegionHq_Delete(int Id, int DeletedBy)
        {
            return this.abstractStateRegionHqDao.StateRegionHq_Delete(Id, DeletedBy);
        }

    }
}
