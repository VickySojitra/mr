﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class EmployeeServices : AbstractEmployeeService
    {
        private AbstractEmployeeDao abstractEmployeeDao;

        public EmployeeServices(AbstractEmployeeDao abstractEmployeeDao)
        {
            this.abstractEmployeeDao = abstractEmployeeDao;
        }

        public override PagedList<AbstractEmployee> SelectAll(PageParam pageParam, string search = "")
        {
            return this.abstractEmployeeDao.SelectAll(pageParam, search);
        }
        public override PagedList<AbstractEmployee> Employee_ReportingPerson(PageParam pageParam, string search = "", int RoleId = 0, bool IsTourPlanDropDown = false)
        {
            return this.abstractEmployeeDao.Employee_ReportingPerson(pageParam, search, RoleId, IsTourPlanDropDown);
        }

        public override SuccessResult<AbstractEmployee> Select(int id)
        {
            return this.abstractEmployeeDao.Select(id);
        }

        public override bool Delete(int id)
        {
            return this.abstractEmployeeDao.Delete(id);
        }

        public override bool DeleteWorkExperience(int empId)
        {
            return this.abstractEmployeeDao.DeleteWorkExperience(empId);
        }

        public override SuccessResult<AbstractEmployee> InsertUpdate(AbstractEmployee abstractAddress)
        {
            return this.abstractEmployeeDao.InsertUpdate(abstractAddress);
        }

        public override PagedList<AbstractQualification> QualificationSelectAll(PageParam pageParam, string search = "")
        {
            return this.abstractEmployeeDao.QualificationSelectAll(pageParam);
        }

        public override PagedList<AbstractSpecialization> SpecializationSelectAll(PageParam pageParam, string search = "")
        {
            return this.abstractEmployeeDao.SpecializationSelectAll(pageParam);
        }

        public override PagedList<AbstractWorkExperience> WorkExperienceByEmployeeId(int empId)
        {
            return this.abstractEmployeeDao.WorkExperienceByEmployeeId(empId);
        }

        public override SuccessResult<AbstractWorkExperience> InsertUpdateWorkExperience(AbstractWorkExperience abstractWorkExp)
        {
            return this.abstractEmployeeDao.InsertUpdateWorkExperience(abstractWorkExp);
        }



        public override SuccessResult<AbstractQualification> InsertUpdateQualification(AbstractQualification abstractQualification)
        {
            return this.abstractEmployeeDao.InsertUpdateQualification(abstractQualification);
        }

        public override SuccessResult<AbstractSpecialization> InsertUpdateSpecialization(AbstractSpecialization abstractSpecialization)
        {
            return this.abstractEmployeeDao.InsertUpdateSpecialization(abstractSpecialization);
        }

        public override SuccessResult<AbstractEmployee> Employee_Login(string Email, string WebPassword)
        {
            return this.abstractEmployeeDao.Employee_Login(Email, WebPassword);
        }

        public override SuccessResult<AbstractEmployee> Employee_ChangePassword(int Id, string OldPassword, string NewPassword,string ConfirmPassword)
        {
            return abstractEmployeeDao.Employee_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
        }


        public override SuccessResult<AbstractEmployee> Employee_ById(int Id)
        {
            return this.abstractEmployeeDao.Employee_ById(Id);
        }

		public override SuccessResult<AbstractQualification> Qualification_ById(int Id)
		{
            return this.abstractEmployeeDao.Qualification_ById(Id);
        }

		public override SuccessResult<AbstractSpecialization> Specialization_ById(int Id)
		{
            return this.abstractEmployeeDao.Specialization_ById(Id);
        }
	}
}
