﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;


namespace MR.Services.V1
{
    public class DSRServices : AbstractDSRServices
    {
        private AbstractDSRDao abstractDSRDao = null;

        public DSRServices(AbstractDSRDao abstractDSRDao)
        {
            this.abstractDSRDao = abstractDSRDao;
        }
        public override PagedList<AbstractDSR> DSR_All(PageParam pageParam, string search, long RouteTypeId, long WorkTypeId, long RouteId, long VisitTypeId, long DoctorId, long ChemistId, int EmployeeId, AbstractDSR abstractDSR)
        {
            return this.abstractDSRDao.DSR_All(pageParam, search, RouteTypeId, WorkTypeId, RouteId, VisitTypeId, DoctorId, ChemistId, EmployeeId, abstractDSR);
        }

        public override SuccessResult<AbstractDSR> DSR_ById(long Id)
        {
            return this.abstractDSRDao.DSR_ById(Id);
        }

        public override SuccessResult<AbstractDSR> DSR_Delete(long Id, long DeletedBy)
        {
            return this.abstractDSRDao.DSR_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractDSR> DSR_Upsert(AbstractDSR abstractDSR)
        {
            return this.abstractDSRDao.DSR_Upsert(abstractDSR);
        }
    }
}
