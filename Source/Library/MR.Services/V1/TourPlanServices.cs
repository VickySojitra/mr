﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
namespace MR.Services.V1
{
    public class TourPlanServices : AbstractTourPlanServices
    {
        private AbstractTourPlanDao abstractTourPlanDao;

        public TourPlanServices(AbstractTourPlanDao abstractTourPlanDao)
        {
            this.abstractTourPlanDao = abstractTourPlanDao;
        }
        public override PagedList<AbstractTourPlan> TourPlan_All(PageParam pageParam, string search)
        {
            return this.abstractTourPlanDao.TourPlan_All(pageParam, search);
        }

        public override SuccessResult<AbstractTourPlan> TourPlan_ById(int Id)
        {
            return this.abstractTourPlanDao.TourPlan_ById(Id);
        }

        public override SuccessResult<AbstractTourPlan> TourPlan_IsSubmitted(int Id, bool IsSubmitted)
        {
            return this.abstractTourPlanDao.TourPlan_IsSubmitted(Id, IsSubmitted);
        }


        public override SuccessResult<AbstractTourPlan> TourPlan_Upsert(AbstractTourPlan abstractTourPlan)
        {
            return this.abstractTourPlanDao.TourPlan_Upsert(abstractTourPlan);
        }
    }
}
