﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class WorkWithMasterServices : AbstractWorkWithMasterServices
    {
        private AbstractWorkWithMasterDao abstractWorkTypeDao;

        public WorkWithMasterServices(AbstractWorkWithMasterDao abstractWorkTypeDao)
        {
            this.abstractWorkTypeDao = abstractWorkTypeDao;
        }

        public override SuccessResult<AbstractWorkWithMaster> WorkWith_Upsert(AbstractWorkWithMaster abstractWorkType)
        {
            return this.abstractWorkTypeDao.WorkWith_Upsert(abstractWorkType);
        }

        public override SuccessResult<AbstractWorkWithMaster> WorkWith_ById(long Id)
        {
            return this.abstractWorkTypeDao.WorkWith_ById(Id);
        }

        public override PagedList<AbstractWorkWithMaster> WorkWith_All(PageParam pageParam, string search)
        {
            return this.abstractWorkTypeDao.WorkWith_All(pageParam, search);
        }

        public override SuccessResult<AbstractWorkWithMaster> WorkWith_Delete(long Id, long DeletedBy)
        {
            return this.abstractWorkTypeDao.WorkWith_Delete(Id, DeletedBy);
        }
    }
}
