﻿using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Services.V1
{
	public class TourPlanManagerService : AbstractTourPlanManagerService
	{
		private AbstractTourPlanManagerDao abstractTPMDao = null;

		public TourPlanManagerService(AbstractTourPlanManagerDao abstractTPMDao)
		{
			this.abstractTPMDao = abstractTPMDao;
		}

		public override PagedList<AbstractTourPlanManager> TourPlanManager_All(PageParam pageParam, string search, int IsSubmitted, string Month, int Headquarter = 0, int Region = 0)
		{
			return this.abstractTPMDao.TourPlanManager_All(pageParam, search, IsSubmitted,Month,Headquarter,Region);

		}
	}
}
