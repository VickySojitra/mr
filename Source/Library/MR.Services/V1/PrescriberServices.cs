﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class PrescriberServices : AbstractPrescriberServices
    {
        private AbstractPrescriberDao abstractPrescriberDao;

        public PrescriberServices(AbstractPrescriberDao abstractPrescriberDao)
        {
            this.abstractPrescriberDao = abstractPrescriberDao;
        }
        public override SuccessResult<AbstractPrescriber> Prescriber_ById(long Id)
        {
            return this.abstractPrescriberDao.Prescriber_ById(Id);
        }

        public override PagedList<AbstractPrescriber> Prescriber_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            return this.abstractPrescriberDao.Prescriber_ByDoctorId(pageParam, DoctorId);
        }

        public override SuccessResult<AbstractPrescriber> Prescriber_Upsert(AbstractPrescriber abstractPrescriber)
        {
            return this.abstractPrescriberDao.Prescriber_Upsert(abstractPrescriber);
        }
    }
}
