﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class ProductHighlightServices : AbstractProductHighlightServices
    {
        private AbstractProductHighlightDao abstractProductHighlightDao;

        public ProductHighlightServices(AbstractProductHighlightDao abstractProductHighlightDao)

        {
            this.abstractProductHighlightDao = abstractProductHighlightDao;

        }
        public override PagedList<AbstractProductHighlight> ProductHighlight_All(PageParam pageParam, string search,long ProductId)
        {
            return this.abstractProductHighlightDao.ProductHighlight_All(pageParam, search, ProductId);
        }

        public override SuccessResult<AbstractProductHighlight> ProductHighlight_ById(long Id)
        {
            return this.abstractProductHighlightDao.ProductHighlight_ById(Id);
        }

public override SuccessResult<AbstractProductHighlight> ProductHighlight_Delete(long Id)
        {
            return this.abstractProductHighlightDao.ProductHighlight_Delete(Id);
        }

public override SuccessResult<AbstractProductHighlight> ProductHighlight_Upsert(AbstractProductHighlight abstractProductHighlight)
        {
            return this.abstractProductHighlightDao.ProductHighlight_Upsert(abstractProductHighlight);
        }

        public override SuccessResult<AbstractProductHighlight> ProductHighlight_DeleteByProductId(long Id)
		{
            return this.abstractProductHighlightDao.ProductHighlight_DeleteByProductId(Id);
        }
    }
}
