﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
namespace MR.Services.V1
{
    public class DoctorProductsServices : AbstractDoctorProductsServices
    {
        private AbstractDoctorProductsDao abstractDoctorProductsDao;

        public DoctorProductsServices(AbstractDoctorProductsDao abstractDoctorProductsDao)
        {
            this.abstractDoctorProductsDao = abstractDoctorProductsDao;
        }

        public override SuccessResult<AbstractDoctorProducts> DoctorProducts_ByDoctorId(long Id)
        {
            return this.abstractDoctorProductsDao.DoctorProducts_ByDoctorId(Id);
        }



        public override SuccessResult<AbstractDoctorProducts> DoctorProducts_Delete(long Id,long DeletedBy)
        {
            return this.abstractDoctorProductsDao.DoctorProducts_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractDoctorProducts> DoctorProducts_Upsert(AbstractDoctorProducts abstractDoctorProducts)
        {
            return this.abstractDoctorProductsDao.DoctorProducts_Upsert(abstractDoctorProducts);
        }
    }
}
