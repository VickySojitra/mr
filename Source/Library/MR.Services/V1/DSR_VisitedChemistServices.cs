﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Services.Contract;

namespace MR.Services.V1
{
    public class DSR_VisitedChemistServices : AbstractDSR_VisitedChemistServices
    {
        private AbstractDSR_VisitedChemistDao abstractDSR_VisitedChemistDao;

        public DSR_VisitedChemistServices(AbstractDSR_VisitedChemistDao abstractDSR_VisitedChemistDao)
        {
            this.abstractDSR_VisitedChemistDao = abstractDSR_VisitedChemistDao;
        }

        public override SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_Upsert(AbstractDSR_VisitedChemist abstractDSR_VisitedChemist)
        {
            return this.abstractDSR_VisitedChemistDao.DSR_VisitedChemist_Upsert(abstractDSR_VisitedChemist);
        }

        public override SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_ById(long Id)
        {
            return this.abstractDSR_VisitedChemistDao.DSR_VisitedChemist_ById(Id);
        }

        public override PagedList<AbstractDSR_VisitedChemist> DSR_VisitedChemist_ByDSRId(PageParam pageParam, long DSRId)
        {
            return this.abstractDSR_VisitedChemistDao.DSR_VisitedChemist_ByDSRId(pageParam, DSRId);
        }


        public override SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_Delete(long Id)
        {
            return this.abstractDSR_VisitedChemistDao.DSR_VisitedChemist_Delete(Id);
        }

    }
}
