﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MR.Services
{
    using Autofac;    
    using Data;
    using MR.Services.Contract;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterModule<DataModule>();
            builder.RegisterType<V1.AddressServices>().As<AbstractAddressService>().InstancePerDependency();
            builder.RegisterType<V1.EmployeeServices>().As<AbstractEmployeeService>().InstancePerDependency();
            builder.RegisterType<V1.RoleServices>().As<AbstractRoleService>().InstancePerDependency();
            builder.RegisterType<V1.DoctorServices>().As<AbstractDoctorService>().InstancePerDependency();
            builder.RegisterType<V1.ChemistServices>().As<AbstractChemistService>().InstancePerDependency();
            builder.RegisterType<V1.RouteServices>().As<AbstractRouteService>().InstancePerDependency();
            builder.RegisterType<V1.WorkWithMasterServices>().As<AbstractWorkWithMasterServices>().InstancePerDependency();
            
            builder.RegisterType<V1.ProductCategoryServices>().As<AbstractProductCategoryServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductTypeServices>().As<AbstractProductTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductManufactureServices>().As<AbstractProductManufactureServices>().InstancePerDependency();
            builder.RegisterType<V1.QuantitySIUnitesServices>().As<AbstractQuantitySIUnitesServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductServices>().As<AbstractProductServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductHightlightsServices>().As<AbstractProductHightlightsServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductImagesServices>().As<AbstractProductImagesServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductPrescriptionServices>().As<AbstractProductPrescriptionServices>().InstancePerDependency();
            builder.RegisterType<V1.PresentationServices>().As<AbstractPresentationServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductHighlightServices>().As<AbstractProductHighlightServices>().InstancePerDependency();

            builder.RegisterType<V1.DSRServices>().As<AbstractDSRServices>().InstancePerDependency();
            builder.RegisterType<V1.DSRStatusServices>().As<AbstractDSRStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.CallInServices>().As<AbstractCallInServices>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedDoctor_ProductsServices>().As<AbstractDSR_VisitedDoctor_ProductsServices>().InstancePerDependency();
            builder.RegisterType<V1.DoctorProductsServices>().As<AbstractDoctorProductsServices>().InstancePerDependency();


            builder.RegisterType<V1.DSR_VisitedDoctorServices>().As<AbstractDSR_VisitedDoctorServices>().InstancePerDependency();

            builder.RegisterType<V1.WorkTypeServices>().As<AbstractWorkTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.VisitTypeServices>().As<AbstractVisitTypeServices>().InstancePerDependency();

            builder.RegisterType<V1.OutcomeServices>().As<AbstractOutcomeServices>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedChemist_ProductsServices>().As<AbstractDSR_VisitedChemist_ProductsServices>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedChemistServices>().As<AbstractDSR_VisitedChemistServices>().InstancePerDependency();


            builder.RegisterType<V1.MR_PresentationServices>().As<AbstractMR_PresentationServices>().InstancePerDependency();
            builder.RegisterType<V1.MR_PresentationResourcesServices>().As<AbstractMR_PresentationResourcesServices>().InstancePerDependency();
            
            
            builder.RegisterType<V1.CategoryMasterServices>().As<AbstractCategoryMasterServices>().InstancePerDependency();


            builder.RegisterType<V1.ProductVsChemistServices>().As<AbstractProductVsChemistServices>().InstancePerDependency();
            builder.RegisterType<V1.ChemistVsDoctorServices>().As<AbstractChemistVsDoctorServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductVsDoctorServices>().As<AbstractProductVsDoctorServices>().InstancePerDependency();

            builder.RegisterType<V1.DSR_VisitedDoctor_CampaignServices>().As<AbstractDSR_VisitedDoctor_CampaignServices>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedDoctor_Campaign_GiftsServices>().As<AbstractDSR_VisitedDoctor_Campaign_GiftsServices>().InstancePerDependency();

            builder.RegisterType<V1.ApplyMasterServices>().As<AbstractApplyMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.GiftServices>().As<AbstractGiftServices>().InstancePerDependency();
            builder.RegisterType <V1.ChampaignMasterServices>().As<AbstractChampaignMasterServices>().InstancePerDependency();

            builder.RegisterType<V1.ApplyMasterServices>().As<AbstractApplyMasterServices>().InstancePerDependency();

            builder.RegisterType<V1.GiftServices>().As<AbstractGiftServices>().InstancePerDependency();

            builder.RegisterType <V1.ChampaignMasterServices>().As<AbstractChampaignMasterServices>().InstancePerDependency();

            builder.RegisterType<V1.PrescriberServices>().As<AbstractPrescriberServices>().InstancePerDependency();
            builder.RegisterType<V1.PrescriberProductsServices>().As<AbstractPrescriberProductsServices>().InstancePerDependency();


            builder.RegisterType<V1.LeaveServices>().As<AbstractLeaveServices>().InstancePerDependency();

            builder.RegisterType<V1.DayTypeMasterServices>().As<AbstractDayTypeMasterServices>().InstancePerDependency();

            builder.RegisterType<V1.LeaveTypeMasterServices>().As<AbstractLeaveTypeMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.TourPlanServices>().As<AbstractTourPlanServices>().InstancePerDependency();
            builder.RegisterType<V1.TourPlanMonthServices>().As<AbstractTourPlanMonthServices>().InstancePerDependency();
            builder.RegisterType<V1.DoctorAndChemistServices>().As<AbstractDoctorAndChemistServices>().InstancePerDependency();
            builder.RegisterType<V1.JointWorkingServices>().As<AbstractJointWorkingServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterWorkAtServices>().As<AbstractMasterWorkAtServices>().InstancePerDependency();
            builder.RegisterType<V1.StateRegionHqServices>().As<AbstractStateRegionHqServices>().InstancePerDependency();
            builder.RegisterType<V1.EmployeeRegionServices>().As<AbstractEmployeeRegionServices>().InstancePerDependency();
            builder.RegisterType<V1.TourPlanManagerService>().As<AbstractTourPlanManagerService>().InstancePerDependency();



            base.Load(builder);
        }
    }
}
