﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Dexoc Solutions">
//     Copyright Dexoc Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MR.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        public const string UserUpsert = "dbo.UserUpsert";
        public const string UserSelectAll = "dbo.UserSelectAll";
        public const string UserSelect = "dbo.UserSelect";
        public const string UserSelectByRoleId = "dbo.UserSelectByRoleId";
        public const string UserDelete = "dbo.UserDelete";
        public const string UserLogin = "dbo.UserLogin";
        public const string UsersStatusChange = "dbo.UsersStatusChange";
        public const string RolesSelectAll = "dbo.RolesSelectAll";
        public const string AddressUpsert = "dbo.AddressUpsert";
        public const string ProductSelectAll = "dbo.ProductSelectAll";
        public const string ProductSelect = "dbo.ProductSelect";
        public const string ProductUpSert = "dbo.ProductUpSert";
        public const string ProductDelete = "dbo.ProductDelete";
        public const string ProductImagesSelect = "dbo.ProductImagesSelect";
        public const string ProductImagesUpsert = "dbo.ProductImagesUpsert";
        public const string ProductImagesDelete = "dbo.ProductImagesDelete";
        public const string ProductImagesSelectByProductId = "dbo.ProductImagesSelectByProductId";
        public const string InventorySelectAll = "dbo.InventorySelectAll";
        public const string InventorySelect = "dbo.InventorySelect";
        public const string InventoryUpSert = "dbo.InventoryUpSert";
        public const string InventoryDelete = "dbo.InventoryDelete";
        public const string CountrySelectAll = "dbo.CountrySelectAll";
        public const string StateSelectAll = "dbo.StateSelectAll";
        public const string CitySelectAll = "dbo.CitySelectAll";
        public const string StateSelectByCountry = "dbo.StateSelectByCountryId";
        public const string CitySelectByState = "dbo.CitySelectByStateId";
        public const string OrderSelectAll = "dbo.OrderSelectAll";
        public const string OrderSelect = "dbo.OrderSelect";
        public const string OrderUpsert = "dbo.OrderUpsert";
        public const string OrderDetailsSelectAll = "dbo.OrderDetailsSelectAll";
        public const string OrderDetailsSelect = "dbo.OrderDetailsSelect";
        public const string OrderDetailsUpsert = "dbo.OrderDetailsUpsert";
        public const string PaymentUpsert = "dbo.PaymentUpsert";

        public const string Headquarter_ById = "dbo.Headquarter_ById";

        public const string StateRegionHq_All = "StateRegionHq_All";
        public const string StateRegionHq_ById = "StateRegionHq_ById";
        public const string StateRegionHq_Upsert = "StateRegionHq_Upsert";
        public const string StateRegionHq_Delete = "StateRegionHq_Delete";
        public const string StateRegionHq_AllRegion = "StateRegionHq_AllRegion";

        #region Address
        public const string AddressSelectAll = "dbo.AddressSelectAll";
        public const string AddressSelect = "dbo.AddressSelect";
        public const string AddressDelete = "dbo.AddressDelete";
        public const string GetAddressByCustId = "dbo.GetAddressByCustId";
        #endregion

        #region Cart
        public const string CartSelectAll = "dbo.CartSelectAll";
        public const string CartSelect = "dbo.CartSelect";
        public const string CartInsert = "dbo.CartInsert";
        public const string CartUpdate = "dbo.CartUpdate";
        public const string CartDelete = "dbo.CartDelete";
        public const string CartSelectByCustomerId = "dbo.CartSelectByCustomerId";
        public const string ClearCart = "dbo.ClearCart";
        public const string CartSelectByCustomerOrSession = "dbo.CartSelectByCustomerOrSession";
        public const string UpdateCustomerIdInCart = "UpdateCustomerIdInCart";
        #endregion

        #region Category
        public const string CategorySelectAll = "dbo.CategorySelectAll";
        public const string CategoryByCompany = "dbo.CategoryByCompany";
        public const string CategorySelectByParentId = "dbo.CategorySelectByParentId";
        public const string API_CategorySelectByParentId = "dbo.API_CategorySelectByParentId";
        public const string CategorySelect = "dbo.CategorySelect";
        public const string CategoryInsert = "dbo.CategoryInsert";
        public const string CategoryUpdate = "dbo.CategoryUpdate";
        public const string CategoryDelete = "dbo.CategoryDelete";
        public const string UpdateMainCategory = "dbo.UpdateMainCategory";
        public const string MainCategorySelectAll = "dbo.MainCategorySelectAll";
        public const string CategoryForProductSelectAll = "dbo.CategoryForProductSelectAll";
        public const string SubCategorySelectAll = "dbo.SubCategorySelectAll";
        public const string CategorySelectBySlugUrl = "dbo.CategorySelectBySlugUrl";
        public const string TopFourCategorySelectAll = "dbo.TopFourCategorySelectAll";
        public const string TopFiveCategorySelectAll = "dbo.TopFiveCategorySelectAll"; 
        #endregion

        #region CouponCode
        public const string CouponCodeSelectAll = "dbo.CouponCodeSelectAll";
        public const string CouponCodeSelect = "dbo.CouponCodeSelect";
        public const string CouponCodeInsert = "dbo.CouponCodeInsert";
        public const string CouponCodeUpdate = "dbo.CouponCodeUpdate";
        public const string CouponCodeDelete = "dbo.CouponCodeDelete";
        public const string CouponCodeStatusChange = "dbo.CouponCodeStatusChange";
        public const string VerifyCouponCode = "dbo.VerifyCouponCode";
        public const string CouponCodeIsIndividualChange = "dbo.CouponCodeIsIndividualChange";
        public const string SelectActiveCouponCodeByCustomerId = "dbo.SelectActiveCouponCodeByCustomerId";
        public const string SelectActiveCouponCodeByProductId = "dbo.SelectActiveCouponCodeByProductId";
        public const string SelectActiveCouponCode = "dbo.SelectActiveCouponCode";
        public const string SelectActiveCouponCodeSpinWheel = "dbo.SelectActiveCouponCodeSpinWheel";
        public const string CheckExistCouponCodeByCustomerId = "dbo.CheckExistCouponCodeByCustomerId";
        #endregion

        #region Customer
        public const string CompanySelect = "dbo.CompanySelect";
        public const string CompanySelectAll = "dbo.CompanySelectAll";
        public const string CompanyUpdate = "dbo.CompanyUpdate";
        public const string CompanyInsert = "dbo.CompanyInsert";
        public const string CompanyDelete = "dbo.CompanyDelete";
        #endregion

        #region Customer
        public const string CustomerSelectAll = "dbo.CustomerSelectAll";
        public const string TargetCustomerSelectAll = "dbo.TargetCustomerSelectAll";
        public const string SelectAllCustomer = "dbo.SelectAllCustomer";
        public const string CustomerSelect = "dbo.CustomerSelect";
        public const string CustomerInsert = "dbo.CustomerInsert";
        public const string CustomerUpdate = "dbo.CustomerUpdate";
        public const string CustomerDelete = "dbo.CustomerDelete";
        public const string CustomerStatusChange = "dbo.CustomerStatusChange";
        public const string CustomerUpdatePassword = "dbo.CustomerUpdatePassword";
        public const string CustomerForgotPassword = "dbo.CustomerForgotPassword";
        public const string CustomerSelectByMobile = "dbo.CustomerSelectByMobile";
        public const string CurrentBalanceUpdate = "dbo.CurrentBalanceUpdate";
        public const string CustomerRefIdInsert = "dbo.CustomerRefIdInsert";
        public const string UpdateCustomerEmail = "UpdateCustomerEmail";
        public const string CustomerLastLoginUpdate = "dbo.CustomerLastLoginUpdate";

        public const string CustomerLogin = "dbo.CustomerLogin";
        public const string VerifyMobile = "dbo.VerifyMobile";
        public const string CheckExistingUser = "dbo.CheckExistingUser";
        #endregion

        #region Feedback
        public const string FeedbackSelectAll = "dbo.FeedbackSelectAll";
        public const string FeedbackSelect = "dbo.FeedbackSelect";
        public const string FeedbackInsert = "dbo.FeedbackInsert";
        public const string FeedbackUpdate = "dbo.FeedbackUpdate";
        public const string FeedbackDelete = "dbo.FeedbackDelete";
        public const string FeedbackStatusChange = "dbo.FeedbackStatusChange";
        public const string TopFiveFeedbackSelectAll = "dbo.TopFiveFeedbackSelectAll";
        public const string GetReviewsByCustId = "dbo.GetReviewsByCustId";
        public const string GetReviewsByProductId = "dbo.GetReviewsByProductId";
        public const string FeedbackInsertByAdmin = "dbo.FeedbackInsertByAdmin";
        #endregion

        #region FeedbackImages
        public const string FeedbackImagesSelectAll = "dbo.FeedbackImagesSelectAll";
        public const string FeedbackImagesSelect = "dbo.FeedbackImagesSelect";
        public const string FeedbackImagesInsert = "dbo.FeedbackImagesInsert";
        public const string FeedbackImagesUpdate = "dbo.FeedbackImagesUpdate";
        public const string FeedbackImagesDelete = "dbo.FeedbackImagesDelete";
        public const string FeedbackImagesByFeedbackId = "dbo.FeedbackImagesByFeedbackId";
        #endregion

        #region Contact
        public const string ContactInsert = "dbo.ContactInsert";
        public const string ContactSelectAll = "dbo.ContactSelectAll";
        #endregion

        #region Inventory        
        public const string InventoryInsert = "dbo.InventoryInsert";        
        public const string InventoryUpdatePrice = "dbo.InventoryUpdatePrice";
        public const string SellerSelectByProductId = "dbo.SellerSelectByProductId";
        #endregion

        #region Order
        public const string PaymentModeSelectAll = "dbo.PaymentModeSelectAll";
        
        public const string OrderUpdate = "dbo.OrderUpdate";
        public const string OrderDelete = "dbo.OrderDelete";
        public const string TopFiveOrderSelectAll = "dbo.TopFiveOrderSelectAll";
        public const string DashboardOrderSelectAll = "dbo.DashboardOrderSelectAll";
        public const string UpdateOrderStatus = "dbo.OrderStatusUpdate";
        public const string ExportOrder = "dbo.ExportOrder";
        public const string UpdateOrderInvoiceNumber = "dbo.UpdateOrderInvoiceNumber";
        public const string PastOrderSelectByCustomerId = "dbo.PastOrderSelectByCustomerId";
        public const string GetOrderDetailsByOrderId = "dbo.GetOrderDetailsByOrderId";
        public const string OrderSelectByState = "dbo.OrderSelectByState";
        public const string OrderSelectBySeller = "dbo.OrderSelectBySeller";
        public const string GenerateWayBillNo = "dbo.GenerateWayBillNo";
        public const string UpdateCourierId = "dbo.UpdateCourierId";
        public const string UpdateShipRocketOrderId = "dbo.UpdateShipRocketOrderId";
        public const string OrderSelectByCouponCode = "dbo.OrderSelectByCouponCode";
        public const string DashboardOrderDetailSelect = "dbo.DashboardOrderDetailSelect";
        public const string OrderPaymentUpdate = "dbo.OrderPaymentUpdate";
        public const string ReplaceStatusChange = "dbo.ReplaceStatusChange";
        public const string GenerateWayBillNoBySelectedOrders = "dbo.GenerateWayBillNoBySelectedOrders";
        #endregion

        #region OrderDetails        
        public const string OrderDetailsUpdate = "dbo.OrderDetailsUpdate";
        public const string OrderDetailsDelete = "dbo.OrderDetailsDelete";
        public const string OrderDetailsByOrderId = "dbo.OrderDetailsByOrderId";
        public const string GetOrdersByCustomerId = "dbo.GetOrdersByCustomerId";
        public const string GetOrderDetailsBySellerId = "dbo.GetOrderDetailsBySellerId";
        public const string GetSingleOrdersByCustOrderId = "dbo.GetSingleOrdersByCustOrderId";
        public const string GetOrderDetailsById = "dbo.GetOrderDetailsById";
        public const string UpdateOrderDetailStatus = "dbo.UpdateOrderDetailStatus";
        public const string CancelOrderDetail = "dbo.CancelOrderDetail";
        public const string OrderUpdateByAdmin = "dbo.OrderUpdateByAdmin";
        public const string DashboardUpdateOrderDetailStatus = "dbo.DashboardUpdateOrderDetailStatus";
        public const string UpdateOrderDetails = "dbo.UpdateOrderDetails";
        #endregion

        #region ProductMain
        public const string ProductMainSelectAll = "dbo.ProductMainSelectAll";
        public const string ProductMainSelect = "dbo.ProductMainSelect";
        public const string ProductMainInsert = "dbo.ProductMainInsert";
        public const string ProductMainUpdate = "dbo.ProductMainUpdate";
        public const string ProductMainDelete = "dbo.ProductMainDelete";
        public const string GetProductById = "dbo.GetProductById";
        public const string GetProductByType = "dbo.GetProductByType";
        public const string GetProductBySellerId = "GetProductBySellerId";
        public const string GetAllProduct = "dbo.GetAllProductByType";
        public const string ProductMainHomeSelectAll = "dbo.ProductHomeSelectAll";
        public const string FeaturedProductSelectAll = "dbo.FeaturedProductSelectAll";
        public const string BestProductSelectAll = "dbo.BestProductSelectAll";
        public const string NewProductSelectAll = "dbo.NewProductSelectAll";
        public const string DefaultProductSelectAll = "dbo.DefaultProductSelectAll";
        public const string ProductMainForOfferProductSelectAll = "dbo.ProductMainForOfferProductSelectAll";
        public const string ProductMainForDealIdVsProductMainIdSelectAll = "dbo.ProductMainForDealIdVsProductMainIdSelectAll";
        public const string ProductsSelectByProductMainId = "dbo.ProductsSelectByProductMainId";
        public const string ProductMainForNewArrivalsProductSelectAll = "dbo.ProductMainForNewArrivalsProductSelectAll";
        public const string ProductMainSelectAdmin = "dbo.ProductMainSelectAdmin";
        public const string ProductViewCountInsert = "dbo.ProductViewCountInsert";
        public const string ProductMainWithProductSelectAll = "dbo.ProductMainWithProductSelectAll";
        public const string UpdateProductSalesCount = "dbo.UpdateProductSalesCount";
        public const string SelectAllProductsClient = "dbo.SelectAllProductsClient";
        public const string ProductSelectAllByType = "dbo.ProductSelectAllByType";
        public const string GetHomePageProductByType = "dbo.GetHomePageProductByType";

        #endregion

        #region Product
        public const string ProductInventorySelectAll = "dbo.ProductInventorySelectAll";
        public const string ProductInventorySelectAllBySeller = "dbo.ProductInventorySelectAllBySeller";
        public const string ProductInventorySelect = "dbo.ProductInventorySelect";
        public const string UpdateProductCurrentStock = "UpdateProductCurrentStock";
        public const string ProductsBySlug = "dbo.ProductsBySlug";
        
        public const string TopFiveProductSelectAll = "dbo.TopFiveProductSelectAll";
        public const string DashboardProductSelectAll = "dbo.DashboardProductSelectAll";
        public const string ProductsByProductMainId = "dbo.ProductsByProductMainId";
        public const string ProductCurrentStockTable = "dbo.ProductCurrentStockTable";
        public const string ProductCurrentStockBySize = "dbo.ProductCurrentStockBySize";
        #endregion

        #region ProductImages
        public const string ProductImagesSelectAll = "dbo.ProductImagesSelectAll";
        public const string UpdateOnlyProductImages = "dbo.UpdateOnlyProductImages";
        public const string ProductImagesCoverChange = "dbo.ProductImagesCoverChange";
        #endregion

        #region Color
        public const string ColorSelectAll = "dbo.ColorSelectAll";
        public const string ColorSelect = "dbo.ColorSelect";
        public const string ColorInsert = "dbo.ColorInsert";
        public const string ColorUpdate = "dbo.ColorUpdate";
        public const string ColorDelete = "dbo.ColorDelete";
        public const string GetColorsByProductMainId = "dbo.GetColorsByProductMainId";
        #endregion

        #region QA
        public const string QASelectAll = "dbo.QASelectAll";
        public const string QASelect = "dbo.QASelect";
        public const string QAInsert = "dbo.QAInsert";
        public const string QAUpdate = "dbo.QAUpdate";
        public const string QADelete = "dbo.QADelete";
        #endregion

        #region Wishlist
        public const string WishlistSelectAll = "dbo.WishlistSelectAll";
        public const string WishlistSelect = "dbo.WishlistSelect";
        public const string WishlistInsert = "dbo.WishlistInsert";
        public const string WishlistUpdate = "dbo.WishlistUpdate";
        public const string WishlistDelete = "dbo.WishlistDelete";
        public const string RemoveWishlist = "dbo.RemoveWishlist";
        public const string GetWishListbyCustId = "dbo.GetWishListbyCustId";
        public const string WishlistSelectByCustomerOrSession = "dbo.WishlistSelectByCustomerOrSession";
        #endregion

        #region Payment
        public const string PaymentSelectAll = "dbo.PaymentSelectAll";
        public const string PaymentSelect = "dbo.PaymentSelect";
        public const string PaymentUpdate = "dbo.PaymentUpdate";
        public const string PaymentDelete = "dbo.PaymentDelete";
        #endregion

        #region User        
        public const string UserPasswordUpdate = "dbo.UserPasswordUpdate";
        public const string VerifyEmail = "dbo.VerifyEmail";
        #endregion

        #region Seller
        public const string SellerSelectAll = "dbo.SellerSelectAll";
        public const string SellerSelect = "dbo.SellerSelect";
        public const string SellerInsert = "dbo.SellerInsert";
        public const string SellerUpdate = "dbo.SellerUpdate";
        public const string SellerDelete = "dbo.SellerDelete";
        public const string ClientSellerSelectByProductId﻿ = "dbo.ClientSellerSelectByProductId﻿";
        public const string APISelectSellerByProductId = "dbo.APISelectSellerByProductId";
        #endregion

        #region Status
        public const string StatusSelectAll = "dbo.StatusSelectAll";
        public const string StatusSelect = "dbo.StatusSelect";
        public const string StatusInsert = "dbo.StatusInsert";
        public const string StatusUpdate = "dbo.StatusUpdate";
        public const string StatusDelete = "dbo.StatusDelete";
        #endregion

        #region Support
        public const string SupportSelectAll = "dbo.SupportSelectAll";
        public const string SupportSelect = "dbo.SupportSelect";
        public const string SupportInsert = "dbo.SupportInsert";
        public const string SupportUpdate = "dbo.SupportUpdate";
        public const string SupportDelete = "dbo.SupportDelete";
        public const string TopFiveSupportSelectAll = "dbo.TopFiveSupportSelectAll";
        public const string DashboardSupportSelectAll = "dbo.DashboardSupportSelectAll";
        public const string SelectSupportByCustomerId = "dbo.SelectSupportByCustomerId";
        public const string ProductSupport = "ProductSupport";
        public const string OrderSupport = "dbo.OrderSupport";
        public const string SupportSelectAllByProductId = "dbo.SupportSelectAllByProductId";
        #endregion

        #region SupportReply
        public const string SupportReplySelectAll = "dbo.SupportReplySelectAll";
        public const string SupportReplyByCustomerSelectAll = "dbo.SupportReplyByCustomerSelectAll";
        public const string SupportReplySelect = "dbo.SupportReplySelect";
        public const string SupportReplyInsert = "dbo.SupportReplyInsert";
        public const string SupportReplyUpdate = "dbo.SupportReplyUpdate";
        public const string SupportReplyDelete = "dbo.SupportReplyDelete";
        #endregion

        #region CustomerVsCouponConsumption
        public const string CustomerVsCouponConsumptionUpdate = "dbo.CustomerVsCouponConsumptionUpdate";
        public const string CustomerVsCouponConsumptionInsert = "dbo.CustomerVsCouponConsumptionInsert";
        #endregion

        #region SellerVsProduct
        public const string SellerVsProductUpdate = "dbo.SellerVsProductUpdate";
        public const string SellerVsProductInsert = "dbo.SellerVsProductInsert";
        #endregion

        #region Notification
        public const string NotificationSelectAll = "dbo.NotificationSelectAll";
        public const string NotificationSelect = "dbo.NotificationSelect";
        public const string NotificationInsert = "dbo.NotificationInsert";
        public const string NotificationUpdate = "dbo.NotificationUpdate";
        public const string NotificationDelete = "dbo.NotificationDelete";
        public const string NotificationSelectByCustomerId = "dbo.NotificationSelectByCustomerId";
        #endregion

        #region Banner
        public const string BannerSelectAll = "dbo.BannerSelectAll";
        public const string BannerSelect = "dbo.BannerSelect";
        public const string BannerInsert = "dbo.BannerInsert";
        public const string BannerUpdate = "dbo.BannerUpdate";
        public const string BannerDelete = "dbo.BannerDelete";
        public const string BannerStatusChange = "dbo.BannerStatusChange";
        public const string APISelectAllBanner = "dbo.APISelectAllBanner";
        #endregion

        #region Testimonials
        public const string TestimonialsSelectAll = "dbo.TestimonialsSelectAll";
        public const string TestimonialsSelect = "dbo.TestimonialsSelect";
        public const string TestimonialsInsert = "dbo.TestimonialsInsert";
        public const string TestimonialsUpdate = "dbo.TestimonialsUpdate";
        public const string TestimonialsDelete = "dbo.TestimonialsDelete";
        public const string TestimonialsStatusChange = "dbo.TestimonialsStatusChange";
        public const string TestimonialsIsActiveSelectAll = "dbo.TestimonialsIsActiveSelectAll";
        #endregion

        #region Offers
        public const string OffersSelectAll = "dbo.OffersSelectAll";
        public const string OffersSelect = "dbo.OffersSelect";
        public const string OffersInsert = "dbo.OffersInsert";
        public const string OffersUpdate = "dbo.OffersUpdate";
        public const string OffersDelete = "dbo.OffersDelete";
        public const string OffersStatusChange = "dbo.OffersStatusChange";
        public const string TopFourOffersSelectAll = "dbo.TopFourOffersSelectAll";
        public const string OfferTotalClickUpdate = "dbo.OfferTotalClickUpdate";
        public const string OffersSelectAllByImageFor = "dbo.OffersSelectAllByImageFor";
        #endregion

        #region OfferProduct
        public const string OfferProductSelectAll = "dbo.OfferProductSelectAll";
        public const string OfferProductSelect = "dbo.OfferProductSelect";
        public const string GetProductsByOfferSlug = "dbo.GetProductsByOfferSlug";
        public const string APIGetProductsByOfferSlug = "dbo.APIGetProductsByOfferSlug";
        public const string GetAllProductsFromAllOffer = "dbo.GetAllProductsFromAllOffer";
        public const string OfferProductInsert = "dbo.OfferProductInsert";
        public const string OfferProductUpdate = "dbo.OfferProductUpdate";
        public const string OfferProductDelete = "dbo.OfferProductDelete";
        #endregion

        #region Setting
        public const string SettingSelectAll = "dbo.SettingSelectAll";
        public const string HomeSettingSelectAll = "dbo.HomeSettingSelectAll";
        public const string SettingSelect = "dbo.SettingSelect";
        public const string SettingInsert = "dbo.SettingInsert";
        public const string SettingUpdate = "dbo.SettingUpdate";
        public const string SettingDelete = "dbo.SettingDelete";
        public const string SettingSelectByKey = "dbo.SettingSelectByKey";
        #endregion

        #region Blog
        public const string BlogSelectAll = "dbo.BlogSelectAll";
        public const string BlogSelect = "dbo.BlogSelect";
        public const string BlogInsert = "dbo.BlogInsert";
        public const string BlogUpdate = "dbo.BlogUpdate";
        public const string BlogDelete = "dbo.BlogDelete";
        public const string TopThreeBlogsSelectAll = "dbo.TopThreeBlogsSelectAll";
        #endregion

        #region Deal
        public const string DealSelectAll = "dbo.DealSelectAll";
        public const string DealSelect = "dbo.DealSelect";
        public const string DealInsert = "dbo.DealInsert";
        public const string DealUpdate = "dbo.DealUpdate";
        public const string DealDelete = "dbo.DealDelete";
        public const string SelectActiveDeal = "dbo.SelectActiveDeal";
        #endregion

        #region DealIdVsProductMainId
        public const string DealIdVsProductMainIdSelectAll = "dbo.DealIdVsProductMainIdSelectAll";
        public const string DealIdVsProductMainIdSelect = "dbo.DealIdVsProductMainIdSelect";
        public const string DealIdVsProductMainIdInsert = "dbo.DealIdVsProductMainIdInsert";
        public const string DealIdVsProductMainIdUpdate = "dbo.DealIdVsProductMainIdUpdate";
        public const string DealIdVsProductMainIdDelete = "dbo.DealIdVsProductMainIdDelete";
        public const string DealStatusChange = "dbo.DealStatusChange";
        public const string GetProductFromDeal = "dbo.GetProductFromDeal";
        #endregion

        #region NewArrivals
        public const string NewArrivalsSelectAll = "dbo.NewArrivalsSelectAll";
        public const string NewArrivalsSelect = "dbo.NewArrivalsSelect";
        public const string NewArrivalsInsert = "dbo.NewArrivalsInsert";
        public const string NewArrivalsUpdate = "dbo.NewArrivalsUpdate";
        public const string NewArrivalsDelete = "dbo.NewArrivalsDelete";
        public const string NewArrivalsStatusChange = "dbo.NewArrivalsStatusChange";
        public const string TopOneNewArrivalsSelectAll = "dbo.TopOneNewArrivalsSelectAll";
        #endregion

        #region NewArrivalsProduct
        public const string NewArrivalsProductSelectAll = "dbo.NewArrivalsProductSelectAll";
        public const string NewArrivalsProductSelect = "dbo.NewArrivalsProductSelect";
        public const string NewArrivalsProductSelectByNewArrivalsSlug = "dbo.NewArrivalsProductSelectByNewArrivalsSlug";
        public const string NewArrivalsProductInsert = "dbo.NewArrivalsProductInsert";
        public const string NewArrivalsProductUpdate = "dbo.NewArrivalsProductUpdate";
        public const string NewArrivalsProductDelete = "dbo.NewArrivalsProductDelete";
        #endregion

        #region ShippingPrice
        public const string GetShppingPrice = "dbo.GetShppingPrice";
        #endregion

        #region CMS
        public const string CMSSelectAll = "dbo.CMSSelectAll";
        public const string CMSSelectByID = "dbo.CMSSelectByID";
        public const string CMSInsert = "dbo.CMSInsert";
        public const string CMSUpdate = "dbo.CMSUpdate";
        public const string CMSDelete = "dbo.CMSDelete";
        public const string CMSSelectByKey = "dbo.CMSSelectByKey";
        #endregion

        #region Message
        public const string MessageSelectAll = "dbo.MessageSelectAll";
        public const string MessageSelect = "dbo.MessageSelect";
        public const string MessageInsert = "dbo.MessageInsert";
        public const string MessageUpdate = "dbo.MessageUpdate";
        public const string MessageDelete = "dbo.MessageDelete";
        #endregion

        #region Email
        public const string EmailSelectAll = "dbo.EmailSelectAll";
        public const string EmailSelect = "dbo.EmailSelect";
        public const string EmailInsert = "dbo.EmailInsert";
        public const string EmailUpdate = "dbo.EmailUpdate";
        public const string EmailDelete = "dbo.EmailDelete";
        #endregion

        #region BankDetails
        public const string BankDetailsSelect = "dbo.BankDetailsSelect";
        public const string BankDetailSelectAll = "dbo.BankDetailSelectAll";
        public const string GetCutomersDeviceToken = "dbo.GetCutomersDeviceToken";
        public const string BankDetailsSelectByCashbackTranscationId = "dbo.BankDetailsSelectByCashbackTranscationId";
        public const string BankDetailsInsert = "dbo.BankDetailsInsert";
        public const string UpdateBankDetailStatus = "dbo.UpdateBankDetailStatus";
        #endregion

        #region Expense
        public const string ExpenseSelectAll = "dbo.ExpenseSelectAll";
        public const string ExpenseSelect = "dbo.ExpenseSelect";
        public const string ExpenseUpdate = "dbo.ExpenseUpdate";
        public const string ExpenseInsert = "dbo.ExpenseInsert";
        public const string ExpenseDelete = "dbo.ExpenseDelete";
        public const string CurrentMonthExpense = "dbo.CurrentMonthExpense";
        #endregion

        #region CashbackTransaction
        public const string CashbackTransactionByCustomerId = "dbo.CashbackTransactionByCustomerId";
        public const string InsertCashbackTransaction = "dbo.InsertCashbackTransaction";
        #endregion

        #region Report
        public const string OrderReportSelectAll = "dbo.OrderReportSelectAll";
        public const string CurrentMonthProfit = "dbo.CurrentMonthProfit";
        public const string ProfitLossReport = "dbo.ProfitLossReport";
        public const string CurrentMonthSales = "dbo.CurrentMonthSales";
        public const string OrderReportMonthSalesSelectAll = "dbo.OrderReportMonthSalesSelectAll";
        public const string CancelOrderSelectAll = "dbo.CancelOrderSelectAll";
        public const string BestProductSelectAllReports = "dbo.BestProductSelectAllReports";
        public const string ReplacedOrderSelectAll = "dbo.ReplacedOrderSelectAll";
        public const string ReturnedOrderSelectAll = "dbo.ReturnedOrderSelectAll";
        #endregion

        #region Search
        public const string HomePageProductSearch = "dbo.HomePageProductSearch";
        public const string SearchProductSelectAll = "dbo.SearchProductSelectAll";
        #endregion
        
        #region Size
        public const string SizeDelete = "dbo.SizeDelete";
        public const string SizeInsert = "dbo.SizeInsert";
        public const string SizeSelect = "dbo.SizeSelect";
        public const string SizeSelectAll = "dbo.SizeSelectAll";
        public const string SizeUpdate = "dbo.SizeUpdate";
        #endregion

        #region Subscriber
        public const string SubscriberInsert = "dbo.SubscriberInsert";
        public const string SubscriberSelectAll = "dbo.SubscriberSelectAll";
        #endregion

        #region ProductLookbook
        public const string ProductLookbookDelete = "dbo.ProductLookbookDelete";
        public const string ProductLookbookInsert = "dbo.ProductLookbookInsert";
        public const string ProductLookbookSelect = "dbo.ProductLookbookSelect";
        public const string ProductLookbookSelectAll = "dbo.ProductLookbookSelectAll";
        public const string ProductLookbookUpdate = "dbo.ProductLookbookUpdate";
        public const string ProductLookbookSelectByProductId = "dbo.ProductLookbookSelectByProductId";
        #endregion

        #region ProductLookbookDetails
        public const string ProductLookbookDetailsDelete = "dbo.ProductLookbookDetailsDelete";
        public const string ProductLookbookDetailsInsert = "dbo.ProductLookbookDetailsInsert";
        public const string ProductLookbookDetailsSelect = "dbo.ProductLookbookDetailsSelect";
        public const string ProductLookbookDetailsSelectAll = "dbo.ProductLookbookDetailsSelectAll";
        public const string ProductLookbookDetailsUpdate = "dbo.ProductLookbookDetailsUpdate";
        #endregion

        public const string StateAll = "dbo.StateAll";
        public const string GetDashboardCount = "dbo.GetDashboardCount";






        #region ProductCategory
        public const string ProductCategory_All = "ProductCategory_All";
        public const string ProductCategory_Upsert = "ProductCategory_Upsert";
        public const string ProductCategory_ById = "ProductCategory_ById";
        public const string ProductCategory_Delete = "ProductCategory_Delete";
        #endregion

        #region ProductType
        public const string ProductType_All = "ProductType_All";
        public const string ProductType_ById = "ProductType_ById";
        public const string ProductType_Upsert = "ProductType_Upsert";
        public const string ProductType_Delete = "ProductType_Delete";
        #endregion

        #region ProductManufacture
        public const string ProductManufacture_All = "ProductManufacture_All";
        public const string ProductManufacture_ById = "ProductManufacture_ById";
        public const string ProductManufacture_Upsert = "ProductManufacture_Upsert";
        public const string ProductManufacture_Delete = "ProductManufacture_Delete";
        #endregion

        #region QuantitySIUnites
        public const string QuantitySIUnites_All = "QuantitySIUnites_All";
        public const string QuantitySIUnites_ById = "QuantitySIUnites_ById";
        public const string QuantitySIUnites_Upsert = "QuantitySIUnites_Upsert";
        #endregion

        #region Product
        public const string Product_All = "Product_All";
        public const string Product_ById = "Product_ById";
        public const string Product_Upsert = "Product_Upsert";
        public const string Product_Delete = "Product_Delete";
        #endregion

        #region ProductHightlights
        public const string ProductHightlights_All = "ProductHightlights_All";
        public const string ProductHightlights_ById = "ProductHightlights_ById";
        public const string ProductHightlights_Upsert = "ProductHightlights_Upsert";
        public const string ProductHightlights_Delete = "ProductHightlights_Delete";
        #endregion

        #region ProductImages
        public const string ProductImages_All = "ProductImages_All";
        public const string ProductImages_ById = "ProductImages_ById";
        public const string ProductImages_Upsert = "ProductImages_Upsert";
        public const string ProductImages_Delete = "ProductImages_Delete";
        #endregion

        #region ProductPrescription
        public const string ProductPrescription_All = "ProductPrescription_All";
        public const string ProductPrescription_ById = "ProductPrescription_ById";
        public const string ProductPrescription_Upsert = "ProductPrescription_Upsert";
        public const string ProductPrescription_Delete = "ProductPrescription_Delete";
        #endregion

        #region Presentation
        public const string Presentation_All = "Presentation_All";
        public const string Presentation_Upsert = "Presentation_Upsert";
        public const string Presentation_Delete = "Presentation_Delete";
        public const string Presentation_ById = "Presentation_ById";
        public const string Presentation_ByProductId = "Presentation_ByProductId";
        #endregion

        #region ProductHighlight
        public const string ProductHighlight_All = "ProductHighlight_All";
        public const string ProductHighlight_Upsert = "ProductHighlight_Upsert";
        public const string ProductHighlight_Delete = "ProductHighlight_Delete";
        public const string ProductHighlight_ById = "ProductHighlight_ById";
        public const string ProductHighlight_DeleteByProductId = "ProductHighlight_DeleteByProductId";        
        #endregion


        public const string DSR_All = "DSR_All";
        public const string DSR_ById = "DSR_ById";
        public const string DSR_Upsert = "DSR_Upsert";
        public const string DSR_Delete = "DSR_Delete";

        public const string CallIn_All = "CallIn_All";
        public const string CallIn_ById = "CallIn_ById";
        public const string CallIn_Upsert = "CallIn_Upsert";
        public const string CallIn_Delete = "CallIn_Delete";

        public const string DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId = "DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId";
        public const string DSR_VisitedDoctor_Products_Upsert = "DSR_VisitedDoctor_Products_Upsert";
        public const string DSR_VisitedDoctor_Products_Delete = "DSR_VisitedDoctor_Products_Delete";

        public const string DoctorProducts_ByDoctorId = "DoctorProducts_ByDoctorId";
        public const string DoctorProducts_Upsert = "DoctorProducts_Upsert";
        public const string DoctorProducts_Delete = "DoctorProducts_Delete";

        public const string DSR_VisitedDoctor_ByDSRId = "DSR_VisitedDoctor_ByDSRId";
        public const string DSR_VisitedDoctor_Upsert = "DSR_VisitedDoctor_Upsert";
        public const string DSR_VisitedDoctor_Delete = "DSR_VisitedDoctor_Delete";
        public const string DSR_VisitedDoctor_ById = "DSR_VisitedDoctor_ById";

        public const string WorkType_Upsert = "WorkType_Upsert";
        public const string WorkType_All = "WorkType_All";
        public const string WorkType_Delete = "WorkType_Delete";
        public const string WorkType_ById = "WorkType_ById";



        public const string VisitType_Upsert = "VisitType_Upsert";
        public const string VisitType_All = "VisitType_All";
        public const string VisitType_Delete = "VisitType_Delete";
        public const string VisitType_ById = "VisitType_ById";

        public const string Outcome_All = "Outcome_All";
        public const string Outcome_ById = "Outcome_ById";
        public const string Outcome_Upsert = "Outcome_Upsert";
        public const string Outcome_Delete = "Outcome_Delete";




        public const string DSR_VisitedChemist_ByDSRId = "DSR_VisitedChemist_ByDSRId";
        public const string DSR_VisitedChemist_ById = "DSR_VisitedChemist_ById";
        public const string DSR_VisitedChemist_Upsert = "DSR_VisitedChemist_Upsert";
        public const string DSR_VisitedChemist_Delete = "DSR_VisitedChemist_Delete";



        public const string DSR_VisitedChemist_Products_ByDSR_VisitedChemistId = "DSR_VisitedChemist_Products_ByDSR_VisitedChemistId";
        public const string DSR_VisitedChemist_Products_Upsert = "DSR_VisitedChemist_Products_Upsert";
        public const string DSR_VisitedChemist_Products_Delete = "DSR_VisitedChemist_Products_Delete";


        public const string MR_Presentation_All = "MR_Presentation_All";
        public const string MR_Presentation_ById = "MR_Presentation_ById";
        public const string MR_Presentation_Upsert = "MR_Presentation_Upsert";
        public const string MR_Presentation_Delete = "MR_Presentation_Delete";

        public const string MR_PresentationResources_ByMR_PresentationId = "MR_PresentationResources_ByMR_PresentationId";
        public const string MR_PresentationResources_Upsert = "MR_PresentationResources_Upsert";
        public const string MR_PresentationResources_Delete = "MR_PresentationResources_Delete";

        public const string ProductVsChemist_All = "ProductVsChemist_All";
        public const string ProductVsChemist_ByProductId = "ProductVsChemist_ByProductId";
        public const string ProductVsChemist_ByChemistId = "ProductVsChemist_ByChemistId";
        public const string ProductVsChemist_Upsert = "ProductVsChemist_Upsert";
        public const string ProductVsChemist_Delete = "ProductVsChemist_Delete";

        public const string ChemistVsDoctor_All = "ChemistVsDoctor_All";
        public const string ChemistVsDoctor_ByDoctorId = "ChemistVsDoctor_ByDoctorId";
        public const string ChemistVsDoctor_ByChemistId = "ChemistVsDoctor_ByChemistId";
        public const string ChemistVsDoctor_Upsert = "ChemistVsDoctor_Upsert";
        public const string ChemistVsDoctor_Delete = "ChemistVsDoctor_Delete";

        public const string ProductVsDoctor_Upsert = "ProductVsDoctor_Upsert";
        public const string ProductVsDoctor_All = "ProductVsDoctor_All";
        public const string ProductVsDoctor_Delete = "ProductVsDoctor_Delete";
        public const string ProductVsDoctor_ByDoctorId = "ProductVsDoctor_ByDoctorId";
        public const string ProductVsDoctor_ByProductId = "ProductVsDoctor_ByProductId";

        public const string DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId = "DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId";
        public const string DSR_VisitedDoctor_Campaign_Gifts_ById = "DSR_VisitedDoctor_Campaign_Gifts_ById";
        public const string DSR_VisitedDoctor_Campaign_Gifts_Upsert = "DSR_VisitedDoctor_Campaign_Gifts_Upsert";
        public const string DSR_VisitedDoctor_Campaign_Gifts_Delete = "DSR_VisitedDoctor_Campaign_Gifts_Delete";

        public const string DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId = "DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId";
        public const string DSR_VisitedDoctor_Campaign_ById = "DSR_VisitedDoctor_Campaign_ById";
        public const string DSR_VisitedDoctor_Campaign_Upsert = "DSR_VisitedDoctor_Campaign_Upsert";
        public const string DSR_VisitedDoctor_Campaign_Delete = "DSR_VisitedDoctor_Campaign_Delete";


        public const string Employee_ById = "Employee_ById";


        public const string Gift_ChampaignMasterId = "Gift_ChampaignMasterId";
        public const string Gift_ById = "Gift_ById";
        public const string Gift_Upsert = "Gift_Upsert";
        public const string Gift_Delete = "Gift_Delete";


        public const string ChampaignMaster_All = "ChampaignMaster_All";
        public const string ChampaignMaster_ById = "ChampaignMaster_ById";
        public const string ChampaignMaster_Upsert = "ChampaignMaster_Upsert";
        public const string ChampaignMaster_Delete = "ChampaignMaster_Delete";

        public const string ApplyMaster_All = "ApplyMaster_All";
        public const string ApplyMaster_Upsert = "ApplyMaster_Upsert";
        public const string ApplyMaster_Delete = "ApplyMaster_Delete";
        public const string ApplyMaster_ById = "ApplyMaster_ById";

        public const string Prescriber_ByDoctorId = "Prescriber_ByDoctorId";
        public const string Prescriber_ById = "Prescriber_ById";
        public const string Prescriber_Upsert = "Prescriber_Upsert";
        public const string PrescriberProducts_ByPrescriberId = "PrescriberProducts_ByPrescriberId";
        public const string PrescriberProducts_ById = "PrescriberProducts_ById";
        public const string PrescriberProducts_Upsert = "PrescriberProducts_Upsert";

        public const string Leave_ByEmployeeId = "Leave_ByEmployeeId";
        public const string Leave_ById = "Leave_ById";
        public const string Leave_Upsert = "Leave_Upsert";

        public const string DayTypeMaster_All = "DayTypeMaster_All";

        public const string LeaveTypeMaster_All = "LeaveTypeMaster_All";


        #region TourPlan
        public const string TourPlan_All = "TourPlan_All";
        public const string TourPlan_ById = "TourPlan_ById";
        public const string TourPlan_IsSubmitted = "TourPlan_IsSubmitted";
        public const string TourPlan_Upsert = "TourPlan_Upsert";
        #endregion

        #region
        public const string TourPlanMonth_All = "TourPlanMonth_All";
        public const string TourPlanMonth_Upsert = "TourPlanMonth_Upsert";
        public const string TourPlanMonth_ById = "TourPlanMonth_ById";
        #endregion

        #region
        public const string DoctorAndChemist_All = "DoctorAndChemist_All";
        public const string DoctorAndChemist_Upsert = "DoctorAndChemist_Upsert";
        public const string DoctorAndChemist_Delete = "DoctorAndChemist_Delete";
        #endregion

        #region
        public const string JointWorkingMonth_All = "JointWorkingMonth_All";
        public const string JointWorkingMonth_Upsert = "JointWorkingMonth_Upsert";
        #endregion

        #region
        public const string JointWorking_All = "JointWorking_All";
        public const string JointWorking_Upsert = "JointWorking_Upsert";
        #endregion


        #region
        public const string MasterWorkAt_All = "MasterWorkAt_All";
        #endregion

        #region
        public const string EmployeeRegion_Upsert = "EmployeeRegion_Upsert";
        public const string EmployeeRegion_ByUserId = "EmployeeRegion_ByUserId";
        #endregion

    }
}
