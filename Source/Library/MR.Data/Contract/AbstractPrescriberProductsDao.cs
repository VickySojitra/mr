﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractPrescriberProductsDao
    {
        public abstract SuccessResult<AbstractPrescriberProducts> PrescriberProducts_Upsert(AbstractPrescriberProducts abstractPrescriberProducts);
        public abstract PagedList<AbstractPrescriberProducts> PrescriberProducts_ByPrescriberId(PageParam pageParam, long PrescriberId);
        public abstract SuccessResult<AbstractPrescriberProducts> PrescriberProducts_ById(long Id);
        

    }
}
