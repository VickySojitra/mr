﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.V1;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractLeaveDao
    {
        public abstract SuccessResult<AbstractLeave> Leave_Upsert(AbstractLeave abstractLeave);
        public abstract PagedList<AbstractLeave> Leave_ByEmployeeId(PageParam pageParam, long EmployeeId);
        public abstract SuccessResult<AbstractLeave> Leave_ById(long Id);

    }
}
