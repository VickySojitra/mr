﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractApplyMasterDao
    {
        public abstract SuccessResult<AbstractApplyMaster> ApplyMaster_Upsert(AbstractApplyMaster abstractApplyMaster);
        public abstract SuccessResult<AbstractApplyMaster> ApplyMaster_ById(long Id);
        public abstract PagedList<AbstractApplyMaster> ApplyMaster_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractApplyMaster> ApplyMaster_Delete(long Id, long DeletedBy);
       
    }
}
