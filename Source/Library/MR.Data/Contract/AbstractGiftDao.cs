﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractGiftDao
    {
        public abstract SuccessResult<AbstractGift> Gift_Upsert(AbstractGift abstractGift);
        public abstract PagedList<AbstractGift> Gift_ChampaignMasterId(PageParam pageParam, long ChampaignMasterId);
        public abstract SuccessResult<AbstractGift> Gift_ById(long Id);
        public abstract SuccessResult<AbstractGift> Gift_Delete(long Id, long DeletedBy);

    }
}
