﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.V1;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractProductVsChemistDao
    {
        public abstract SuccessResult<AbstractProductVsChemist> ProductVsChemist_Upsert(AbstractProductVsChemist abstractProductVsChemist);
        public abstract PagedList<AbstractProductVsChemist> ProductVsChemist_ByChemistId(PageParam pageParam, long ChemistId);
        public abstract PagedList<AbstractProductVsChemist> ProductVsChemist_All(PageParam pageParam, string search, long ProductId, long ChemistId);
        public abstract PagedList<AbstractProductVsChemist> ProductVsChemist_ByProductId(PageParam pageParam, long ProductId);
        public abstract SuccessResult<AbstractProductVsChemist> ProductVsChemist_Delete(long Id, int deletedby);

    }
}
