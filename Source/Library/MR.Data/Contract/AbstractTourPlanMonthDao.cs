﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractTourPlanMonthDao
    {
        public abstract SuccessResult<AbstractTourPlanMonth> TourPlanMonth_Upsert(AbstractTourPlanMonth abstractTourPlanMonth);
        public abstract SuccessResult<AbstractTourPlanMonth> TourPlanMonth_ById(int Id);
     
        public abstract PagedList<AbstractTourPlanMonth> TourPlanMonth_All(PageParam pageParam, string search, int TourPlanId);


    }
}