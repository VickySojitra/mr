﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.V1;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractProductHightlightsDao
    {
        public abstract SuccessResult<AbstractProductHightlights> ProductHightlights_Upsert(AbstractProductHightlights abstractProductHightlights);
        public abstract SuccessResult<AbstractProductHightlights> ProductHightlights_ById(long Id);
        public abstract PagedList<AbstractProductHightlights> ProductHightlights_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductHightlights> ProductHightlights_Delete(long Id);

    }
}
