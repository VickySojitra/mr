﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractWorkTypeDao
    {
        public abstract SuccessResult<AbstractWorkType> WorkType_Upsert(AbstractWorkType abstractWorkType);
        public abstract PagedList<AbstractWorkType> WorkType_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractWorkType> WorkType_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractWorkType> WorkType_ById(long Id);


    }
}
