﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractDoctorProductsDao
    {
        public abstract SuccessResult<AbstractDoctorProducts> DoctorProducts_Upsert(AbstractDoctorProducts abstractDoctorProducts);
        public abstract SuccessResult<AbstractDoctorProducts> DoctorProducts_ByDoctorId(long Id);

        public abstract SuccessResult<AbstractDoctorProducts> DoctorProducts_Delete(long Id, long DeletedBy);

    }
}
