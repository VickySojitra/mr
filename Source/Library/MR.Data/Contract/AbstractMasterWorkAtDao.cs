﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.V1;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractMasterWorkAtDao
    {
        public abstract PagedList<AbstractMasterWorkAt> MasterWorkAt_All(PageParam pageParam, string search);

    }
}
