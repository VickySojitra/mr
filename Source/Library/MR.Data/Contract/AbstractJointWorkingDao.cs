﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractJointWorkingDao
    {
        public abstract SuccessResult<AbstractJointWorking> JointWorking_Upsert(AbstractJointWorking abstractJointWorking);
        public abstract PagedList<AbstractJointWorking> JointWorking_All(PageParam pageParam, string search, int TourPlanMonthId, int JointWorkingId);
       
    }
}
