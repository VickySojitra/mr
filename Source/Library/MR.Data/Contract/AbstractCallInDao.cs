﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractCallInDao
    {
        public abstract SuccessResult<AbstractCallIn> CallIn_Upsert(AbstractCallIn abstractCallIn);
        public abstract SuccessResult<AbstractCallIn> CallIn_ById(long Id);
        public abstract PagedList<AbstractCallIn> CallIn_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCallIn> CallIn_Delete(long Id, long DeletedBy);

    }
}
