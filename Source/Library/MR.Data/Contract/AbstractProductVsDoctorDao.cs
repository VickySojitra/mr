﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractProductVsDoctorDao
    {
        public abstract SuccessResult<AbstractProductVsDoctor> ProductVsDoctor_Upsert(AbstractProductVsDoctor abstractProductVsDoctor);
        public abstract PagedList<AbstractProductVsDoctor> ProductVsDoctor_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductVsDoctor> ProductVsDoctor_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractProductVsDoctor> ProductVsDoctor_ByProductId(PageParam pageParam, long ProductId);
        public abstract PagedList<AbstractProductVsDoctor> ProductVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId);
        

    }
}
