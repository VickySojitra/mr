﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractLeaveTypeMasterDao
    {
        public abstract PagedList<AbstractLeaveTypeMaster> LeaveTypeMaster_All(PageParam pageParam, string search);
       
    }
}
