﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractProductHighlightDao
    {
        public abstract SuccessResult<AbstractProductHighlight> ProductHighlight_Upsert(AbstractProductHighlight abstractProductHighlight);
        public abstract SuccessResult<AbstractProductHighlight> ProductHighlight_ById(long Id);
        public abstract PagedList<AbstractProductHighlight> ProductHighlight_All(PageParam pageParam, string search, long ProductId);
        public abstract SuccessResult<AbstractProductHighlight> ProductHighlight_Delete(long Id);
        public abstract SuccessResult<AbstractProductHighlight> ProductHighlight_DeleteByProductId(long Id);        
    }
}
