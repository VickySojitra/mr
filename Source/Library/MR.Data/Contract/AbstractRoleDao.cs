﻿using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractRoleDao
    {
        public abstract PagedList<AbstractRole> SelectAll(PageParam pageParam, string search = "");
       
        public abstract SuccessResult<AbstractRole> InsertUpdate(AbstractRole abstractRole);
    }
}
