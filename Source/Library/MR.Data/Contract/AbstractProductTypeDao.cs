﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractProductTypeDao
    {
        public abstract SuccessResult<AbstractProductType> ProductType_Upsert(AbstractProductType abstractProductType);
        public abstract SuccessResult<AbstractProductType> ProductType_ById(long Id);
        public abstract PagedList<AbstractProductType> ProductType_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractProductType> ProductType_Delete(long Id);
    }
}
