﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractChampaignMasterDao
    {
        public abstract SuccessResult<AbstractChampaignMaster> ChampaignMaster_Upsert(AbstractChampaignMaster abstractChampaignMaster);
        public abstract PagedList<AbstractChampaignMaster> ChampaignMaster_All(PageParam pageParam, string search, long ApplyMasterId, long RegionId, long HeadquarterId);
        public abstract SuccessResult<AbstractChampaignMaster> ChampaignMaster_ById( long Id);
        public abstract SuccessResult<AbstractChampaignMaster> ChampaignMaster_Delete(long Id, long DeletedBy);

    }
}
