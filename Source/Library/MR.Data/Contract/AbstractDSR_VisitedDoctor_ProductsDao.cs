﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractDSR_VisitedDoctor_ProductsDao
    {
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_Upsert(AbstractDSR_VisitedDoctor_Products abstractDSR_VisitedDoctor_Products);
        public abstract PagedList<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId);
        public abstract SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_Delete(long Id);

    }
}