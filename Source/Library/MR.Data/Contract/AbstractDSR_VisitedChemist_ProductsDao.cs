﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.V1;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractDSR_VisitedChemist_ProductsDao
    {
        public abstract SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_Upsert(AbstractDSR_VisitedChemist_Products abstractDSR_VisitedChemist_Products);
        public abstract PagedList<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(PageParam pageParam, long DSR_VisitedChemistId);
        public abstract SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_Delete(long Id);

    }
}