﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;

namespace MR.Data.Contract
{
    public abstract class AbstractVisitTypeDao
    {
        public abstract SuccessResult<AbstractVisitType> VisitType_Upsert(AbstractVisitType abstractVisitType);
        public abstract PagedList<AbstractVisitType> VisitType_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractVisitType> VisitType_Delete(long Id,long DeletedBy);
        public abstract SuccessResult<AbstractVisitType> VisitType_ById(long Id);
       

    }
}
