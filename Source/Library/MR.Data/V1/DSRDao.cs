﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSRDao : AbstractDSRDao
    {
        public override PagedList<AbstractDSR> DSR_All(PageParam pageParam, string search="", long RouteTypeId=0, 
            long WorkTypeId = 0, long RouteId = 0, long VisitTypeId = 0, long DoctorId = 0, long ChemistId = 0, 
            int EmployeeId = 0, AbstractDSR abstractDSR = null)
        {
            PagedList<AbstractDSR> DSR = new PagedList<AbstractDSR>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RouteTypeId",RouteTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@WorkTypeId",WorkTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RouteId", RouteId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VisitTypeId", VisitTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", EmployeeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

			if (abstractDSR != null)
			{
				param.Add("@StatusId", abstractDSR.StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
				param.Add("@Date", abstractDSR.Date, dbType: DbType.String, direction: ParameterDirection.Input);
				param.Add("@HeadquaterId", abstractDSR.HeadquaterId, dbType: DbType.Int32, direction: ParameterDirection.Input);
				param.Add("@RegionId", abstractDSR.RegionId, dbType: DbType.Int32, direction: ParameterDirection.Input);
			}
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_All, param, commandType: CommandType.StoredProcedure);
                DSR.Values.AddRange(task.Read<DSR>());
                DSR.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR;
        }

        public override SuccessResult<AbstractDSR> DSR_ById(long Id)
        {
            SuccessResult<AbstractDSR> DSR = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_ById, param, commandType: CommandType.StoredProcedure);
                DSR = task.Read<SuccessResult<AbstractDSR>>().SingleOrDefault();
                DSR.Item = task.Read<DSR>().SingleOrDefault();
            }

            return DSR;
        }

        public override SuccessResult<AbstractDSR> DSR_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDSR> DSR = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_Delete, param, commandType: CommandType.StoredProcedure);
                DSR = task.Read<SuccessResult<AbstractDSR>>().SingleOrDefault();
                DSR.Item = task.Read<DSR>().SingleOrDefault();
            }

            return DSR;
        }

        public override SuccessResult<AbstractDSR> DSR_Upsert(AbstractDSR abstractDSR)
        {
            SuccessResult<AbstractDSR> DSR = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Date", abstractDSR.Date, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RouteTypeId", abstractDSR.RouteTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@WorkTypeId", abstractDSR.WorkTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RouteId", abstractDSR.RouteId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VisitTypeId", abstractDSR.VisitTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", abstractDSR.DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId ", abstractDSR.ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmployeeId ", abstractDSR.EmployeeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId ", abstractDSR.StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
           
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR = task.Read<SuccessResult<AbstractDSR>>().SingleOrDefault();
                DSR.Item = task.Read<DSR>().SingleOrDefault();
            }

            return DSR;
        }
    }
}
