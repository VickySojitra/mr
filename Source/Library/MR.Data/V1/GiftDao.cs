﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class GiftDao : AbstractGiftDao
    {
        public override SuccessResult<AbstractGift> Gift_ById(long Id)
        {
            SuccessResult<AbstractGift> Gift = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Gift_ById, param, commandType: CommandType.StoredProcedure);
                Gift = task.Read<SuccessResult<AbstractGift>>().SingleOrDefault();
                Gift.Item = task.Read<Gift>().SingleOrDefault();
            }

            return Gift;
        }

        public override PagedList<AbstractGift> Gift_ChampaignMasterId(PageParam pageParam, long ChampaignMasterId)
        {
            PagedList<AbstractGift> Gift = new PagedList<AbstractGift>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChampaignMasterId", ChampaignMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Gift_ChampaignMasterId, param, commandType: CommandType.StoredProcedure);
                Gift.Values.AddRange(task.Read<Gift>());
                Gift.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Gift;
        }

        public override SuccessResult<AbstractGift> Gift_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractGift> Gift = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Gift_Delete, param, commandType: CommandType.StoredProcedure);
                Gift = task.Read<SuccessResult<AbstractGift>>().SingleOrDefault();
                Gift.Item = task.Read<Gift>().SingleOrDefault();
            }

            return Gift;
        }

        public override SuccessResult<AbstractGift> Gift_Upsert(AbstractGift abstractGift)
        {
            SuccessResult<AbstractGift> Gift = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractGift.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ChampaignMasterId", abstractGift.ChampaignMasterId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@GiftName", abstractGift.GiftName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GiftQty", abstractGift.GiftQty, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractGift.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractGift.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            
            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Gift_Upsert, param, commandType: CommandType.StoredProcedure);
                Gift = task.Read<SuccessResult<AbstractGift>>().SingleOrDefault();
                Gift.Item = task.Read<Gift>().SingleOrDefault();
            }

            return Gift;
        }
    }
}
