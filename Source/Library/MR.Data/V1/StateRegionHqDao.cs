﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class StateRegionHqDao : AbstractStateRegionHqDao
    {

        public override SuccessResult<AbstractStateRegionHq> StateRegionHq_Upsert(AbstractStateRegionHq abstractStateRegionHq)
        {
            SuccessResult<AbstractStateRegionHq> StateRegionHq = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractStateRegionHq.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractStateRegionHq.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractStateRegionHq.ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@GrandParentId", abstractStateRegionHq.GrandParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractStateRegionHq.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractStateRegionHq.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.StateRegionHq_Upsert, param, commandType: CommandType.StoredProcedure);
                StateRegionHq = task.Read<SuccessResult<AbstractStateRegionHq>>().SingleOrDefault();
                StateRegionHq.Item = task.Read<StateRegionHq>().SingleOrDefault();
            }

            return StateRegionHq;
        }

        public override SuccessResult<AbstractStateRegionHq> StateRegionHq_ById(int Id)
        {
            SuccessResult<AbstractStateRegionHq> StateRegionHq = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.StateRegionHq_ById, param, commandType: CommandType.StoredProcedure);
                StateRegionHq = task.Read<SuccessResult<AbstractStateRegionHq>>().SingleOrDefault();
                StateRegionHq.Item = task.Read<StateRegionHq>().SingleOrDefault();
            }

            return StateRegionHq;
        }

        public override PagedList<AbstractStateRegionHq> StateRegionHq_All(PageParam pageParam, string search, int ParentId, int GrandParentId)
        {
            PagedList<AbstractStateRegionHq> StateRegionHq = new PagedList<AbstractStateRegionHq>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@GrandParentId", GrandParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.StateRegionHq_All, param, commandType: CommandType.StoredProcedure);
                StateRegionHq.Values.AddRange(task.Read<StateRegionHq>());
                StateRegionHq.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return StateRegionHq;
        }

        public override PagedList<AbstractStateRegionHq> StateRegionHq_AllRegion()
        {
            PagedList<AbstractStateRegionHq> StateRegionHq = new PagedList<AbstractStateRegionHq>();

            var param = new DynamicParameters();

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.StateRegionHq_AllRegion, param, commandType: CommandType.StoredProcedure);
                StateRegionHq.Values.AddRange(task.Read<StateRegionHq>());
                StateRegionHq.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return StateRegionHq;
        }

        public override SuccessResult<AbstractStateRegionHq> StateRegionHq_Delete(int Id, int DeletedBy)
        {
            SuccessResult<AbstractStateRegionHq> StateRegionHq = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.StateRegionHq_Delete, param, commandType: CommandType.StoredProcedure);
                StateRegionHq = task.Read<SuccessResult<AbstractStateRegionHq>>().SingleOrDefault();
                StateRegionHq.Item = task.Read<StateRegionHq>().SingleOrDefault();
            }

            return StateRegionHq;
        }

        

    }
}
