﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class TourPlanMonthDao : AbstractTourPlanMonthDao
    {
        public override PagedList<AbstractTourPlanMonth> TourPlanMonth_All(PageParam pageParam, string search, int TourPlanId)
        {
            PagedList<AbstractTourPlanMonth> TourPlanMonth = new PagedList<AbstractTourPlanMonth>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TourPlanId", TourPlanId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlanMonth_All, param, commandType: CommandType.StoredProcedure);
                TourPlanMonth.Values.AddRange(task.Read<TourPlanMonth>());
                TourPlanMonth.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TourPlanMonth;
        }

        public override SuccessResult<AbstractTourPlanMonth> TourPlanMonth_Upsert(AbstractTourPlanMonth abstractTourPlanMonth)
        {
            SuccessResult<AbstractTourPlanMonth> TourPlanMonth = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTourPlanMonth.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@TourPlanId", abstractTourPlanMonth.TourPlanId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@Date", abstractTourPlanMonth.Date, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@Day", abstractTourPlanMonth.Day, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@WorkAtId", abstractTourPlanMonth.WorkAtId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@WorkTypeId", abstractTourPlanMonth.WorkTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@WorkRouteIds", abstractTourPlanMonth.WorkRouteIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractTourPlanMonth.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlanMonth_Upsert, param, commandType: CommandType.StoredProcedure);
                TourPlanMonth = task.Read<SuccessResult<AbstractTourPlanMonth>>().SingleOrDefault();
                TourPlanMonth.Item = task.Read<TourPlanMonth>().SingleOrDefault();
            }

            return TourPlanMonth;
        }

        public override SuccessResult<AbstractTourPlanMonth> TourPlanMonth_ById(int Id)
        {
            SuccessResult<AbstractTourPlanMonth> TourPlanMonth = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlanMonth_ById, param, commandType: CommandType.StoredProcedure);
                TourPlanMonth = task.Read<SuccessResult<AbstractTourPlanMonth>>().SingleOrDefault();
                TourPlanMonth.Item = task.Read<TourPlanMonth>().SingleOrDefault();
            }

            return TourPlanMonth;
        }


    }
}
