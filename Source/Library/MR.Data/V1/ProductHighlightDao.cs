﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductHighlightDao : AbstractProductHighlightDao
    {

        public override SuccessResult<AbstractProductHighlight> ProductHighlight_Upsert(AbstractProductHighlight abstractProductHighlight)
        {
            SuccessResult<AbstractProductHighlight> ProductHighlight = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductHighlight.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProductHighlight.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractProductHighlight.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductHighlight.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProductHighlight.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHighlight_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductHighlight = task.Read<SuccessResult<AbstractProductHighlight>>().SingleOrDefault();
                ProductHighlight.Item = task.Read<ProductHighlight>().SingleOrDefault();
            }

            return ProductHighlight;
        }

        public override SuccessResult<AbstractProductHighlight> ProductHighlight_ById(long Id)
        {
            SuccessResult<AbstractProductHighlight> ProductHighlight = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHighlight_ById, param, commandType: CommandType.StoredProcedure);
                ProductHighlight = task.Read<SuccessResult<AbstractProductHighlight>>().SingleOrDefault();
                ProductHighlight.Item = task.Read<ProductHighlight>().SingleOrDefault();
            }

            return ProductHighlight;
        }

        public override PagedList<AbstractProductHighlight> ProductHighlight_All(PageParam pageParam, string search, long ProductId)
        {
            PagedList<AbstractProductHighlight> ProductHighlight = new PagedList<AbstractProductHighlight>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHighlight_All, param, commandType: CommandType.StoredProcedure);
                ProductHighlight.Values.AddRange(task.Read<ProductHighlight>());
                ProductHighlight.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductHighlight;
        }

        public override SuccessResult<AbstractProductHighlight> ProductHighlight_Delete(long Id)
        {
            SuccessResult<AbstractProductHighlight> ProductHighlight = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHighlight_Delete, param, commandType: CommandType.StoredProcedure);
                ProductHighlight = task.Read<SuccessResult<AbstractProductHighlight>>().SingleOrDefault();
                ProductHighlight.Item = task.Read<ProductHighlight>().SingleOrDefault();
            }

            return ProductHighlight;
        }

        public override SuccessResult<AbstractProductHighlight> ProductHighlight_DeleteByProductId(long Id)
        {
            SuccessResult<AbstractProductHighlight> ProductHighlight = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHighlight_DeleteByProductId, param, commandType: CommandType.StoredProcedure);
                ProductHighlight = task.Read<SuccessResult<AbstractProductHighlight>>().SingleOrDefault();
                ProductHighlight.Item = task.Read<ProductHighlight>().SingleOrDefault();
            }

            return ProductHighlight;
        }

    }
}
