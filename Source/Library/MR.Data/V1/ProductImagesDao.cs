﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductImagesDao : AbstractProductImagesDao
    {

        public override SuccessResult<AbstractProductImages> ProductImages_Upsert(AbstractProductImages abstractProductImages)
        {
            SuccessResult<AbstractProductImages> ProductImages = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductImages.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@URL", abstractProductImages.URL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractProductImages.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductImages.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProductImages.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductImages_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductImages = task.Read<SuccessResult<AbstractProductImages>>().SingleOrDefault();
                ProductImages.Item = task.Read<ProductImages>().SingleOrDefault();
            }

            return ProductImages;
        }

        public override SuccessResult<AbstractProductImages> ProductImages_ById(long Id)
        {
            SuccessResult<AbstractProductImages> ProductImages = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductImages_ById, param, commandType: CommandType.StoredProcedure);
                ProductImages = task.Read<SuccessResult<AbstractProductImages>>().SingleOrDefault();
                ProductImages.Item = task.Read<ProductImages>().SingleOrDefault();
            }

            return ProductImages;
        }

        public override PagedList<AbstractProductImages> ProductImages_All(PageParam pageParam, string search, long ProductId)
        {
            PagedList<AbstractProductImages> ProductImages = new PagedList<AbstractProductImages>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductImages_All, param, commandType: CommandType.StoredProcedure);
                ProductImages.Values.AddRange(task.Read<ProductImages>());
                ProductImages.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductImages;
        }

        public override SuccessResult<AbstractProductImages> ProductImages_Delete(long Id)
        {
            SuccessResult<AbstractProductImages> ProductImages = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductImages_Delete, param, commandType: CommandType.StoredProcedure);
                ProductImages = task.Read<SuccessResult<AbstractProductImages>>().SingleOrDefault();
                ProductImages.Item = task.Read<ProductImages>().SingleOrDefault();
            }

            return ProductImages;
        }



    }
}
