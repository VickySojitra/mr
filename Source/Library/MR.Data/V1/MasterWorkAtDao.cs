﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class MasterWorkAtDao : AbstractMasterWorkAtDao
    {

        

        public override PagedList<AbstractMasterWorkAt> MasterWorkAt_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterWorkAt> MasterWorkAt = new PagedList<AbstractMasterWorkAt>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterWorkAt_All, param, commandType: CommandType.StoredProcedure);
                MasterWorkAt.Values.AddRange(task.Read<MasterWorkAt>());
                MasterWorkAt.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterWorkAt;
        }

        



    }
}
