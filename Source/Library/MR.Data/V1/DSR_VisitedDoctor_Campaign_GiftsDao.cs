﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSR_VisitedDoctor_Campaign_GiftsDao : AbstractDSR_VisitedDoctor_Campaign_GiftsDao
    {

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_Upsert(AbstractDSR_VisitedDoctor_Campaign_Gifts abstractDSR_VisitedDoctor_Campaign_Gifts)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR_VisitedDoctor_Campaign_Gifts.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedDoctor_CampaignId", abstractDSR_VisitedDoctor_Campaign_Gifts.DSR_VisitedDoctor_CampaignId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@GiftId", abstractDSR_VisitedDoctor_Campaign_Gifts.GiftId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR_VisitedDoctor_Campaign_Gifts.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR_VisitedDoctor_Campaign_Gifts.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_Gifts_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign_Gifts = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts>>().SingleOrDefault();
                DSR_VisitedDoctor_Campaign_Gifts.Item = task.Read<DSR_VisitedDoctor_Campaign_Gifts>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Campaign_Gifts;
        }




        public override PagedList<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId(PageParam pageParam, long DSR_VisitedDoctor_CampaignId)
        {
            PagedList<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts = new PagedList<AbstractDSR_VisitedDoctor_Campaign_Gifts>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedDoctor_CampaignId", DSR_VisitedDoctor_CampaignId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_Gifts_ByDSR_VisitedDoctor_CampaignId, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign_Gifts.Values.AddRange(task.Read<DSR_VisitedDoctor_Campaign_Gifts>());
                DSR_VisitedDoctor_Campaign_Gifts.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR_VisitedDoctor_Campaign_Gifts;
        }


        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_ById(long Id)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_Gifts_ById, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign_Gifts = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts>>().SingleOrDefault();
                DSR_VisitedDoctor_Campaign_Gifts.Item = task.Read<DSR_VisitedDoctor_Campaign_Gifts>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Campaign_Gifts;
        }
        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts> DSR_VisitedDoctor_Campaign_Gifts = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_Gifts_Delete, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign_Gifts = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Campaign_Gifts>>().SingleOrDefault();
                DSR_VisitedDoctor_Campaign_Gifts.Item = task.Read<DSR_VisitedDoctor_Campaign_Gifts>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Campaign_Gifts;
        }



    }
}
