﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ChemistVsDoctorDao : AbstractChemistVsDoctorDao
    {
        public override PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_All(PageParam pageParam, string search, long DoctorId, long ChemistId)
        {
            PagedList<AbstractChemistVsDoctor> ChemistVsDoctor = new PagedList<AbstractChemistVsDoctor>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DoctorId", DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChemistVsDoctor_All, param, commandType: CommandType.StoredProcedure);
                ChemistVsDoctor.Values.AddRange(task.Read<ChemistVsDoctor>());
                ChemistVsDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ChemistVsDoctor;
        }

        public override PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            PagedList<AbstractChemistVsDoctor> ChemistVsDoctor = new PagedList<AbstractChemistVsDoctor>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChemistVsDoctor_ByDoctorId, param, commandType: CommandType.StoredProcedure);
                ChemistVsDoctor.Values.AddRange(task.Read<ChemistVsDoctor>());
                ChemistVsDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ChemistVsDoctor;
        }

        public override PagedList<AbstractChemistVsDoctor> ChemistVsDoctor_ByChemistId(PageParam pageParam, long ChemistId)
        {
            PagedList<AbstractChemistVsDoctor> ChemistVsDoctor = new PagedList<AbstractChemistVsDoctor>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChemistVsDoctor_ByChemistId, param, commandType: CommandType.StoredProcedure);
                ChemistVsDoctor.Values.AddRange(task.Read<ChemistVsDoctor>());
                ChemistVsDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ChemistVsDoctor;
        }

        public override SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChemistVsDoctor_Delete, param, commandType: CommandType.StoredProcedure);
                ChemistVsDoctor = task.Read<SuccessResult<AbstractChemistVsDoctor>>().SingleOrDefault();
                ChemistVsDoctor.Item = task.Read<ChemistVsDoctor>().SingleOrDefault();
            }

            return ChemistVsDoctor;
        }

        public override SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor_Upsert(AbstractChemistVsDoctor abstractChemistVsDoctor)
        {
            SuccessResult<AbstractChemistVsDoctor> ChemistVsDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractChemistVsDoctor.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", abstractChemistVsDoctor.DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId ", abstractChemistVsDoctor.ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractChemistVsDoctor.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractChemistVsDoctor.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChemistVsDoctor_Upsert, param, commandType: CommandType.StoredProcedure);
                ChemistVsDoctor = task.Read<SuccessResult<AbstractChemistVsDoctor>>().SingleOrDefault();
                ChemistVsDoctor.Item = task.Read<ChemistVsDoctor>().SingleOrDefault();
            }

            return ChemistVsDoctor;
        }
    }
}
