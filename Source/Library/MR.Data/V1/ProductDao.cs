﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductDao : AbstractProductDao
    {

        public override SuccessResult<AbstractProduct> Product_Upsert(AbstractProduct abstractProduct)
        {
            SuccessResult<AbstractProduct> Product = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProduct.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProduct.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CategoryId", abstractProduct.CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractProduct.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ProductTypeId", abstractProduct.ProductTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Quantity", abstractProduct.Quantity, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SIUnitesId", abstractProduct.SIUnitesId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ManufactureId", abstractProduct.ManufactureId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StoreBelowTemprature", abstractProduct.StoreBelowTemprature, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PrescriptionId", abstractProduct.PrescriptionId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NRV", abstractProduct.NRV, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractProduct.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Price", abstractProduct.Price, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@SellingPrice", abstractProduct.SellingPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProduct.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProduct.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Product_Upsert, param, commandType: CommandType.StoredProcedure);
                Product = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                Product.Item = task.Read<Product>().SingleOrDefault();
            }

            return Product;
        }

        public override SuccessResult<AbstractProduct> Product_ById(long Id)
        {
            SuccessResult<AbstractProduct> Product = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Product_ById, param, commandType: CommandType.StoredProcedure);
                Product = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                Product.Item = task.Read<Product>().SingleOrDefault();
            }

            return Product;
        }

        public override PagedList<AbstractProduct> Product_All(PageParam pageParam, string search, long CategoryId, long ProductTypeId, long SIUnitesId, long ManufactureId, long PrescriptionId)
        {
            PagedList<AbstractProduct> Product = new PagedList<AbstractProduct>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CategoryId", CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductTypeId", ProductTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SIUnitesId", SIUnitesId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ManufactureId", ManufactureId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PrescriptionId", PrescriptionId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Product_All, param, commandType: CommandType.StoredProcedure, commandTimeout:120);
                Product.Values.AddRange(task.Read<Product>());
                Product.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Product;
        }

        public override SuccessResult<AbstractProduct> Product_Delete(long Id)
        {
            SuccessResult<AbstractProduct> Product = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Product_Delete, param, commandType: CommandType.StoredProcedure);
                Product = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                Product.Item = task.Read<Product>().SingleOrDefault();
            }

            return Product;
        }



    }
}
