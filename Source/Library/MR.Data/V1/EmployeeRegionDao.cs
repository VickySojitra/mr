﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class EmployeeRegionDao : AbstractEmployeeRegionDao
    {

        public override SuccessResult<AbstractEmployeeRegion> EmployeeRegion_Upsert(AbstractEmployeeRegion abstractEmployeeRegion)
        {
            SuccessResult<AbstractEmployeeRegion> EmployeeRegion = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractEmployeeRegion.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractEmployeeRegion.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RegionIds", abstractEmployeeRegion.RegionIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractEmployeeRegion.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.EmployeeRegion_Upsert, param, commandType: CommandType.StoredProcedure);
                EmployeeRegion = task.Read<SuccessResult<AbstractEmployeeRegion>>().SingleOrDefault();
                EmployeeRegion.Item = task.Read<EmployeeRegion>().SingleOrDefault();
            }

            return EmployeeRegion;
        }

        public override PagedList<AbstractEmployeeRegion> EmployeeRegion_ByUserId(PageParam pageParam, string search, int UserId)
        {
            PagedList<AbstractEmployeeRegion> EmployeeRegion = new PagedList<AbstractEmployeeRegion>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.EmployeeRegion_ByUserId, param, commandType: CommandType.StoredProcedure);
                EmployeeRegion.Values.AddRange(task.Read<EmployeeRegion>());
                EmployeeRegion.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return EmployeeRegion;
        }


    }
}
