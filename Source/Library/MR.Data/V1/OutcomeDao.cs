﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class OutcomeDao : AbstractOutcomeDao
    {

        public override SuccessResult<AbstractOutcome> Outcome_Upsert(AbstractOutcome abstractOutcome)
        {
            SuccessResult<AbstractOutcome> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOutcome.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractOutcome.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOutcome.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractOutcome.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Outcome_Upsert, param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOutcome>>().SingleOrDefault();
                Outcome.Item = task.Read<Outcome>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractOutcome> Outcome_ById(long Id)
        {
            SuccessResult<AbstractOutcome> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Outcome_ById, param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOutcome>>().SingleOrDefault();
                Outcome.Item = task.Read<Outcome>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractOutcome> Outcome_All(PageParam pageParam, string search)
        {
            PagedList<AbstractOutcome> Outcome = new PagedList<AbstractOutcome>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Outcome_All, param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Outcome>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractOutcome> Outcome_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractOutcome> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Outcome_Delete, param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOutcome>>().SingleOrDefault();
                Outcome.Item = task.Read<Outcome>().SingleOrDefault();
            }

            return Outcome;
        }

        

    }
}
