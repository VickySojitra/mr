﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MR.Data.V1
{
    public class AddressDao : AbstractAddressDao
    {
        //public override PagedList<AbstractAddress> SelectAll(PageParam pageParam)
        //{
        //    PagedList<AbstractAddress> classes = new PagedList<AbstractAddress>();

        //    var param = new DynamicParameters();
        //    param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
        //    param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.AddressSelectAll, param, commandType: CommandType.StoredProcedure);
        //        classes.Values.AddRange(task.Read<AbstractAddress>());
        //        classes.TotalRecords = task.Read<long>().SingleOrDefault();
        //    }
        //    return classes;
        //}

        //public override PagedList<AbstractCountry> ContrySelectAll(PageParam pageParam, string search="")
        //{
        //    PagedList<AbstractCountry> classes = new PagedList<AbstractCountry>();

        //    var param = new DynamicParameters();
        //    param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
        //    param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
        //    param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.CountrySelectAll, param, commandType: CommandType.StoredProcedure);
        //        classes.Values.AddRange(task.Read<Country>());
        //        classes.TotalRecords = task.Read<long>().SingleOrDefault();
        //    }

        //    return classes;
        //}

        public override PagedList<AbstractState> StateSelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractState> classes = new PagedList<AbstractState>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.StateSelectAll, param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<State>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return classes;
        }

        public override PagedList<AbstractCity> CitySelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractCity> classes = new PagedList<AbstractCity>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CitySelectAll, param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<City>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return classes;
        }

        public override PagedList<AbstractHeadquarter> HeadquarterSelectAll(PageParam pageParam, string search = "", int RouteType = 0, int RegionId = 0)
        {
            PagedList<AbstractHeadquarter> classes = new PagedList<AbstractHeadquarter>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RouteType", RouteType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RegionId", RegionId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("HeadquarterSelectAll", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<Headquarter>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return classes;
        }

        public override PagedList<AbstractRegion> RegionSelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractRegion> classes = new PagedList<AbstractRegion>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("RegionSelectAll", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<Region>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return classes;
        }

        //public override PagedList<AbstractAddress> GetAddressByEmpId(int EmpId)
        //{
        //    PagedList<AbstractAddress> classes = new PagedList<AbstractAddress>();

        //    var param = new DynamicParameters();
        //    param.Add("@CustomerId", EmpId, dbType: DbType.Int32, direction: ParameterDirection.Input);


        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.GetAddressByCustId, param, commandType: CommandType.StoredProcedure);
        //        classes.Values.AddRange(task.Read<Address>());
        //        classes.TotalRecords = task.Read<long>().SingleOrDefault();
        //    }
        //    return classes;
        //}

        public override SuccessResult<AbstractAddress> Select(int id)
        {
            SuccessResult<AbstractAddress> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("AddressSelectById", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                address.Item = task.Read<Address>().SingleOrDefault();
            }
            return address;
        }

        public override PagedList<AbstractCity> CitySelectByStateId(int stateId)
        {
            PagedList<AbstractCity> address = new PagedList<AbstractCity>();
            var param = new DynamicParameters();
            param.Add("@StateId", stateId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CitySelectByState, param, commandType: CommandType.StoredProcedure);
                address.Values.AddRange(task.Read<City>());
                address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return address;
        }

        //public override PagedList<AbstractState> StateSelectByCountryId(int countryId)
        //{
        //    PagedList<AbstractState> address = new PagedList<AbstractState>();
        //    var param = new DynamicParameters();
        //    param.Add("@CountryId", countryId, dbType: DbType.Int32, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.StateSelectByCountry, param, commandType: CommandType.StoredProcedure);
        //        address.Values.AddRange(task.Read<State>());
        //        address.TotalRecords = task.Read<long>().SingleOrDefault();
        //    }
        //    return address;
        //}

        public override SuccessResult<AbstractAddress> InsertUpdateAddress(AbstractAddress abstractAddress)
        {
            SuccessResult<AbstractAddress> address = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAddress.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Address1", abstractAddress.Address1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Address2", abstractAddress.Address2, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Pincode", abstractAddress.Pincode, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Headquarter", abstractAddress.Headquarter, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Region", abstractAddress.Region, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@State", abstractAddress.State, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@City", abstractAddress.City, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractAddress.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractAddress.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddressUpsert, param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                address.Item = task.Read<Address>().SingleOrDefault();
            }

            return address;
        }

        public override bool Delete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.AddressDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override SuccessResult<AbstractCity> InsertUpdateCity(AbstractCity abstractCity)
        {
            SuccessResult<AbstractCity> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractCity.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractCity.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractCity.StateId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCity.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCity.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("CityUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractCity>>().SingleOrDefault();
                address.Item = task.Read<City>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractState> InsertUpdateState(AbstractState abstractState)
        {
            SuccessResult<AbstractState> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractState.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractState.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractState.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractState.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("StateUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractState>>().SingleOrDefault();
                address.Item = task.Read<State>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractHeadquarter> InsertUpdateHeadquarter(AbstractHeadquarter abstractHeadquarter)
        {
            SuccessResult<AbstractHeadquarter> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractHeadquarter.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractHeadquarter.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractHeadquarter.IsActive, DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ManagerId", abstractHeadquarter.ManagerId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RouteType", abstractHeadquarter.RouteType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RegionId", abstractHeadquarter.RegionId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractHeadquarter.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractHeadquarter.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("HeadquarterUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractHeadquarter>>().SingleOrDefault();
                address.Item = task.Read<Headquarter>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractRegion> InsertUpdateRegion(AbstractRegion abstractRegion)
        {
            SuccessResult<AbstractRegion> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractRegion.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractRegion.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractRegion.IsActive, DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ManagerId", abstractRegion.ManagerId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractRegion.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractRegion.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("RegionUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractRegion>>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractRegion> Region_ById(int Id)
        {
            SuccessResult<AbstractRegion> address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Region_ById", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractRegion>>().SingleOrDefault();
                address.Item = task.Read<Region>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractHeadquarter> Headquarter_ById(int Id)
        {
            SuccessResult<AbstractHeadquarter> address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Headquarter_ById, param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractHeadquarter>>().SingleOrDefault();
                address.Item = task.Read<Headquarter>().SingleOrDefault();
            }
            return address;
        }

        public override SuccessResult<AbstractCity> City_ById(int Id)
        {
            SuccessResult<AbstractCity> address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("City_ById", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractCity>>().SingleOrDefault();
                address.Item = task.Read<City>().SingleOrDefault();
            }
            return address;
        }

        public override SuccessResult<AbstractState> State_ById(int Id)
        {
            SuccessResult<AbstractState> address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("State_ById", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractState>>().SingleOrDefault();
                address.Item = task.Read<State>().SingleOrDefault();
            }
            return address;
        }
    }
}
