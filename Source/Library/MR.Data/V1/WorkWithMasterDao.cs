﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class WorkWithMasterDao : AbstractWorkWithMasterDao
    {

        public override SuccessResult<AbstractWorkWithMaster> WorkWith_Upsert(AbstractWorkWithMaster abstractWorkType)
        {
            SuccessResult<AbstractWorkWithMaster> VisitType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractWorkType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractWorkType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractWorkType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractWorkType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("WorkWithMaster_Upsert", param, commandType: CommandType.StoredProcedure);
                VisitType = task.Read<SuccessResult<AbstractWorkWithMaster>>().SingleOrDefault();
                VisitType.Item = task.Read<WorkWithMaster>().SingleOrDefault();
            }

            return VisitType;
        }

        public override SuccessResult<AbstractWorkWithMaster> WorkWith_ById(long Id)
        {
            SuccessResult<AbstractWorkWithMaster> VisitType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("WorkWithMaster_ById", param, commandType: CommandType.StoredProcedure);
                VisitType = task.Read<SuccessResult<AbstractWorkWithMaster>>().SingleOrDefault();
                VisitType.Item = task.Read<WorkWithMaster>().SingleOrDefault();
            }

            return VisitType;
        }

        public override PagedList<AbstractWorkWithMaster> WorkWith_All(PageParam pageParam, string search)
        {
            PagedList<AbstractWorkWithMaster> VisitType = new PagedList<AbstractWorkWithMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("WorkWithMaster_All", param, commandType: CommandType.StoredProcedure);
                VisitType.Values.AddRange(task.Read<WorkWithMaster>());
                VisitType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return VisitType;
        }

        public override SuccessResult<AbstractWorkWithMaster> WorkWith_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractWorkWithMaster> VisitType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("WorkWithMaster_Delete", param, commandType: CommandType.StoredProcedure);
                VisitType = task.Read<SuccessResult<AbstractWorkWithMaster>>().SingleOrDefault();
                VisitType.Item = task.Read<WorkWithMaster>().SingleOrDefault();
            }

            return VisitType;
        }

             

    }
}
