﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductTypeDao : AbstractProductTypeDao
    {
        public override PagedList<AbstractProductType> ProductType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractProductType> ProductType = new PagedList<AbstractProductType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductType_All, param, commandType: CommandType.StoredProcedure);
                ProductType.Values.AddRange(task.Read<ProductType>());
                ProductType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductType;
        }

        public override SuccessResult<AbstractProductType> ProductType_ById(long Id)
        {
            SuccessResult<AbstractProductType> ProductType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductType_ById, param, commandType: CommandType.StoredProcedure);
                ProductType = task.Read<SuccessResult<AbstractProductType>>().SingleOrDefault();
                ProductType.Item = task.Read<ProductType>().SingleOrDefault();
            }

            return ProductType;
        }

        public override SuccessResult<AbstractProductType> ProductType_Delete(long Id)
        {
            SuccessResult<AbstractProductType> ProductType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductType_Delete, param, commandType: CommandType.StoredProcedure);
                ProductType = task.Read<SuccessResult<AbstractProductType>>().SingleOrDefault();
                ProductType.Item = task.Read<ProductType>().SingleOrDefault();
            }

            return ProductType;
        }

        public override SuccessResult<AbstractProductType> ProductType_Upsert(AbstractProductType abstractProductType)
        {
            SuccessResult<AbstractProductType> ProductType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProductType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractProductType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductType_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductType = task.Read<SuccessResult<AbstractProductType>>().SingleOrDefault();
                ProductType.Item = task.Read<ProductType>().SingleOrDefault();
            }

            return ProductType;
        }
    }
}
