﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MR.Data.V1
{
	public class DoctorDao : AbstractDoctorDao
	{
		public override PagedList<AbstractDoctor> SelectAll(PageParam pageParam, string search = "",int TourPlanMonthId = 0)
		{
			PagedList<AbstractDoctor> classes = new PagedList<AbstractDoctor>();

			var param = new DynamicParameters();
			param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
			param.Add("@TourPlanMonthId", TourPlanMonthId, dbType: DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("DoctorSelectAll", param, commandType: CommandType.StoredProcedure);
				classes.Values.AddRange(task.Read<Doctor>());
				classes.TotalRecords = task.Read<long>().SingleOrDefault();
			}
			return classes;
		}

		public override SuccessResult<AbstractDoctor> Select(int id)
		{
			SuccessResult<AbstractDoctor> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("DoctorSelect", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractDoctor>>().SingleOrDefault();
				address.Item = task.Read<Doctor>().SingleOrDefault();
			}
			return address;
		}

		public override SuccessResult<AbstractDoctor> InsertUpdate(AbstractDoctor abstractDoctor)
		{
			SuccessResult<AbstractDoctor> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", abstractDoctor.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@FirstName", abstractDoctor.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
			param.Add("@LastName", abstractDoctor.LastName, DbType.String, direction: ParameterDirection.Input);
			param.Add("@IsActive", abstractDoctor.IsActive, DbType.Boolean, direction: ParameterDirection.Input);
			param.Add("@Gender", (int)abstractDoctor.Gender, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Mobile", abstractDoctor.Mobile, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Phone", abstractDoctor.Phone, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Birthdate", abstractDoctor.Birthdate, DbType.DateTime, direction: ParameterDirection.Input);
			param.Add("@Anniversary", abstractDoctor.Anniversary, DbType.DateTime, direction: ParameterDirection.Input);
			param.Add("@Photo", abstractDoctor.Photo, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Specialization", abstractDoctor.Specialization, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Qualification", abstractDoctor.Qualification, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Category", abstractDoctor.Category, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@WebSite", abstractDoctor.WebSite, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Address", abstractDoctor.Address, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@BuisnessPotential", abstractDoctor.BuisnessPotential, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Remarks", abstractDoctor.Remarks, DbType.String, direction: ParameterDirection.Input);
			param.Add("@CreatedBy", abstractDoctor.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@UpdatedBy", abstractDoctor.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("DoctorUpsert", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractDoctor>>().SingleOrDefault();
				address.Item = task.Read<Doctor>().SingleOrDefault();
			}

			return address;
		}

		public override PagedList<AbstractDoctorCategory> SelectAllDoctorCategory(PageParam pageParam, string search = "")
		{
			PagedList<AbstractDoctorCategory> classes = new PagedList<AbstractDoctorCategory>();

			var param = new DynamicParameters();
			param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("DoctorCategorySelectAll", param, commandType: CommandType.StoredProcedure);
				classes.Values.AddRange(task.Read<DoctorCategory>());
				classes.TotalRecords = task.Read<long>().SingleOrDefault();
			}
			return classes;
		}

		public override SuccessResult<AbstractDoctorCategory> InsertUpdateDoctorCategory(AbstractDoctorCategory abstractCategory)
		{
			SuccessResult<AbstractDoctorCategory> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", abstractCategory.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Name", abstractCategory.Name, DbType.String, direction: ParameterDirection.Input);
			param.Add("@CreatedBy", abstractCategory.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@UpdatedBy", abstractCategory.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("DoctorCategoryUpsert", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractDoctorCategory>>().SingleOrDefault();
				address.Item = task.Read<DoctorCategory>().SingleOrDefault();
			}

			return address;
		}

		public override SuccessResult<AbstractDoctorCategory> DoctorCategory_ById(int id)
		{
			SuccessResult<AbstractDoctorCategory> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);
			
			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("DoctorCategory_ById", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractDoctorCategory>>().SingleOrDefault();
				address.Item = task.Read<DoctorCategory>().SingleOrDefault();
			}

			return address;
		}
	}
}
