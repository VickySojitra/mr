﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductPrescriptionDao : AbstractProductPrescriptionDao
    {

        public override SuccessResult<AbstractProductPrescription> ProductPrescription_Upsert(AbstractProductPrescription abstractProductPrescription)
        {
            SuccessResult<AbstractProductPrescription> ProductPrescription = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductPrescription.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProductPrescription.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductPrescription.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProductPrescription.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductPrescription_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductPrescription = task.Read<SuccessResult<AbstractProductPrescription>>().SingleOrDefault();
                ProductPrescription.Item = task.Read<ProductPrescription>().SingleOrDefault();
            }

            return ProductPrescription;
        }

        public override SuccessResult<AbstractProductPrescription> ProductPrescription_ById(long Id)
        {
            SuccessResult<AbstractProductPrescription> ProductPrescription = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductPrescription_ById, param, commandType: CommandType.StoredProcedure);
                ProductPrescription = task.Read<SuccessResult<AbstractProductPrescription>>().SingleOrDefault();
                ProductPrescription.Item = task.Read<ProductPrescription>().SingleOrDefault();
            }

            return ProductPrescription;
        }

        public override PagedList<AbstractProductPrescription> ProductPrescription_All(PageParam pageParam, string search)
        {
            PagedList<AbstractProductPrescription> ProductPrescription = new PagedList<AbstractProductPrescription>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductPrescription_All, param, commandType: CommandType.StoredProcedure);
                ProductPrescription.Values.AddRange(task.Read<ProductPrescription>());
                ProductPrescription.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductPrescription;
        }

        public override SuccessResult<AbstractProductPrescription> ProductPrescription_Delete(long Id)
        {
            SuccessResult<AbstractProductPrescription> ProductPrescription = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductPrescription_Delete, param, commandType: CommandType.StoredProcedure);
                ProductPrescription = task.Read<SuccessResult<AbstractProductPrescription>>().SingleOrDefault();
                ProductPrescription.Item = task.Read<ProductPrescription>().SingleOrDefault();
            }

            return ProductPrescription;
        }



    }
}
