﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class TourPlanDao : AbstractTourPlanDao
    {
        public override PagedList<AbstractTourPlan> TourPlan_All(PageParam pageParam, string search)
        {
            PagedList<AbstractTourPlan> TourPlan = new PagedList<AbstractTourPlan>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsSubmitted", 0, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Month", "", dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlan_All, param, commandType: CommandType.StoredProcedure);
                TourPlan.Values.AddRange(task.Read<TourPlan>());
                TourPlan.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TourPlan;
        }

        public override SuccessResult<AbstractTourPlan> TourPlan_ById(int Id)
        {
            SuccessResult<AbstractTourPlan> TourPlan = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlan_ById, param, commandType: CommandType.StoredProcedure);
                TourPlan = task.Read<SuccessResult<AbstractTourPlan>>().SingleOrDefault();
                TourPlan.Item = task.Read<TourPlan>().SingleOrDefault();
            }

            return TourPlan;
        }


        public override SuccessResult<AbstractTourPlan> TourPlan_IsSubmitted(int Id, bool IsSubmitted)
        {
            SuccessResult<AbstractTourPlan> TourPlan = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsSubmitted", IsSubmitted, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlan_IsSubmitted, param, commandType: CommandType.StoredProcedure);
                TourPlan = task.Read<SuccessResult<AbstractTourPlan>>().SingleOrDefault();
                TourPlan.Item = task.Read<TourPlan>().SingleOrDefault();
            }

            return TourPlan;
        }


        public override SuccessResult<AbstractTourPlan> TourPlan_Upsert(AbstractTourPlan abstractTourPlan)
        {
            SuccessResult<AbstractTourPlan> TourPlan = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTourPlan.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Month", abstractTourPlan.Month, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SubmittedDate", abstractTourPlan.SubmittedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastDate", abstractTourPlan.LastDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractTourPlan.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTourPlan.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", abstractTourPlan.EmployeeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TourPlan_Upsert, param, commandType: CommandType.StoredProcedure);
                TourPlan = task.Read<SuccessResult<AbstractTourPlan>>().SingleOrDefault();
                TourPlan.Item = task.Read<TourPlan>().SingleOrDefault();
            }

            return TourPlan;
        }
    }
}
