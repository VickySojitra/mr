﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class LeaveDao : AbstractLeaveDao
    {

        public override SuccessResult<AbstractLeave> Leave_Upsert(AbstractLeave abstractLeave)
        {
            SuccessResult<AbstractLeave> Leave = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLeave.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", abstractLeave.EmployeeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LeaveTypeId", abstractLeave.LeaveTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DayTypeId", abstractLeave.DayTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromDate", abstractLeave.FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", abstractLeave.ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LeaveReason", abstractLeave.LeaveReason, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLeave.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLeave.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Leave_Upsert, param, commandType: CommandType.StoredProcedure);
                Leave = task.Read<SuccessResult<AbstractLeave>>().SingleOrDefault();
                Leave.Item = task.Read<Leave>().SingleOrDefault();
            }

            return Leave;
        }




        public override PagedList<AbstractLeave> Leave_ByEmployeeId(PageParam pageParam, long EmployeeId)
        {
            PagedList<AbstractLeave> Leave = new PagedList<AbstractLeave>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", EmployeeId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Leave_ByEmployeeId, param, commandType: CommandType.StoredProcedure);
                Leave.Values.AddRange(task.Read<Leave>());
                Leave.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Leave;
        }


        public override SuccessResult<AbstractLeave> Leave_ById(long Id)
        {
            SuccessResult<AbstractLeave> Leave = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Leave_ById, param, commandType: CommandType.StoredProcedure);
                Leave = task.Read<SuccessResult<AbstractLeave>>().SingleOrDefault();
                Leave.Item = task.Read<Leave>().SingleOrDefault();
            }

            return Leave;
        }
        


    }
}
