﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class JointWorkingDao : AbstractJointWorkingDao
    {
        public override PagedList<AbstractJointWorking> JointWorking_All(PageParam pageParam, string search, int TourPlanMonthId, int JointWorkingId)
        {
            PagedList<AbstractJointWorking> JointWorking = new PagedList<AbstractJointWorking>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TourPlanMonthId", TourPlanMonthId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JointWorkingId", JointWorkingId, dbType: DbType.Int32, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JointWorking_All, param, commandType: CommandType.StoredProcedure);
                JointWorking.Values.AddRange(task.Read<JointWorking>());
                JointWorking.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JointWorking;
        }

        public override SuccessResult<AbstractJointWorking> JointWorking_Upsert(AbstractJointWorking abstractJointWorking)
        {
            SuccessResult<AbstractJointWorking> JointWorking = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJointWorking.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TourPlanMonthId", abstractJointWorking.TourPlanMonthId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DoctorAndChemistId", abstractJointWorking.DoctorAndChemistId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@EmployeeIds", abstractJointWorking.EmployeeIds, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractJointWorking.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JointWorking_Upsert, param, commandType: CommandType.StoredProcedure);
                JointWorking = task.Read<SuccessResult<AbstractJointWorking>>().SingleOrDefault();
                JointWorking.Item = task.Read<JointWorking>().SingleOrDefault();
            }

            return JointWorking;
        }
    }
}
