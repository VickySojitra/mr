﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class PrescriberDao : AbstractPrescriberDao
    {
        public override SuccessResult<AbstractPrescriber> Prescriber_ById(long Id)
        {
            SuccessResult<AbstractPrescriber> Prescriber = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Prescriber_ById, param, commandType: CommandType.StoredProcedure);
                Prescriber = task.Read<SuccessResult<AbstractPrescriber>>().SingleOrDefault();
                Prescriber.Item = task.Read<Prescriber>().SingleOrDefault();
            }

            return Prescriber;
        }

        public override PagedList<AbstractPrescriber> Prescriber_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            PagedList<AbstractPrescriber> Prescriber = new PagedList<AbstractPrescriber>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Prescriber_ByDoctorId, param, commandType: CommandType.StoredProcedure);
                Prescriber.Values.AddRange(task.Read<Prescriber>());
                Prescriber.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Prescriber;
        }

        public override SuccessResult<AbstractPrescriber> Prescriber_Upsert(AbstractPrescriber abstractPrescriber)
        {
            SuccessResult<AbstractPrescriber> Prescriber = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPrescriber.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DoctorId", abstractPrescriber.DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", abstractPrescriber.EmployeeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPrescriber.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractPrescriber.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Prescriber_Upsert, param, commandType: CommandType.StoredProcedure);
                Prescriber = task.Read<SuccessResult<AbstractPrescriber>>().SingleOrDefault();
                Prescriber.Item = task.Read<Prescriber>().SingleOrDefault();
            }

            return Prescriber;
        }
    }
}
