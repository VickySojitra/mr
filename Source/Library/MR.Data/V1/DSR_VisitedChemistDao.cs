﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSR_VisitedChemistDao : AbstractDSR_VisitedChemistDao
    {

        public override SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_Upsert(AbstractDSR_VisitedChemist abstractDSR_VisitedChemist)
        {
            SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR_VisitedChemist.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSRId", abstractDSR_VisitedChemist.DSRId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", abstractDSR_VisitedChemist.ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallInId", abstractDSR_VisitedChemist.CallInId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OutcomeId", abstractDSR_VisitedChemist.OutcomeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@WorkWithId", abstractDSR_VisitedChemist.WorkWithId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Remarks", abstractDSR_VisitedChemist.Remarks, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR_VisitedChemist.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR_VisitedChemist.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist = task.Read<SuccessResult<AbstractDSR_VisitedChemist>>().SingleOrDefault();
                DSR_VisitedChemist.Item = task.Read<DSR_VisitedChemist>().SingleOrDefault();
            }

            return DSR_VisitedChemist;
        }

        public override SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_ById(long Id)
        {
            SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_ById, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist = task.Read<SuccessResult<AbstractDSR_VisitedChemist>>().SingleOrDefault();
                DSR_VisitedChemist.Item = task.Read<DSR_VisitedChemist>().SingleOrDefault();
            }

            return DSR_VisitedChemist;
        }

        public override PagedList<AbstractDSR_VisitedChemist> DSR_VisitedChemist_ByDSRId(PageParam pageParam, long DSRId)
        {
            PagedList<AbstractDSR_VisitedChemist> DSR_VisitedChemist = new PagedList<AbstractDSR_VisitedChemist>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSRId", DSRId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_ByDSRId, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist.Values.AddRange(task.Read<DSR_VisitedChemist>());
                DSR_VisitedChemist.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR_VisitedChemist;
        }

        public override SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist_Delete(long Id)
        {
            SuccessResult<AbstractDSR_VisitedChemist> DSR_VisitedChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_Delete, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist = task.Read<SuccessResult<AbstractDSR_VisitedChemist>>().SingleOrDefault();
                DSR_VisitedChemist.Item = task.Read<DSR_VisitedChemist>().SingleOrDefault();
            }

            return DSR_VisitedChemist;
        }



    }
}
