﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DoctorAndChemistDao : AbstractDoctorAndChemistDao
    {
        public override PagedList<AbstractDoctorAndChemist> DoctorAndChemist_All(PageParam pageParam, string search, int TourPlanMonthId, int EmployeeId)
        {
            PagedList<AbstractDoctorAndChemist> DoctorAndChemist = new PagedList<AbstractDoctorAndChemist>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TourPlanMonthId", TourPlanMonthId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", EmployeeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorAndChemist_All, param, commandType: CommandType.StoredProcedure);
                DoctorAndChemist.Values.AddRange(task.Read<DoctorAndChemist>());
                DoctorAndChemist.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DoctorAndChemist;
        }

        public override SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorAndChemist_Delete, param, commandType: CommandType.StoredProcedure);
                DoctorAndChemist = task.Read<SuccessResult<AbstractDoctorAndChemist>>().SingleOrDefault();
                DoctorAndChemist.Item = task.Read<DoctorAndChemist>().SingleOrDefault();
            }

            return DoctorAndChemist;
        }

        public override SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist_Upsert(AbstractDoctorAndChemist abstractDoctorAndChemist)
        {
            SuccessResult<AbstractDoctorAndChemist> DoctorAndChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDoctorAndChemist.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TourPlanMonthId", abstractDoctorAndChemist.TourPlanMonthId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DoctorIds", abstractDoctorAndChemist.DoctorIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ChemistIds", abstractDoctorAndChemist.ChemistIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDoctorAndChemist.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractDoctorAndChemist.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", abstractDoctorAndChemist.EmployeeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorAndChemist_Upsert, param, commandType: CommandType.StoredProcedure);
                DoctorAndChemist = task.Read<SuccessResult<AbstractDoctorAndChemist>>().SingleOrDefault();
                DoctorAndChemist.Item = task.Read<DoctorAndChemist>().SingleOrDefault();
            }

            return DoctorAndChemist;
        }
    }
}
