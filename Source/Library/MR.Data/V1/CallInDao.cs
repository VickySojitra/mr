﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class CallInDao : AbstractCallInDao
    {
        public override PagedList<AbstractCallIn> CallIn_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCallIn> CallIn = new PagedList<AbstractCallIn>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallIn_All, param, commandType: CommandType.StoredProcedure);
                CallIn.Values.AddRange(task.Read<CallIn>());
                CallIn.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CallIn;
        }

        public override SuccessResult<AbstractCallIn> CallIn_ById(long Id)
        {
            SuccessResult<AbstractCallIn> CallIn = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallIn_ById, param, commandType: CommandType.StoredProcedure);
                CallIn = task.Read<SuccessResult<AbstractCallIn>>().SingleOrDefault();
                CallIn.Item = task.Read<CallIn>().SingleOrDefault();
            }

            return CallIn;
        }

        public override SuccessResult<AbstractCallIn> CallIn_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractCallIn> CallIn = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallIn_Delete, param, commandType: CommandType.StoredProcedure);
                CallIn = task.Read<SuccessResult<AbstractCallIn>>().SingleOrDefault();
                CallIn.Item = task.Read<CallIn>().SingleOrDefault();
            }

            return CallIn;
        }

        public override SuccessResult<AbstractCallIn> CallIn_Upsert(AbstractCallIn abstractCallIn)
        {
            SuccessResult<AbstractCallIn> CallIn = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCallIn.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractCallIn.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCallIn.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractCallIn.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy ", abstractCallIn.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallIn_Upsert, param, commandType: CommandType.StoredProcedure);
                CallIn = task.Read<SuccessResult<AbstractCallIn>>().SingleOrDefault();
                CallIn.Item = task.Read<CallIn>().SingleOrDefault();
            }

            return CallIn;
        }
    }
}
