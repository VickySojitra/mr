﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class WorkTypeDao : AbstractWorkTypeDao
    {

        public override SuccessResult<AbstractWorkType> WorkType_Upsert(AbstractWorkType abstractWorkType)
        {
            SuccessResult<AbstractWorkType> WorkType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractWorkType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractWorkType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractWorkType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractWorkType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.WorkType_Upsert, param, commandType: CommandType.StoredProcedure);
                WorkType = task.Read<SuccessResult<AbstractWorkType>>().SingleOrDefault();
                WorkType.Item = task.Read<WorkType>().SingleOrDefault();
            }

            return WorkType;
        }

        public override SuccessResult<AbstractWorkType> WorkType_ById(long Id)
        {
            SuccessResult<AbstractWorkType> WorkType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.WorkType_ById, param, commandType: CommandType.StoredProcedure);
                WorkType = task.Read<SuccessResult<AbstractWorkType>>().SingleOrDefault();
                WorkType.Item = task.Read<WorkType>().SingleOrDefault();
            }

            return WorkType;
        }

        public override PagedList<AbstractWorkType> WorkType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractWorkType> WorkType = new PagedList<AbstractWorkType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.WorkType_All, param, commandType: CommandType.StoredProcedure);
                WorkType.Values.AddRange(task.Read<WorkType>());
                WorkType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return WorkType;
        }

        public override SuccessResult<AbstractWorkType> WorkType_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractWorkType> WorkType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.WorkType_Delete, param, commandType: CommandType.StoredProcedure);
                WorkType = task.Read<SuccessResult<AbstractWorkType>>().SingleOrDefault();
                WorkType.Item = task.Read<WorkType>().SingleOrDefault();
            }

            return WorkType;
        }



    }
}
