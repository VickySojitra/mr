﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class QuantitySIUnitesDao : AbstractQuantitySIUnitesDao
    {
        public override PagedList<AbstractQuantitySIUnites> QuantitySIUnites_All(PageParam pageParam, string search)
        {
            PagedList<AbstractQuantitySIUnites> QuantitySIUnites = new PagedList<AbstractQuantitySIUnites>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.QuantitySIUnites_All, param, commandType: CommandType.StoredProcedure);
                QuantitySIUnites.Values.AddRange(task.Read<QuantitySIUnites>());
                QuantitySIUnites.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return QuantitySIUnites;
        }

        public override SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites_ById(long Id)
        {
            SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.QuantitySIUnites_ById, param, commandType: CommandType.StoredProcedure);
                QuantitySIUnites = task.Read<SuccessResult<AbstractQuantitySIUnites>>().SingleOrDefault();
                QuantitySIUnites.Item = task.Read<QuantitySIUnites>().SingleOrDefault();
            }

            return QuantitySIUnites;
        }


        public override SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites_Upsert(AbstractQuantitySIUnites abstractQuantitySIUnites)
        {
            SuccessResult<AbstractQuantitySIUnites> QuantitySIUnites = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractQuantitySIUnites.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractQuantitySIUnites.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractQuantitySIUnites.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractQuantitySIUnites.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.QuantitySIUnites_Upsert, param, commandType: CommandType.StoredProcedure);
                QuantitySIUnites = task.Read<SuccessResult<AbstractQuantitySIUnites>>().SingleOrDefault();
                QuantitySIUnites.Item = task.Read<QuantitySIUnites>().SingleOrDefault();
            }

            return QuantitySIUnites;
        }
    }
}
