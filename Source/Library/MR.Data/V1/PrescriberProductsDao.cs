﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class PrescriberProductsDao : AbstractPrescriberProductsDao
    {
        public override SuccessResult<AbstractPrescriberProducts> PrescriberProducts_ById(long Id)
        {
            SuccessResult<AbstractPrescriberProducts> PrescriberProducts = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PrescriberProducts_ById, param, commandType: CommandType.StoredProcedure);
                PrescriberProducts = task.Read<SuccessResult<AbstractPrescriberProducts>>().SingleOrDefault();
                PrescriberProducts.Item = task.Read<PrescriberProducts>().SingleOrDefault();
            }

            return PrescriberProducts;
        }

        public override PagedList<AbstractPrescriberProducts> PrescriberProducts_ByPrescriberId(PageParam pageParam, long PrescriberId)
        {
            PagedList<AbstractPrescriberProducts> PrescriberProducts = new PagedList<AbstractPrescriberProducts>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PrescriberId", PrescriberId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PrescriberProducts_ByPrescriberId, param, commandType: CommandType.StoredProcedure);
                PrescriberProducts.Values.AddRange(task.Read<PrescriberProducts>());
                PrescriberProducts.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PrescriberProducts;
        }

        public override SuccessResult<AbstractPrescriberProducts> PrescriberProducts_Upsert(AbstractPrescriberProducts abstractPrescriberProducts)
        {
            SuccessResult<AbstractPrescriberProducts> PrescriberProducts = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPrescriberProducts.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PrescriberId", abstractPrescriberProducts.PrescriberId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractPrescriberProducts.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Price", abstractPrescriberProducts.Price, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Qty", abstractPrescriberProducts.Qty, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TotalBusiness", abstractPrescriberProducts.TotalBusiness, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPrescriberProducts.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractPrescriberProducts.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PrescriberProducts_Upsert, param, commandType: CommandType.StoredProcedure);
                PrescriberProducts = task.Read<SuccessResult<AbstractPrescriberProducts>>().SingleOrDefault();
                PrescriberProducts.Item = task.Read<PrescriberProducts>().SingleOrDefault();
            }

            return PrescriberProducts;
        }
    }
}
