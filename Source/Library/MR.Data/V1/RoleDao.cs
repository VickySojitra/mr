﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MR.Data.V1
{
	public class RoleDao : AbstractRoleDao
	{
		public override PagedList<AbstractRole> SelectAll(PageParam pageParam, string search = "")
		{
			PagedList<AbstractRole> classes = new PagedList<AbstractRole>();

			var param = new DynamicParameters();
			param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("RoleSelectAll", param, commandType: CommandType.StoredProcedure);
				classes.Values.AddRange(task.Read<Role>());
				classes.TotalRecords = task.Read<long>().SingleOrDefault();
			}

			return classes;
		}

		public override SuccessResult<AbstractRole> InsertUpdate(AbstractRole abstractRole)
		{
			SuccessResult<AbstractRole> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", abstractRole.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Name", abstractRole.Name, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Details", abstractRole.Details, DbType.String, direction: ParameterDirection.Input);
			param.Add("@CreatedBy", abstractRole.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@UpdatedBy", abstractRole.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("RoleUpsert", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractRole>>().SingleOrDefault();
				address.Item = task.Read<Role>().SingleOrDefault();
			}

			return address;
		}
	}
}
