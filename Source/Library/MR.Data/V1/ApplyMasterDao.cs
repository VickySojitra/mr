﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ApplyMasterDao : AbstractApplyMasterDao
    {

        public override SuccessResult<AbstractApplyMaster> ApplyMaster_Upsert(AbstractApplyMaster abstractApplyMaster)
        {
            SuccessResult<AbstractApplyMaster> ApplyMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractApplyMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractApplyMaster.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractApplyMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractApplyMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ApplyMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                ApplyMaster = task.Read<SuccessResult<AbstractApplyMaster>>().SingleOrDefault();
                ApplyMaster.Item = task.Read<ApplyMaster>().SingleOrDefault();
            }

            return ApplyMaster;
        }

        public override SuccessResult<AbstractApplyMaster> ApplyMaster_ById(long Id)
        {
            SuccessResult<AbstractApplyMaster> ApplyMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ApplyMaster_ById, param, commandType: CommandType.StoredProcedure);
                ApplyMaster = task.Read<SuccessResult<AbstractApplyMaster>>().SingleOrDefault();
                ApplyMaster.Item = task.Read<ApplyMaster>().SingleOrDefault();
            }

            return ApplyMaster;
        }

        public override PagedList<AbstractApplyMaster> ApplyMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractApplyMaster> ApplyMaster = new PagedList<AbstractApplyMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ApplyMaster_All, param, commandType: CommandType.StoredProcedure);
                ApplyMaster.Values.AddRange(task.Read<ApplyMaster>());
                ApplyMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ApplyMaster;
        }

        public override SuccessResult<AbstractApplyMaster> ApplyMaster_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractApplyMaster> ApplyMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ApplyMaster_Delete, param, commandType: CommandType.StoredProcedure);
                ApplyMaster = task.Read<SuccessResult<AbstractApplyMaster>>().SingleOrDefault();
                ApplyMaster.Item = task.Read<ApplyMaster>().SingleOrDefault();
            }

            return ApplyMaster;
        }


    }
}
