﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Data.V1
{
    public class DSRStatusDao : AbstractDSRStatusDao
    {
        public override PagedList<AbstractDSRStatus> DSRStatus_All(PageParam pageParam, string search = "")
        {
            PagedList<AbstractDSRStatus> classes = new PagedList<AbstractDSRStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("DSRStatus_All", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<DSRStatus>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }
    }
}
