﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductManufactureDao : AbstractProductManufactureDao
    {
        public override PagedList<AbstractProductManufacture> ProductManufacture_All(PageParam pageParam, string search)
        {
            PagedList<AbstractProductManufacture> ProductManufacture = new PagedList<AbstractProductManufacture>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
          


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductManufacture_All, param, commandType: CommandType.StoredProcedure);
                ProductManufacture.Values.AddRange(task.Read<ProductManufacture>());
                ProductManufacture.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductManufacture;
        }

        public override SuccessResult<AbstractProductManufacture> ProductManufacture_ById(long Id)
        {
            SuccessResult<AbstractProductManufacture> ProductManufacture = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductManufacture_ById, param, commandType: CommandType.StoredProcedure);
                ProductManufacture = task.Read<SuccessResult<AbstractProductManufacture>>().SingleOrDefault();
                ProductManufacture.Item = task.Read<ProductManufacture>().SingleOrDefault();
            }

            return ProductManufacture;
        }

        public override SuccessResult<AbstractProductManufacture> ProductManufacture_Delete(long Id)
        {
            SuccessResult<AbstractProductManufacture> ProductManufacture = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductManufacture_Delete, param, commandType: CommandType.StoredProcedure);
                ProductManufacture = task.Read<SuccessResult<AbstractProductManufacture>>().SingleOrDefault();
                ProductManufacture.Item = task.Read<ProductManufacture>().SingleOrDefault();
            }

            return ProductManufacture;
        }

        public override SuccessResult<AbstractProductManufacture> ProductManufacture_Upsert(AbstractProductManufacture abstractProductManufacture)
        {
            SuccessResult<AbstractProductManufacture> ProductManufacture = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductManufacture.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProductManufacture.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Website", abstractProductManufacture.Website, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductManufacture.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractProductManufacture.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductManufacture_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductManufacture = task.Read<SuccessResult<AbstractProductManufacture>>().SingleOrDefault();
                ProductManufacture.Item = task.Read<ProductManufacture>().SingleOrDefault();
            }

            return ProductManufacture;
        }
    }
}
