﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ChampaignMasterDao : AbstractChampaignMasterDao
    {
        public override PagedList<AbstractChampaignMaster> ChampaignMaster_All(PageParam pageParam, string search, long ApplyMasterId, long RegionId, long HeadquarterId)
        {
            PagedList<AbstractChampaignMaster> ChampaignMaster = new PagedList<AbstractChampaignMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ApplyMasterId", ApplyMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RegionId", RegionId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@HeadquarterId", HeadquarterId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChampaignMaster_All, param, commandType: CommandType.StoredProcedure);
                ChampaignMaster.Values.AddRange(task.Read<ChampaignMaster>());
                ChampaignMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ChampaignMaster;
        }

        public override SuccessResult<AbstractChampaignMaster> ChampaignMaster_ById(long Id)
        {
            SuccessResult<AbstractChampaignMaster> ChampaignMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChampaignMaster_ById, param, commandType: CommandType.StoredProcedure);
                ChampaignMaster = task.Read<SuccessResult<AbstractChampaignMaster>>().SingleOrDefault();
                ChampaignMaster.Item = task.Read<ChampaignMaster>().SingleOrDefault();
            }

            return ChampaignMaster;
        }

        public override SuccessResult<AbstractChampaignMaster> ChampaignMaster_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractChampaignMaster> ChampaignMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChampaignMaster_Delete, param, commandType: CommandType.StoredProcedure);
                ChampaignMaster = task.Read<SuccessResult<AbstractChampaignMaster>>().SingleOrDefault();
                ChampaignMaster.Item = task.Read<ChampaignMaster>().SingleOrDefault();
            }

            return ChampaignMaster;
        }

        public override SuccessResult<AbstractChampaignMaster> ChampaignMaster_Upsert(AbstractChampaignMaster abstractChampaignMaster)
        {
            SuccessResult<AbstractChampaignMaster> ChampaignMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractChampaignMaster.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CampaignName", abstractChampaignMaster.CampaignName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StartDate", abstractChampaignMaster.StartDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EndDate", abstractChampaignMaster.EndDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ApplyMasterId", abstractChampaignMaster.ApplyMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RegionId", abstractChampaignMaster.RegionId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@HeadquarterId", abstractChampaignMaster.HeadquarterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractChampaignMaster.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractChampaignMaster.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChampaignMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                ChampaignMaster = task.Read<SuccessResult<AbstractChampaignMaster>>().SingleOrDefault();
                ChampaignMaster.Item = task.Read<ChampaignMaster>().SingleOrDefault();
            }

            return ChampaignMaster;
        }
    }
}
