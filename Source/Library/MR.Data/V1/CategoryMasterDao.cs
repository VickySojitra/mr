﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Data.V1
{
    public class CategoryMasterDao : AbstractCategoryMasterDao
    {
        public override PagedList<AbstractCategoryMaster> CategoryMaster_All(PageParam pageParam,string search ="", long FieldNameId = 0)
        {
            PagedList<AbstractCategoryMaster> classes = new PagedList<AbstractCategoryMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FieldNameId", FieldNameId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("CategoryMaster_All", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<CategoryMaster>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override SuccessResult<AbstractCategoryMaster> CategoryMaster_ById(long Id)
        {
            SuccessResult<AbstractCategoryMaster> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("CategoryMaster_ById", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractCategoryMaster>>().SingleOrDefault();
                address.Item = task.Read<CategoryMaster>().SingleOrDefault();
            }
            return address;
        }

        public override SuccessResult<AbstractCategoryMaster> CategoryMaster_Upsert(AbstractCategoryMaster abstractCategoryMaster)
        {
            SuccessResult<AbstractCategoryMaster> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractCategoryMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FieldNameId", abstractCategoryMaster.FieldNameId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractCategoryMaster.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractCategoryMaster.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCategoryMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCategoryMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("CategoryMaster_Upsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractCategoryMaster>>().SingleOrDefault();
                address.Item = task.Read<CategoryMaster>().SingleOrDefault();
            }
            return address;
        }

        public override PagedList<AbstractFieldName> FieldName_All(PageParam pageParam, string search = "", int masterId = 0)
        {
            PagedList<AbstractFieldName> classes = new PagedList<AbstractFieldName>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MasterId", masterId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("FieldName_All", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<FieldName>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override PagedList<AbstractFieldName> Master_All(PageParam pageParam, string search = "")
        {
            PagedList<AbstractFieldName> classes = new PagedList<AbstractFieldName>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Masters_All", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<FieldName>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }
    }
}
