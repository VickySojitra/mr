﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DoctorProductsDao : AbstractDoctorProductsDao
    {
        public override SuccessResult<AbstractDoctorProducts> DoctorProducts_ByDoctorId(long Id)
        {
            SuccessResult<AbstractDoctorProducts> DoctorProducts = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorProducts_ByDoctorId, param, commandType: CommandType.StoredProcedure);
                DoctorProducts = task.Read<SuccessResult<AbstractDoctorProducts>>().SingleOrDefault();
                DoctorProducts.Item = task.Read<DoctorProducts>().SingleOrDefault();
            }

            return DoctorProducts;
        }

     

      

        public override SuccessResult<AbstractDoctorProducts> DoctorProducts_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDoctorProducts> DoctorProducts = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorProducts_Delete, param, commandType: CommandType.StoredProcedure);
                DoctorProducts = task.Read<SuccessResult<AbstractDoctorProducts>>().SingleOrDefault();
                DoctorProducts.Item = task.Read<DoctorProducts>().SingleOrDefault();
            }

            return DoctorProducts;
        }

        public override SuccessResult<AbstractDoctorProducts> DoctorProducts_Upsert(AbstractDoctorProducts abstractDoctorProducts)
        {
            SuccessResult<AbstractDoctorProducts> DoctorProducts = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDoctorProducts.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId ", abstractDoctorProducts.DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId ", abstractDoctorProducts.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ExpectedSaleQty ", abstractDoctorProducts.ExpectedSaleQty, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDoctorProducts.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDoctorProducts.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorProducts_Upsert, param, commandType: CommandType.StoredProcedure);
                DoctorProducts = task.Read<SuccessResult<AbstractDoctorProducts>>().SingleOrDefault();
                DoctorProducts.Item = task.Read<DoctorProducts>().SingleOrDefault();
            }

            return DoctorProducts;
        }
    }
}
