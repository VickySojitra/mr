﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSR_VisitedDoctor_ProductsDao : AbstractDSR_VisitedDoctor_ProductsDao
    {
        public override PagedList<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId)
        {
            PagedList<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products = new PagedList<AbstractDSR_VisitedDoctor_Products>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedDoctorId", DSR_VisitedDoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Products_ByDSR_VisitedDoctorId, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Products.Values.AddRange(task.Read<DSR_VisitedDoctor_Products>());
                DSR_VisitedDoctor_Products.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR_VisitedDoctor_Products;
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_Delete(long Id)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Products_Delete, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Products = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Products>>().SingleOrDefault();
                DSR_VisitedDoctor_Products.Item = task.Read<DSR_VisitedDoctor_Products>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Products;
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products_Upsert(AbstractDSR_VisitedDoctor_Products abstractDSR_VisitedDoctor_Products)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Products> DSR_VisitedDoctor_Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR_VisitedDoctor_Products.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedDoctorId ", abstractDSR_VisitedDoctor_Products.DSR_VisitedDoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId ", abstractDSR_VisitedDoctor_Products.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderBookedQty ", abstractDSR_VisitedDoctor_Products.OrderBookedQty, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR_VisitedDoctor_Products.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR_VisitedDoctor_Products.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            ;
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Products_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Products = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Products>>().SingleOrDefault();
                DSR_VisitedDoctor_Products.Item = task.Read<DSR_VisitedDoctor_Products>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Products;
        }
    }
}
