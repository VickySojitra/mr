﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class VisitTypeDao : AbstractVisitTypeDao
    {

        public override SuccessResult<AbstractVisitType> VisitType_Upsert(AbstractVisitType abstractVisitType)
        {
            SuccessResult<AbstractVisitType> VisitType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractVisitType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractVisitType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractVisitType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractVisitType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VisitType_Upsert, param, commandType: CommandType.StoredProcedure);
                VisitType = task.Read<SuccessResult<AbstractVisitType>>().SingleOrDefault();
                VisitType.Item = task.Read<VisitType>().SingleOrDefault();
            }

            return VisitType;
        }

        public override SuccessResult<AbstractVisitType> VisitType_ById(long Id)
        {
            SuccessResult<AbstractVisitType> VisitType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VisitType_ById, param, commandType: CommandType.StoredProcedure);
                VisitType = task.Read<SuccessResult<AbstractVisitType>>().SingleOrDefault();
                VisitType.Item = task.Read<VisitType>().SingleOrDefault();
            }

            return VisitType;
        }

        public override PagedList<AbstractVisitType> VisitType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractVisitType> VisitType = new PagedList<AbstractVisitType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VisitType_All, param, commandType: CommandType.StoredProcedure);
                VisitType.Values.AddRange(task.Read<VisitType>());
                VisitType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return VisitType;
        }

        public override SuccessResult<AbstractVisitType> VisitType_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractVisitType> VisitType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VisitType_Delete, param, commandType: CommandType.StoredProcedure);
                VisitType = task.Read<SuccessResult<AbstractVisitType>>().SingleOrDefault();
                VisitType.Item = task.Read<VisitType>().SingleOrDefault();
            }

            return VisitType;
        }

             

    }
}
