﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class MR_PresentationResourcesDao : AbstractMR_PresentationResourcesDao
    {

        public override SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources_Upsert(AbstractMR_PresentationResources abstractMR_PresentationResources)
        {
            SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMR_PresentationResources.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@@MR_PresentationId", abstractMR_PresentationResources.MR_PresentationId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@@PresentationId", abstractMR_PresentationResources.PresentationId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMR_PresentationResources.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractMR_PresentationResources.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_PresentationResources_Upsert, param, commandType: CommandType.StoredProcedure);
                MR_PresentationResources = task.Read<SuccessResult<AbstractMR_PresentationResources>>().SingleOrDefault();
                MR_PresentationResources.Item = task.Read<MR_PresentationResources>().SingleOrDefault();
            }

            return MR_PresentationResources;
        }

       
        public override PagedList<AbstractMR_PresentationResources> MR_PresentationResources_ByMR_PresentationId(PageParam pageParam, long MR_PresentationId)
        {
            PagedList<AbstractMR_PresentationResources> MR_PresentationResources = new PagedList<AbstractMR_PresentationResources>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MR_PresentationId", MR_PresentationId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_PresentationResources_ByMR_PresentationId, param, commandType: CommandType.StoredProcedure);
                MR_PresentationResources.Values.AddRange(task.Read<MR_PresentationResources>());
                MR_PresentationResources.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MR_PresentationResources;
        }

        public override SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractMR_PresentationResources> MR_PresentationResources = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_PresentationResources_Delete, param, commandType: CommandType.StoredProcedure);
                MR_PresentationResources = task.Read<SuccessResult<AbstractMR_PresentationResources>>().SingleOrDefault();
                MR_PresentationResources.Item = task.Read<MR_PresentationResources>().SingleOrDefault();
            }

            return MR_PresentationResources;
        }



    }
}
