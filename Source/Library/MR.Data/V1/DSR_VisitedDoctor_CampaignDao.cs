﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSR_VisitedDoctor_CampaignDao : AbstractDSR_VisitedDoctor_CampaignDao
    {

        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_Upsert(AbstractDSR_VisitedDoctor_Campaign abstractDSR_VisitedDoctor_Campaign)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR_VisitedDoctor_Campaign.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedDoctorId", abstractDSR_VisitedDoctor_Campaign.DSR_VisitedDoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChampaignMasterId", abstractDSR_VisitedDoctor_Campaign.ChampaignMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR_VisitedDoctor_Campaign.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR_VisitedDoctor_Campaign.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Campaign>>().SingleOrDefault();
                DSR_VisitedDoctor_Campaign.Item = task.Read<DSR_VisitedDoctor_Campaign>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Campaign;
        }



        
        public override PagedList<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId(PageParam pageParam, long DSR_VisitedDoctorId)
        {
            PagedList<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign = new PagedList<AbstractDSR_VisitedDoctor_Campaign>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedDoctorId", DSR_VisitedDoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_ByDSR_VisitedDoctorId, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign.Values.AddRange(task.Read<DSR_VisitedDoctor_Campaign>());
                DSR_VisitedDoctor_Campaign.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR_VisitedDoctor_Campaign;
        }


        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_ById(long Id)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_ById, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Campaign>>().SingleOrDefault();
                DSR_VisitedDoctor_Campaign.Item = task.Read<DSR_VisitedDoctor_Campaign>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Campaign;
        }
        public override SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDSR_VisitedDoctor_Campaign> DSR_VisitedDoctor_Campaign = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Campaign_Delete, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor_Campaign = task.Read<SuccessResult<AbstractDSR_VisitedDoctor_Campaign>>().SingleOrDefault();
                DSR_VisitedDoctor_Campaign.Item = task.Read<DSR_VisitedDoctor_Campaign>().SingleOrDefault();
            }

            return DSR_VisitedDoctor_Campaign;
        }



    }
}
