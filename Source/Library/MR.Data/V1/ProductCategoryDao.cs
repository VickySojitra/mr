﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductCategoryDao : AbstractProductCategoryDao
    {
        public override PagedList<AbstractProductCategory> ProductCategory_All(PageParam pageParam, string search)
        {
            PagedList<AbstractProductCategory> ProductCategory = new PagedList<AbstractProductCategory>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductCategory_All, param, commandType: CommandType.StoredProcedure);
                ProductCategory.Values.AddRange(task.Read<ProductCategory>());
                ProductCategory.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductCategory;
        }

        public override SuccessResult<AbstractProductCategory> ProductCategory_ById(long Id)
        {
            SuccessResult<AbstractProductCategory> ProductCategory = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductCategory_ById, param, commandType: CommandType.StoredProcedure);
                ProductCategory = task.Read<SuccessResult<AbstractProductCategory>>().SingleOrDefault();
                ProductCategory.Item = task.Read<ProductCategory>().SingleOrDefault();
            }

            return ProductCategory;
        }

        public override SuccessResult<AbstractProductCategory> ProductCategory_Delete(long Id)
        {
            SuccessResult<AbstractProductCategory> ProductCategory = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductCategory_Delete, param, commandType: CommandType.StoredProcedure);
                ProductCategory = task.Read<SuccessResult<AbstractProductCategory>>().SingleOrDefault();
                ProductCategory.Item = task.Read<ProductCategory>().SingleOrDefault();
            }

            return ProductCategory;
        }

        public override SuccessResult<AbstractProductCategory> ProductCategory_Upsert(AbstractProductCategory abstractProductCategory)
        {
            SuccessResult<AbstractProductCategory> ProductCategory = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductCategory.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProductCategory.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductCategory.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractProductCategory.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductCategory_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductCategory = task.Read<SuccessResult<AbstractProductCategory>>().SingleOrDefault();
                ProductCategory.Item = task.Read<ProductCategory>().SingleOrDefault();
            }

            return ProductCategory;
        }
    }
}
