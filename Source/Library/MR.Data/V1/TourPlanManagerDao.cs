﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MR.Data.V1
{
	public class TourPlanManagerDao : AbstractTourPlanManagerDao
	{
		public override PagedList<AbstractTourPlanManager> TourPlanManager_All(PageParam pageParam, string search, int IsSubmitted, string month, int Headquarter = 0, int Region = 0)
		{
			PagedList<AbstractTourPlanManager> tour = new PagedList<AbstractTourPlanManager>();

			var param = new DynamicParameters();
			param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
			param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
			param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
			param.Add("@IsSubmitted", IsSubmitted, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Month", month, dbType: DbType.String, direction: ParameterDirection.Input);
			param.Add("@Headquarter", Headquarter, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Region", Region, dbType: DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple(SQLConfig.TourPlan_All, param, commandType: CommandType.StoredProcedure);
				tour.Values.AddRange(task.Read<TourPlanManager>());
				tour.TotalRecords = task.Read<long>().SingleOrDefault();
			}
			return tour;
		}
	}
}
