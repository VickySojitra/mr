﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class PresentationDao : AbstractPresentationDao
    {

        public override SuccessResult<AbstractPresentation> Presentation_Upsert(AbstractPresentation abstractPresentation)
        {
            SuccessResult<AbstractPresentation> Presentation = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPresentation.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileURL", abstractPresentation.FileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileName", abstractPresentation.FileName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Size", abstractPresentation.Size, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileType", abstractPresentation.FileType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPresentation.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractPresentation.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractPresentation.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Presentation_Upsert, param, commandType: CommandType.StoredProcedure);
                Presentation = task.Read<SuccessResult<AbstractPresentation>>().SingleOrDefault();
                Presentation.Item = task.Read<Presentation>().SingleOrDefault();
            }

            return Presentation;
        }

        public override SuccessResult<AbstractPresentation> Presentation_ById(long Id)
        {
            SuccessResult<AbstractPresentation> Presentation = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Presentation_ById, param, commandType: CommandType.StoredProcedure);
                Presentation = task.Read<SuccessResult<AbstractPresentation>>().SingleOrDefault();
                Presentation.Item = task.Read<Presentation>().SingleOrDefault();
            }

            return Presentation;
        }

        public override PagedList<AbstractPresentation> Presentation_All(PageParam pageParam, string search, int ProductId)
        {
            PagedList<AbstractPresentation> Presentation = new PagedList<AbstractPresentation>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int32, direction: ParameterDirection.Input);            


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Presentation_All, param, commandType: CommandType.StoredProcedure);
                Presentation.Values.AddRange(task.Read<Presentation>());
                Presentation.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Presentation;
        }

        public override SuccessResult<AbstractPresentation> Presentation_Delete(long Id )
        {
            SuccessResult<AbstractPresentation> Presentation = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Presentation_Delete, param, commandType: CommandType.StoredProcedure);
                Presentation = task.Read<SuccessResult<AbstractPresentation>>().SingleOrDefault();
                Presentation.Item = task.Read<Presentation>().SingleOrDefault();
            }

            return Presentation;
        }

        public override PagedList<AbstractPresentation> Presentation_ByProductId(PageParam pageParam, long ProductId)
        {
            PagedList<AbstractPresentation> Presentation = new PagedList<AbstractPresentation>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Presentation_ByProductId, param, commandType: CommandType.StoredProcedure);
                Presentation.Values.AddRange(task.Read<Presentation>());
                Presentation.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Presentation;
        }

    }
}
