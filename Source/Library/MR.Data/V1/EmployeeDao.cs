﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MR.Data.V1
{
    public class EmployeeDao : AbstractEmployeeDao
    {
        public override PagedList<AbstractEmployee> SelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractEmployee> classes = new PagedList<AbstractEmployee>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("EmployeeSelectAll", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<Employee>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override PagedList<AbstractEmployee> Employee_ReportingPerson(PageParam pageParam, string search = "",int RoleId = 0, bool IsTourPlanDropDown = false)
        {
            PagedList<AbstractEmployee> classes = new PagedList<AbstractEmployee>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RoleId", RoleId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsTourPlanDropDown", IsTourPlanDropDown, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Employee_ReportingPerson", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<Employee>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override SuccessResult<AbstractEmployee> Select(int id)
        {
            SuccessResult<AbstractEmployee> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("EmployeeSelect", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractEmployee>>().SingleOrDefault();
                address.Item = task.Read<Employee>().SingleOrDefault();
            }
            return address;
        }

        public override SuccessResult<AbstractEmployee> Employee_ById(int Id)
        {
            SuccessResult<AbstractEmployee> Employee = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Employee_ById, param, commandType: CommandType.StoredProcedure);
                Employee = task.Read<SuccessResult<AbstractEmployee>>().SingleOrDefault();
                Employee.Item = task.Read<Employee>().SingleOrDefault();
            }

            return Employee;
        }

        public override bool Delete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>("EmployeeDelete", param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override bool DeleteWorkExperience(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>("WorkExperienceDeleteByEmpID", param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override SuccessResult<AbstractEmployee> InsertUpdate(AbstractEmployee abstractAddress)
        {
            SuccessResult<AbstractEmployee> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractAddress.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractAddress.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractAddress.LastName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractAddress.IsActive, DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Gender", (int)abstractAddress.Gender, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Mobile", abstractAddress.Mobile, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Phone", abstractAddress.Phone, DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmergencyNumber", abstractAddress.EmergencyNumber, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Birthdate", abstractAddress.Birthdate, DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@Anniversary", abstractAddress.Anniversary, DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@Email", abstractAddress.Email, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Photo", abstractAddress.Photo, DbType.String, direction: ParameterDirection.Input);
            param.Add("@PermanentAddress", abstractAddress.PermanentAddress, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CurrentAddress", abstractAddress.CurrentAddress, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AddressProof", abstractAddress.AddressProof, DbType.String, direction: ParameterDirection.Input);
            param.Add("@DrivingLicense", abstractAddress.DrivingLicense, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Remarks", abstractAddress.Remarks, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Qualification", abstractAddress.Qualification, DbType.String, direction: ParameterDirection.Input);
            param.Add("@JoiningDate", abstractAddress.JoiningDate, DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@ConfirmationDate", abstractAddress.ConfirmationDate, DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@Role", abstractAddress.Role, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Headquarter", abstractAddress.Headquarter, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Region", abstractAddress.Region, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ReportingPerson", abstractAddress.ReportingPerson, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@SalaryAmount", abstractAddress.SalaryAmount, DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@PaymentMode", abstractAddress.PaymentMode, DbType.String, direction: ParameterDirection.Input);
            param.Add("@BankName", abstractAddress.BankName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@BankBranch", abstractAddress.BankBranch, DbType.String, direction: ParameterDirection.Input);
            param.Add("@BankAccountNo", abstractAddress.BankAccountNo, DbType.String, direction: ParameterDirection.Input);
            param.Add("@BankIFSCCode", abstractAddress.BankIFSCCode, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractAddress.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractAddress.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@WebPassword", abstractAddress.WebPassword, DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobilePassword", abstractAddress.MobilePassword, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("EmployeeUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractEmployee>>().SingleOrDefault();
                address.Item = task.IsConsumed == false? task.Read<Employee>().SingleOrDefault() : null;
            }

            return address;
        }

        public override PagedList<AbstractQualification> QualificationSelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractQualification> classes = new PagedList<AbstractQualification>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("QualificationSelectAll", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<Qualification>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override PagedList<AbstractSpecialization> SpecializationSelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractSpecialization> classes = new PagedList<AbstractSpecialization>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("SpecializationSelectAll", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<Specialization>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override PagedList<AbstractWorkExperience> WorkExperienceByEmployeeId(int empId)
        {
            PagedList<AbstractWorkExperience> classes = new PagedList<AbstractWorkExperience>();

            var param = new DynamicParameters();
            param.Add("@EmployeeId", empId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("WorkExperienceByEmployeeId", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<WorkExperience>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }



        public override SuccessResult<AbstractWorkExperience> InsertUpdateWorkExperience(AbstractWorkExperience abstractWorkExp)
        {
            SuccessResult<AbstractWorkExperience> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractWorkExp.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", abstractWorkExp.EmployeeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractWorkExp.CompanyName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Position", abstractWorkExp.Position, DbType.String, direction: ParameterDirection.Input);
            param.Add("@JoiningDate", abstractWorkExp.JoiningDate, DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@LeavingDate", abstractWorkExp.LeavingDate, DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@Salary", abstractWorkExp.Salary, DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@LeavingReason", abstractWorkExp.LeavingReason, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractWorkExp.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractWorkExp.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("WorkExperienceUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractWorkExperience>>().SingleOrDefault();
                address.Item = task.Read<WorkExperience>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractQualification> InsertUpdateQualification(AbstractQualification abstractQualification)
        {
            SuccessResult<AbstractQualification> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractQualification.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractQualification.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractQualification.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractQualification.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("QualificationUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractQualification>>().SingleOrDefault();
                address.Item = task.Read<Qualification>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractSpecialization> InsertUpdateSpecialization(AbstractSpecialization abstractSpecialization)
        {
            SuccessResult<AbstractSpecialization> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractSpecialization.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractSpecialization.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractSpecialization.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractSpecialization.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("SpecializationUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractSpecialization>>().SingleOrDefault();
                address.Item = task.Read<Specialization>().SingleOrDefault();
            }

            return address;
        }

        public override SuccessResult<AbstractEmployee> Employee_Login(string Email, string WebPassword)
        {
            SuccessResult<AbstractEmployee> address = null;
            var param = new DynamicParameters();
            param.Add("@Email", Email, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", WebPassword, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Employee_Login", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractEmployee>>().SingleOrDefault();
                address.Item = task.Read<Employee>().SingleOrDefault();
            }

            return address;

        }

        public override SuccessResult<AbstractEmployee> Employee_ChangePassword(int Id, string OldPassword, string NewPassword,string ConfirmPassword)
        {
            SuccessResult<AbstractEmployee> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", Id, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", OldPassword, DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", NewPassword, DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", ConfirmPassword, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Employee_ChangePassword", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractEmployee>>().SingleOrDefault();
                address.Item = task.Read<Employee>().SingleOrDefault();
            }

            return address;
        }

		public override SuccessResult<AbstractQualification> Qualification_ById(int Id)
		{
            SuccessResult<AbstractQualification> Employee = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Qualification_ById", param, commandType: CommandType.StoredProcedure);
                Employee = task.Read<SuccessResult<AbstractQualification>>().SingleOrDefault();
                Employee.Item = task.Read<Qualification>().SingleOrDefault();
            }

            return Employee;
        }

		public override SuccessResult<AbstractSpecialization> Specialization_ById(int Id)
		{
            SuccessResult<AbstractSpecialization> Employee = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Specialization_ById", param, commandType: CommandType.StoredProcedure);
                Employee = task.Read<SuccessResult<AbstractSpecialization>>().SingleOrDefault();
                Employee.Item = task.Read<Specialization>().SingleOrDefault();
            }

            return Employee;
        }
	}
}
