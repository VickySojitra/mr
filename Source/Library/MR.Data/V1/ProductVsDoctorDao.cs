﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductVsDoctorDao : AbstractProductVsDoctorDao
    {

        public override SuccessResult<AbstractProductVsDoctor> ProductVsDoctor_Upsert(AbstractProductVsDoctor abstractProductVsDoctor)
        {
            SuccessResult<AbstractProductVsDoctor> ProductVsDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductVsDoctor.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DoctorId", abstractProductVsDoctor.DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractProductVsDoctor.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Quantity", abstractProductVsDoctor.Quantity, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductVsDoctor.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProductVsDoctor.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsDoctor_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductVsDoctor = task.Read<SuccessResult<AbstractProductVsDoctor>>().SingleOrDefault();
                ProductVsDoctor.Item = task.Read<ProductVsDoctor>().SingleOrDefault();
            }

            return ProductVsDoctor;
        }

       

        public override PagedList<AbstractProductVsDoctor> ProductVsDoctor_All(PageParam pageParam, string search)
        {
            PagedList<AbstractProductVsDoctor> ProductVsDoctor = new PagedList<AbstractProductVsDoctor>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsDoctor_All, param, commandType: CommandType.StoredProcedure);
                ProductVsDoctor.Values.AddRange(task.Read<ProductVsDoctor>());
                ProductVsDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductVsDoctor;
        }

        public override SuccessResult<AbstractProductVsDoctor> ProductVsDoctor_Delete(long Id,long DeletedBy)
        {
            SuccessResult<AbstractProductVsDoctor> ProductVsDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsDoctor_Delete, param, commandType: CommandType.StoredProcedure);
                ProductVsDoctor = task.Read<SuccessResult<AbstractProductVsDoctor>>().SingleOrDefault();
                ProductVsDoctor.Item = task.Read<ProductVsDoctor>().SingleOrDefault();
            }

            return ProductVsDoctor;
        }

        public override PagedList<AbstractProductVsDoctor> ProductVsDoctor_ByDoctorId(PageParam pageParam, long DoctorId)
        {
            PagedList<AbstractProductVsDoctor> ProductVsDoctor = new PagedList<AbstractProductVsDoctor>();
            var param = new DynamicParameters();


            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsDoctor_ByDoctorId, param, commandType: CommandType.StoredProcedure);
                ProductVsDoctor.Values.AddRange(task.Read<ProductVsDoctor>());
                ProductVsDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return ProductVsDoctor;
        }


        public override PagedList<AbstractProductVsDoctor> ProductVsDoctor_ByProductId(PageParam pageParam, long ProductId)
        {
            PagedList<AbstractProductVsDoctor> ProductVsDoctor = new PagedList<AbstractProductVsDoctor>();
            var param = new DynamicParameters();

            
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsDoctor_ByProductId, param, commandType: CommandType.StoredProcedure);
                ProductVsDoctor.Values.AddRange(task.Read<ProductVsDoctor>());
                ProductVsDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return ProductVsDoctor;
        }



    }
}
