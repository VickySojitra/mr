﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class LeaveTypeMasterDao : AbstractLeaveTypeMasterDao
    {

       
        public override PagedList<AbstractLeaveTypeMaster> LeaveTypeMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLeaveTypeMaster> LeaveTypeMaster = new PagedList<AbstractLeaveTypeMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LeaveTypeMaster_All, param, commandType: CommandType.StoredProcedure);
                LeaveTypeMaster.Values.AddRange(task.Read<LeaveTypeMaster>());
                LeaveTypeMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LeaveTypeMaster;
        }

       


    }
}
