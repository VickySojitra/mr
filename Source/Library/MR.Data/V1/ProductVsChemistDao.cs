﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductVsChemistDao : AbstractProductVsChemistDao
    {

        public override SuccessResult<AbstractProductVsChemist> ProductVsChemist_Upsert(AbstractProductVsChemist abstractProductVsChemist)
        {
            SuccessResult<AbstractProductVsChemist> ProductVsChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductVsChemist.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", abstractProductVsChemist.ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractProductVsChemist.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Quantity", abstractProductVsChemist.Quantity, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductVsChemist.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProductVsChemist.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsChemist_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductVsChemist = task.Read<SuccessResult<AbstractProductVsChemist>>().SingleOrDefault();
                ProductVsChemist.Item = task.Read<ProductVsChemist>().SingleOrDefault();
            }

            return ProductVsChemist;
        }

       

        public override PagedList<AbstractProductVsChemist> ProductVsChemist_All(PageParam pageParam, string search, long ProductId, long ChemistId)
        {
            PagedList<AbstractProductVsChemist> ProductVsChemist = new PagedList<AbstractProductVsChemist>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsChemist_All, param, commandType: CommandType.StoredProcedure);
                ProductVsChemist.Values.AddRange(task.Read<ProductVsChemist>());
                ProductVsChemist.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductVsChemist;
        }

        public override PagedList<AbstractProductVsChemist> ProductVsChemist_ByProductId(PageParam pageParam, long ProductId)
        {
            PagedList<AbstractProductVsChemist> ProductVsChemist = new PagedList<AbstractProductVsChemist>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsChemist_ByProductId, param, commandType: CommandType.StoredProcedure);
                ProductVsChemist.Values.AddRange(task.Read<ProductVsChemist>());
                ProductVsChemist.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductVsChemist;
        }


        public override PagedList<AbstractProductVsChemist> ProductVsChemist_ByChemistId(PageParam pageParam, long ChemistId)
        {
            PagedList<AbstractProductVsChemist> ProductVsChemist = new PagedList<AbstractProductVsChemist>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ChemistId", ChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsChemist_ByChemistId, param, commandType: CommandType.StoredProcedure);
                ProductVsChemist.Values.AddRange(task.Read<ProductVsChemist>());
                ProductVsChemist.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductVsChemist;
        }

        public override SuccessResult<AbstractProductVsChemist> ProductVsChemist_Delete(long Id, int deletedby)
        {
            SuccessResult<AbstractProductVsChemist> ProductVsChemist = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", deletedby, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductVsChemist_Delete, param, commandType: CommandType.StoredProcedure);
                ProductVsChemist = task.Read<SuccessResult<AbstractProductVsChemist>>().SingleOrDefault();
                ProductVsChemist.Item = task.Read<ProductVsChemist>().SingleOrDefault();
            }

            return ProductVsChemist;
        }



    }
}
