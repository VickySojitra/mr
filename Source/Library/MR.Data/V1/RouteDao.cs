﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MR.Data.V1
{
	public class RouteDao : AbstractRouteDao
	{
		public override PagedList<AbstractRoute> SelectAll(PageParam pageParam, string search = "", int RegionId = 0, int RouteTypeId = 0)
		{
			PagedList<AbstractRoute> classes = new PagedList<AbstractRoute>();

			var param = new DynamicParameters();
			param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@RegionId", RegionId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RouteTypeId", RouteTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("RouteSelectAll", param, commandType: CommandType.StoredProcedure);
				classes.Values.AddRange(task.Read<Route>());
				classes.TotalRecords = task.Read<long>().SingleOrDefault();
			}
			return classes;
		}

		public override SuccessResult<AbstractRoute> Select(int id)
		{
			SuccessResult<AbstractRoute> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("RouteSelect", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractRoute>>().SingleOrDefault();
				address.Item = task.Read<Route>().SingleOrDefault();
			}
			return address;
		}

		public override SuccessResult<AbstractRoute> InsertUpdate(AbstractRoute abstractRoute)
		{
			SuccessResult<AbstractRoute> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", abstractRoute.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Name", abstractRoute.Name, dbType: DbType.String, direction: ParameterDirection.Input);			
			param.Add("@IsActive", abstractRoute.IsActive, DbType.Boolean, direction: ParameterDirection.Input);
			param.Add("@RouteStart", abstractRoute.RouteStart, DbType.String, direction: ParameterDirection.Input);
			param.Add("@RouteEnd", abstractRoute.RouteEnd, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Type", abstractRoute.Type, DbType.Int32, direction: ParameterDirection.Input);			
			param.Add("@Distance", abstractRoute.Distance, DbType.Decimal, direction: ParameterDirection.Input);
			param.Add("@TotalFair", abstractRoute.TotalFair, DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@RegionId", abstractRoute.RegionId, DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractRoute.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@UpdatedBy", abstractRoute.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("RouteUpsert", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractRoute>>().SingleOrDefault();
				address.Item = task.Read<Route>().SingleOrDefault();
			}

			return address;
		}

        public override PagedList<AbstractRouteType> SelectAllRouteType(PageParam pageParam, string search = "")
        {
            PagedList<AbstractRouteType> classes = new PagedList<AbstractRouteType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("RouteTypeSelectAll", param, commandType: CommandType.StoredProcedure);
                classes.Values.AddRange(task.Read<RouteType>());
                classes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return classes;
        }

        public override SuccessResult<AbstractRouteType> InsertUpdateRouteType(AbstractRouteType abstractRouteType)
        {
            SuccessResult<AbstractRouteType> address = null;
            var param = new DynamicParameters();
            param.Add("@Id", abstractRouteType.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractRouteType.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractRouteType.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractRouteType.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("RoleTypeUpsert", param, commandType: CommandType.StoredProcedure);
                address = task.Read<SuccessResult<AbstractRouteType>>().SingleOrDefault();
                address.Item = task.Read<RouteType>().SingleOrDefault();
            }

            return address;
        }
    }
}
