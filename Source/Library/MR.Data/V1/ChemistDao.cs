﻿using Dapper;
using MR.Common;
using MR.Common.Paging;
using MR.Entities.Contract;
using MR.Entities.V1;
using MR.Services.Contract;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MR.Data.V1
{
	public class ChemistDao : AbstractChemistDao
	{
		public override PagedList<AbstractChemist> SelectAll(PageParam pageParam, string search = "", int TourPlanMonthId = 0)
		{
			PagedList<AbstractChemist> classes = new PagedList<AbstractChemist>();

			var param = new DynamicParameters();
			param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
			param.Add("@TourPlanMonthId", TourPlanMonthId, dbType: DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("ChemistSelectAll", param, commandType: CommandType.StoredProcedure);
				classes.Values.AddRange(task.Read<Chemist>());
				classes.TotalRecords = task.Read<long>().SingleOrDefault();
			}
			return classes;
		}

		public override SuccessResult<AbstractChemist> Select(int id)
		{
			SuccessResult<AbstractChemist> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("ChemistSelect", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractChemist>>().SingleOrDefault();
				address.Item = task.Read<Chemist>().SingleOrDefault();
			}
			return address;
		}

		public override SuccessResult<AbstractChemist> InsertUpdate(AbstractChemist abstractChemist)
		{
			SuccessResult<AbstractChemist> address = null;
			var param = new DynamicParameters();
			param.Add("@Id", abstractChemist.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Name", abstractChemist.Name, dbType: DbType.String, direction: ParameterDirection.Input);			
			param.Add("@IsActive", abstractChemist.IsActive, DbType.Boolean, direction: ParameterDirection.Input);
			param.Add("@Mobile", abstractChemist.Mobile, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Phone", abstractChemist.Phone, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Headquarter", abstractChemist.Headquarter, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Type", (int)abstractChemist.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@Region", abstractChemist.Region, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@ContactPerson", abstractChemist.ContactPerson, DbType.String, direction: ParameterDirection.Input);
			param.Add("@WebSite", abstractChemist.WebSite, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Address", abstractChemist.Address, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@BuisnessPotential", abstractChemist.BuisnessPotential, DbType.String, direction: ParameterDirection.Input);
			param.Add("@Remarks", abstractChemist.Remarks, DbType.String, direction: ParameterDirection.Input);
			param.Add("@CreatedBy", abstractChemist.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
			param.Add("@UpdatedBy", abstractChemist.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);

			using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
			{
				var task = con.QueryMultiple("ChemistUpsert", param, commandType: CommandType.StoredProcedure);
				address = task.Read<SuccessResult<AbstractChemist>>().SingleOrDefault();
				address.Item = task.Read<Chemist>().SingleOrDefault();
			}

			return address;
		}
	}
}
