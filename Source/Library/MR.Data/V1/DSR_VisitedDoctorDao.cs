﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSR_VisitedDoctorDao : AbstractDSR_VisitedDoctorDao
    {

        public override SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_Upsert(AbstractDSR_VisitedDoctor abstractDSR_VisitedDoctor)
        {
            SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR_VisitedDoctor.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSRId", abstractDSR_VisitedDoctor.DSRId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DoctorId", abstractDSR_VisitedDoctor.DoctorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallInId", abstractDSR_VisitedDoctor.CallInId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OutcomeId", abstractDSR_VisitedDoctor.OutcomeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@WorkWithId", abstractDSR_VisitedDoctor.WorkWithId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Remarks", abstractDSR_VisitedDoctor.Remarks, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR_VisitedDoctor.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR_VisitedDoctor.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor = task.Read<SuccessResult<AbstractDSR_VisitedDoctor>>().SingleOrDefault();
                DSR_VisitedDoctor.Item = task.Read<DSR_VisitedDoctor>().SingleOrDefault();
            }

            return DSR_VisitedDoctor;
        }

        public override SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_ById(long Id)
        {
            SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_ById, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor = task.Read<SuccessResult<AbstractDSR_VisitedDoctor>>().SingleOrDefault();
                DSR_VisitedDoctor.Item = task.Read<DSR_VisitedDoctor>().SingleOrDefault();
            }

            return DSR_VisitedDoctor;
        }



        public override SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_Delete(long Id)
        {
            SuccessResult<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_Delete, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor = task.Read<SuccessResult<AbstractDSR_VisitedDoctor>>().SingleOrDefault();
                DSR_VisitedDoctor.Item = task.Read<DSR_VisitedDoctor>().SingleOrDefault();
            }

            return DSR_VisitedDoctor;
        }

        public override PagedList<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor_ByDSRId(PageParam pageParam, long DSRId)
        {
            PagedList<AbstractDSR_VisitedDoctor> DSR_VisitedDoctor = new PagedList<AbstractDSR_VisitedDoctor>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DSRId", DSRId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedDoctor_ByDSRId, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedDoctor.Values.AddRange(task.Read<DSR_VisitedDoctor>());
                DSR_VisitedDoctor.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR_VisitedDoctor;
        }
    }
}
