﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DayTypeMasterDao : AbstractDayTypeMasterDao
    {

        

        public override PagedList<AbstractDayTypeMaster> DayTypeMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractDayTypeMaster> DayTypeMaster = new PagedList<AbstractDayTypeMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
         

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DayTypeMaster_All, param, commandType: CommandType.StoredProcedure);
                DayTypeMaster.Values.AddRange(task.Read<DayTypeMaster>());
                DayTypeMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DayTypeMaster;
        }

    }
}
