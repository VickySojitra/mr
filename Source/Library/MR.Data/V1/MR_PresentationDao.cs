﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class MR_PresentationDao : AbstractMR_PresentationDao
    {

        public override SuccessResult<AbstractMR_Presentation> MR_Presentation_Upsert(AbstractMR_Presentation abstractMR_Presentation)
        {
            SuccessResult<AbstractMR_Presentation> MR_Presentation = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMR_Presentation.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", abstractMR_Presentation.EmployeeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PresentationName", abstractMR_Presentation.PresentationName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractMR_Presentation.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMR_Presentation.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractMR_Presentation.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_Presentation_Upsert, param, commandType: CommandType.StoredProcedure);
                MR_Presentation = task.Read<SuccessResult<AbstractMR_Presentation>>().SingleOrDefault();
                MR_Presentation.Item = task.Read<MR_Presentation>().SingleOrDefault();
            }

            return MR_Presentation;
        }

        public override SuccessResult<AbstractMR_Presentation> MR_Presentation_ById(long Id)
        {
            SuccessResult<AbstractMR_Presentation> MR_Presentation = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_Presentation_ById, param, commandType: CommandType.StoredProcedure);
                MR_Presentation = task.Read<SuccessResult<AbstractMR_Presentation>>().SingleOrDefault();
                MR_Presentation.Item = task.Read<MR_Presentation>().SingleOrDefault();
            }

            return MR_Presentation;
        }

        public override PagedList<AbstractMR_Presentation> MR_Presentation_All(PageParam pageParam, string search, long EmployeeId, long ProductId)
        {
            PagedList<AbstractMR_Presentation> MR_Presentation = new PagedList<AbstractMR_Presentation>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmployeeId", EmployeeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_Presentation_All, param, commandType: CommandType.StoredProcedure);
                MR_Presentation.Values.AddRange(task.Read<MR_Presentation>());
                MR_Presentation.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MR_Presentation;
        }

        public override SuccessResult<AbstractMR_Presentation> MR_Presentation_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractMR_Presentation> MR_Presentation = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MR_Presentation_Delete, param, commandType: CommandType.StoredProcedure);
                MR_Presentation = task.Read<SuccessResult<AbstractMR_Presentation>>().SingleOrDefault();
                MR_Presentation.Item = task.Read<MR_Presentation>().SingleOrDefault();
            }

            return MR_Presentation;
        }



    }
}
