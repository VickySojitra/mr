﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class ProductHightlightsDao : AbstractProductHightlightsDao
    {

        public override SuccessResult<AbstractProductHightlights> ProductHightlights_Upsert(AbstractProductHightlights abstractProductHightlights)
        {
            SuccessResult<AbstractProductHightlights> ProductHightlights = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProductHightlights.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProductHightlights.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProductHightlights.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractProductHightlights.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHightlights_Upsert, param, commandType: CommandType.StoredProcedure);
                ProductHightlights = task.Read<SuccessResult<AbstractProductHightlights>>().SingleOrDefault();
                ProductHightlights.Item = task.Read<ProductHightlights>().SingleOrDefault();
            }

            return ProductHightlights;
        }

        public override SuccessResult<AbstractProductHightlights> ProductHightlights_ById(long Id)
        {
            SuccessResult<AbstractProductHightlights> ProductHightlights = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHightlights_ById, param, commandType: CommandType.StoredProcedure);
                ProductHightlights = task.Read<SuccessResult<AbstractProductHightlights>>().SingleOrDefault();
                ProductHightlights.Item = task.Read<ProductHightlights>().SingleOrDefault();
            }

            return ProductHightlights;
        }

        public override PagedList<AbstractProductHightlights> ProductHightlights_All(PageParam pageParam, string search)
        {
            PagedList<AbstractProductHightlights> ProductHightlights = new PagedList<AbstractProductHightlights>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHightlights_All, param, commandType: CommandType.StoredProcedure);
                ProductHightlights.Values.AddRange(task.Read<ProductHightlights>());
                ProductHightlights.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ProductHightlights;
        }

        public override SuccessResult<AbstractProductHightlights> ProductHightlights_Delete(long Id)
        {
            SuccessResult<AbstractProductHightlights> ProductHightlights = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProductHightlights_Delete, param, commandType: CommandType.StoredProcedure);
                ProductHightlights = task.Read<SuccessResult<AbstractProductHightlights>>().SingleOrDefault();
                ProductHightlights.Item = task.Read<ProductHightlights>().SingleOrDefault();
            }

            return ProductHightlights;
        }



    }
}
