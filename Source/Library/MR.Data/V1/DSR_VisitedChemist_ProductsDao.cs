﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MR.Common;
using MR.Common.Paging;
using MR.Data.Contract;
using MR.Entities.Contract;
using MR.Entities.V1;
using Dapper;

namespace MR.Data.V1
{
    public class DSR_VisitedChemist_ProductsDao : AbstractDSR_VisitedChemist_ProductsDao
    {

        public override SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_Upsert(AbstractDSR_VisitedChemist_Products abstractDSR_VisitedChemist_Products)
        {
            SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDSR_VisitedChemist_Products.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedChemistId", abstractDSR_VisitedChemist_Products.DSR_VisitedChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractDSR_VisitedChemist_Products.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderBookedQty", abstractDSR_VisitedChemist_Products.OrderBookedQty, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDSR_VisitedChemist_Products.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractDSR_VisitedChemist_Products.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_Products_Upsert, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist_Products = task.Read<SuccessResult<AbstractDSR_VisitedChemist_Products>>().SingleOrDefault();
                DSR_VisitedChemist_Products.Item = task.Read<DSR_VisitedChemist_Products>().SingleOrDefault();
            }

            return DSR_VisitedChemist_Products;
        }

        public override PagedList<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_ByDSR_VisitedChemistId(PageParam pageParam, long DSR_VisitedChemistId)
        {
            PagedList<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products = new PagedList<AbstractDSR_VisitedChemist_Products>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DSR_VisitedChemistId", DSR_VisitedChemistId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_Products_ByDSR_VisitedChemistId, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist_Products.Values.AddRange(task.Read<DSR_VisitedChemist_Products>());
                DSR_VisitedChemist_Products.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DSR_VisitedChemist_Products;
        }


        public override SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products_Delete(long Id)
        {
            SuccessResult<AbstractDSR_VisitedChemist_Products> DSR_VisitedChemist_Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DSR_VisitedChemist_Products_Delete, param, commandType: CommandType.StoredProcedure);
                DSR_VisitedChemist_Products = task.Read<SuccessResult<AbstractDSR_VisitedChemist_Products>>().SingleOrDefault();
                DSR_VisitedChemist_Products.Item = task.Read<DSR_VisitedChemist_Products>().SingleOrDefault();
            }

            return DSR_VisitedChemist_Products;
        }



    }
}