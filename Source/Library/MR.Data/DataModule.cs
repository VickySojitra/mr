﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Dexoc Solutions">
//     Copyright Dexoc Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MR.Data
{
    using Autofac;
    using MR.Data.Contract;
	using MR.Services.Contract;

	/// <summary>
	/// Contract Class for DataModule.
	/// </summary>
	public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.AddressDao>().As<AbstractAddressDao>().InstancePerDependency();
            builder.RegisterType<V1.EmployeeDao>().As<AbstractEmployeeDao>().InstancePerDependency();
            builder.RegisterType<V1.RoleDao>().As<AbstractRoleDao>().InstancePerDependency();
            builder.RegisterType<V1.DoctorDao>().As<AbstractDoctorDao>().InstancePerDependency();
            builder.RegisterType<V1.ChemistDao>().As<AbstractChemistDao>().InstancePerDependency();
            builder.RegisterType<V1.RouteDao>().As<AbstractRouteDao>().InstancePerDependency();
            builder.RegisterType<V1.WorkWithMasterDao>().As<AbstractWorkWithMasterDao>().InstancePerDependency();
            
            builder.RegisterType<V1.ProductCategoryDao>().As<AbstractProductCategoryDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductTypeDao>().As<AbstractProductTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductManufactureDao>().As<AbstractProductManufactureDao>().InstancePerDependency();
            builder.RegisterType<V1.QuantitySIUnitesDao>().As<AbstractQuantitySIUnitesDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductDao>().As<AbstractProductDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductHightlightsDao>().As<AbstractProductHightlightsDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductImagesDao>().As<AbstractProductImagesDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductPrescriptionDao>().As<AbstractProductPrescriptionDao>().InstancePerDependency();
            builder.RegisterType<V1.PresentationDao>().As<AbstractPresentationDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductHighlightDao>().As<AbstractProductHighlightDao>().InstancePerDependency();

            builder.RegisterType<V1.DSRDao>().As<AbstractDSRDao>().InstancePerDependency();
            builder.RegisterType<V1.DSRStatusDao>().As<AbstractDSRStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.CallInDao>().As<AbstractCallInDao>().InstancePerDependency();
            builder.RegisterType<V1.DoctorProductsDao>().As<AbstractDoctorProductsDao>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedDoctor_ProductsDao>().As<AbstractDSR_VisitedDoctor_ProductsDao>().InstancePerDependency();

            builder.RegisterType<V1.DSR_VisitedDoctorDao>().As<AbstractDSR_VisitedDoctorDao>().InstancePerDependency();

            builder.RegisterType<V1.WorkTypeDao>().As<AbstractWorkTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.VisitTypeDao>().As<AbstractVisitTypeDao>().InstancePerDependency();

            builder.RegisterType<V1.OutcomeDao>().As<AbstractOutcomeDao>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedChemist_ProductsDao>().As<AbstractDSR_VisitedChemist_ProductsDao>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedChemistDao>().As<AbstractDSR_VisitedChemistDao>().InstancePerDependency();

            builder.RegisterType<V1.MR_PresentationDao>().As<AbstractMR_PresentationDao>().InstancePerDependency();
            builder.RegisterType<V1.MR_PresentationResourcesDao>().As<AbstractMR_PresentationResourcesDao>().InstancePerDependency();
            
            
            builder.RegisterType<V1.CategoryMasterDao>().As<AbstractCategoryMasterDao>().InstancePerDependency();

            builder.RegisterType<V1.ProductVsChemistDao>().As<AbstractProductVsChemistDao>().InstancePerDependency();
            builder.RegisterType<V1.ChemistVsDoctorDao>().As<AbstractChemistVsDoctorDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductVsDoctorDao>().As<AbstractProductVsDoctorDao>().InstancePerDependency();

            builder.RegisterType<V1.DSR_VisitedDoctor_CampaignDao>().As<AbstractDSR_VisitedDoctor_CampaignDao>().InstancePerDependency();
            builder.RegisterType<V1.DSR_VisitedDoctor_Campaign_GiftsDao>().As<AbstractDSR_VisitedDoctor_Campaign_GiftsDao>().InstancePerDependency();

            builder.RegisterType<V1.ApplyMasterDao>().As<AbstractApplyMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.GiftDao>().As<AbstractGiftDao>().InstancePerDependency();
            builder.RegisterType < V1.ChampaignMasterDao>().As<AbstractChampaignMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.GiftDao>().As<AbstractGiftDao>().InstancePerDependency();

            builder.RegisterType <V1.ChampaignMasterDao>().As<AbstractChampaignMasterDao>().InstancePerDependency();

            builder.RegisterType<V1.PrescriberDao>().As<AbstractPrescriberDao>().InstancePerDependency();
            builder.RegisterType<V1.PrescriberProductsDao>().As<AbstractPrescriberProductsDao>().InstancePerDependency();

            builder.RegisterType<V1.LeaveDao>().As<AbstractLeaveDao>().InstancePerDependency();

            builder.RegisterType<V1.DayTypeMasterDao>().As<AbstractDayTypeMasterDao>().InstancePerDependency();

            builder.RegisterType<V1.LeaveTypeMasterDao>().As<AbstractLeaveTypeMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.TourPlanDao>().As<AbstractTourPlanDao>().InstancePerDependency();
            builder.RegisterType<V1.TourPlanMonthDao>().As<AbstractTourPlanMonthDao>().InstancePerDependency();

            builder.RegisterType<V1.DoctorAndChemistDao>().As<AbstractDoctorAndChemistDao>().InstancePerDependency();
            builder.RegisterType<V1.JointWorkingDao>().As<AbstractJointWorkingDao>().InstancePerDependency();

            builder.RegisterType<V1.MasterWorkAtDao>().As<AbstractMasterWorkAtDao>().InstancePerDependency();
            builder.RegisterType<V1.StateRegionHqDao>().As<AbstractStateRegionHqDao>().InstancePerDependency();
            builder.RegisterType<V1.EmployeeRegionDao>().As<AbstractEmployeeRegionDao>().InstancePerDependency();
            builder.RegisterType<V1.TourPlanManagerDao>().As<AbstractTourPlanManagerDao>().InstancePerDependency();



            base.Load(builder);
        }
    }
}
